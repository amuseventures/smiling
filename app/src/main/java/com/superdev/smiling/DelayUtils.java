package com.superdev.smiling;

import android.os.Handler;

/**
 * Created by don on 2/14/2017.
 */

public class DelayUtils {


    public interface DelayCallback{
        void afterDelay();
    }

    public static void delay(int secs, final DelayUtils.DelayCallback delayCallback){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                delayCallback.afterDelay();
            }
        }, secs * 1000); // afterDelay will be executed after (secs*1000) milliseconds.
    }
}
