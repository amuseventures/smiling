package com.superdev.smiling.storage;

import android.graphics.Bitmap;
import android.net.Uri;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.superdev.smiling.rx.FirebaseObservableListeners;

import java.io.ByteArrayOutputStream;
import java.io.File;

import rx.Observable;

/**
 * Created by marco on 18/08/16.
 */

public class FirebaseStorageService implements StorageService {

    private final StorageReference firebaseStorage;

    private final FirebaseObservableListeners firebaseObservableListeners;

    private static final String DOCS_FOLDER = "docs";
    private static final String IMAGE_FOLDER = "chatimages";
    private static final String AUDIO_FOLDER = "audio";
    private static final String VIDEO_FOLDER = "video";
    private static final String CONTACT_FOLDER = "contact";
    private static final String VOICE_FOLDER = "voice";

    public FirebaseStorageService(FirebaseStorage firebaseStorage, FirebaseObservableListeners firebaseObservableListeners) {
        this.firebaseStorage = firebaseStorage.getReference();
        this.firebaseObservableListeners = firebaseObservableListeners;
    }

    @Override
    public StorageReference getProfileImageReference(String image) {
        return firebaseStorage.child(image);
    }

    @Override
    public StorageReference getChatImageReference(String image) {
        return firebaseStorage.child(IMAGE_FOLDER).child(image);
    }

    @Override
    public StorageReference getChatImageReference() {
        return firebaseStorage.child(IMAGE_FOLDER);
    }

    @Override
    public StorageReference getChatAudioReference() {
        return firebaseStorage.child(AUDIO_FOLDER);
    }

    @Override
    public StorageReference getChatVoiceReference() {
        return firebaseStorage.child(VOICE_FOLDER);
    }

    @Override
    public StorageReference getChatVideoReference() {
        return firebaseStorage.child(VIDEO_FOLDER);
    }

    @Override
    public StorageReference getContactReference() {
        return firebaseStorage.child(CONTACT_FOLDER);
    }

    @Override
    public StorageReference getDocsReference() {
        return firebaseStorage.child(DOCS_FOLDER);
    }

    @Override
    public Observable<String> uploadImage(Bitmap bitmap) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
        byte[] data = baos.toByteArray();
        final String imageRef = bitmap.hashCode() + System.currentTimeMillis() + ".jpg";

        return firebaseObservableListeners.uploadTask(data, firebaseStorage.child(imageRef), imageRef);
    }

    @Override
    public Observable<String> uploadImage(byte[] bytes) {
        final String imageRef = System.currentTimeMillis() + ".jpg";
        return firebaseObservableListeners.uploadTask(bytes, firebaseStorage.child(imageRef),imageRef);
    }

    @Override
    public Observable<String> uploadImage(Uri fileUri) {
        final String imageRef = fileUri.hashCode() + System.currentTimeMillis() + ".jpg";
        StorageReference imageGalleryRef = firebaseStorage.child("chatimages");
        return firebaseObservableListeners.uploadFileTask(fileUri, imageGalleryRef.child(imageRef), imageRef);
    }

    @Override
    public Observable<File> downloadImage(String image) {
        return null;
    }

}
