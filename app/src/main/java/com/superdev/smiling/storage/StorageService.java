package com.superdev.smiling.storage;

import android.graphics.Bitmap;
import android.net.Uri;

import com.google.firebase.storage.StorageReference;

import java.io.File;

import rx.Observable;

/**
 * Created by marco on 18/08/16.
 */

public interface StorageService {

    StorageReference getProfileImageReference(String image);

    StorageReference getChatImageReference(String image);

    StorageReference getChatImageReference();

    StorageReference getChatAudioReference();

    StorageReference getChatVoiceReference();

    StorageReference getChatVideoReference();

    StorageReference getContactReference();

    StorageReference getDocsReference();

    Observable<String> uploadImage(Bitmap bitmap);

    Observable<String> uploadImage(byte[] bytes);

    Observable<String> uploadImage(Uri file);

    Observable<File> downloadImage(String image);

}
