package com.superdev.smiling.profile.service;

import com.superdev.smiling.channel.database.ChannelsDatabase;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.user.data_model.User;

/**
 * Created by gautam on 13/10/17.
 */

public class PersistedChannelProfileService implements ChannelProfileService {

    private ChannelsDatabase channelsDatabase;

    public PersistedChannelProfileService(ChannelsDatabase channelsDatabase) {
        this.channelsDatabase = channelsDatabase;
    }


    @Override
    public void changeGroupName(String channelId, String groupName) {
        channelsDatabase.setGroupName(channelId, groupName);
    }

    @Override
    public void changeGroupImage(String channelId, String groupImage) {
        channelsDatabase.setGroupImage(channelId, groupImage);
    }

}
