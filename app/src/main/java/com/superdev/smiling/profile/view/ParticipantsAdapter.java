package com.superdev.smiling.profile.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.superdev.smiling.R;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;
import com.superdev.smiling.userpicker.displayer.ContactUserDisplayer;

import java.util.ArrayList;

/**
 * Created by gautam on 13/10/17.
 */

public class ParticipantsAdapter extends RecyclerView.Adapter<ParticipantsViewHolder> {

    private Users users = new Users(new ArrayList<User>());

    private ChannelProfileDisplayer.ChannelProfileActionListener actionListener;

    private final LayoutInflater inflater;

    private User channelOwner;

    public ParticipantsAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void update(Users users, User channelOwner) {
        this.users = users;
        this.users = users.sortedByName();
        this.channelOwner = channelOwner;
        notifyDataSetChanged();
    }

    public Users getUsers() {
        return users;
    }

    @Override
    public ParticipantsViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        return new ParticipantsViewHolder((ParticipantsView) inflater.inflate(R.layout.participants_item_layout, parent, false));
    }

    public void attach(ChannelProfileDisplayer.ChannelProfileActionListener actionListener) {
        this.actionListener = actionListener;
    }

    public void detach(ChannelProfileDisplayer.ChannelProfileActionListener actionListener) {
        this.actionListener = null;
    }

    @Override
    public void onBindViewHolder(ParticipantsViewHolder holder, int i) {
        User user = getUsers().getUserAt(i);
        holder.bind(user, channelOwner.getUid(), listener);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    private final ParticipantsViewHolder.ParticipantsSelectionListener listener = new ParticipantsViewHolder.ParticipantsSelectionListener() {
        @Override
        public void onParticipantsSelected(User user) {
            if (actionListener != null) {
                actionListener.onParticipantsPressed(user);
            }
        }
    };
}
