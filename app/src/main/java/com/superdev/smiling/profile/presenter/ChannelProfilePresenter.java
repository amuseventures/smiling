package com.superdev.smiling.profile.presenter;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.superdev.smiling.MimeTypeEnum;
import com.superdev.smiling.analytics.Analytics;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.channel.presenter.Config;
import com.superdev.smiling.channel.service.ChannelService;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.groupconversation.data_model.GroupChat;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.groupconversation.service.GroupConversationService;
import com.superdev.smiling.navigation.AndroidChannelProfileNavigator;
import com.superdev.smiling.navigation.ChannelProfileNavigator;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.profile.service.ChannelProfileService;
import com.superdev.smiling.profile.view.ChannelProfileDisplayer;
import com.superdev.smiling.rx.FCM;
import com.superdev.smiling.storage.StorageService;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.service.UserService;

import rx.Observer;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by gautam on 13/10/17.
 */

public class ChannelProfilePresenter {

    private final ChannelProfileDisplayer profileDisplayer;

    private final ChannelService channelService;

    private final UserService userService;

    private final ChannelProfileService channelProfileService;

    private final PhoneLoginService loginService;

    private final StorageService storageService;

    private final GroupConversationService groupConversationService;

    private final Config config;

    private final AndroidChannelProfileNavigator navigator;

    private final Analytics analytics;

    private final Channel channel;

    private Subscription channelUserSubscription, groupConversationSubscription;

    private Subscription userSubscription;

    private Subscription mediaSubscription;

    public ChannelProfilePresenter(
            Channel channel,
            ChannelProfileDisplayer profileDisplayer,
            ChannelService channelService,
            UserService userService,
            ChannelProfileService channelProfileService,
            PhoneLoginService loginService,
            StorageService storageService,
            Config config,
            AndroidChannelProfileNavigator navigator,
            Analytics analytics,
            GroupConversationService groupConversationService
    ) {
        this.channel = channel;
        this.profileDisplayer = profileDisplayer;
        this.channelService = channelService;
        this.userService = userService;
        this.channelProfileService = channelProfileService;
        this.loginService = loginService;
        this.storageService = storageService;
        this.config = config;
        this.navigator = navigator;
        this.analytics = analytics;
        this.groupConversationService = groupConversationService;


        userSubscription = userService.getUser(channel.getOwner())
                .subscribe(new Action1<DatabaseResult<User>>() {
                    @Override
                    public void call(DatabaseResult<User> userDatabaseResult) {
                        if (userDatabaseResult.isSuccess()) {
                            profileDisplayer.display(channel, userDatabaseResult.getData());
                        }
                    }
                });

        channelUserSubscription = channelService.getOwnersOfChannel(channel)
                .subscribe(databaseResult -> {
                    if (databaseResult.isSuccess()) {
                        profileDisplayer.displayParticipants(databaseResult.getData());
                    }
                });

        groupConversationSubscription = groupConversationService.getGroupMedia(channel)
                .subscribe(new Action1<DatabaseResult<GroupChat>>() {
                    @Override
                    public void call(DatabaseResult<GroupChat> groupChatDatabaseResult) {
                        if (groupChatDatabaseResult.isSuccess()) {
                            profileDisplayer.displayMedia(groupChatDatabaseResult.getData());
                        }
                    }
                });

    }


    public void startPresenting() {
        navigator.attach(dialogListener);
        profileDisplayer.attach(interactionListener);
    }

    public void stopPresenting() {
        navigator.detach(dialogListener);
        profileDisplayer.detach(interactionListener);
        if (userSubscription != null) {
            userSubscription.unsubscribe();
        }

        if (channelUserSubscription != null) {
            channelUserSubscription.unsubscribe();
        }

        if (groupConversationSubscription != null) {
            groupConversationSubscription.unsubscribe();
        }

    }

    private ChannelProfileDisplayer.ChannelProfileActionListener interactionListener = new ChannelProfileDisplayer.ChannelProfileActionListener() {
        @Override
        public void onUpPressed() {
            navigator.toParent();
        }

        @Override
        public void onEditPressed(String registrationName) {
            navigator.showNameChangeActivity(registrationName);
        }

        @Override
        public void onImagePressed(View view, Channel channel) {
            navigator.showImagePreview(view, channel);
        }

        @Override
        public void onChangeImagePressed() {
            navigator.showImagePicker();
        }

        @Override
        public void onRemovePressed() {

        }

        @Override
        public void addParticipantsPressed(Channel channel) {
            navigator.toAddMembers(channel);
        }

        @Override
        public void onParticipantsPressed(User user) {
            navigator.showParticipantsInfoDialog(user, channel);
        }

        @Override
        public void onExitGroup(User user) {
            navigator.showExitDialog(user, channel);
        }

        @Override
        public void onMediaClick(View view, GroupMessage message) {
            navigator.toFullScreenImage(view, message);
        }

        @Override
        public void onMoreMediaClick(Channel channel) {
            navigator.toAllGroupMediaActivity(channel);
        }
    };

    private final ChannelProfileNavigator.ChannelProfileDialogListener dialogListener = new ChannelProfileNavigator.ChannelProfileDialogListener() {
        @Override
        public void onNameSelected(String groupName) {
            channelProfileService.changeGroupName(channel.getUid(), groupName);
            profileDisplayer.updateName(groupName);
        }

        @Override
        public void onExitGroup(User user, Channel channel) {
            profileDisplayer.showProgressDialog();
            channelService.removeOwnerFromPrivateChannel(channel, user)
                    .subscribe(new Observer<DatabaseResult<User>>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable throwable) {

                        }

                        @Override
                        public void onNext(DatabaseResult<User> userDatabaseResult) {
                            if (!userDatabaseResult.isSuccess()) {
                                Log.d("TAG", "onNext: " + "rttot");
                            } else {
                                new AsyncTask<Void, Void, Void>() {

                                    @Override
                                    protected Void doInBackground(Void... voids) {
                                        GroupMessage group = new GroupMessage(user, channel, String.format("%1s left the group", user.getName()),
                                                MimeTypeEnum.REMOVE.getValue(), com.superdev.smiling.conversation.data_model.Status.SEEN.getValue());
                                        FCM.sendFCMNotification(user.getFcm(), channel.getName(), String.format("%1s left the group", user.getName()), group);
                                        return null;
                                    }

                                    @Override
                                    protected void onPreExecute() {
                                        super.onPreExecute();
                                        profileDisplayer.hideProgressDialog();
                                        profileDisplayer.showSuccess();
                                    }
                                }.execute();
                            }
                        }
                    });
        }

        @Override
        public void onImageSelected(byte[] imageBytes, Bitmap bitmap) {
            profileDisplayer.updateProfileImage(bitmap);
            profileDisplayer.showProgressBar(true);
            storageService.uploadImage(imageBytes)
                    .subscribe(image -> {
                        if (image != null) {
                            channelProfileService.changeGroupImage(channel.getUid(), image);
                            profileDisplayer.showProgressBar(false);
                        }
                    });
        }

        @Override
        public void onProfileRemove() {
            channelProfileService.changeGroupImage(channel.getUid(), null);
            profileDisplayer.updateProfileImage(null);
        }
    };

    private Observer<DatabaseResult<User>> updateOnActionResult() {
        return new Observer<DatabaseResult<User>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(DatabaseResult<User> userDatabaseResult) {
                if (!userDatabaseResult.isSuccess()) {
                    Log.d("TAG", "onNext: " + "rttot");
                    //
                } else {
                    profileDisplayer.showSuccess();
                }
            }
        };
    }

}
