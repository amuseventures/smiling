package com.superdev.smiling.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.sinch.android.rtc.SinchError;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.navigation.AndroidProfileNavigator;
import com.superdev.smiling.profile.presenter.ProfilePresenter;
import com.superdev.smiling.profile.view.ProfileDisplayer;
import com.superdev.smiling.sinch.SinchBaseActivity;
import com.superdev.smiling.sinch.SinchService;

/**
 * Created by marco on 19/08/16.
 */

public class ProfileActivity extends SinchBaseActivity implements SinchService.StartFailedListener {

    private ProfilePresenter presenter;
    private AndroidProfileNavigator navigator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        ProfileDisplayer profileDisplayer = findViewById(R.id.profileView);

        navigator = new AndroidProfileNavigator(this);
        presenter = new ProfilePresenter(
                Dependencies.INSTANCE.getPhoneLoginService(),
                Dependencies.INSTANCE.getUserService(),
                Dependencies.INSTANCE.getProfileService(),
                Dependencies.INSTANCE.getStorageService(),
                profileDisplayer,
                navigator,
                getIntent().getStringExtra(Constants.SENDER_ID),
                getIntent().getStringExtra(Constants.DESTINATION_ID),
                Dependencies.INSTANCE.getConversationService()
        );
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getSinchServiceInterface() != null && !getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(Remember.getString(Constants.FIREBASE_USERS_PHONE, ""));
        } else {
            presenter.setSinchService(getSinchServiceInterface());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!navigator.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.stopPresenting();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            supportFinishAfterTransition();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        supportFinishAfterTransition();
    }

    @Override
    protected void onServiceConnected() {
        if (getSinchServiceInterface() != null && getSinchServiceInterface().isStarted()) {
            presenter.setSinchService(getSinchServiceInterface());
        } else {
            getSinchServiceInterface().startClient(Remember.getString(Constants.FIREBASE_USERS_PHONE, ""));
            getSinchServiceInterface().setStartListener(this);
        }
    }

    @Override
    protected void onServiceDisconnected() {

    }

    @Override
    public void onStartFailed(SinchError error) {

    }

    @Override
    public void onStarted() {
        presenter.setSinchService(getSinchServiceInterface());
    }
}
