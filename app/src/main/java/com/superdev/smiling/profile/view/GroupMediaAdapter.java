package com.superdev.smiling.profile.view;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.firebase.storage.StorageReference;
import com.superdev.smiling.BaseAdapter;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.Utils;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by gautam on 25/10/17.
 */

public class GroupMediaAdapter extends BaseAdapter<GroupMediaAdapter.GroupMediaViewHolder, GroupMessage> {

    private List<GroupMessage> messageList;

    private OnMediaItemClick onMediaItemClick;

    public GroupMediaAdapter() {
        messageList = new ArrayList<>();
    }

    public void setOnMediaItemClick(OnMediaItemClick onMediaItemClick) {
        this.onMediaItemClick = onMediaItemClick;
    }

    @Override
    public void setData(List<GroupMessage> messageList) {
        this.messageList = messageList;
        notifyDataSetChanged();
    }

    @Override
    public GroupMediaViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.media_thumb_views, parent, false);
        return new GroupMediaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GroupMediaViewHolder holder, int i) {
        GroupMessage message = messageList.get(i);
        setUpImage(message, holder);
        holder.mediaThumbs.setOnClickListener(view -> {
            if (onMediaItemClick != null) {
                onMediaItemClick.onItemClick(view, message);
            }
        });
    }

    private void setUpImage(GroupMessage message, GroupMediaViewHolder holder) {
        File file;
        if (!Remember.getString(Constants.FIREBASE_USER_ID, "").equalsIgnoreCase(message.getAuthor().getUid())) {
            file = new File(Utils.getImagePath() + "/" + message.getFileModel().getFileName());
        } else {
            file = new File(Utils.getImageSentPath() + "/" + message.getFileModel().getFileName());
        }
        if (file.exists()) {
            setImage(file, holder);
        } else if (message.getFileModel().getFileUri() != null) {
            File file1 = new File(Utils.getRealPathFromURI(holder.mediaThumbs.getContext(), Uri.parse(message.getFileModel().getFileUri())));
            setImage(file1, holder);
        } else {
            StorageReference storageRef = Dependencies.INSTANCE.getStorageService().getChatImageReference();
            StorageReference imageRef = storageRef.child(message.getFileModel().getFileName());
            File downloadFile;
            if (!Remember.getString(Constants.FIREBASE_USER_ID, "").equalsIgnoreCase(message.getAuthor().getUid())) {
                downloadFile = new File(Utils.getImagePath() + "/" + message.getFileModel().getFileName());
            } else {
                downloadFile = new File(Utils.getImageSentPath() + "/" + message.getFileModel().getFileName());
            }
            if (!downloadFile.exists()) {
                try {
                    downloadFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (!downloadFile.exists()) {
                return;
            }
            imageRef.getFile(downloadFile)
                    .addOnFailureListener(e -> {

                    }).addOnSuccessListener(taskSnapshot -> {
                setUpImage(message, holder);
            }).addOnProgressListener(taskSnapshot -> {
            });

        }
    }

    private void setImage(File file, GroupMediaViewHolder holder) {
        Glide.with(holder.mediaThumbs.getContext())
                .load(Uri.fromFile(file))
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        holder.mediaThumbs.setImageBitmap(resource);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return messageList != null && !messageList.isEmpty() ? messageList.size() : 0;
    }

    static class GroupMediaViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.media_thumbs)
        ImageView mediaThumbs;

        GroupMediaViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnMediaItemClick {

        void onItemClick(View view, GroupMessage message);
    }
}
