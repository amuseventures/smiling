package com.superdev.smiling.profile.service;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.user.data_model.User;

/**
 * Created by gautam on 13/10/17.
 */

public interface ChannelProfileService {

    void changeGroupName(String channelId, String groupName);

    void changeGroupImage(String channelId, String groupImage);

}
