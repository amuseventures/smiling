package com.superdev.smiling.profile;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.superdev.smiling.BaseActivity;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.navigation.AndroidChannelNavigator;
import com.superdev.smiling.navigation.AndroidChannelProfileNavigator;
import com.superdev.smiling.navigation.AndroidNavigator;
import com.superdev.smiling.profile.presenter.ChannelProfilePresenter;
import com.superdev.smiling.profile.view.ChannelProfileDisplayer;

public class ChannelProfileActivity extends BaseActivity {

    private ChannelProfilePresenter presenter;

    private AndroidChannelProfileNavigator channelNavigator;

    private ChannelProfileDisplayer displayer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_profile);
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }
        channelNavigator = new AndroidChannelProfileNavigator(this);
        Channel channel = getIntent().getParcelableExtra(Constants.FIREBASE_CHANNELS);
        displayer = findViewById(R.id.channel_profile_view);
        presenter = new ChannelProfilePresenter(
                channel,
                displayer,
                Dependencies.INSTANCE.getChannelService(),
                Dependencies.INSTANCE.getUserService(),
                Dependencies.INSTANCE.getChannelProfileService(),
                Dependencies.INSTANCE.getPhoneLoginService(),
                Dependencies.INSTANCE.getStorageService(),
                Dependencies.INSTANCE.getConfig(),
                channelNavigator,
                Dependencies.INSTANCE.getAnalytics(),
                Dependencies.INSTANCE.getGroupConversationService()
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                channelNavigator.toParent();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //presenter.stopPresenting();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!channelNavigator.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
