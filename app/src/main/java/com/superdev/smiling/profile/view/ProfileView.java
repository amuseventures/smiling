package com.superdev.smiling.profile.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.conversation.data_model.Chat;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.user.data_model.User;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by marco on 09/09/16.
 */

public class ProfileView extends CoordinatorLayout implements ProfileDisplayer, AppBarLayout.OnOffsetChangedListener, MediaAdapter.OnMediaItemClick {

    @BindView(R.id.image)
    ImageView image;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;

    @BindView(R.id.appbar)
    AppBarLayout appBarLayout;

    @BindView(R.id.toolbar_header_view)
    protected HeaderView toolbarHeaderView;

    @BindView(R.id.float_header_view)
    protected HeaderView floatHeaderView;

    @BindView(R.id.scrollView)
    NestedScrollView scrollView;

    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;

    @BindView(R.id.status)
    EmojiconTextView status;

    @BindView(R.id.phone)
    TextView phone;

    @BindView(R.id.message_btn)
    ImageButton messageBtn;

    @BindView(R.id.audio_call_btn)
    ImageButton audioCallBtn;

    @BindView(R.id.video_call_btn)
    ImageButton videoCallBtn;

    @BindView(R.id.media_scroller)
    RecyclerView mediaScroller;

    @BindView(R.id.media_info)
    TextView mediaInfo;

    private String sender;

    private String destination;

    private boolean isHideToolbarView = false;

    private ProfileActionListener actionListener;

    private User user;

    public ProfileView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_profile_view, this);
        ButterKnife.bind(this);
        initUi();
    }

    private void initUi() {
        appBarLayout.addOnOffsetChangedListener(this);
    }

    @Override
    public void display(User user) {
        //No user here
    }

    @Override
    public void display(User user, String sender) {
        this.user = user;
        long lastSeen = user.getLastSeen();
        this.sender = sender;
        this.destination = user.getUid();
        String lastString;
        if (lastSeen == 0)
            lastString = getContext().getString(R.string.chat_toolbar_lastseen_online);
        else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/HH/mm", Locale.US);
            Date resultdate = new Date(lastSeen);
            String timestamp = sdf.format(resultdate);
            String today = Utils.getCurrentTimestamp();
            String[] time1 = timestamp.split("/");
            String[] time2 = today.split("/");
            if ((time1[0] + time1[1] + time1[2]).equals(time2[0] + time2[1] + time2[2])) {
                String s = getResources().getString(R.string.chat_toolbar_lastseen_offline_today);
                lastString = s.replace("time", time1[3] + ":" + time1[4]);
            } else {
                String s = getResources().getString(R.string.chat_toolbar_lastseen_offline);
                s = s.replace("date", time1[2] + "/" + time1[1]);
                s = s.replace("time", time1[3] + ":" + time1[4]);
                lastString = s;
            }
        }
        toolbarHeaderView.bindTo(user.getName(), lastString);
        floatHeaderView.bindTo(user.getName(), lastString);
        Utils.loadImage(user.getImage(), image, getContext(),R.drawable.avatar_contact_large);
        status.setText(user.getStatus());
        phone.setText(user.getPhone());
    }

    @Override
    public void displayMedia(Chat chat) {
        if (chat == null || chat.getMessages() == null || chat.getMessages().isEmpty()) {
            return;
        }
        MediaAdapter mediaAdapter = new MediaAdapter();
        mediaAdapter.setOnMediaItemClick(this);
        mediaScroller.setAdapter(mediaAdapter);
        mediaScroller.setHasFixedSize(true);
        mediaScroller.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mediaAdapter.setData(chat.getMessages());
    }

    @Override
    public void updateProfileImage(Bitmap bitmap) {
        image.setImageBitmap(bitmap);
        image.setDrawingCacheEnabled(true);
        image.buildDrawingCache();
    }

    @Override
    public void showProgressBar(boolean isVisible) {

    }

    @Override
    public void attach(ProfileActionListener profileActionListener) {
        this.actionListener = profileActionListener;
    }

    @Override
    public void detach(ProfileActionListener profileActionListener) {
        this.actionListener = null;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        if (percentage == 1f && isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;

        } else if (percentage < 1f && !isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
        }
    }

    @OnClick(R.id.image)
    public void onViewClicked(View view) {
        actionListener.onImagePressed(view);
    }

    @OnClick({R.id.message_btn, R.id.audio_call_btn, R.id.video_call_btn, R.id.media_info})
    public void onButtonViewClicked(View view) {
        switch (view.getId()) {
            case R.id.message_btn:
                actionListener.onMessagePressed(sender, destination);
                break;
            case R.id.audio_call_btn:
                actionListener.onCallPressed(user);
                break;
            case R.id.video_call_btn:
                actionListener.onVideoCallPressed(user);
                break;
            case R.id.media_info:
                actionListener.onMoreMediaClick(sender, destination);
                break;
        }
    }

    @Override
    public void onItemClick(View view, Message message) {
        actionListener.onMediaClick(view, message);
    }
}
