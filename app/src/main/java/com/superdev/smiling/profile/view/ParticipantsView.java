package com.superdev.smiling.profile.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.superdev.smiling.Constants;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.Utils;
import com.superdev.smiling.user.data_model.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by gautam on 13/10/17.
 */

public class ParticipantsView extends FrameLayout {

    @BindView(R.id.participant_row_photo)
    CircleImageView participantRowPhoto;

    @BindView(R.id.contact_selector)
    FrameLayout contactSelector;

    @BindView(R.id.participant_row_name)
    EmojiconTextView participantRowName;

    @BindView(R.id.participant_row_access_type)
    TextView participantRowAccessType;

    @BindView(R.id.participants_row_status)
    EmojiconTextView participantsRowStatus;

    private int layoutResId;

    public ParticipantsView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int[] attrsArray = {
                    android.R.attr.layout
            };
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            layoutResId = array.getResourceId(0, R.layout.merge_participants_view);
            array.recycle();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), layoutResId, this);
        ButterKnife.bind(this);
    }

    void display(User user, String ownerId) {
        Utils.loadImageElseBlack(user.getImage(), participantRowPhoto, getContext(),R.drawable.avatar_contact_large);
        participantRowName.setText(user.getName());
        participantsRowStatus.setText(user.getStatus());
        participantRowAccessType.setVisibility(ownerId.equalsIgnoreCase(user.getUid()) ? VISIBLE : GONE);
    }
}
