package com.superdev.smiling.profile.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.superdev.smiling.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by gautam on 13/10/17.
 */

public class ChannelHeaderView extends RelativeLayout {

    @BindView(R.id.name)
    EmojiconTextView name;

    @BindView(R.id.created_by)
    TextView createdBy;

    @BindView(R.id.iv_edit_name)
    ImageView ivEditName;

    public ChannelHeaderView(Context context) {
        super(context);
    }

    public ChannelHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ChannelHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ChannelHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    public void bindTo(String name, String createBy, OnEditNameSelectedListener listener) {
        this.name.setText(name);
        this.createdBy.setText(createBy);
        this.ivEditName.setOnClickListener(listener::onEditName);
    }

    public void setName(String channelName) {
        this.name.setText(channelName);
    }

    public void setTextSize(float size) {
        name.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
    }

    public String getName() {
        return name.getText().toString();
    }

    public interface OnEditNameSelectedListener {

        void onEditName(View view);

    }

}

