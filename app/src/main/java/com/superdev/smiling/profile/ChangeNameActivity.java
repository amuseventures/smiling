package com.superdev.smiling.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.superdev.smiling.BaseActivity;
import com.superdev.smiling.Constants;
import com.superdev.smiling.R;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

public class ChangeNameActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_user_name)
    EmojiconEditText tvUserName;

    @BindView(R.id.tv_counter)
    TextView tvCounter;

    @BindView(R.id.iv_smiley)
    ImageView ivSmiley;

    @BindView(R.id.main_root)
    RelativeLayout mainRoot;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_name);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.change_name_title);
        }
        String name = getIntent().getStringExtra(Constants.FIREBASE_USERS_NAME);

        EmojIconActions emojIcon = new EmojIconActions(this, mainRoot, tvUserName, ivSmiley);
        emojIcon.ShowEmojIcon();

        tvUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (s.length() > 0) {
                    tvCounter.setVisibility(View.VISIBLE);
                    tvCounter.setText(String.format(Locale.getDefault(), "%d", 24 - s.toString().length()));
                } else {
                    tvCounter.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        tvUserName.setText(name);
    }

    @OnClick({R.id.btn_cancel, R.id.btn_ok})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.btn_ok:
                Intent intent = new Intent();
                intent.putExtra(Constants.FIREBASE_USERS_NAME, tvUserName.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
    }
}
