package com.superdev.smiling.profile.service;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by marco on 09/09/16.
 */

public class FirebaseProfileService implements ProfileService {

    private final FirebaseAuth firebaseAuth;
    private final FirebaseUser firebaseUser;

    public FirebaseProfileService(FirebaseAuth firebaseAuth) {
        this.firebaseAuth = firebaseAuth;
        this.firebaseUser = firebaseAuth.getCurrentUser();
    }

    @Override
    public void removeUser() {
        firebaseUser.delete();
        firebaseAuth.signOut();
    }

    @Override
    public void removeProfilePhoto() {

    }

}
