package com.superdev.smiling.profile.presenter;

import android.graphics.Bitmap;
import android.view.View;

import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.navigation.ProfileNavigator;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.profile.service.ProfileService;
import com.superdev.smiling.profile.view.ProfileDisplayer;
import com.superdev.smiling.storage.StorageService;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.service.UserService;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by marco on 09/09/16.
 */

public class UserProfilePresenter {

    private final PhoneLoginService loginService;
    private final UserService userService;
    private final ProfileService profileService;
    private final StorageService storageService;
    private final ProfileDisplayer profileDisplayer;
    private final ProfileNavigator navigator;

    private User self;

    private Subscription loginSubscription;

    private Subscription userSubscription;

    public UserProfilePresenter(PhoneLoginService loginService,
                                UserService userService,
                                ProfileService profileService,
                                StorageService storageService,
                                ProfileDisplayer loginDisplayer,
                                ProfileNavigator navigator) {
        this.loginService = loginService;
        this.userService = userService;
        this.profileService = profileService;
        this.storageService = storageService;
        this.profileDisplayer = loginDisplayer;
        this.navigator = navigator;
    }

    public void startPresenting() {
        navigator.attach(dialogListener);
        profileDisplayer.attach(actionListener);
        loginSubscription = loginService.getAuthentication()
                .subscribe(new Action1<Authentication>() {
                    @Override
                    public void call(final Authentication authentication) {
                        if (authentication.isSuccess()) {
                            userService.getUser(authentication.getUser().getUid())
                                    .subscribe(new Action1<DatabaseResult<User>>() {
                                        @Override
                                        public void call(DatabaseResult<User> userDatabaseResult) {
                                            if (userDatabaseResult.isSuccess()) {
                                                self = userDatabaseResult.getData();
                                                profileDisplayer.display(userDatabaseResult.getData());
                                            }
                                        }
                                    });
                        } else {
                            navigator.toParent();
                        }
                    }
                });
    }

    public void stopPresenting() {
        navigator.detach(dialogListener);
        profileDisplayer.detach(actionListener);
        loginSubscription.unsubscribe();
        if (userSubscription != null)
            userSubscription.unsubscribe();
    }

    private ProfileDisplayer.ProfileActionListener actionListener = new ProfileDisplayer.ProfileActionListener() {

        @Override
        public void onUpPressed() {
            navigator.toParent();
        }

        @Override
        public void onStatusPressed(String currentStatus) {
            navigator.toStatusActivity(currentStatus);
        }

        @Override
        public void onEditPressed(String registrationName) {
            navigator.showNameChangeActivity(registrationName);
        }

        @Override
        public void onImagePressed(View view) {
            navigator.showImagePreview(view, self);
        }

        @Override
        public void onRemovePressed() {
            navigator.showRemoveDialog();
        }

        @Override
        public void onMessagePressed(String sender, String destination) {

        }

        @Override
        public void onCallPressed(User user) {

        }

        @Override
        public void onVideoCallPressed(User user) {

        }

        @Override
        public void onMediaClick(View view, Message message) {

        }

        @Override
        public void onMoreMediaClick(String sender, String destination) {

        }

        @Override
        public void onChangeImagePressed() {
            navigator.showImagePicker();
        }
    };

    private ProfileNavigator.ProfileDialogListener dialogListener = new ProfileNavigator.ProfileDialogListener() {

        @Override
        public void onNameSelected(String text) {
            userService.setName(self, text);
        }

        @Override
        public void onRemoveSelected() {
            profileService.removeUser();
        }

        @Override
        public void onImageSelected(byte[] imageBytes, Bitmap bitmap) {
            profileDisplayer.updateProfileImage(bitmap);
            profileDisplayer.showProgressBar(true);
            storageService.uploadImage(imageBytes)
                    .subscribe(new Action1<String>() {
                        @Override
                        public void call(String image) {
                            if (image != null) {
                                userService.setProfileImage(self, image);
                                profileDisplayer.showProgressBar(false);
                            }
                        }
                    });
        }

        @Override
        public void onProfileRemove() {
            userService.setProfileImage(self, null);
            profileDisplayer.updateProfileImage(null);
        }

    };

}
