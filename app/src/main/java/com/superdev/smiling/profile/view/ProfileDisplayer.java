package com.superdev.smiling.profile.view;

import android.graphics.Bitmap;
import android.view.View;

import com.superdev.smiling.conversation.data_model.Chat;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.user.data_model.User;

/**
 * Created by marco on 09/09/16.
 */

public interface ProfileDisplayer {

    void display(User user);

    void display(User user, String sender);

    void displayMedia(Chat chat);

    void updateProfileImage(Bitmap bitmap);

    void showProgressBar(boolean isVisible);

    void attach(ProfileActionListener profileActionListener);

    void detach(ProfileActionListener profileActionListener);

    interface ProfileActionListener {

        void onUpPressed();

        void onStatusPressed(String currentStatus);

        void onEditPressed(String registrationName);

        void onImagePressed(View view);

        void onChangeImagePressed();

        void onRemovePressed();

        void onMessagePressed(String sender, String destination);

        void onCallPressed(User user);

        void onVideoCallPressed(User user);

        void onMediaClick(View view, Message message);

        void onMoreMediaClick(String sender, String destination);

    }

}
