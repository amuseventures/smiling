package com.superdev.smiling.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.superdev.smiling.BaseActivity;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.navigation.AndroidProfileNavigator;
import com.superdev.smiling.profile.presenter.UserProfilePresenter;
import com.superdev.smiling.profile.view.ProfileDisplayer;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by marco on 19/08/16.
 */

public class UserProfileActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private UserProfilePresenter presenter;

    private AndroidProfileNavigator navigator;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.profile_toolbar_title);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        ProfileDisplayer profileDisplayer = findViewById(R.id.profileView);

        navigator = new AndroidProfileNavigator(this);
        presenter = new UserProfilePresenter(
                Dependencies.INSTANCE.getPhoneLoginService(),
                Dependencies.INSTANCE.getUserService(),
                Dependencies.INSTANCE.getProfileService(),
                Dependencies.INSTANCE.getStorageService(),
                profileDisplayer,
                navigator
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!navigator.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.stopPresenting();
    }

}
