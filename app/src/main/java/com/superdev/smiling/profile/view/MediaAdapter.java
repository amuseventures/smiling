package com.superdev.smiling.profile.view;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.firebase.storage.StorageReference;
import com.superdev.smiling.BaseAdapter;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.conversation.data_model.Message;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by gautam on 25/10/17.
 */

public class MediaAdapter extends BaseAdapter<MediaAdapter.MediaViewHolder, Message> {

    private List<Message> messageList;

    private OnMediaItemClick onMediaItemClick;

    public MediaAdapter() {
        messageList = new ArrayList<>();
    }

    public void setOnMediaItemClick(OnMediaItemClick onMediaItemClick) {
        this.onMediaItemClick = onMediaItemClick;
    }

    @Override
    public void setData(List<Message> messageList) {
        this.messageList = messageList;
        notifyDataSetChanged();
    }

    @Override
    public MediaViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.media_thumb_views, parent, false);
        return new MediaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MediaViewHolder holder, int i) {
        Message message = messageList.get(i);
        setUpImage(message, holder);
        holder.mediaThumbs.setOnClickListener(view -> {
            if (onMediaItemClick != null) {
                onMediaItemClick.onItemClick(view, message);
            }
        });
    }

    private void setUpImage(Message message, MediaViewHolder holder) {
        File file;
        if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
            file = new File(Utils.getImagePath() + "/" + message.getFileModel().getFileName());
        } else {
            file = new File(Utils.getImageSentPath() + "/" + message.getFileModel().getFileName());
        }
        if (file.exists()) {
            setImage(file, holder);
        } else if (message.getFileModel().getFileUri() != null) {
            File file1 = new File(Utils.getRealPathFromURI(holder.mediaThumbs.getContext(), Uri.parse(message.getFileModel().getFileUri())));
            setImage(file1, holder);
        } else {

            StorageReference storageRef = Dependencies.INSTANCE.getStorageService().getChatImageReference();
            StorageReference imageRef = storageRef.child(message.getFileModel().getFileName());
            File downloadFile;
            if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
                downloadFile = new File(Utils.getImagePath() + "/" + message.getFileModel().getFileName());
            } else {
                downloadFile = new File(Utils.getImageSentPath() + "/" + message.getFileModel().getFileName());
            }
            if (!downloadFile.exists()) {
                try {
                    downloadFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (!downloadFile.exists()) {
                return;
            }
            imageRef.getFile(downloadFile)
                    .addOnFailureListener(e -> {

                    }).addOnSuccessListener(taskSnapshot -> {
                setUpImage(message, holder);
            }).addOnProgressListener(taskSnapshot -> {
            });
        }
    }

    private void setImage(File file, MediaViewHolder holder) {
        Glide.with(holder.mediaThumbs.getContext())
                .load(Uri.fromFile(file))
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        holder.mediaThumbs.setImageBitmap(resource);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return messageList != null && !messageList.isEmpty() ? messageList.size() : 0;
    }

    static class MediaViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.media_thumbs)
        ImageView mediaThumbs;

        MediaViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnMediaItemClick {

        void onItemClick(View view, Message message);
    }
}
