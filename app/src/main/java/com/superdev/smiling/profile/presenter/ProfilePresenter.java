package com.superdev.smiling.profile.presenter;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.superdev.smiling.Constants;
import com.superdev.smiling.conversation.data_model.Chat;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.conversation.service.ConversationService;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.navigation.ProfileNavigator;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.profile.service.ProfileService;
import com.superdev.smiling.profile.view.ProfileDisplayer;
import com.superdev.smiling.sinch.SinchService;
import com.superdev.smiling.storage.StorageService;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.service.UserService;

import rx.Observer;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by marco on 09/09/16.
 */

public class ProfilePresenter {

    private final PhoneLoginService loginService;
    private final UserService userService;
    private final ProfileService profileService;
    private final StorageService storageService;
    private final ConversationService conversationService;
    private final ProfileDisplayer profileDisplayer;
    private final ProfileNavigator navigator;
    private User self;
    private Subscription userSubscription, mediaSubscription;
    private String destination;
    private String sender;

    private SinchService.SinchServiceInterface mSinchService;

    public ProfilePresenter(PhoneLoginService loginService,
                            UserService userService,
                            ProfileService profileService,
                            StorageService storageService,
                            ProfileDisplayer loginDisplayer,
                            ProfileNavigator navigator,
                            String sender,
                            String destination,
                            ConversationService conversationService) {
        this.loginService = loginService;
        this.userService = userService;
        this.profileService = profileService;
        this.storageService = storageService;
        this.profileDisplayer = loginDisplayer;
        this.navigator = navigator;
        this.destination = destination;
        this.sender = sender;
        this.conversationService = conversationService;

        userSubscription = userService.getUser(destination)
                .subscribe(new Action1<DatabaseResult<User>>() {
                    @Override
                    public void call(DatabaseResult<User> userDatabaseResult) {
                        if (userDatabaseResult.isSuccess()) {
                            self = userDatabaseResult.getData();
                            profileDisplayer.display(userDatabaseResult.getData(), sender);
                        }
                    }
                });


    }

    public void startPresenting() {
        navigator.attach(dialogListener);
        profileDisplayer.attach(actionListener);
        mediaSubscription = conversationService.syncMedia(sender, destination)
                .subscribe(new Observer<DatabaseResult<Chat>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onNext(DatabaseResult<Chat> chatDatabaseResult) {
                        if (chatDatabaseResult.isSuccess()) {
                            Chat chat = chatDatabaseResult.getData();
                            profileDisplayer.displayMedia(chat);
                        }
                    }
                });

    }

    public void setSinchService(SinchService.SinchServiceInterface mSinchService) {
        this.mSinchService = mSinchService;
    }

    public void stopPresenting() {
        navigator.detach(dialogListener);
        profileDisplayer.detach(actionListener);
        if (userSubscription != null)
            userSubscription.unsubscribe();
    }

    private ProfileDisplayer.ProfileActionListener actionListener = new ProfileDisplayer.ProfileActionListener() {

        @Override
        public void onUpPressed() {
            //navigator.toParent();
        }

        @Override
        public void onStatusPressed(String currentStatus) {

        }

        @Override
        public void onEditPressed(String registrationName) {
            // navigator.showNameChangeActivity(registrationName);
        }

        @Override
        public void onImagePressed(View view) {
            navigator.showImagePreview(view, self);
        }

        @Override
        public void onRemovePressed() {
            // navigator.showRemoveDialog();
        }

        @Override
        public void onMessagePressed(String sender, String destination) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.SENDER_ID, sender);
            bundle.putString(Constants.DESTINATION_ID, destination);
            navigator.toSelectedConversation(bundle);
        }

        @Override
        public void onCallPressed(User user) {
            navigator.placeCall(mSinchService, user);
        }

        @Override
        public void onVideoCallPressed(User user) {
            navigator.placeVideoCall(mSinchService, user);
        }

        @Override
        public void onMediaClick(View view, Message message) {
            navigator.toFullScreenImage(view, message);
        }

        @Override
        public void onMoreMediaClick(String sender, String destination) {
            navigator.toMediaActivity(sender, destination);
        }

        @Override
        public void onChangeImagePressed() {

        }
    };

    private ProfileNavigator.ProfileDialogListener dialogListener = new ProfileNavigator.ProfileDialogListener() {

        @Override
        public void onNameSelected(String text) {
            //userService.setName(self, text);
        }

        @Override
        public void onRemoveSelected() {
            // profileService.removeUser();
        }

        @Override
        public void onImageSelected(byte[] imageBytes, Bitmap bitmap) {

        }

        @Override
        public void onProfileRemove() {
            Log.d("TAG", "onProfileRemove: ");
        }

    };

}
