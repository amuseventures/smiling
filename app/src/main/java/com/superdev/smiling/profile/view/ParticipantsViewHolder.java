package com.superdev.smiling.profile.view;

import android.support.v7.widget.RecyclerView;

import com.superdev.smiling.user.data_model.User;

/**
 * Created by gautam on 13/10/17.
 */

public class ParticipantsViewHolder extends RecyclerView.ViewHolder {

    private ParticipantsView itemView;

    public ParticipantsViewHolder(ParticipantsView itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    public void bind(User user, String channelOwnerId, ParticipantsSelectionListener listener) {
        itemView.display(user, channelOwnerId);
        itemView.setOnClickListener(view -> listener.onParticipantsSelected(user));
    }

    public interface ParticipantsSelectionListener {

        void onParticipantsSelected(User user);

    }
}
