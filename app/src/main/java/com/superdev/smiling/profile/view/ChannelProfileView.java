package com.superdev.smiling.profile.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.superdev.smiling.Constants;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.groupconversation.data_model.GroupChat;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;
import com.superdev.smiling.views.EmptyRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by gautam on 13/10/17.
 */

public class ChannelProfileView extends CoordinatorLayout implements AppBarLayout.OnOffsetChangedListener, ChannelProfileDisplayer, GroupMediaAdapter.OnMediaItemClick {


    @BindView(R.id.toolbar_header_view)
    protected ChannelHeaderView toolbarChannelHeaderView;

    @BindView(R.id.float_header_view)
    protected ChannelHeaderView floatChannelHeaderView;

    @BindView(R.id.image)
    ImageView image;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;

    @BindView(R.id.appbar)
    AppBarLayout appbar;

    @BindView(R.id.participants_title)
    TextView participantsTitle;

    @BindView(R.id.participants_recycler_view)
    EmptyRecyclerView participantsRecyclerView;

    @BindView(R.id.participants_card)
    FrameLayout participantsCard;

    @BindView(R.id.participants_card_bottom)
    View participantsCardBottom;

    @BindView(R.id.exit_group_icon)
    ImageView exitGroupIcon;

    @BindView(R.id.exit_group_text)
    TextView exitGroupText;

    @BindView(R.id.exit_group_btn)
    LinearLayout exitGroupBtn;

    @BindView(R.id.scrollView)
    NestedScrollView scrollView;

    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;

    @BindView(R.id.media_title)
    TextView mediaTitle;

    @BindView(R.id.media_info)
    TextView mediaInfo;

    @BindView(R.id.media_scroller)
    EmptyRecyclerView mediaScroller;

    @BindView(R.id.media_card)
    LinearLayout mediaCard;

    private Channel channel;

    @BindView(R.id.add_participants_button)
    LinearLayout addParticipantsButton;

    private MaterialDialog materialDialog;

    private User channelOwner;

    private ParticipantsAdapter adapter;

    private boolean isHideToolbarView = false;

    private ChannelProfileActionListener profileActionListener;

    private User currentUser;

    public ChannelProfileView(Context context, AttributeSet attrs) {
        super(context, attrs);
        adapter = new ParticipantsAdapter(LayoutInflater.from(context));
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_channel_profile_view, this);
        ButterKnife.bind(this);
        initUi();
        participantsRecyclerView.setAdapter(adapter);
        participantsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        participantsRecyclerView.setHasFixedSize(true);
        exitGroupBtn.setOnClickListener(view -> {
            if (profileActionListener != null && currentUser != null) {
                profileActionListener.onExitGroup(currentUser);
            }
        });

        mediaInfo.setOnClickListener(view -> {
            if (profileActionListener != null) {
                profileActionListener.onMoreMediaClick(channel);
            }
        });
    }

    private void initUi() {
        appbar.addOnOffsetChangedListener(this);
    }

    @OnClick(R.id.add_participants_button)
    void onAddParticipants() {
        if (profileActionListener != null && channel != null) {
            profileActionListener.addParticipantsPressed(channel);
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        if (percentage == 1f && isHideToolbarView) {
            toolbarChannelHeaderView.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;

        } else if (percentage < 1f && !isHideToolbarView) {
            toolbarChannelHeaderView.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
        }
    }

    @Override
    public void display(Channel channel, User user) {
        this.channelOwner = user;
        this.channel = channel;
        String name = user.getName().split(" ")[0];
        String createdString = (user.getUid().equalsIgnoreCase(Remember.getString(Constants.FIREBASE_USER_ID, ""))
                ? getContext().getString(R.string.group_creator_you) : String.format(getContext().getString(R.string.group_creator_name), name + ", " + Utils.getGroupDate(channel.getCreatedAt())));
        toolbarChannelHeaderView.bindTo(channel.getName(), createdString, listener);
        floatChannelHeaderView.bindTo(channel.getName(), createdString, listener);
        Utils.loadImage(channel.getImage(), image, getContext(), R.drawable.avatar_group_large);
        if (channelOwner.getUid().equalsIgnoreCase(Remember.getString(Constants.FIREBASE_USER_ID, ""))) {
            addParticipantsButton.setVisibility(VISIBLE);
        } else {
            addParticipantsButton.setVisibility(GONE);
        }
    }

    @Override
    public void displayParticipants(Users users) {
        adapter.update(users, channelOwner);
        for (User user : users.getUsers()) {
            if (user.getUid().equalsIgnoreCase(Remember.getString(Constants.FIREBASE_USER_ID, ""))) {
                currentUser = user;
                break;
            }
        }
        participantsTitle.setText(String.format(getContext().getString(R.string.total_participants), users.size()));

    }

    @Override
    public void displayMedia(GroupChat chat) {
        if (chat == null || chat.getGroupMessages() == null || chat.getGroupMessages().isEmpty()) {
            return;
        }
        GroupMediaAdapter mediaAdapter = new GroupMediaAdapter();
        mediaAdapter.setOnMediaItemClick(this);
        mediaScroller.setAdapter(mediaAdapter);
        mediaScroller.setHasFixedSize(true);
        mediaScroller.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mediaAdapter.setData(chat.getGroupMessages());
    }

    @Override
    public void updateProfileImage(Bitmap bitmap) {
        image.setImageBitmap(bitmap);

    }

    @Override
    public void updateName(String name) {
        floatChannelHeaderView.setName(name);
        toolbarChannelHeaderView.setName(name);
    }

    @Override
    public void showProgressDialog() {
        if (materialDialog == null) {
            materialDialog = new MaterialDialog.Builder(getContext())
                    .title("Removing participant")
                    .content("Please wait...")
                    .progress(true, 0)
                    .build();
            materialDialog.show();
        } else {
            materialDialog.dismiss();
        }
    }

    @Override
    public void hideProgressDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void attach(ChannelProfileActionListener profileActionListener) {
        this.profileActionListener = profileActionListener;
        adapter.attach(profileActionListener);
    }

    @Override
    public void detach(ChannelProfileActionListener profileActionListener) {
        this.profileActionListener = null;
        adapter.detach(profileActionListener);
    }

    @Override
    public void showSuccess() {
        profileActionListener.onUpPressed();
    }


    @Override
    public void showProgressBar(boolean b) {

    }

    @OnClick(R.id.image)
    public void onViewClicked(View view) {
        profileActionListener.onImagePressed(view, channel);
    }

    ChannelHeaderView.OnEditNameSelectedListener listener = view -> {
        PopupMenu popup = new PopupMenu(getContext(), view);
        popup.getMenuInflater().inflate(R.menu.group_profile_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.action_change_name:
                    profileActionListener.onEditPressed(floatChannelHeaderView.getName());
                    break;
                case R.id.action_change_image:
                    profileActionListener.onChangeImagePressed();
                    break;
            }
            return false;
        });
        popup.show();
    };

    @Override
    public void onItemClick(View view, GroupMessage message) {
        profileActionListener.onMediaClick(view, message);
    }
}