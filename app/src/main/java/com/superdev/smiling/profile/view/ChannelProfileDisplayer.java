package com.superdev.smiling.profile.view;

import android.graphics.Bitmap;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.groupconversation.data_model.GroupChat;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;

/**
 * Created by gautam on 13/10/17.
 */

public interface ChannelProfileDisplayer {

    void display(Channel channel, User user);

    void displayParticipants(Users users);

    void displayMedia(GroupChat chat);

    void updateProfileImage(Bitmap bitmap);

    void updateName(String name);

    void showProgressDialog();

    void hideProgressDialog();

    void attach(ChannelProfileActionListener profileActionListener);

    void detach(ChannelProfileActionListener profileActionListener);

    void showSuccess();

    void showProgressBar(boolean b);

    interface ChannelProfileActionListener {

        void onUpPressed();

        void onEditPressed(String registrationName);

        void onImagePressed(View view, Channel channel);

        void onChangeImagePressed();

        void onRemovePressed();

        void addParticipantsPressed(Channel channel);

        void onParticipantsPressed(User user);

        void onExitGroup(User user);

        void onMediaClick(View view, GroupMessage message);

        void onMoreMediaClick(Channel channel);
    }
}
