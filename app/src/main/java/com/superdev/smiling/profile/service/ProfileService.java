package com.superdev.smiling.profile.service;

/**
 * Created by marco on 09/09/16.
 */

public interface ProfileService {

    void removeUser();

    void removeProfilePhoto();

}
