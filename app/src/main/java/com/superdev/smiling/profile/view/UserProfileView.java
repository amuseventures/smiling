package com.superdev.smiling.profile.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.conversation.data_model.Chat;
import com.superdev.smiling.user.data_model.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by marco on 09/09/16.
 */

public class UserProfileView extends LinearLayout implements ProfileDisplayer, View.OnClickListener {

    @BindView(R.id.photo_btn)
    CircleImageView photoBtn;

    @BindView(R.id.change_photo_progress)
    ProgressBar changePhotoProgress;

    @BindView(R.id.change_photo_btn)
    ImageButton changePhotoBtn;

    @BindView(R.id.registration_name)
    EmojiconTextView registrationName;

    @BindView(R.id.change_registration_name_btn)
    ImageButton changeRegistrationNameBtn;

    @BindView(R.id.status_and_phone_title)
    TextView statusAndPhoneTitle;

    @BindView(R.id.status)
    EmojiconTextView status;

    @BindView(R.id.status_card)
    FrameLayout statusCard;

    @BindView(R.id.status_separator)
    View statusSeparator;

    @BindView(R.id.phone)
    TextView phone;

    @BindView(R.id.phone_card)
    FrameLayout phoneCard;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private ProfileActionListener actionListener;

    public UserProfileView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_user_profile_view, this);
        ButterKnife.bind(this);
    }

    @Override
    public void display(User user) {
        registrationName.setText(user.getName());
        phone.setText(user.getPhone());
        status.setText(user.getStatus());
        Utils.loadImageElseBlack(user.getImage(), photoBtn, getContext(),R.drawable.avatar_contact_large);
    }

    @Override
    public void display(User user, String sender) {
        //No Use here
    }

    @Override
    public void displayMedia(Chat chat) {

    }

    @Override
    public void updateProfileImage(Bitmap bitmap) {
        photoBtn.setImageBitmap(bitmap);
        photoBtn.setDrawingCacheEnabled(true);
        photoBtn.buildDrawingCache();
    }

    @Override
    public void showProgressBar(boolean isVisible) {
        showProgress(isVisible);
    }

    @Override
    public void attach(ProfileActionListener profileActionListener) {
        this.actionListener = profileActionListener;

        photoBtn.setOnClickListener(this);
        changeRegistrationNameBtn.setOnClickListener(this);
        toolbar.setNavigationOnClickListener(navigationClickListener);
        changePhotoBtn.setOnClickListener(this);
        statusCard.setOnClickListener(this);
    }

    @Override
    public void detach(ProfileActionListener profileActionListener) {
        this.actionListener = null;

        photoBtn.setOnClickListener(null);
        changeRegistrationNameBtn.setOnClickListener(null);
        changePhotoBtn.setOnClickListener(null);
        toolbar.setNavigationOnClickListener(null);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.photo_btn:
                actionListener.onImagePressed(view);
                break;
            case R.id.change_registration_name_btn:
                actionListener.onEditPressed(registrationName.getText().toString());
                break;
            case R.id.change_photo_btn:
                actionListener.onChangeImagePressed();
                break;
            case R.id.status_card:
                actionListener.onStatusPressed(status.getText().toString());
                break;
        }
    }

    private final OnClickListener navigationClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            actionListener.onUpPressed();
        }
    };

    private void showProgress(boolean isVisible) {
        changePhotoProgress.setVisibility(isVisible ? VISIBLE : GONE);
    }

}
