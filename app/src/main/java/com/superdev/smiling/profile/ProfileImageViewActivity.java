package com.superdev.smiling.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.superdev.smiling.Constants;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileImageViewActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.image_view)
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_image_view);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        Intent intent = getIntent();
        String userName = intent.getStringExtra(Constants.FIREBASE_USERS_NAME);
        String userImage = intent.getStringExtra(Constants.FIREBASE_USERS_IMAGE);

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(userName);
        }

        Utils.loadImage(userImage, imageView, ProfileImageViewActivity.this,R.drawable.avatar_contact_large);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            supportFinishAfterTransition();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        supportFinishAfterTransition();
    }
}
