package com.superdev.smiling.channel.database;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.superdev.smiling.Constants;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.rx.FirebaseObservableListeners;
import com.superdev.smiling.user.data_model.User;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;


public class FirebaseChannelsDatabase implements ChannelsDatabase {

    private final DatabaseReference publicChannelsDB;
    private final DatabaseReference privateChannelsDB;
    private final DatabaseReference channelsDB;
    private final DatabaseReference ownersDB;
    private final FirebaseObservableListeners firebaseObservableListeners;

    public FirebaseChannelsDatabase(FirebaseDatabase firebaseDatabase, FirebaseObservableListeners firebaseObservableListeners) {
        this.publicChannelsDB = firebaseDatabase.getReference("public-channels-index");
        this.privateChannelsDB = firebaseDatabase.getReference("private-channels-index");
        this.channelsDB = firebaseDatabase.getReference("channels");
        this.ownersDB = firebaseDatabase.getReference("owners");
        this.firebaseObservableListeners = firebaseObservableListeners;
    }

    @Override
    public Observable<List<String>> observePublicChannelIds() {
        return firebaseObservableListeners.listenToValueEvents(publicChannelsDB, getKeys());
    }

    @Override
    public Observable<Channel> getChannelById(String channelId) {
        return firebaseObservableListeners.listenToValueEvents(channelsDB.child(channelId), asChannel());
    }

    @Override
    public Observable<List<String>> observePrivateChannelIdsFor(User user) {
        return firebaseObservableListeners.listenToValueEvents(privateChannelsDB.child(user.getUid()), getKeys());
    }

    @Override
    public Observable<Channel> readChannelFor(String channelUid) {
        return firebaseObservableListeners.listenToSingleValueEvents(channelsDB.child(channelUid), asChannel());
    }

    @Override
    public Observable<Channel> writeChannel(Channel newChannel) {
        return firebaseObservableListeners.setValue(ChannelConverter.toFirebaseChannel(newChannel), channelsDB.child(newChannel.getUid()), newChannel);
    }

    @Override
    public Observable<Channel> addOwnerToPrivateChannel(User user, Channel channel) {
        return firebaseObservableListeners.setValue(user.getName(), ownersDB.child(channel.getUid()).child(user.getUid()), channel);
    }

    @Override
    public Observable<Channel> removeOwnerFromPrivateChannel(User user, Channel channel) {
        return firebaseObservableListeners.removeValue(ownersDB.child(channel.getUid()).child(user.getUid()), channel);
    }

    @Override
    public Observable<Channel> removeOwnerFromPrivateChannel(String user, Channel channel) {
        return firebaseObservableListeners.removeValue(ownersDB.child(channel.getName()).child(user), channel);
    }

    @Override
    public Observable<Channel> addChannelToUserPrivateChannelIndex(User user, Channel channel) {
        return firebaseObservableListeners.setValue(user.getUid(), privateChannelsDB.child(user.getUid()).child(channel.getUid()), channel);
    }

    @Override
    public Observable<Channel> removeChannelFromUserPrivateChannelIndex(User user, Channel channel) {
        return firebaseObservableListeners.removeValue(privateChannelsDB.child(user.getUid()).child(channel.getUid()), channel);
    }

    @Override
    public Observable<Channel> removeChannelFromUserPrivateChannelIndex(String user, Channel channel) {
        return firebaseObservableListeners.removeValue(privateChannelsDB.child(user).child(channel.getUid()), channel);
    }

    @Override
    public Observable<List<String>> observeOwnerIdsFor(Channel channel) {
        return firebaseObservableListeners.listenToValueEvents(ownersDB.child(channel.getUid()), getKeys());
    }

    @Override
    public void setGroupName(String channelId, String groupName) {
        channelsDB.child(channelId).child(Constants.FIREBASE_USERS_NAME).setValue(groupName);
    }

    @Override
    public void setGroupImage(String channelId, String image) {
        channelsDB.child(channelId).child(Constants.FIREBASE_USERS_IMAGE).setValue(image);
    }

    private static Func1<DataSnapshot, Channel> asChannel() {
        return dataSnapshot -> ChannelConverter.fromFirebaseChannel(dataSnapshot.getValue(FirebaseChannel.class));
    }

    private static Func1<DataSnapshot, List<String>> getKeys() {
        return dataSnapshot -> {
            List<String> keys = new ArrayList<>();
            if (dataSnapshot.hasChildren()) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child : children) {
                    keys.add(child.getKey());
                }
            }
            return keys;
        };
    }
}

