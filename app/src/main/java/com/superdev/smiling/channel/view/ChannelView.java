package com.superdev.smiling.channel.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel.model.Channel;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

public class ChannelView extends FrameLayout {

    @BindView(R.id.profileImageView)
    CircleImageView profileImageView;

    @BindView(R.id.nameTextView)
    EmojiconTextView nameTextView;

    @BindView(R.id.timeTextView)
    TextView timeTextView;

    @BindView(R.id.messageTextView)
    EmojiconTextView messageTextView;

    @BindView(R.id.content_layout)
    LinearLayout contentLayout;

    public ChannelView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_channel_item_view, this);
        ButterKnife.bind(this);

    }

    public void display(Channel channel) {
        Utils.loadImageElseWhite(channel.getImage(), profileImageView, getContext(), R.drawable.avatar_group);
        nameTextView.setText(channel.getName());
    }
}
