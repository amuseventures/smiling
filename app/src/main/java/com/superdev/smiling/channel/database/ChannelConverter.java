package com.superdev.smiling.channel.database;

import com.superdev.smiling.channel.model.Channel;

public class ChannelConverter {

    static FirebaseChannel toFirebaseChannel(Channel channel) {
        return new FirebaseChannel(channel.getUid(), channel.getName(), channel.getOwner(), channel.getImage(), channel.getCreatedAt());
    }

    static Channel fromFirebaseChannel(FirebaseChannel firebaseChannel) {
        return new Channel(firebaseChannel.getUid(), firebaseChannel.getName(), firebaseChannel.getOwner(), firebaseChannel.getImage(), firebaseChannel.getCreatedAt());
    }

}
