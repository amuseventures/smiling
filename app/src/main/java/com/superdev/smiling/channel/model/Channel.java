package com.superdev.smiling.channel.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Channel implements Parcelable {

    public String uid;

    public String name;

    public String owner;

    public String image;

    public long createdAt;

    public Channel() {

    }

    protected Channel(Parcel in) {
        uid = in.readString();
        name = in.readString();
        owner = in.readString();
        image = in.readString();
        createdAt = in.readLong();
    }

    public static final Creator<Channel> CREATOR = new Creator<Channel>() {
        @Override
        public Channel createFromParcel(Parcel in) {
            return new Channel(in);
        }

        @Override
        public Channel[] newArray(int size) {
            return new Channel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(uid);
        parcel.writeString(name);
        parcel.writeString(owner);
        parcel.writeString(image);
        parcel.writeLong(createdAt);
    }

    public Channel(String uid, String name, String owner) {
        this.uid = uid;
        this.name = name;
        this.owner = owner;
    }

    public Channel(String uid, String name, String owner, String image, long createdAt) {
        this.uid = uid;
        this.name = name;
        this.owner = owner;
        this.image = image;
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public String getOwner() {
        return owner;
    }

    public String getUid() {
        return uid;
    }

    public String getImage() {
        return image;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Channel channel = (Channel) o;

        return uid != null ? uid.equals(channel.uid) : channel.uid == null && name != null ? name.equals(channel.name) : channel.name == null
                && owner.equals(channel.owner);
    }

    @Override
    public int hashCode() {
        int result = uid != null ? uid.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
