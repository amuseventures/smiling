package com.superdev.smiling.channel.view;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.superdev.smiling.R;
import com.superdev.smiling.channel.displayer.ChannelsDisplayer;
import com.superdev.smiling.channel.model.Channels;
import com.superdev.smiling.views.EmptyRecyclerView;
import com.superdev.smiling.views.TextViewWithImages;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChannelsView extends LinearLayout implements ChannelsDisplayer {

    private final ChannelsAdapter channelsAdapter;

    @BindView(R.id.conversationsRecyclerView)
    EmptyRecyclerView channels;

    @BindView(R.id.btn_float)
    FloatingActionButton btnFloat;

    @BindView(R.id.welcome_chats_message)
    TextViewWithImages view;


    private ChannelsInteractionListener channelsInteractionListener;

    public ChannelsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        channelsAdapter = new ChannelsAdapter(LayoutInflater.from(context));
        setOrientation(VERTICAL);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_channels_view, this);
        ButterKnife.bind(this);
        view.setText(R.string.welcome_chats_message);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        channels.setLayoutManager(layoutManager);
        channels.setAdapter(channelsAdapter);
        channels.setEmptyView(view);
    }

    @Override
    public void display(Channels channels) {
        channelsAdapter.update(channels);
    }

    @Override
    public void attach(ChannelsInteractionListener channelsInteractionListener) {
        this.channelsInteractionListener = channelsInteractionListener;
        channelsAdapter.attach(channelsInteractionListener);
        btnFloat.setOnClickListener(fabClickListener);
    }

    @Override
    public void detach(ChannelsInteractionListener channelsInteractionListener) {
        channelsAdapter.detach(channelsInteractionListener);
        btnFloat.setOnClickListener(null);
        this.channelsInteractionListener = null;
    }

    private final OnClickListener fabClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            channelsInteractionListener.onAddNewChannel();
        }
    };
}
