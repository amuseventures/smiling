package com.superdev.smiling.channel.presenter;

public interface Config {

    public boolean orderChannelsByName();

}