package com.superdev.smiling.channel.displayer;


import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.channel.model.Channels;

public interface ChannelsDisplayer {

    void display(Channels channels);

    void attach(ChannelsInteractionListener channelsInteractionListener);

    void detach(ChannelsInteractionListener channelsInteractionListener);

    interface ChannelsInteractionListener {

        void onChannelSelected(Channel channel);

        void onAddNewChannel();

        void onInviteUsersClicked();

        void onChannelLongPressed(Channel channel);
    }
}
