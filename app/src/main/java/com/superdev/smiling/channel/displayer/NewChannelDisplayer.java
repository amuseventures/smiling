package com.superdev.smiling.channel.displayer;

import android.graphics.Bitmap;
import android.view.View;

import com.superdev.smiling.user.data_model.User;

import java.util.ArrayList;
import java.util.List;

public interface NewChannelDisplayer {

    void attach(ChannelCreationListener channelCreationListener);

    void detach(ChannelCreationListener channelCreationListener);

    void showChannelCreationError();

    void updateProfileImage(Bitmap bitmap);

    void display(ArrayList<User> userArrayList);

    void hideProgressDialog();

    void showProgressDialog();

    void showProgressBar();

    void hideProgressBar();

    interface ChannelCreationListener {

        void onCreateChannelClicked(String channelName, List<User> selectedMembers);

        void onCancel();

        void onChannelImageClicked();
    }
}
