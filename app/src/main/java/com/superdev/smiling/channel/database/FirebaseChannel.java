package com.superdev.smiling.channel.database;

class FirebaseChannel {

    private String uid;

    private String name;

    private String owner;

    private String image;

    private long createdAt;


    @SuppressWarnings("unused") // used by Firebase
    public FirebaseChannel() {
    }

    public FirebaseChannel(String uid, String name, String owner, String image, long createdAt) {
        this.uid = uid;
        this.name = name;
        this.owner = owner;
        this.image = image;
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public String getUid() {
        return uid;
    }

    public String getOwner() {
        return owner;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public String getImage() {
        return image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FirebaseChannel that = (FirebaseChannel) o;

        return name.equals(that.name) && uid.equals(that.uid);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + uid.hashCode();
        return result;
    }
}
