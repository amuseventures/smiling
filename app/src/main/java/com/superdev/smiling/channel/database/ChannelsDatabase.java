package com.superdev.smiling.channel.database;


import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.user.data_model.User;

import java.util.List;

import rx.Observable;


public interface ChannelsDatabase {

    Observable<List<String>> observePublicChannelIds();

    Observable<Channel> getChannelById(String channelId);

    Observable<List<String>> observePrivateChannelIdsFor(User user);

    Observable<Channel> readChannelFor(String channelName);

    Observable<Channel> writeChannel(Channel newChannel);

    Observable<Channel> addOwnerToPrivateChannel(User user, Channel channel);

    Observable<Channel> removeOwnerFromPrivateChannel(User user, Channel channel);

    Observable<Channel> removeOwnerFromPrivateChannel(String user, Channel channel);

    Observable<Channel> addChannelToUserPrivateChannelIndex(User user, Channel channel);

    Observable<Channel> removeChannelFromUserPrivateChannelIndex(User user, Channel channel);

    Observable<Channel> removeChannelFromUserPrivateChannelIndex(String user, Channel channel);

    Observable<List<String>> observeOwnerIdsFor(Channel channel);

    void setGroupName(String channelId, String groupName);

    void setGroupImage(String channelId, String image);

}
