package com.superdev.smiling.channel.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.superdev.smiling.R;
import com.superdev.smiling.channel.displayer.NewChannelDisplayer;
import com.superdev.smiling.group.view.SelectedMemberAdapter;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.views.AutoFitRecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

public class NewChannelView extends LinearLayout implements NewChannelDisplayer {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.channelProfileView)
    CircleImageView channelProfileView;

    @BindView(R.id.image_upload_progress_bar)
    ProgressBar imageUploadProgressBar;

    @BindView(R.id.image_frame)
    FrameLayout imageFrame;

    @BindView(R.id.edit_channel_name)
    EmojiconEditText editChannelName;

    @BindView(R.id.user_layout)
    RelativeLayout userLayout;

    @BindView(R.id.iv_smiley)
    ImageView ivSmiley;

    @BindView(R.id.text)
    TextView text;

    @BindView(R.id.relative)
    RelativeLayout relative;

    @BindView(R.id.appbar)
    AppBarLayout appbar;

    @BindView(R.id.create_group)
    FloatingActionButton createGroup;

    @BindView(R.id.channels)
    AutoFitRecyclerView channels;

    @BindView(R.id.tvTitle)
    EmojiconTextView tvTitle;

    @BindView(R.id.tvSubTitle)
    EmojiconTextView tvSubTitle;

    @BindView(R.id.currentRoot)
    CoordinatorLayout currentRoot;

    private SelectedMemberAdapter adapter;

    private MaterialDialog materialDialog;

    private ChannelCreationListener channelCreationListener;

    public NewChannelView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
        adapter = new SelectedMemberAdapter(LayoutInflater.from(context));
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_new_channel_view, this);
        ButterKnife.bind(this);
        setupToolbar();

        EmojIconActions emojIcon = new EmojIconActions(getContext(), currentRoot, editChannelName, ivSmiley);
        emojIcon.ShowEmojIcon();
    }

    private void setupToolbar() {
        tvTitle.setText(R.string.new_group);
        tvSubTitle.setText(R.string.add_subject);
    }

    @Override
    public void attach(final ChannelCreationListener channelCreationListener) {
        this.channelCreationListener = channelCreationListener;
        editChannelName.addTextChangedListener(channelNameTextWatcher);
        createGroup.setOnClickListener(channelCreateListener);
        channelProfileView.setOnClickListener(channelCreateListener);
        toolbar.setNavigationOnClickListener(navigationOnClickListener);
        //channels.addItemDecoration(new ChannelItemDecoration());
        channels.setAdapter(adapter);
    }

    @Override
    public void detach(ChannelCreationListener channelCreationListener) {
        editChannelName.removeTextChangedListener(channelNameTextWatcher);
        createGroup.setOnClickListener(null);
        toolbar.setNavigationOnClickListener(null);
        this.channelCreationListener = null;
    }

    @Override
    public void showChannelCreationError() {
        setChannelNameError(R.string.channel_cannot_be_created);
    }

    @Override
    public void updateProfileImage(Bitmap bitmap) {
        channelProfileView.setImageBitmap(bitmap);
        channelProfileView.setDrawingCacheEnabled(true);
        channelProfileView.buildDrawingCache();
    }

    @Override
    public void display(ArrayList<User> userArrayList) {
        adapter.update(userArrayList);
    }

    @Override
    public void hideProgressDialog() {
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
    }

    @Override
    public void showProgressDialog() {
        if (materialDialog == null) {
            materialDialog = new MaterialDialog.Builder(getContext())
                    .title("Creating Group")
                    .content("Please wait...")
                    .progress(true, 0)
                    .build();
            materialDialog.show();
        } else {
            materialDialog.dismiss();
        }
    }

    @Override
    public void showProgressBar() {
        imageUploadProgressBar.setVisibility(VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        imageUploadProgressBar.setVisibility(GONE);
    }

    private void setChannelNameError(int stringId) {
        editChannelName.setError(getContext().getString(stringId));
    }

    private final TextWatcher channelNameTextWatcher = new TextWatcher() {

        private boolean isValidInput;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            isValidInput = inputWasEmpty(start, before);
        }

        private boolean inputWasEmpty(int start, int before) {
            return start == 0 && before == 0;
        }


        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private final OnClickListener channelCreateListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.channelProfileView:
                    channelCreationListener.onChannelImageClicked();
                    break;
                case R.id.create_group:
                    if (adapter.getUserList() != null && !adapter.getUserList().isEmpty()) {
                        channelCreationListener.onCreateChannelClicked(editChannelName.getText().toString(), adapter.getUserList());
                    }
                    break;
            }
        }
    };


    private final OnClickListener navigationOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            channelCreationListener.onCancel();
        }
    };

    private class ChannelItemDecoration extends RecyclerView.ItemDecoration {

        private final int itemPaddingInPixel = getResources().getDimensionPixelOffset(R.dimen.channel_item_padding);
        private final int gridPaddingInPixel = getResources().getDimensionPixelOffset(R.dimen.channel_grid_padding);

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.top = itemPaddingInPixel;
            outRect.bottom = itemPaddingInPixel;
            outRect.left = itemPaddingInPixel;
            outRect.right = itemPaddingInPixel;

            int position = parent.getChildAdapterPosition(view);
            int spanCount = ((GridLayoutManager) parent.getLayoutManager()).getSpanCount();

            if (isTopRow(position, spanCount)) {
                outRect.top += gridPaddingInPixel;
            } else if (isBottomRow(parent, position, spanCount)) {
                outRect.bottom += gridPaddingInPixel;
            }

            if (isLeftEdge(position, spanCount)) {
                outRect.left += gridPaddingInPixel;
            } else if (isRightEdge(position, spanCount)) {
                outRect.right += gridPaddingInPixel;
            }
        }

        private boolean isTopRow(int position, int spanCount) {
            return position < spanCount;
        }

        private boolean isBottomRow(RecyclerView parent, int position, int spanCount) {
            return position >= parent.getAdapter().getItemCount() - spanCount;
        }

        private boolean isLeftEdge(int position, int spanCount) {
            return (position % spanCount) == 0;
        }

        private boolean isRightEdge(int position, int spanCount) {
            return (position % spanCount) == (spanCount - 1);
        }
    }
}
