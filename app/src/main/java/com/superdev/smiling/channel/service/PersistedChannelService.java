package com.superdev.smiling.channel.service;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Remember;
import com.superdev.smiling.channel.database.ChannelsDatabase;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.channel.model.Channels;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.main.MainActivity;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;
import com.superdev.smiling.user.database.UserDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;


public class PersistedChannelService implements ChannelService {

    private final ChannelsDatabase channelsDatabase;
    private final UserDatabase userDatabase;
    private final GcmPubSub gcmPubSub;

    public PersistedChannelService(ChannelsDatabase channelsDatabase, UserDatabase userDatabase, GcmPubSub gcmPubSub) {
        this.channelsDatabase = channelsDatabase;
        this.userDatabase = userDatabase;
        this.gcmPubSub = gcmPubSub;
    }

    @Override
    public Observable<DatabaseResult<Channels>> getChannelsFor(User user) {
        return Observable.combineLatest(publicChannels(), privateChannelsFor(user), mergeChannels())
                .map(DatabaseResult::new)
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    @Override
    public Observable<Channel> getChannelById(String channelId) {
        return channelsDatabase.getChannelById(channelId);
    }

    private Observable<List<Channel>> publicChannels() {
        return channelsDatabase.observePublicChannelIds()
                .flatMap(channelsFromIds());
    }

    private Observable<List<Channel>> privateChannelsFor(User user) {
        return channelsDatabase.observePrivateChannelIdsFor(user)
                .flatMap(channelsFromIds());
    }


    private Func1<List<String>, Observable<List<Channel>>> channelsFromIds() {
        return channelIds -> Observable.from(channelIds)
                .flatMap(channelFromId())
                .toList();
    }

    private Func1<String, Observable<Channel>> channelFromId() {
        return channelsDatabase::readChannelFor;
    }

    private Func2<List<Channel>, List<Channel>, Channels> mergeChannels() {
        return (channels, channels2) -> {
            List<Channel> mergedChannels = new ArrayList<>(channels);
            mergedChannels.addAll(channels2);
            return new Channels(mergedChannels);
        };
    }

    @Override
    public Observable<DatabaseResult<Channel>> createPrivateChannel(final Channel newChannel, User owner) {
        return channelsDatabase.addOwnerToPrivateChannel(owner, newChannel)
                .flatMap(addUserAsChannelOwner(owner))
                .flatMap(writeChannel())
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    private Func1<Channel, Observable<DatabaseResult<Channel>>> writeChannel() {
        return channel -> channelsDatabase.writeChannel(channel)
                .map(DatabaseResult::new);
    }

    @Override
    public Observable<DatabaseResult<User>> addOwnerToPrivateChannel(final Channel channel, final User newOwner) {
        return channelsDatabase.addOwnerToPrivateChannel(newOwner, channel)
                .flatMap(addUserAsChannelOwner(newOwner))
                .map(channel1 -> new DatabaseResult<>(newOwner))
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    private Func1<Channel, Observable<Channel>> addUserAsChannelOwner(final User user) {
        return channel -> channelsDatabase.addChannelToUserPrivateChannelIndex(user, channel);
    }

    @Override
    public Observable<DatabaseResult<User>> removeOwnerFromPrivateChannel(final Channel channel, final User removedOwner) {
        return channelsDatabase.removeOwnerFromPrivateChannel(removedOwner, channel)
                .flatMap(removeChannelReferenceFromUser(removedOwner))
                .map(channel1 -> new DatabaseResult<>(removedOwner))
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    @Override
    public Observable<DatabaseResult<String>> removeOwnerFromPrivateChannel(final Channel channel, final String removedOwner) {
        return channelsDatabase.removeOwnerFromPrivateChannel(removedOwner, channel)
                .flatMap(removeChannelReferenceFromUser(removedOwner))
                .map(channel1 -> new DatabaseResult<>(removedOwner))
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    private Func1<Channel, Observable<Channel>> removeChannelReferenceFromUser(final User user) {
        return channel -> channelsDatabase.removeChannelFromUserPrivateChannelIndex(user, channel);
    }

    private Func1<Channel, Observable<Channel>> removeChannelReferenceFromUser(final String user) {
        return channel -> channelsDatabase.removeChannelFromUserPrivateChannelIndex(user, channel);
    }


    @Override
    public Observable<DatabaseResult<Users>> getOwnersOfChannel(Channel channel) {
        return channelsDatabase.observeOwnerIdsFor(channel)
                .flatMap(getUsersFromIds())
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    @Override
    public void subscribeToChannel(Channel channel, User user) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    gcmPubSub.subscribe(user.getFcm(), "/topics/" + channel.getUid(), null);
                    Log.d("TAG", "Subscribed: " + user.getName());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    private Func1<List<String>, Observable<DatabaseResult<Users>>> getUsersFromIds() {
        return userIds -> Observable.from(userIds)
                .flatMap(getUserFromId())
                .toList()
                .map((Func1<List<User>, DatabaseResult<Users>>) rs -> new DatabaseResult<>(new Users(rs)));
    }

    private Func1<String, Observable<User>> getUserFromId() {
        return userDatabase::readUserFrom;
    }
}
