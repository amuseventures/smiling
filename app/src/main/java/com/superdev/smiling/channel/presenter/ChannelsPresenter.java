package com.superdev.smiling.channel.presenter;

import com.superdev.smiling.analytics.Analytics;
import com.superdev.smiling.channel.displayer.ChannelsDisplayer;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.channel.model.Channels;
import com.superdev.smiling.channel.service.ChannelService;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.navigation.ChannelNavigator;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.user.data_model.User;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

public class ChannelsPresenter {

    private final ChannelsDisplayer channelsDisplayer;
    private final ChannelService channelService;
    private final PhoneLoginService loginService;
    private final Config config;
    private final ChannelNavigator navigator;
    private final Analytics analytics;

    private Subscription subscription;
    private User user;

    public ChannelsPresenter(
            ChannelsDisplayer channelsDisplayer,
            ChannelService channelService,
            PhoneLoginService loginService,
            Config config,
            ChannelNavigator navigator,
            Analytics analytics
    ) {
        this.channelsDisplayer = channelsDisplayer;
        this.channelService = channelService;
        this.loginService = loginService;
        this.config = config;
        this.navigator = navigator;
        this.analytics = analytics;
    }

    public void startPresenting() {
        channelsDisplayer.attach(channelsInteractionListener);
        subscription = loginService.getAuthentication()
                .filter(successfullyAuthenticated())
                .doOnNext(authentication -> user = authentication.getUser())
                .flatMap(channelsForUser())
                .map(sortIfConfigured())
                .subscribe(new Observer<Channels>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Channels channels) {
                        channelsDisplayer.display(channels);
                    }
                });
    }

    private Func1<Authentication, Observable<Channels>> channelsForUser() {
        return authentication -> channelService.getChannelsFor(authentication.getUser())
                .map(channelsDatabaseResult -> {
                    if (channelsDatabaseResult.isSuccess()) {
                        return channelsDatabaseResult.getData();
                    } else {
                        return null;
                    }
                });
    }

    private Func1<Channels, Channels> sortIfConfigured() {
        return channels -> {
            if (config.orderChannelsByName()) {
                return channels.sortedByName();
            } else {
                return channels;
            }
        };
    }

    private Func1<Authentication, Boolean> successfullyAuthenticated() {
        return Authentication::isSuccess;
    }

    public void stopPresenting() {
        subscription.unsubscribe();
        channelsDisplayer.detach(channelsInteractionListener);
    }

    private final ChannelsDisplayer.ChannelsInteractionListener channelsInteractionListener = new ChannelsDisplayer.ChannelsInteractionListener() {
        @Override
        public void onChannelSelected(Channel channel) {
            analytics.trackSelectChannel(channel.getName());
            navigator.toChannel(channel);
        }

        @Override
        public void onAddNewChannel() {
            analytics.trackCreateChannel(user.getUid());
            navigator.toCreateChannel();
        }

        @Override
        public void onInviteUsersClicked() {
            analytics.trackSendInvitesSelected(user.getUid());
            //navigator.toShareInvite(linkFactory.inviteLinkFrom(user.getUid()).toString());
        }

        @Override
        public void onChannelLongPressed(Channel channel) {
            // navigator.openDeleteChannelDialog(user.getUid(), channel);
        }
    };
}
