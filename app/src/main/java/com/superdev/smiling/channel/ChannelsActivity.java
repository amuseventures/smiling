package com.superdev.smiling.channel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import com.superdev.smiling.BaseActivity;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.channel.presenter.ChannelsPresenter;
import com.superdev.smiling.navigation.AndroidChannelNavigator;
import com.superdev.smiling.navigation.AndroidNavigator;


public class ChannelsActivity extends BaseActivity {

    private ChannelsPresenter channelsPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channels);
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.groups_title);
        }
        AndroidChannelNavigator channelNavigator = new AndroidChannelNavigator(this, new AndroidNavigator(this));
        channelsPresenter = new ChannelsPresenter(
                findViewById(R.id.channels_view),
                Dependencies.INSTANCE.getChannelService(),
                Dependencies.INSTANCE.getPhoneLoginService(),
                Dependencies.INSTANCE.getConfig(),
                channelNavigator,
                Dependencies.INSTANCE.getAnalytics()
        );
    }

    @Override
    protected void onStart() {
        super.onStart();
        channelsPresenter.startPresenting();
    }

    @Override
    protected void onStop() {
        channelsPresenter.stopPresenting();
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
