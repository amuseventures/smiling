package com.superdev.smiling.channel.presenter;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.google.android.gms.gcm.GcmPubSub;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.MimeTypeEnum;
import com.superdev.smiling.analytics.ErrorLogger;
import com.superdev.smiling.channel.displayer.NewChannelDisplayer;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.channel.service.ChannelService;
import com.superdev.smiling.conversation.data_model.AudioModel;
import com.superdev.smiling.conversation.data_model.ContactModel;
import com.superdev.smiling.conversation.data_model.DocModel;
import com.superdev.smiling.conversation.data_model.MapModel;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.navigation.AndroidChannelNavigator;
import com.superdev.smiling.navigation.ChannelNavigator;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.rx.FCM;
import com.superdev.smiling.storage.StorageService;
import com.superdev.smiling.user.data_model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Action1;

public class NewChannelPresenter {

    private final NewChannelDisplayer newChannelDisplayer;
    private final ChannelService channelService;
    private final PhoneLoginService loginService;
    private final AndroidChannelNavigator navigator;
    private final ErrorLogger errorLogger;
    private final StorageService storageService;
    private User user;
    private Subscription subscription;
    private ArrayList<User> selectedUsers;
    private String imageName;

    public NewChannelPresenter(NewChannelDisplayer newChannelDisplayer,
                               ChannelService channelService,
                               PhoneLoginService loginService,
                               AndroidChannelNavigator navigator,
                               ErrorLogger errorLogger,
                               ArrayList<User> selectedUsers,
                               StorageService storageService) {
        this.newChannelDisplayer = newChannelDisplayer;
        this.channelService = channelService;
        this.loginService = loginService;
        this.navigator = navigator;
        this.errorLogger = errorLogger;
        this.selectedUsers = selectedUsers;
        this.storageService = storageService;

    }

    public void startPresenting() {
        navigator.attach(imageSelectedListener);
        newChannelDisplayer.attach(channelCreationListener);
        newChannelDisplayer.display(selectedUsers);
        subscription = loginService.getAuthentication().subscribe(new Observer<Authentication>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Authentication authentication) {
                Dependencies.INSTANCE.getUserService().getUser(authentication.getUser().getUid())
                        .subscribe(new Action1<DatabaseResult<User>>() {
                            @Override
                            public void call(DatabaseResult<User> userDatabaseResult) {
                                if (userDatabaseResult.isSuccess()) {
                                    user = userDatabaseResult.getData();
                                }
                            }
                        });
            }
        });

    }

    public void stopPresenting() {
        newChannelDisplayer.detach(channelCreationListener);
        navigator.detach(imageSelectedListener);
        subscription.unsubscribe();
    }

    private AndroidChannelNavigator.OnImageSelectedListener imageSelectedListener = new ChannelNavigator.OnImageSelectedListener() {
        @Override
        public void onImageSelected(Uri photoUri, String message) {

        }

        @Override
        public void onMapSelected(MapModel message) {

        }

        @Override
        public void onDocSelected(DocModel docModel) {

        }

        @Override
        public void onAudioSelected(AudioModel audioModel) {

        }

        @Override
        public void onContactSelected(ContactModel contactModel) {

        }

        @Override
        public void onCabFinished() {

        }

        @Override
        public void onMessageDeleted(boolean deleteMedia, GroupMessage message) {

        }

        @Override
        public void onImageSelected(byte[] imageBytes, Bitmap bitmap) {
            newChannelDisplayer.updateProfileImage(bitmap);
            newChannelDisplayer.showProgressBar();
            storageService.uploadImage(imageBytes)
                    .subscribe(new Action1<String>() {
                        @Override
                        public void call(String image) {
                            if (image != null) {
                                imageName = image;
                                newChannelDisplayer.hideProgressBar();
                            }
                        }
                    });
        }

        @Override
        public void onProfileRemove() {
        }

        @Override
        public void onUserLogin() {

        }

        @Override
        public void onUserLoginError() {

        }
    };
    private NewChannelDisplayer.ChannelCreationListener channelCreationListener = new NewChannelDisplayer.ChannelCreationListener() {

        @Override
        public void onCreateChannelClicked(String channelName, List<User> selectedMembers) {
            String channelId = (user.getUid() + System.currentTimeMillis()).hashCode() + "";
            String image = !TextUtils.isEmpty(imageName) ? imageName : "";
            Channel newChannel = new Channel(channelId, channelName.trim(), user.getUid(), image, new Date().getTime());
            subscription = create(newChannel, selectedMembers).subscribe(new Observer<DatabaseResult<Channel>>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(DatabaseResult<Channel> databaseResult) {
                    if (databaseResult.isSuccess()) {
                        new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected Void doInBackground(Void... voids) {
                                navigator.subscribeToChannel(newChannel.getUid());
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                newChannelDisplayer.hideProgressDialog();
                                navigator.toChannelWithClearedHistory(newChannel);
                                //navigator.startService();
                            }
                        }.execute();

                    } else {
                        errorLogger.reportError(databaseResult.getFailure(), "Channel creation failed");
                        newChannelDisplayer.showChannelCreationError();
                    }
                }
            });
        }

        @Override
        public void onCancel() {
            navigator.toParent();
        }

        @Override
        public void onChannelImageClicked() {
            navigator.toSelectChannelImage();
        }
    };

    private Observable<DatabaseResult<Channel>> create(Channel newChannel, List<User> userList) {
        newChannelDisplayer.showProgressDialog();
        for (User member : userList) {
            channelService.addOwnerToPrivateChannel(newChannel, member)
                    .subscribe(userDatabaseResult -> {
                        if (!userDatabaseResult.isSuccess()) {
                            errorLogger.reportError(userDatabaseResult.getFailure(), "Cannot update channel owners");
                            //newChannelDisplayer.showFailure();
                        } else {
                            new AsyncTask<Void, Void, Void>() {

                                @Override
                                protected Void doInBackground(Void... voids) {
                                    GroupMessage group = new GroupMessage(user, newChannel, String.format("%1s added you", user.getName()),
                                            MimeTypeEnum.MEMBER.getValue(), com.superdev.smiling.conversation.data_model.Status.SEEN.getValue());
                                    FCM.sendFCMNotification(member.getFcm(), newChannel.getName(), String.format("%1s added you in %2s group", user.getName(), newChannel.getName()), group);
                                    return null;
                                }
                            }.execute();

                        }
                    });
        }
        return channelService.createPrivateChannel(newChannel, user);
    }
}
