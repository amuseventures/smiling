package com.superdev.smiling.channel.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.superdev.smiling.channel.model.Channel;


class ChannelViewHolder extends RecyclerView.ViewHolder {

    private final ChannelView channelView;

    public ChannelViewHolder(ChannelView itemView) {
        super(itemView);
        this.channelView = itemView;
    }

    public void bind(final Channel channel, final ChannelSelectionListener listener) {
        channelView.display(channel);
        channelView.setOnClickListener(v -> listener.onChannelSelected(channel));
        channelView.setOnLongClickListener(view -> {
            listener.onChannelLongPressed(channel);
            return false;
        });
    }

    public interface ChannelSelectionListener {
        void onChannelSelected(Channel channel);
        void onChannelLongPressed(Channel channel);
    }
}
