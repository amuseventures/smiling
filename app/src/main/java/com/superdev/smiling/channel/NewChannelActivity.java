package com.superdev.smiling.channel;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.superdev.smiling.BaseActivity;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.channel.presenter.NewChannelPresenter;
import com.superdev.smiling.navigation.AndroidChannelNavigator;
import com.superdev.smiling.navigation.AndroidNavigator;


public class NewChannelActivity extends BaseActivity {

    private NewChannelPresenter newChannelPresenter;
    private AndroidChannelNavigator channelNavigator;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_channel);
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        channelNavigator = new AndroidChannelNavigator(this, new AndroidNavigator(this));
        newChannelPresenter = new NewChannelPresenter(findViewById(R.id.create_channel_view),
                Dependencies.INSTANCE.getChannelService(),
                Dependencies.INSTANCE.getPhoneLoginService(),
                channelNavigator,
                Dependencies.INSTANCE.getErrorLogger()
                , getIntent().getParcelableArrayListExtra(Constants.FIREBASE_SELECTED_MEMBERS),
                Dependencies.INSTANCE.getStorageService()
        );
    }

    @Override
    protected void onStart() {
        super.onStart();
        newChannelPresenter.startPresenting();
    }

    @Override
    protected void onStop() {
        newChannelPresenter.stopPresenting();
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!channelNavigator.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
