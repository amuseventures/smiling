package com.superdev.smiling.channel.service;


import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.channel.model.Channels;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;

import rx.Observable;


public interface ChannelService {

    Observable<DatabaseResult<Channels>> getChannelsFor(User user);

    Observable<Channel> getChannelById(String channelId);

    Observable<DatabaseResult<Channel>> createPrivateChannel(Channel newChannel, User owner);

    Observable<DatabaseResult<User>> addOwnerToPrivateChannel(Channel channel, User newOwner);

    Observable<DatabaseResult<User>> removeOwnerFromPrivateChannel(Channel channel, User removedOwner);

    Observable<DatabaseResult<String>> removeOwnerFromPrivateChannel(Channel channel, String removedOwner);

    Observable<DatabaseResult<Users>> getOwnersOfChannel(Channel channel);

    void subscribeToChannel(Channel channel, User user);
}
