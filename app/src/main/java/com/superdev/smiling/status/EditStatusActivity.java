package com.superdev.smiling.status;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.superdev.smiling.Constants;
import com.superdev.smiling.R;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

public class EditStatusActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.edit_status)
    EmojiconEditText editStatus;

    @BindView(R.id.tv_counter)
    TextView tvCounter;

    @BindView(R.id.user_layout)
    RelativeLayout userLayout;

    @BindView(R.id.iv_smiley)
    ImageView ivSmiley;

    @BindView(R.id.currentRoot)
    RelativeLayout currentRoot;

    private FirebaseAuth firebaseAuth;

    private FirebaseUser firebaseUser;

    private DatabaseReference databaseReference;

    private String status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_status);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.add_status);
        }
        status = getIntent().getStringExtra(Constants.FIREBASE_USER_STATUS);
        editStatus.setText(status);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        EmojIconActions emojIcon = new EmojIconActions(this, currentRoot, editStatus, ivSmiley);
        emojIcon.ShowEmojIcon();

        editStatus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (s.length() > 0) {
                    tvCounter.setVisibility(View.VISIBLE);
                    tvCounter.setText(String.format(Locale.getDefault(), "%d", 140 - s.toString().length()));
                } else {
                    tvCounter.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.cancel_btn, R.id.btn_ok})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancel_btn:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.btn_ok:
                if (firebaseUser != null) {
                    final Map<String, Object> values = new HashMap<>();
                    values.put(Constants.FIREBASE_USERS_STATUS, editStatus.getText().toString());
                    databaseReference.child(Constants.FIREBASE_USERS)
                            .child(firebaseUser.getUid()).updateChildren(values);
                    Intent intent = new Intent();
                    intent.putExtra(Constants.FIREBASE_USER_STATUS, editStatus.getText().toString());
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
                }
        }
    }
}