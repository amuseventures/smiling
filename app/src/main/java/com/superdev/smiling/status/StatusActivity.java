package com.superdev.smiling.status;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.superdev.smiling.Constants;
import com.superdev.smiling.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

public class StatusActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.status_info)
    TextView statusInfo;

    @BindView(R.id.status_tv)
    EmojiconTextView statusTv;

    @BindView(R.id.progressbar_small)
    ProgressBar progressbarSmall;

    @BindView(R.id.round_more_btn)
    ImageView roundMoreBtn;

    @BindView(R.id.status_layout)
    LinearLayout statusLayout;

    private String status;

    public static final int REQUEST_EDIT_STATUS = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.about_title);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        status = getIntent().getStringExtra(Constants.FIREBASE_USER_STATUS);
        statusTv.setText(status);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.status_layout)
    public void onViewClicked() {
        Intent intent = new Intent(StatusActivity.this, EditStatusActivity.class);
        intent.putExtra(Constants.FIREBASE_USER_STATUS, status);
        startActivityForResult(intent, REQUEST_EDIT_STATUS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_EDIT_STATUS)
            switch (resultCode) {
                case RESULT_OK:
                    status = data.getStringExtra(Constants.FIREBASE_USER_STATUS);
                    statusTv.setText(status);
                    break;
                case RESULT_CANCELED:
                    break;
            }
    }
}
