package com.superdev.smiling.conversation.data_model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by gautam on 8/17/17.
 */

public class MapModel implements Parcelable {

    private String latitude;
    private String longitude;
    private String address;
    private String placeId;

    public MapModel() {
    }

    public MapModel(String latitude, String longitude, String address, String placeId) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        this.placeId = placeId;
    }

    public MapModel(Parcel in) {
        latitude = in.readString();
        longitude = in.readString();
        address = in.readString();
        placeId = in.readString();
    }

    public static final Creator<MapModel> CREATOR = new Creator<MapModel>() {
        @Override
        public MapModel createFromParcel(Parcel in) {
            return new MapModel(in);
        }

        @Override
        public MapModel[] newArray(int size) {
            return new MapModel[size];
        }
    };

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(latitude);
        parcel.writeString(longitude);
        parcel.writeString(address);
        parcel.writeString(placeId);
    }
}
