package com.superdev.smiling.conversation.data_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

/**
 * Created by gautam on 9/8/17.
 */

public class AudioModel implements Parcelable {

    private String audioName;
    private String audioUrl;
    private long audioSize;
    private String mimeType;
    private int isUploaded;

    @Exclude
    private String audioLocalUrl;

    public AudioModel() {

    }

    public AudioModel(String audioName, String audioUrl, long audioSize, String mimeType, int isUploaded, String audioLocalUrl) {
        this.audioName = audioName;
        this.audioUrl = audioUrl;
        this.audioSize = audioSize;
        this.mimeType = mimeType;
        this.isUploaded = isUploaded;
        this.audioLocalUrl = audioLocalUrl;
    }

    protected AudioModel(Parcel in) {
        audioName = in.readString();
        audioUrl = in.readString();
        audioSize = in.readLong();
        mimeType = in.readString();
        isUploaded = in.readInt();
        audioLocalUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(audioName);
        dest.writeString(audioUrl);
        dest.writeLong(audioSize);
        dest.writeString(mimeType);
        dest.writeInt(isUploaded);
        dest.writeString(audioLocalUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AudioModel> CREATOR = new Creator<AudioModel>() {
        @Override
        public AudioModel createFromParcel(Parcel in) {
            return new AudioModel(in);
        }

        @Override
        public AudioModel[] newArray(int size) {
            return new AudioModel[size];
        }
    };

    public String getAudioName() {
        return audioName;
    }

    public void setAudioName(String audioName) {
        this.audioName = audioName;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public long getAudioSize() {
        return audioSize;
    }

    public void setAudioSize(long audioSize) {
        this.audioSize = audioSize;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getAudioLocalUrl() {
        return audioLocalUrl;
    }

    public void setAudioLocalUrl(String audioLocalUrl) {
        this.audioLocalUrl = audioLocalUrl;
    }

    public void setIsUploaded(int isUploaded) {
        this.isUploaded = isUploaded;
    }

    public int getIsUploaded() {
        return isUploaded;
    }
}
