package com.superdev.smiling.conversation;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.superdev.smiling.BaseActivity;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.common.SquaredImageView;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.service.UserService;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;

public class TextAndImageSendActivity extends BaseActivity {

    @BindView(R.id.profileImageView)
    CircleImageView profileImageView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.main)
    SquaredImageView main;

    @BindView(R.id.iv_smiley)
    ImageView ivSmiley;

    @BindView(R.id.messageEditText)
    EmojiconEditText messageEditText;

    @BindView(R.id.compose_layout)
    RelativeLayout composeLayout;

    @BindView(R.id.sendButton)
    FloatingActionButton sendButton;

    @BindView(R.id.conversation)
    CoordinatorLayout conversation;

    private UserService userService;


    private String destination, channelId;
    private String path;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_and_image_send);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        userService = Dependencies.INSTANCE.getUserService();
        Intent intent = getIntent();

        path = intent.getStringExtra("image");
        if (intent.hasExtra("destination")) {
            destination = intent.getStringExtra("destination");
        }
        if (intent.hasExtra("channelId")) {
            channelId = intent.getStringExtra("channelId");
        }
        EmojIconActions emojIcon = new EmojIconActions(TextAndImageSendActivity.this, conversation, messageEditText, ivSmiley);
        emojIcon.ShowEmojIcon();
        File file = new File(path);

        Glide.with(this)
                .load(Uri.fromFile(file))
                .into(main);


        Subscriber conversationSubscriber = new Subscriber<User>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(final User user) {
                Utils.loadImageElseWhite(user.getImage(), profileImageView, TextAndImageSendActivity.this,R.drawable.avatar_contact_large);
            }
        };
        Subscription subscription;
        if (!TextUtils.isEmpty(destination)) {
            subscription = userService.getUser(destination)
                    .subscribe(conversationSubscriber);
        }

        if (!TextUtils.isEmpty(channelId)) {
            subscription = Dependencies.INSTANCE.getChannelService().getChannelById(channelId)
                    .subscribe(new Observer<Channel>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(Channel channel) {
                            Utils.loadImageElseWhite(channel.getImage(), profileImageView, TextAndImageSendActivity.this,R.drawable.avatar_contact_large);
                        }
                    });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
    }

    @OnClick(R.id.sendButton)
    public void onViewClicked() {
        Intent intent = new Intent();
        intent.putExtra("FILE_PATH", path);
        intent.putExtra("message", messageEditText.getText().toString());
        setResult(RESULT_OK, intent);
        onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
