package com.superdev.smiling.conversation;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.superdev.smiling.BaseActivity;
import com.superdev.smiling.BuildConfig;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.conversation.presenter.ConversationPresenter;
import com.superdev.smiling.conversation.view.ConversationDisplayer;
import com.superdev.smiling.conversation.view.ConversationView;
import com.superdev.smiling.link.FirebaseDynamicLinkFactory;
import com.superdev.smiling.navigation.AndroidConversationViewNavigator;
import com.superdev.smiling.profile.ProfileActivity;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by marco on 29/07/16.
 */

public class ConversationActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {

    private ConversationPresenter presenter;

    private ConversationDisplayer conversationDisplayer;

    private AndroidConversationViewNavigator navigator;

    public static boolean isInForeground;

    private String destination;

    private String sender;

    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        FirebaseDynamicLinkFactory firebaseDynamicLinkFactory = new FirebaseDynamicLinkFactory(
                getResources().getString(R.string.dynamic_link_url),
                getResources().getString(R.string.deepLinkBaseUrl),
                BuildConfig.APPLICATION_ID
        );

        conversationDisplayer = (ConversationView) findViewById(R.id.conversationView);
        navigator = new AndroidConversationViewNavigator(this, getIntent().getStringExtra(Constants.SENDER_ID), getIntent().getStringExtra("destination"));
        GoogleApiClient mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, this)
                .build();
        destination = getIntent().getStringExtra(Constants.DESTINATION_ID);
        sender = getIntent().getStringExtra(Constants.SENDER_ID);
        presenter = new ConversationPresenter(
                firebaseDynamicLinkFactory,
                Dependencies.INSTANCE.getConversationService(),
                conversationDisplayer,
                Dependencies.INSTANCE.getUserService(),
                sender,
                destination,
                navigator,
                mGoogleApiClient
        );

        toolbar.setOnClickListener(view -> {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra(Constants.DESTINATION_ID, destination);
            intent.putExtra(Constants.SENDER_ID, sender);
            CircleImageView circleImageView = view.findViewById(R.id.profileImageView);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                startActivity(intent);
            } else {
                ActivityOptions options = ActivityOptions
                        .makeSceneTransitionAnimation(this, circleImageView, getString(R.string.transition_photo));
                // start the new activity
                startActivity(intent, options.toBundle());
            }
        });
        presenter.startPresenting();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        isInForeground = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isInForeground = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.stopPresenting();
        presenter.detach();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        conversationDisplayer.inflateMenu(getMenuInflater(), menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            navigator.toMainActivity();
        } else if (item.getItemId() == R.id.action_attach) {
            navigator.showBottomSheet();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!navigator.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
