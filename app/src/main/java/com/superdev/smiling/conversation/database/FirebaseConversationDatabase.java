package com.superdev.smiling.conversation.database;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.superdev.smiling.Constants;
import com.superdev.smiling.MimeTypeEnum;
import com.superdev.smiling.conversation.data_model.Chat;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.rx.FirebaseObservableListeners;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by marco on 29/07/16.
 */

public class FirebaseConversationDatabase implements ConversationDatabase {

    private static final int DEFAULT_LIMIT = 1000;

    private final DatabaseReference userChat;
    private final DatabaseReference notificationRef;
    private final FirebaseObservableListeners firebaseObservableListeners;

    public FirebaseConversationDatabase(FirebaseDatabase firebaseDatabase, FirebaseObservableListeners firebaseObservableListeners) {
        userChat = firebaseDatabase.getReference(Constants.FIREBASE_CHAT);
        notificationRef = firebaseDatabase.getReference(Constants.FIREBASE_NOTIFICATION).child(Constants.FIREBASE_CHAT);
        this.firebaseObservableListeners = firebaseObservableListeners;
    }

    private DatabaseReference messagesOfUser(String self, String destination) {
        return userChat.child(self).child(destination).child(Constants.FIREBASE_CHAT_MESSAGES);
    }

    private DatabaseReference messageOfUser(String self, String destination, String messageKey) {
        return userChat.child(self).child(destination).child(Constants.FIREBASE_CHAT_MESSAGES).child(messageKey).child(Constants.FIREBASE_MESSAGE_STATUS);
    }

    @Override
    public Observable<Message> observeAddMessage(String self, String destination) {
        return firebaseObservableListeners.listenToAddChildEvents(messagesOfUser(self, destination), toMessage());
    }

    @Override
    public Observable<Message> observeLastMessage(String self, String destination) {
        return firebaseObservableListeners.listenToSingleValueEvents(messagesOfUser(self, destination).limitToLast(1), toLastMessage());
    }

    @Override
    public Observable<Chat> observeChat(String self, String destination) {
        return firebaseObservableListeners.listenToValueEvents(messagesOfUser(self, destination).limitToLast(DEFAULT_LIMIT), toChat());
    }

    @Override
    public Observable<Chat> observeMedia(String self, String destination) {
        DatabaseReference databaseReference = messagesOfUser(self, destination);
        return firebaseObservableListeners.listenToValueEvents(databaseReference.orderByChild("msgType").equalTo(MimeTypeEnum.IMAGE.getValue()).limitToLast(20), toChat());
    }

    @Override
    public Observable<Boolean> removeChat(String self, String destination, String key) {
        return firebaseObservableListeners.removeValue(messagesOfUser(self, destination).child(key), true);
    }

    private Func1<DataSnapshot, Chat> toChat() {
        return dataSnapshot -> {
            Iterable<DataSnapshot> children = dataSnapshot.getChildren();
            List<Message> messages = new ArrayList<>();
            for (DataSnapshot child : children) {
                Message message = child.getValue(Message.class);
                messages.add(message);
            }
            return new Chat(messages);//.sortedByDate();
        };
    }

    private Func1<DataSnapshot, Message> toLastMessage() {
        return dataSnapshot -> {
            Iterable<DataSnapshot> children = dataSnapshot.getChildren();
            List<Message> messages = new ArrayList<>();
            for (DataSnapshot child : children) {
                Message message = child.getValue(Message.class);
                messages.add(message);
            }
            return messages.get(0);
        };
    }

    private Func1<DataSnapshot, Message> toMessage() {
        return dataSnapshot -> dataSnapshot.getValue(Message.class);
    }

    private Func1<DataSnapshot, String> asString() {
        return dataSnapshot -> dataSnapshot.getValue(String.class);
    }

    @Override
    public void sendMessage(final String user, final Message message) {
        String senderMsgKey = userChat.child(user).child(message.getDestination()).child(Constants.FIREBASE_CHAT_MESSAGES).push().getKey();
        message.setMessageKey(senderMsgKey);
        userChat.child(user).child(message.getDestination()).child(Constants.FIREBASE_CHAT_MESSAGES).child(senderMsgKey).setValue(message);
        userChat.child(message.getDestination()).child(user).child(Constants.FIREBASE_CHAT_MESSAGES).child(senderMsgKey).setValue(message);
        notificationRef.push().setValue(message);
    }

    @Override
    public Observable<Boolean> observeTyping(String self, String destination) {
        return firebaseObservableListeners.listenToValueEvents(userChat.child(destination).child(self).child(Constants.FIREBASE_CHAT_TYPING), asBoolean());
    }

    @Override
    public Observable<String> observeUpdateMessage(String self, String destination, String messageKey) {
        return firebaseObservableListeners.listenToValueEvents(messageOfUser(self, destination, messageKey), asString());
    }

    @Override
    public void setTyping(String self, String destination, Boolean value) {
        userChat.child(self).child(destination).child(Constants.FIREBASE_CHAT_TYPING).setValue(value);
    }

    @Override
    public void updateMessage(Message message) {
        userChat.child(message.getSender()).child(message.getDestination()).child(Constants.FIREBASE_CHAT_MESSAGES).child(message.getMessageKey()).setValue(message);
    }

    @Override
    public Observable<Chat> observeAllMedia(String self, String destination) {
        DatabaseReference databaseReference = messagesOfUser(self, destination);
        return firebaseObservableListeners.listenToValueEvents(databaseReference.orderByChild("msgType").equalTo(MimeTypeEnum.IMAGE.getValue()).limitToLast(DEFAULT_LIMIT), toChat());
    }

    @Override
    public Observable<Chat> observeAllDocuments(String self, String destination) {
        DatabaseReference databaseReference = messagesOfUser(self, destination);
        return firebaseObservableListeners.listenToValueEvents(databaseReference.orderByChild("msgType").equalTo(MimeTypeEnum.DOCUMENT.getValue()).limitToLast(DEFAULT_LIMIT), toChat());
    }

    private Func1<DataSnapshot, Boolean> asBoolean() {
        return dataSnapshot -> dataSnapshot.getValue(Boolean.class);
    }
}
