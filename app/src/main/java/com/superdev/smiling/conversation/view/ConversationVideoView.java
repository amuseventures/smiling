package com.superdev.smiling.conversation.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.InflateException;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.common.SquaredImageView;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.conversation.data_model.Status;
import com.superdev.smiling.conversation.service.ConversationService;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.storage.StorageService;

import butterknife.BindView;
import butterknife.ButterKnife;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by gautam on 9/11/17.
 */

public class ConversationVideoView extends FrameLayout {

    @Nullable
    @BindView(R.id.name_in_group_tv)
    EmojiconTextView nameInGroupTv;

    @Nullable
    @BindView(R.id.pushname_in_group_tv)
    EmojiconTextView pushnameInGroupTv;

    @Nullable
    @BindView(R.id.name_in_group)
    LinearLayout nameInGroup;

    @Nullable
    @BindView(R.id.dateTextView)
    TextView dateTextView;

    @Nullable
    @BindView(R.id.quoted_message_holder)
    FrameLayout quotedMessageHolder;


    @BindView(R.id.thumb)
    SquaredImageView thumb;

    @BindView(R.id.info)
    TextView info;

    @BindView(R.id.progress_bar)
    CircularProgressView progressBar;

    @BindView(R.id.cancel_btn)
    ImageView cancelBtn;

    @BindView(R.id.control_btn)
    Button controlBtn;

    @BindView(R.id.control_frame)
    FrameLayout controlFrame;

    @BindView(R.id.invisible_press_surface)
    FrameLayout invisiblePressSurface;

    @BindView(R.id.play_button)
    ImageView playButton;

    @BindView(R.id.play_frame)
    FrameLayout playFrame;

    @BindView(R.id.date)
    TextView tvDate;

    @Nullable
    @BindView(R.id.status)
    ImageView status;

    @BindView(R.id.date_layout)
    FrameLayout dateLayout;

    @BindView(R.id.media_container)
    FrameLayout mediaContainer;

    @BindView(R.id.caption)
    TextView caption;

    @BindView(R.id.date_caption)
    TextView dateCaption;

    @BindView(R.id.date_wrapper)
    LinearLayout dateWrapper;

    @BindView(R.id.text_and_date)
    FrameLayout textAndDate;

    @BindView(R.id.main_layout)
    LinearLayout mainLayout;

    @BindView(R.id.main_frame)
    FrameLayout mainFrame;

    private int layoutResId;

    private StorageService storageService;

    private ConversationService conversationService;

    public ConversationVideoView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int[] attrsArray = {
                    android.R.attr.layout
            };
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            layoutResId = array.getResourceId(0, R.layout.merge_conversation_video_item_destination);
            array.recycle();
        }
        storageService = Dependencies.INSTANCE.getStorageService();
        conversationService = Dependencies.INSTANCE.getConversationService();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), layoutResId, this);
        try {
            ButterKnife.bind(this);
        } catch (InflateException e) {
            e.printStackTrace();
        }
    }

    public void display(final Message message) {
        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(Utils.getRealPathFromURI(getContext(), Uri.parse(message.getVideoModel().getLocalVideoUrl())), MediaStore.Video.Thumbnails.MINI_KIND);
        thumb.setImageBitmap(bitmap);
        if(message.getMessage() != null)
            caption.setText(message.getMessage());
        final String timestamp = message.getTimestamp();
        String time = Utils.getTime(message.getTimestamp());
        String date = Utils.getDate(message.getTimestamp());
        String today = Utils.getCurrentTimestamp();
        String yesterday = Utils.getYesterdayTimeStamp();

        String[] time1 = date.split("/");
        String[] time2 = today.split("/");
        String[] time3 = yesterday.split("/");
        if ((time1[2] + time1[1] + time1[0]).equals(time2[0] + time2[1] + time2[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_today));
        } else if ((time1[2] + time1[1] + time1[0]).equals(time3[0] + time3[1] + time3[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_yesterday));
        } else {
            if (dateTextView != null)
                dateTextView.setText(Utils.getDateString(timestamp));
        }
        tvDate.setText(time);
        setMessageStatus(message);

        if (!message.getSelf().equals(message.getDestination())) {
            mainLayout.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }
        if (!message.getSelf().equals(message.getDestination())) {
            message.setMessageStatus(Status.SEEN.getValue());
            setMessageStatus(message);
        }
    }

    public void display(Channel channel, final GroupMessage message, String self) {
        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(Utils.getRealPathFromURI(getContext(), Uri.parse(message.getVideoModel().getLocalVideoUrl())), MediaStore.Video.Thumbnails.MINI_KIND);
        thumb.setImageBitmap(bitmap);
        setMessageStatus(message);
        if(message.getMessage() != null)
            caption.setText(message.getMessage());
        final String timestamp = message.getTimestamp();
        String time = Utils.getTime(message.getTimestamp());
        String date = Utils.getDate(message.getTimestamp());
        String today = Utils.getCurrentTimestamp();
        String yesterday = Utils.getYesterdayTimeStamp();

        String[] time1 = date.split("/");
        String[] time2 = today.split("/");
        String[] time3 = yesterday.split("/");
        if ((time1[2] + time1[1] + time1[0]).equals(time2[0] + time2[1] + time2[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_today));
        } else if ((time1[2] + time1[1] + time1[0]).equals(time3[0] + time3[1] + time3[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_yesterday));
        } else {
            if (dateTextView != null)
                dateTextView.setText(Utils.getDateString(timestamp));
        }
        tvDate.setText(time);
        if ( ! self.equals(message.getAuthor().getUid())) {
            if (nameInGroup != null) {
                nameInGroup.setVisibility(VISIBLE);
            }
            if (!TextUtils.isEmpty(message.getAuthor().getShortName())) {
                if (nameInGroupTv != null) {
                    nameInGroupTv.setText(message.getAuthor().getShortName());
                }
            } else {
                if (nameInGroupTv != null) {
                    nameInGroupTv.setText(message.getAuthor().getPhone());
                }
            }
        }
    }

    private void setMessageStatus(Message message) {
        if (status != null) {
            if (Status.SEEN.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_read));
            } else if (Status.DELIVERED.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_received));
            } else if (Status.SENT.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_server_receive));
            } else {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_gray_waiting_2));
            }
        }
    }

    private void setMessageStatus(GroupMessage message) {
        if (status != null) {
            if (Status.SEEN.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_read));
            } else if (Status.DELIVERED.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_received));
            } else if (Status.SENT.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_server_receive));
            } else {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_gray_waiting_2));
            }
        }
    }

    public FrameLayout getMainFrame() {
        return mainFrame;
    }

}
