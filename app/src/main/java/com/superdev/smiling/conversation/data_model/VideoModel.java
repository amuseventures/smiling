package com.superdev.smiling.conversation.data_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

/**
 * Created by gautam on 9/11/17.
 */

public class VideoModel implements Parcelable {

    private String videoType;

    private String videoUrl;

    private String videoName;

    private long videoSize;

    @Exclude
    private String localVideoUrl;

    private int isUploaded;

    public VideoModel(String videoType, String videoUrl, String videoName, long videoSize, String localVideoUrl, int isUploaded) {
        this.videoType = videoType;
        this.videoUrl = videoUrl;
        this.videoName = videoName;
        this.videoSize = videoSize;
        this.localVideoUrl = localVideoUrl;
        this.isUploaded = isUploaded;
    }

    public VideoModel(Parcel in) {
        videoType = in.readString();
        videoUrl = in.readString();
        videoName = in.readString();
        videoSize = in.readLong();
        localVideoUrl = in.readString();
        isUploaded = in.readInt();
    }

    public static final Creator<VideoModel> CREATOR = new Creator<VideoModel>() {
        @Override
        public VideoModel createFromParcel(Parcel in) {
            return new VideoModel(in);
        }

        @Override
        public VideoModel[] newArray(int size) {
            return new VideoModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(videoType);
        parcel.writeString(videoUrl);
        parcel.writeString(videoName);
        parcel.writeLong(videoSize);
        parcel.writeString(localVideoUrl);
        parcel.writeInt(isUploaded);
    }

    public String getVideoType() {
        return videoType;
    }

    public void setVideoType(String videoType) {
        this.videoType = videoType;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public long getVideoSize() {
        return videoSize;
    }

    public void setVideoSize(long videoSize) {
        this.videoSize = videoSize;
    }

    public String getLocalVideoUrl() {
        return localVideoUrl;
    }

    public void setLocalVideoUrl(String localVideoUrl) {
        this.localVideoUrl = localVideoUrl;
    }

    public int getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(int isUploaded) {
        this.isUploaded = isUploaded;
    }
}
