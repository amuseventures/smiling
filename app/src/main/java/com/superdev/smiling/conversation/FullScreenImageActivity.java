package com.superdev.smiling.conversation;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.bumptech.glide.Glide;
import com.superdev.smiling.BaseActivity;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.common.NormalImageView;
import com.superdev.smiling.common.SquaredImageView;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.service.UserService;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import rx.Subscriber;
import rx.Subscription;

public class FullScreenImageActivity extends BaseActivity {

    @BindView(R.id.profileImageView)
    CircleImageView profileImageView;

    @BindView(R.id.nameTextView)
    EmojiconTextView nameTextView;

    @BindView(R.id.lastSeenTextView)
    EmojiconTextView lastSeenTextView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.image_view)
    NormalImageView imageView;

    private String id;

    private Message message;

    private GroupMessage groupMessage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        UserService userService = Dependencies.INSTANCE.getUserService();

        String self;

        if (getIntent().hasExtra(Constants.MESSAGE_EXTRA)) {
            message = getIntent().getParcelableExtra(Constants.MESSAGE_EXTRA);
        }
        if (getIntent().hasExtra(Constants.GROUP_MESSAGE_EXTRA)) {
            groupMessage = getIntent().getParcelableExtra(Constants.GROUP_MESSAGE_EXTRA);
        }
        if (message != null) {
            self = message.getSelf();
        } else {
            self = getIntent().getStringExtra(Constants.SENDER_ID);
        }

        File file = null;
        if (message != null) {
            if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
                file = new File(Utils.getImagePath() + "/" + message.getFileModel().getFileName());
            } else {
                file = new File(Utils.getImageSentPath() + "/" + message.getFileModel().getFileName());
            }
        } else if (groupMessage != null) {
            if (!self.equalsIgnoreCase(groupMessage.getAuthor().getUid())) {
                file = new File(Utils.getImagePath() + "/" + groupMessage.getFileModel().getFileName());
            } else {
                file = new File(Utils.getImageSentPath() + "/" + groupMessage.getFileModel().getFileName());
            }
        }

        Glide.with(this)
                .load(Uri.fromFile(file))
                .into(imageView);

        Subscriber conversationSubscriber = new Subscriber<User>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(final User user) {
                Utils.loadImageElseWhite(user.getImage(), profileImageView, FullScreenImageActivity.this,R.drawable.avatar_contact_large);
                nameTextView.setText(user.getName());
                if (message != null) {
                    lastSeenTextView.setText(Utils.getMediaDateTime(message.getTimestamp()));
                } else {
                    lastSeenTextView.setText(Utils.getMediaDateTime(groupMessage.getTimestamp()));
                }

            }
        };

        if (message != null) {
            id = message.getSelf();
        } else if (groupMessage != null) {
            id = groupMessage.getAuthor().getUid();
        }
        Subscription subscription = userService.getUser(id)
                .subscribe(conversationSubscriber);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        supportFinishAfterTransition();
    }
}
