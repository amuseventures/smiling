package com.superdev.smiling.conversation.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.DocModel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.conversation.data_model.Status;
import com.superdev.smiling.conversation.service.ConversationService;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.groupconversation.service.GroupConversationService;
import com.superdev.smiling.storage.StorageService;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by gautam on 9/5/17.
 */

public class ConversationDocView extends FrameLayout {

    @Nullable
    @BindView(R.id.dateTextView)
    TextView dateTextView;

    @Nullable
    @BindView(R.id.name_in_group_tv)
    EmojiconTextView nameInGroupTv;

    @Nullable
    @BindView(R.id.pushname_in_group_tv)
    EmojiconTextView pushnameInGroupTv;

    @Nullable
    @BindView(R.id.name_in_group)
    LinearLayout nameInGroup;

    @Nullable
    @BindView(R.id.quoted_message_holder)
    FrameLayout quotedMessageHolder;

    @BindView(R.id.preview)
    ImageView preview;

    @BindView(R.id.preview_separator)
    View previewSeparator;

    @BindView(R.id.icon)
    ImageView icon;

    @BindView(R.id.title)
    EmojiconTextView title;

    @BindView(R.id.control_btn)
    ImageButton controlBtn;

    @BindView(R.id.progressbar)
    CircularProgressView progressbar;

    @BindView(R.id.control_btn_holder)
    FrameLayout controlBtnHolder;

    @BindView(R.id.info)
    TextView info;

    @BindView(R.id.bullet_info)
    TextView bulletInfo;

    @BindView(R.id.file_size)
    TextView fileSize;

    @BindView(R.id.bullet_file_size)
    TextView bulletFileSize;

    @BindView(R.id.file_type)
    TextView fileType;

    @BindView(R.id.date_wrapper)
    LinearLayout dateWrapper;

    @BindView(R.id.main_layout)
    LinearLayout mainLayout;

    @BindView(R.id.main_frame)
    FrameLayout mainFrame;

    @BindView(R.id.date)
    TextView tvDate;

    @Nullable
    @BindView(R.id.status)
    ImageView status;

    private StorageService storageService;

    private ConversationService conversationService;

    private GroupConversationService groupConversationService;

    private int layoutResId;

    public ConversationDocView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int[] attrsArray = {
                    android.R.attr.layout
            };
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            layoutResId = array.getResourceId(0, R.layout.merge_conversation_doc_item_destination);
            array.recycle();
            storageService = Dependencies.INSTANCE.getStorageService();
            conversationService = Dependencies.INSTANCE.getConversationService();
            groupConversationService = Dependencies.INSTANCE.getGroupConversationService();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), layoutResId, this);
        ButterKnife.bind(this);
    }

    public void display(final Message message) {
        setUpTime(message);
        setMessageStatus(message);
        title.setText(message.getDocModel().getOriginalFileName());
        setUpDocPreview(message);
        if (message.getDocModel().getIsUploaded() == 0) {
            uploadFile(message);
        }

        File file;
        if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
            file = new File(Utils.getDocumentPath() + "/" + message.getDocModel().getDocName());
        } else {
            file = new File(Utils.getSentDocumentPath() + "/" + message.getDocModel().getDocName());
        }

        if (!file.exists() && message.getDocModel().getIsUploaded() == 1) {
            controlBtn.setImageResource(R.drawable.inline_audio_download);
        } else if (file.exists()) {
            controlBtn.setVisibility(GONE);
        }
        controlBtn.setOnClickListener(view -> {
            if (!file.exists() || message.getSelf().equalsIgnoreCase(message.getDestination())) {
                downloadFile(message);
            } else {
                uploadFile(message);
            }
        });

        if (!message.getSelf().equals(message.getDestination())) {
            mainLayout.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        if (message.getSelf().equals(message.getDestination())) {
            message.setMessageStatus(Status.SEEN.getValue());
            setMessageStatus(message);
        }
    }

    public void display(Channel channel, final GroupMessage message, String self) {
        setUpTime(message);
        setMessageStatus(message);
        title.setText(message.getDocModel().getOriginalFileName());
        setUpDocPreview(message, self);
        if (message.getDocModel().getIsUploaded() == 0) {
            uploadFile(channel, message, self);
        }

        controlBtn.setOnClickListener(view -> {
            if (!self.equalsIgnoreCase(message.getAuthor().getUid())) {
                downloadFile(message, self);
            } else {
                uploadFile(channel, message, self);
            }
        });

        if (self.equals(message.getAuthor().getUid())) {
            mainLayout.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        if (!self.equals(message.getAuthor().getUid())) {
            if (nameInGroup != null) {
                nameInGroup.setVisibility(VISIBLE);
            }
            if (!TextUtils.isEmpty(message.getAuthor().getShortName())) {
                if (nameInGroupTv != null) {
                    nameInGroupTv.setText(message.getAuthor().getShortName());
                }
            } else {
                if (nameInGroupTv != null) {
                    nameInGroupTv.setText(message.getAuthor().getPhone());
                }
            }
        }
    }

    private void setMessageStatus(Message message) {
        if (status != null) {
            if (Status.SEEN.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_read));
            } else if (Status.DELIVERED.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_received));
            } else if (Status.SENT.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_server_receive));
            } else {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_gray_waiting_2));
            }
        }
    }

    private void setMessageStatus(GroupMessage message) {
        if (status != null) {
            if (Status.SEEN.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_read));
            } else if (Status.DELIVERED.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_received));
            } else if (Status.SENT.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_server_receive));
            } else {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_gray_waiting_2));
            }
        }
    }

    private void setUpDocPreview(Message message) {
        switch (message.getDocModel().getDocType()) {
            case "PDF":
                generateImageFromPdf(message);
                icon.setImageBitmap(Utils.writeTextOnBitmap(getContext(), R.drawable.icon_file_pdf, "PDF"));
                break;
            case "WORD":
                icon.setImageResource(R.drawable.icon_file_doc);
                preview.setVisibility(GONE);
                break;
            case "EXCEL":
                preview.setVisibility(GONE);
                icon.setImageResource(R.drawable.icon_file_xls);
                readXLSFile(message);
                break;
            case "PPT":
                icon.setImageResource(R.drawable.icon_file_ppt);
                preview.setVisibility(GONE);
                break;
            case "TXT":
                icon.setImageResource(R.drawable.icon_file_doc);
                preview.setVisibility(GONE);
                break;
            case "UNKNOWN":
                String fileExtension = droidninja.filepicker.utils.Utils.getFileExtension(message.getDocModel().getOriginalFileName());
                icon.setImageBitmap(Utils.writeTextOnBitmap(getContext(), R.drawable.icon_file_unknown, fileExtension.toUpperCase()));
                preview.setVisibility(GONE);
                break;
        }

    }

    private void setUpDocPreview(GroupMessage message, String self) {
        switch (message.getDocModel().getDocType()) {
            case "PDF":
                generateImageFromPdf(message, self);
                icon.setImageBitmap(Utils.writeTextOnBitmap(getContext(), R.drawable.icon_file_pdf, "PDF"));
                break;
            case "WORD":
                icon.setImageResource(R.drawable.icon_file_doc);
                preview.setVisibility(GONE);
                break;
            case "EXCEL":
                preview.setVisibility(GONE);
                icon.setImageResource(R.drawable.icon_file_xls);
                readXLSFile(message, self);
                break;
            case "PPT":
                icon.setImageResource(R.drawable.icon_file_ppt);
                preview.setVisibility(GONE);
                break;
            case "TXT":
                icon.setImageResource(R.drawable.icon_file_doc);
                preview.setVisibility(GONE);
                break;
            case "UNKNOWN":
                String fileExtension = droidninja.filepicker.utils.Utils.getFileExtension(message.getDocModel().getOriginalFileName());
                icon.setImageBitmap(Utils.writeTextOnBitmap(getContext(), R.drawable.icon_file_unknown, fileExtension.toUpperCase()));
                preview.setVisibility(GONE);
                break;
        }
    }

    private void setUpTime(Message message) {
        final String timestamp = message.getTimestamp();
        String time = Utils.getTime(message.getTimestamp());
        String date = Utils.getDate(message.getTimestamp());
        String today = Utils.getCurrentTimestamp();
        String yesterday = Utils.getYesterdayTimeStamp();

        String[] time1 = date.split("/");
        String[] time2 = today.split("/");
        String[] time3 = yesterday.split("/");
        if ((time1[2] + time1[1] + time1[0]).equals(time2[0] + time2[1] + time2[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_today));
        } else if ((time1[2] + time1[1] + time1[0]).equals(time3[0] + time3[1] + time3[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_yesterday));
        } else {
            if (dateTextView != null)
                dateTextView.setText(Utils.getDateString(timestamp));
        }
        tvDate.setText(time);
    }

    private void setUpTime(GroupMessage message) {
        final String timestamp = message.getTimestamp();
        String time = Utils.getTime(message.getTimestamp());
        String date = Utils.getDate(message.getTimestamp());
        String today = Utils.getCurrentTimestamp();
        String yesterday = Utils.getYesterdayTimeStamp();

        String[] time1 = date.split("/");
        String[] time2 = today.split("/");
        String[] time3 = yesterday.split("/");
        if ((time1[2] + time1[1] + time1[0]).equals(time2[0] + time2[1] + time2[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_today));
        } else if ((time1[2] + time1[1] + time1[0]).equals(time3[0] + time3[1] + time3[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_yesterday));
        } else {
            if (dateTextView != null)
                dateTextView.setText(Utils.getDateString(timestamp));
        }
        tvDate.setText(time);
    }

    @SuppressLint("DefaultLocale")
    void generateImageFromPdf(Message message) {
        int pageNumber = 0;
        PdfiumCore pdfiumCore = new PdfiumCore(getContext());
        File file;
        if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
            file = new File(Utils.getDocumentPath() + "/" + message.getDocModel().getDocName());
        } else {
            file = new File(Utils.getSentDocumentPath() + "/" + message.getDocModel().getDocName());
        }
        if (file.exists()) {
            try {
                ParcelFileDescriptor fd = getContext().getContentResolver().openFileDescriptor(Uri.fromFile(file), "r");
                PdfDocument pdfDocument = pdfiumCore.newDocument(fd);
                pdfiumCore.openPage(pdfDocument, pageNumber);
                int width = pdfiumCore.getPageWidthPoint(pdfDocument, pageNumber);
                int height = pdfiumCore.getPageHeightPoint(pdfDocument, pageNumber);
                Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
                pdfiumCore.renderPageBitmap(pdfDocument, bmp, pageNumber, 0, 0, width, height);
                Glide.with(getContext()).load(bitmapToByte(bmp)).asBitmap().into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        preview.setImageBitmap(resource);
                        if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
                            message.setMessageStatus(Status.SEEN.getValue());
                            conversationService.updateMessage(message);
                            setMessageStatus(message);
                        }
                    }
                });
                preview.setVisibility(VISIBLE);
                bulletInfo.setVisibility(VISIBLE);
                controlBtn.setVisibility(GONE);
                info.setText(String.format("%d Pages", pdfiumCore.getPageCount(pdfDocument)));
                fileSize.setText(Utils.readableFileSize(message.getDocModel().getDocSize()));
                fileType.setText(message.getDocModel().getDocType());
                pdfiumCore.closeDocument(pdfDocument); // important!
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            preview.setVisibility(GONE);
            controlBtn.setVisibility(VISIBLE);
            bulletInfo.setVisibility(GONE);
            fileSize.setText(Utils.readableFileSize(message.getDocModel().getDocSize()));
            fileType.setText(message.getDocModel().getDocType());
        }
    }

    @SuppressLint("DefaultLocale")
    void generateImageFromPdf(GroupMessage message, String self) {
        int pageNumber = 0;
        PdfiumCore pdfiumCore = new PdfiumCore(getContext());
        File file;
        if (!self.equalsIgnoreCase(message.getAuthor().getUid())) {
            file = new File(Utils.getDocumentPath() + "/" + message.getDocModel().getDocName());
        } else {
            file = new File(Utils.getSentDocumentPath() + "/" + message.getDocModel().getDocName());
        }
        if (file.exists()) {
            try {
                ParcelFileDescriptor fd = getContext().getContentResolver().openFileDescriptor(Uri.fromFile(file), "r");
                PdfDocument pdfDocument = pdfiumCore.newDocument(fd);
                pdfiumCore.openPage(pdfDocument, pageNumber);
                int width = pdfiumCore.getPageWidthPoint(pdfDocument, pageNumber);
                int height = pdfiumCore.getPageHeightPoint(pdfDocument, pageNumber);
                Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
                pdfiumCore.renderPageBitmap(pdfDocument, bmp, pageNumber, 0, 0, width, height);
                Glide.with(getContext()).load(bitmapToByte(bmp)).asBitmap().into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        preview.setImageBitmap(resource);
                        /*if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
                            message.setMessageStatus(Status.SEEN.getValue());
                            conversationService.updateMessage(message);
                            setMessageStatus(message);
                        }*/
                    }
                });
                preview.setVisibility(VISIBLE);
                bulletInfo.setVisibility(VISIBLE);
                controlBtn.setVisibility(GONE);
                info.setText(String.format("%d Pages", pdfiumCore.getPageCount(pdfDocument)));
                fileSize.setText(Utils.readableFileSize(message.getDocModel().getDocSize()));
                fileType.setText(message.getDocModel().getDocType());
                pdfiumCore.closeDocument(pdfDocument); // important!
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            preview.setVisibility(GONE);
            controlBtn.setVisibility(VISIBLE);
            bulletInfo.setVisibility(GONE);
            fileSize.setText(Utils.readableFileSize(message.getDocModel().getDocSize()));
            fileType.setText(message.getDocModel().getDocType());
        }
    }

    private byte[] bitmapToByte(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, stream);
        return stream.toByteArray();
    }

    @SuppressLint("DefaultLocale")
    void readXLSFile(Message message) {
        File file;
        if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
            file = new File(Utils.getDocumentPath() + "/" + message.getDocModel().getDocName());
        } else {
            file = new File(Utils.getSentDocumentPath() + "/" + message.getDocModel().getDocName());
        }
        if (file.exists()) {
            try {
                FileInputStream finStream = new FileInputStream(file.getAbsolutePath());
                HSSFWorkbook wb = new HSSFWorkbook(finStream);
                wb.getNumberOfSheets();
                bulletInfo.setVisibility(VISIBLE);
                controlBtn.setVisibility(GONE);
                info.setText(String.format("%d sheets", wb.getNumberOfSheets()));
                fileSize.setText(Utils.readableFileSize(message.getDocModel().getDocSize()));
                fileType.setText(message.getDocModel().getDocType());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            preview.setVisibility(GONE);
            bulletInfo.setVisibility(GONE);
            controlBtn.setVisibility(VISIBLE);
            fileSize.setText(Utils.readableFileSize(message.getDocModel().getDocSize()));
            fileType.setText(message.getDocModel().getDocType());
        }
    }

    @SuppressLint("DefaultLocale")
    void readXLSFile(GroupMessage message, String self) {
        File file;
        if (!self.equalsIgnoreCase(message.getAuthor().getUid())) {
            file = new File(Utils.getDocumentPath() + "/" + message.getDocModel().getDocName());
        } else {
            file = new File(Utils.getSentDocumentPath() + "/" + message.getDocModel().getDocName());
        }
        if (file.exists()) {
            try {
                FileInputStream finStream = new FileInputStream(file.getAbsolutePath());
                HSSFWorkbook wb = new HSSFWorkbook(finStream);
                wb.getNumberOfSheets();
                bulletInfo.setVisibility(VISIBLE);
                controlBtn.setVisibility(GONE);
                info.setText(String.format("%d sheets", wb.getNumberOfSheets()));
                fileSize.setText(Utils.readableFileSize(message.getDocModel().getDocSize()));
                fileType.setText(message.getDocModel().getDocType());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            preview.setVisibility(GONE);
            bulletInfo.setVisibility(GONE);
            controlBtn.setVisibility(VISIBLE);
            fileSize.setText(Utils.readableFileSize(message.getDocModel().getDocSize()));
            fileType.setText(message.getDocModel().getDocType());
        }
    }

    private void showProgressBar(boolean isVisible) {
        progressbar.setVisibility(isVisible ? VISIBLE : GONE);
    }

    private void uploadFile(final Message messageModel) {
        showProgressBar(true);
        controlBtn.setVisibility(GONE);
        File file;
        if (messageModel.getSelf().equalsIgnoreCase(messageModel.getDestination())) {
            file = new File(Utils.getDocumentPath() + "/" + messageModel.getDocModel().getDocName());
        } else {
            file = new File(Utils.getSentDocumentPath() + "/" + messageModel.getDocModel().getDocName());
        }
        if (!file.exists()) {
            Toast.makeText(getContext(), "File doest not exists", Toast.LENGTH_SHORT).show();
        }
        StorageReference storageRef = storageService.getDocsReference();
        Uri fileUri = Uri.fromFile(file);
        StorageReference documentRef = storageRef.child(messageModel.getDocModel().getDocName());
        StorageMetadata storageMetadata = new StorageMetadata.Builder()
                .setContentType(messageModel.getDocModel().getMimeType()).build();
        UploadTask uploadTask = documentRef.putFile(fileUri, storageMetadata);
        uploadTask
                .addOnFailureListener(e -> {
                    showProgressBar(false);
                    controlBtn.setVisibility(VISIBLE);
                })
                .addOnSuccessListener(taskSnapshot -> {
                    DocModel docModel = messageModel.getDocModel();
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    docModel.setDocUrl(downloadUrl.toString());
                    docModel.setDocSize(taskSnapshot.getTotalByteCount());
                    docModel.setIsUploaded(1);
                    messageModel.setDocModel(docModel);
                    messageModel.setMessageStatus(Status.SENT.getValue());
                    conversationService.sendMessage(messageModel.getSelf(), messageModel);
                    setMessageStatus(messageModel);
                    final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
                    exec.schedule(new Runnable() {
                        @Override
                        public void run() {
                            messageModel.setMessageStatus(Status.DELIVERED.getValue());
                            setMessageStatus(messageModel);
                        }
                    }, 1, TimeUnit.SECONDS);
                    showProgressBar(false);
                    controlBtn.setVisibility(GONE);
                }).addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
            progressbar.setProgress((int) progress);
        });
    }

    private void downloadFile(final Message messageModel) {
        showProgressBar(true);
        controlBtn.setVisibility(GONE);
        StorageReference storageReference = storageService.getDocsReference();
        StorageReference documentRef = storageReference.child(messageModel.getDocModel().getDocName());
        File file;
        if (messageModel.getSelf().equalsIgnoreCase(messageModel.getDestination())) {
            file = new File(Utils.getDocumentPath() + "/" + messageModel.getDocModel().getDocName());
        } else {
            file = new File(Utils.getSentDocumentPath() + "/" + messageModel.getDocModel().getDocName());
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!file.exists()) {
            return;
        }
        documentRef.getFile(file)
                .addOnFailureListener(e -> {
                    showProgressBar(false);
                    controlBtn.setVisibility(VISIBLE);

                }).addOnSuccessListener(taskSnapshot -> {
            showProgressBar(false);
            controlBtn.setVisibility(GONE);
            setUpDocPreview(messageModel);

        }).addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
            progressbar.setProgress((int) progress);

        });
    }

    private void uploadFile(Channel channel, final GroupMessage messageModel, String self) {
        showProgressBar(true);
        controlBtn.setVisibility(GONE);
        File file;
        if (!self.equalsIgnoreCase(messageModel.getAuthor().getUid())) {
            file = new File(Utils.getDocumentPath() + "/" + messageModel.getDocModel().getDocName());
        } else {
            file = new File(Utils.getSentDocumentPath() + "/" + messageModel.getDocModel().getDocName());
        }
        if (!file.exists()) {
            Toast.makeText(getContext(), "File doest not exists", Toast.LENGTH_SHORT).show();
        }
        StorageReference storageRef = storageService.getDocsReference();
        Uri fileUri = Uri.fromFile(file);
        StorageReference documentRef = storageRef.child(messageModel.getDocModel().getDocName());
        StorageMetadata storageMetadata = new StorageMetadata.Builder()
                .setContentType(messageModel.getDocModel().getMimeType()).build();
        UploadTask uploadTask = documentRef.putFile(fileUri, storageMetadata);
        uploadTask
                .addOnFailureListener(e -> {
                    showProgressBar(false);
                    controlBtn.setVisibility(VISIBLE);
                })
                .addOnSuccessListener(taskSnapshot -> {
                    DocModel docModel = messageModel.getDocModel();
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    docModel.setDocUrl(downloadUrl.toString());
                    docModel.setDocSize(taskSnapshot.getTotalByteCount());
                    docModel.setIsUploaded(1);
                    messageModel.setDocModel(docModel);
                    messageModel.setMessageStatus(Status.SENT.getValue());
                    groupConversationService.sendMessage(channel, messageModel);
                    setMessageStatus(messageModel);
                    final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
                    exec.schedule(new Runnable() {
                        @Override
                        public void run() {
                            messageModel.setMessageStatus(Status.DELIVERED.getValue());
                            setMessageStatus(messageModel);
                        }
                    }, 1, TimeUnit.SECONDS);
                    showProgressBar(false);
                    controlBtn.setVisibility(GONE);
                }).addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
            progressbar.setProgress((int) progress);
        });
    }

    private void downloadFile(final GroupMessage messageModel, String self) {
        showProgressBar(true);
        controlBtn.setVisibility(GONE);
        StorageReference storageReference = storageService.getDocsReference();
        StorageReference documentRef = storageReference.child(messageModel.getDocModel().getDocName());
        File file;
        if (!self.equalsIgnoreCase(messageModel.getAuthor().getUid())) {
            file = new File(Utils.getDocumentPath() + "/" + messageModel.getDocModel().getDocName());
        } else {
            file = new File(Utils.getSentDocumentPath() + "/" + messageModel.getDocModel().getDocName());
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!file.exists()) {
            return;
        }
        documentRef.getFile(file)
                .addOnFailureListener(e -> {
                    showProgressBar(false);
                    controlBtn.setVisibility(VISIBLE);

                }).addOnSuccessListener(taskSnapshot -> {
            showProgressBar(false);
            controlBtn.setVisibility(GONE);
            setUpDocPreview(messageModel, self);

        }).addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
            progressbar.setProgress((int) progress);

        });
    }

    public FrameLayout getMainFrame() {
        return mainFrame;
    }
}
