package com.superdev.smiling.conversation.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatSeekBar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.AudioModel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.conversation.data_model.Status;
import com.superdev.smiling.conversation.service.ConversationService;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.groupconversation.service.GroupConversationService;
import com.superdev.smiling.storage.StorageService;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

import static com.superdev.smiling.Utils.formatTimespan;

/**
 * Created by gautam on 9/8/17.
 */
@SuppressWarnings("All")
public class ConversationAudioView extends FrameLayout {

    @Nullable
    @BindView(R.id.dateTextView)
    public TextView dateTextView;

    @Nullable
    @BindView(R.id.control_btn_play)
    public ImageButton controlBtnPlay;

    @Nullable
    @BindView(R.id.control_btn_pause)
    public ImageButton controlBtnPause;

    @Nullable
    @BindView(R.id.upload_button)
    public ImageButton uploadButton;

    @Nullable
    @BindView(R.id.download_button)
    public ImageButton downloadButton;

    @Nullable
    @BindView(R.id.progress_bar_1)
    public CircularProgressView progressBar1;

    @Nullable
    @BindView(R.id.audio_seekbar)
    public AppCompatSeekBar audioSeekbar;

    @Nullable
    @BindView(R.id.description)
    public TextView description;

    @Nullable
    @BindView(R.id.picture)
    public ImageView picture;

    @Nullable
    @BindView(R.id.icon)
    public ImageView icon;

    @BindView(R.id.duration)
    public TextView duration;

    @BindView(R.id.picture_frame)
    public FrameLayout pictureFrame;

    @Nullable
    @BindView(R.id.thumbnail)
    public FrameLayout thumbnail;

    @Nullable
    @BindView(R.id.main_layout)
    public LinearLayout mainLayout;

    @Nullable
    @BindView(R.id.main_frame)
    public FrameLayout mainFrame;

    @BindView(R.id.date)
    public TextView tvDate;

    @Nullable
    @BindView(R.id.status)
    public ImageView status;

    @Nullable
    @BindView(R.id.name_in_group_tv)
    public EmojiconTextView nameInGroupTv;

    @Nullable
    @BindView(R.id.pushname_in_group_tv)
    public EmojiconTextView pushnameInGroupTv;

    @Nullable
    @BindView(R.id.name_in_group)
    public LinearLayout nameInGroup;


    private int layoutResId;

    private StorageService storageService;

    private ConversationService conversationService;

    private GroupConversationService groupConversationService;


    public ConversationAudioView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
        storageService = Dependencies.INSTANCE.getStorageService();
        conversationService = Dependencies.INSTANCE.getConversationService();
        groupConversationService = Dependencies.INSTANCE.getGroupConversationService();
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int[] attrsArray = {
                    android.R.attr.layout
            };
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            layoutResId = array.getResourceId(0, R.layout.merge_conversation_audio_item_destination);
            array.recycle();
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), layoutResId, this);
        ButterKnife.bind(this);
    }

    void display(Message message) {
        setUpTime(message);
        setMessageStatus(message);
        description.setText(Utils.readableFileSize(message.getAudioModel().getAudioSize()));
        setUpAudio(message);
        downloadButton.setOnClickListener(view -> downloadFile(message));
        uploadButton.setOnClickListener(view -> uploadFile(message));

        if (message.getAudioModel().getIsUploaded() == 0) {
            uploadFile(message);
        }

        if (!message.getSelf().equals(message.getDestination())) {
            mainLayout.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        if (message.getSelf().equals(message.getDestination())) {
            message.setMessageStatus(Status.SEEN.getValue());
            setMessageStatus(message);
        }
    }

    void display(Channel channel, GroupMessage message, String self) {
        setUpTime(message);
        setMessageStatus(message);
        description.setText(Utils.readableFileSize(message.getAudioModel().getAudioSize()));
        setUpAudio(message, self);

        downloadButton.setOnClickListener(view -> downloadFile(message, self));

        uploadButton.setOnClickListener(view -> uploadFile(channel, message, self));

        if (message.getAudioModel().getIsUploaded() == 0) {
            uploadFile(channel, message, self);
        }

        if (self.equals(message.getAuthor().getUid())) {
            mainLayout.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        if (self.equals(message.getAuthor().getUid())) {
            if (nameInGroup != null) {
                nameInGroup.setVisibility(VISIBLE);
            }
            if (!TextUtils.isEmpty(message.getAuthor().getShortName())) {
                if (nameInGroupTv != null) {
                    nameInGroupTv.setText(message.getAuthor().getShortName());
                }
            } else {
                if (nameInGroupTv != null) {
                    nameInGroupTv.setText(message.getAuthor().getPhone());
                }
            }
        }
    }

    private void setMessageStatus(Message message) {
        if (status != null) {
            if (Status.SEEN.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_read));
            } else if (Status.DELIVERED.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_received));
            } else if (Status.SENT.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_server_receive));
            } else {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_gray_waiting_2));
            }
        }
    }

    private void setMessageStatus(GroupMessage message) {
        if (status != null) {
            if (Status.SEEN.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_read));
            } else if (Status.DELIVERED.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_received));
            } else if (Status.SENT.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_server_receive));
            } else {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_gray_waiting_2));
            }
        }
    }

    private void setUpAudio(Message message) {
        File file;
        if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
            file = new File(Utils.getAudioPath() + "/" + message.getAudioModel().getAudioName());
        } else {
            file = new File(Utils.getAudioSentPath() + "/" + message.getAudioModel().getAudioName());
        }
        if (!file.exists() && message.getSelf().equalsIgnoreCase(message.getDestination()) && message.getAudioModel().getIsUploaded() == 1) {
            downloadFile(message);
        }

        if (file.exists() && message.getAudioModel().getIsUploaded() == 1) {
            try {
                Uri uri = Uri.fromFile(file);
                if (uri != null) {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(getContext(), uri);
                    String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                    int millSecond = Integer.parseInt(durationStr);
                    duration.setText(formatTimespan(millSecond));
                }
            } catch (Exception ignored) {
            }
            controlBtnPlay.setVisibility(VISIBLE);
            downloadButton.setVisibility(GONE);
            uploadButton.setVisibility(GONE);
            controlBtnPause.setVisibility(GONE);
            audioSeekbar.setThumb(ContextCompat.getDrawable(getContext(), R.drawable.seek_thumb));
        } else if (!file.exists() && message.getAudioModel().getIsUploaded() == 1) {
            controlBtnPlay.setVisibility(GONE);
            downloadButton.setVisibility(VISIBLE);
            uploadButton.setVisibility(GONE);
            controlBtnPause.setVisibility(GONE);
        } else {
            if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
                downloadButton.setVisibility(VISIBLE);
                uploadButton.setVisibility(GONE);
                controlBtnPlay.setVisibility(GONE);
                controlBtnPause.setVisibility(GONE);
            } else {
                uploadButton.setVisibility(VISIBLE);
                downloadButton.setVisibility(GONE);
                controlBtnPlay.setVisibility(GONE);
                controlBtnPause.setVisibility(GONE);
            }
            audioSeekbar.setThumb(null);
            controlBtnPlay.setVisibility(GONE);
        }
    }

    private void setUpAudio(GroupMessage message, String self) {
        File file;
        if (!self.equalsIgnoreCase(message.getAuthor().getUid())) {
            file = new File(Utils.getAudioPath() + "/" + message.getAudioModel().getAudioName());
        } else {
            file = new File(Utils.getAudioSentPath() + "/" + message.getAudioModel().getAudioName());
        }
        if (!file.exists() && !self.equals(message.getAuthor().getUid()) && message.getAudioModel().getIsUploaded() == 1) {
            downloadFile(message, self);
        }
        if (file.exists() && message.getAudioModel().getIsUploaded() == 1) {
            try {
                Uri uri = Uri.fromFile(file);
                if (uri != null) {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(getContext(), uri);
                    String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                    int millSecond = Integer.parseInt(durationStr);
                    duration.setText(formatTimespan(millSecond));
                }
            } catch (Exception ignored) {
            }
            controlBtnPlay.setVisibility(VISIBLE);
            downloadButton.setVisibility(GONE);
            uploadButton.setVisibility(GONE);
            controlBtnPause.setVisibility(GONE);
            audioSeekbar.setThumb(ContextCompat.getDrawable(getContext(), R.drawable.seek_thumb));
        } else if (!file.exists() && message.getAudioModel().getIsUploaded() == 1) {
            controlBtnPlay.setVisibility(GONE);
            downloadButton.setVisibility(VISIBLE);
            uploadButton.setVisibility(GONE);
            controlBtnPause.setVisibility(GONE);
        } else {
            if (!self.equalsIgnoreCase(message.getAuthor().getUid())) {
                downloadButton.setVisibility(VISIBLE);
                uploadButton.setVisibility(GONE);
                controlBtnPlay.setVisibility(GONE);
                controlBtnPause.setVisibility(GONE);
            } else {
                uploadButton.setVisibility(VISIBLE);
                downloadButton.setVisibility(GONE);
                controlBtnPlay.setVisibility(GONE);
                controlBtnPause.setVisibility(GONE);
            }
            audioSeekbar.setThumb(null);
            controlBtnPlay.setVisibility(GONE);
        }
    }

    private void showProgressiveView(boolean isVisible) {
        progressBar1.setVisibility(isVisible ? VISIBLE : GONE);
    }

    private void setUpTime(Message message) {
        final String timestamp = message.getTimestamp();
        String time = Utils.getTime(message.getTimestamp());
        String date = Utils.getDate(message.getTimestamp());
        String today = Utils.getCurrentTimestamp();
        String yesterday = Utils.getYesterdayTimeStamp();

        String[] time1 = date.split("/");
        String[] time2 = today.split("/");
        String[] time3 = yesterday.split("/");
        if ((time1[2] + time1[1] + time1[0]).equals(time2[0] + time2[1] + time2[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_today));
        } else if ((time1[2] + time1[1] + time1[0]).equals(time3[0] + time3[1] + time3[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_yesterday));
        } else {
            if (dateTextView != null)
                dateTextView.setText(Utils.getDateString(timestamp));
        }
        tvDate.setText(time);
    }

    private void setUpTime(GroupMessage message) {
        final String timestamp = message.getTimestamp();
        String time = Utils.getTime(message.getTimestamp());
        String date = Utils.getDate(message.getTimestamp());
        String today = Utils.getCurrentTimestamp();
        String yesterday = Utils.getYesterdayTimeStamp();

        String[] time1 = date.split("/");
        String[] time2 = today.split("/");
        String[] time3 = yesterday.split("/");
        if ((time1[2] + time1[1] + time1[0]).equals(time2[0] + time2[1] + time2[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_today));
        } else if ((time1[2] + time1[1] + time1[0]).equals(time3[0] + time3[1] + time3[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_yesterday));
        } else {
            if (dateTextView != null)
                dateTextView.setText(Utils.getDateString(timestamp));
        }
        tvDate.setText(time);
    }

    private void downloadFile(final Message messageModel) {
        showProgressiveView(true);
        downloadButton.setVisibility(GONE);
        StorageReference storageReference = storageService.getChatAudioReference();
        StorageReference documentRef = storageReference.child(messageModel.getAudioModel().getAudioName());
        File audioFile;
        if (messageModel.getSelf().equalsIgnoreCase(messageModel.getDestination())) {
            audioFile = new File(Utils.getAudioPath() + "/" + messageModel.getAudioModel().getAudioName());
        } else {
            audioFile = new File(Utils.getAudioSentPath() + "/" + messageModel.getAudioModel().getAudioName());
        }
        if (!audioFile.exists()) {
            try {
                audioFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!audioFile.exists()) {
            return;
        }
        documentRef.getFile(audioFile)
                .addOnFailureListener(e -> {
                    showProgressiveView(false);
                    downloadButton.setVisibility(VISIBLE);

                }).addOnSuccessListener(taskSnapshot -> {
            showProgressiveView(false);
            downloadButton.setVisibility(GONE);
            setUpAudio(messageModel);
            audioSeekbar.setThumb(getContext().getResources().getDrawable(R.drawable.seek_thumb));
        }).addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
            progressBar1.setProgress((int) progress);

        });
    }

    private void downloadFile(final GroupMessage messageModel, String self) {
        showProgressiveView(true);
        downloadButton.setVisibility(GONE);
        StorageReference storageReference = storageService.getChatAudioReference();
        StorageReference documentRef = storageReference.child(messageModel.getAudioModel().getAudioName());
        File audioFile;
        if (!self.equalsIgnoreCase(messageModel.getAuthor().getUid())) {
            audioFile = new File(Utils.getAudioPath() + "/" + messageModel.getAudioModel().getAudioName());
        } else {
            audioFile = new File(Utils.getAudioSentPath() + "/" + messageModel.getAudioModel().getAudioName());
        }
        if (!audioFile.exists()) {
            try {
                audioFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!audioFile.exists()) {
            return;
        }
        documentRef.getFile(audioFile)
                .addOnFailureListener(e -> {
                    showProgressiveView(false);
                    downloadButton.setVisibility(VISIBLE);
                }).addOnSuccessListener(taskSnapshot -> {
            showProgressiveView(false);
            downloadButton.setVisibility(GONE);
            setUpAudio(messageModel, self);
            audioSeekbar.setThumb(getContext().getResources().getDrawable(R.drawable.seek_thumb));
        }).addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
            progressBar1.setProgress((int) progress);
        });
    }

    private void uploadFile(final Message messageModel) {
        showProgressiveView(true);
        uploadButton.setVisibility(GONE);
        StorageReference storageRef = storageService.getChatAudioReference();
        final File audioFile = new File(messageModel.getAudioModel().getAudioLocalUrl());
        Uri fileUri = Uri.fromFile(audioFile);
        StorageReference documentRef = storageRef.child(messageModel.getAudioModel().getAudioName());
        StorageMetadata storageMetadata = new StorageMetadata.Builder()
                .setContentType(messageModel.getAudioModel().getMimeType()).build();
        UploadTask uploadTask = documentRef.putFile(fileUri, storageMetadata);
        uploadTask
                .addOnFailureListener(e -> {
                    showProgressiveView(false);
                    uploadButton.setVisibility(VISIBLE);
                    messageModel.setMessageStatus(Status.WAITING.getValue());
                    setMessageStatus(messageModel);
                })
                .addOnSuccessListener(taskSnapshot -> {
                    AudioModel audioModel = messageModel.getAudioModel();
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    audioModel.setAudioUrl(downloadUrl.toString());
                    audioModel.setAudioSize(taskSnapshot.getTotalByteCount());
                    audioModel.setIsUploaded(1);
                    messageModel.setAudioModel(audioModel);
                    messageModel.setMessageStatus(Status.DELIVERED.getValue());
                    conversationService.sendMessage(messageModel.getSelf(), messageModel);
                    setMessageStatus(messageModel);
                    showProgressiveView(false);
                    uploadButton.setVisibility(GONE);
                    setUpAudio(messageModel);
                    audioSeekbar.setThumb(getContext().getResources().getDrawable(R.drawable.seek_thumb));
                }).addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
            progressBar1.setProgress((int) progress);
        });
    }

    private void uploadFile(Channel channel, final GroupMessage messageModel, String self) {
        showProgressiveView(true);
        uploadButton.setVisibility(GONE);
        StorageReference storageRef = storageService.getChatAudioReference();
        final File audioFile = new File(messageModel.getAudioModel().getAudioLocalUrl());
        Uri fileUri = Uri.fromFile(audioFile);
        StorageReference documentRef = storageRef.child(messageModel.getAudioModel().getAudioName());
        StorageMetadata storageMetadata = new StorageMetadata.Builder()
                .setContentType(messageModel.getAudioModel().getMimeType()).build();
        UploadTask uploadTask = documentRef.putFile(fileUri, storageMetadata);
        uploadTask
                .addOnFailureListener(e -> {
                    showProgressiveView(false);
                    uploadButton.setVisibility(VISIBLE);
                    messageModel.setMessageStatus(Status.WAITING.getValue());
                    setMessageStatus(messageModel);
                })
                .addOnSuccessListener(taskSnapshot -> {
                    AudioModel audioModel = messageModel.getAudioModel();
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    audioModel.setAudioUrl(downloadUrl.toString());
                    audioModel.setAudioSize(taskSnapshot.getTotalByteCount());
                    audioModel.setIsUploaded(1);
                    messageModel.setAudioModel(audioModel);
                    messageModel.setMessageStatus(Status.DELIVERED.getValue());
                    groupConversationService.sendMessage(channel, messageModel);
                    setMessageStatus(messageModel);
                    showProgressiveView(false);
                    uploadButton.setVisibility(GONE);
                    setUpAudio(messageModel, self);
                    audioSeekbar.setThumb(getContext().getResources().getDrawable(R.drawable.seek_thumb));
                }).addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
            progressBar1.setProgress((int) progress);
        });
    }

    public FrameLayout getMainFrame() {
        return mainFrame;
    }


}
