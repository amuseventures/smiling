package com.superdev.smiling.conversation.view;

/**
 * Created by Bounyavong on 10/2/2017.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.superdev.smiling.Utils;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;

import java.io.File;

/**
 * Created by marco on 29/07/16.
 */
@SuppressWarnings("ALL")
public class ConversationVoiceViewHolder extends RecyclerView.ViewHolder {

    private final ConversationVoiceView conversationVoiceView;

    public ConversationVoiceViewHolder(ConversationVoiceView conversationVoiceView) {
        super(conversationVoiceView);
        this.conversationVoiceView = conversationVoiceView;
    }

    public void bind(final Message message, final ConversationVoiceViewListener conversationVoiceViewListener) {
        conversationVoiceView.display(message);
        File file;
        if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
            file = new File(Utils.getVoicePath() + "/" + message.getVoiceModel().getVoiceName());
        } else {
            file = new File(Utils.getVoiceSentPath() + "/" + message.getVoiceModel().getVoiceName());
        }

        conversationVoiceView.mainLayout.setOnClickListener(view -> conversationVoiceViewListener.onClick(view, message));

        conversationVoiceView.mainLayout.setOnLongClickListener(view -> {
            conversationVoiceViewListener.onLongClick(view, message);
            return false;
        });
    }

    public void bind(Channel channel, final GroupMessage message, String self, final GroupConversationVoiceViewListener conversationVoiceViewListener) {

        conversationVoiceView.display(channel, message, self);

        conversationVoiceView.mainLayout.setOnClickListener(view -> conversationVoiceViewListener.onClick(view, message));

        conversationVoiceView.mainLayout.setOnLongClickListener(view -> {
            conversationVoiceViewListener.onLongClick(view, message);
            return false;
        });
    }

    public interface ConversationVoiceViewListener {
        void onClick(View view, Message message);

        void onLongClick(View view, Message message);
    }

    public interface GroupConversationVoiceViewListener {
        void onClick(View view, GroupMessage message);

        void onLongClick(View view, GroupMessage message);
    }



    public FrameLayout getMainFrame() {
        return conversationVoiceView.getMainFrame();
    }

    public ConversationVoiceView getConversationVoiceView() {
        return conversationVoiceView;
    }
}
