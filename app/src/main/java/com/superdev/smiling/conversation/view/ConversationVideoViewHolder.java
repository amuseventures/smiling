package com.superdev.smiling.conversation.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;

/**
 * Created by marco on 29/07/16.
 */

public class ConversationVideoViewHolder extends RecyclerView.ViewHolder {

    private final ConversationVideoView conservationVideoView;

    public ConversationVideoViewHolder(ConversationVideoView conservationVideoView) {
        super(conservationVideoView);
        this.conservationVideoView = conservationVideoView;
    }

    public void bind(final Message message, final ConversationVideoViewListener conversationVideoViewListener) {
        conservationVideoView.display(message);
        conservationVideoView.mainLayout.setOnClickListener(view -> conversationVideoViewListener.onClick(view, message));

        conservationVideoView.mainLayout.setOnLongClickListener(view -> {
            conversationVideoViewListener.onLongClick(view, message);
            return false;
        });
    }

    public void bind(Channel channel, final GroupMessage message,String self, final GroupConversationVideoViewListener conversationVideoViewListener) {
        conservationVideoView.display(channel, message, self);
        conservationVideoView.mainLayout.setOnClickListener(view -> conversationVideoViewListener.onClick(view, message));

        conservationVideoView.mainLayout.setOnLongClickListener(view -> {
            conversationVideoViewListener.onLongClick(view, message);
            return false;
        });
    }

    public interface ConversationVideoViewListener {
        void onClick(View view, Message message);

        void onLongClick(View view, Message message);
    }

    public interface GroupConversationVideoViewListener {
        void onClick(View view, GroupMessage message);

        void onLongClick(View view, GroupMessage message);
    }

    public FrameLayout getMainFrame() {
        return conservationVideoView.getMainFrame();
    }
}
