package com.superdev.smiling.conversation.data_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

/**
 * Created by gautam on 9/5/17.
 */

public class DocModel implements Parcelable {

    private String docName;
    private String docType;
    private String docUrl;
    private long docSize;
    private String mimeType;
    private String originalFileName;
    @Exclude
    private String docLocalUrl;

    private int isUploaded;

    public DocModel() {
        //Used By Firebase
    }

    public DocModel(String originalFileName, String docName, String docType, String docUrl, long docSize, String mimeType, String docLocalUrl, int isUploaded) {
        this.docName = docName;
        this.docType = docType;
        this.docUrl = docUrl;
        this.docSize = docSize;
        this.mimeType = mimeType;
        this.docLocalUrl = docLocalUrl;
        this.isUploaded = isUploaded;
        this.originalFileName = originalFileName;
    }

    public DocModel(Parcel in) {
        docName = in.readString();
        originalFileName = in.readString();
        docType = in.readString();
        docUrl = in.readString();
        mimeType = in.readString();
        docSize = in.readLong();
        docLocalUrl = in.readString();
        isUploaded = in.readInt();
    }

    public static final Creator<DocModel> CREATOR = new Creator<DocModel>() {
        @Override
        public DocModel createFromParcel(Parcel in) {
            return new DocModel(in);
        }

        @Override
        public DocModel[] newArray(int size) {
            return new DocModel[size];
        }
    };

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocUrl() {
        return docUrl;
    }

    public void setDocUrl(String docUrl) {
        this.docUrl = docUrl;
    }

    public long getDocSize() {
        return docSize;
    }

    public void setDocSize(long docSize) {
        this.docSize = docSize;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    @Exclude
    public String getDocLocalUrl() {
        return docLocalUrl;
    }

    @Exclude
    public void setDocLocalUrl(String docLocalUrl) {
        this.docLocalUrl = docLocalUrl;
    }

    public int getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(int isUploaded) {
        this.isUploaded = isUploaded;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(docName);
        parcel.writeString(originalFileName);
        parcel.writeString(docType);
        parcel.writeString(docUrl);
        parcel.writeString(mimeType);
        parcel.writeLong(docSize);
        parcel.writeString(docLocalUrl);
        parcel.writeInt(isUploaded);
    }

    @Override
    public String toString() {
        return docName + "\n" + docType + "\n" + docSize + "\n" + mimeType;
    }
}
