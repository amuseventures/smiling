package com.superdev.smiling.conversation.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;

/**
 * Created by marco on 29/07/16.
 */

public class ConversationDocViewHolder extends RecyclerView.ViewHolder {

    private final ConversationDocView conversationDocView;

    public ConversationDocViewHolder(ConversationDocView conversationDocView) {
        super(conversationDocView);
        this.conversationDocView = conversationDocView;
    }

    public void bind(final Message message, final ConversationDocViewListener conversationDocViewListener) {
        conversationDocView.display(message);

        conversationDocView.mainLayout.setOnClickListener(view -> conversationDocViewListener.onClick(view, message));

        conversationDocView.mainLayout.setOnLongClickListener(view -> {
            conversationDocViewListener.onLongClick(view, message);
            return false;
        });
    }

    public void bind(Channel channel, final GroupMessage message, String self,  final GroupConversationDocViewListener conversationDocViewListener) {
        conversationDocView.display(channel,message, self);

        conversationDocView.mainLayout.setOnClickListener(view -> conversationDocViewListener.onClick(view, message));

        conversationDocView.mainLayout.setOnLongClickListener(view -> {
            conversationDocViewListener.onLongClick(view, message);
            return false;
        });
    }

    public interface ConversationDocViewListener {

        void onClick(View view, Message message);

        void onLongClick(View view, Message message);
    }

    public interface GroupConversationDocViewListener {

        void onClick(View view, GroupMessage message);

        void onLongClick(View view, GroupMessage message);
    }

    public FrameLayout getMainFrame() {
        return conversationDocView.getMainFrame();
    }
}
