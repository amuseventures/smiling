package com.superdev.smiling.conversation.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;

/**
 * Created by marco on 29/07/16.
 */

public class ConversationFileViewHolder extends RecyclerView.ViewHolder {

    private final ConversationFileView conversationFileView;

    public ConversationFileViewHolder(ConversationFileView messageView) {
        super(messageView);
        this.conversationFileView = messageView;
    }

    public void bind(final Message message, final ConversationFileViewListener conversationFileViewListener) {
        conversationFileView.display(message);

        conversationFileView.mainLayout.setOnClickListener(view -> conversationFileViewListener.onClick(view, message));
        conversationFileView.mainLayout.setOnLongClickListener(view -> {
            conversationFileViewListener.onLongClick(view, message);
            return false;
        });

    }

    public void bind(Channel channel, final GroupMessage message, String self, final GroupConversationFileViewListener conversationFileViewListener) {
        conversationFileView.display(channel, message, self);

        conversationFileView.mainLayout.setOnClickListener(view -> conversationFileViewListener.onClick(view, message));
        conversationFileView.mainLayout.setOnLongClickListener(view -> {
            conversationFileViewListener.onLongClick(view, message);
            return false;
        });

    }

    public interface ConversationFileViewListener {

        void onClick(View view, Message message);

        void onLongClick(View view, Message message);
    }

    public interface GroupConversationFileViewListener {

        void onClick(View view, GroupMessage message);

        void onLongClick(View view, GroupMessage message);
    }

    public FrameLayout getMainFrame() {
        return conversationFileView.getMainFrame();
    }

}
