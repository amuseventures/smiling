package com.superdev.smiling.conversation.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.ImageCompress;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.common.SquaredImageView;
import com.superdev.smiling.conversation.data_model.FileModel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.conversation.data_model.Status;
import com.superdev.smiling.conversation.service.ConversationService;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.groupconversation.service.GroupConversationService;
import com.superdev.smiling.storage.StorageService;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by marco on 29/07/16.
 */

public class ConversationFileView extends FrameLayout {

    @Nullable
    @BindView(R.id.dateTextView)
    TextView dateTextView;

    @Nullable
    @BindView(R.id.name_in_group_tv)
    EmojiconTextView nameInGroupTv;

    @Nullable
    @BindView(R.id.pushname_in_group_tv)
    EmojiconTextView pushnameInGroupTv;

    @Nullable
    @BindView(R.id.name_in_group)
    LinearLayout nameInGroup;

    @Nullable
    @BindView(R.id.quoted_message_holder)
    FrameLayout quotedMessageHolder;

    @BindView(R.id.image)
    SquaredImageView image;

    @BindView(R.id.progress_bar)
    CircularProgressView progressBar;

    @BindView(R.id.cancel_download)
    ImageView cancelDownload;

    @BindView(R.id.control_btn)
    Button controlBtn;

    @BindView(R.id.control_frame)
    FrameLayout controlFrame;

    @BindView(R.id.date_layout)
    FrameLayout dateLayout;

    @BindView(R.id.media_container)
    FrameLayout mediaContainer;

    @BindView(R.id.caption)
    EmojiconTextView caption;

    @BindView(R.id.date_caption)
    TextView dateCaption;

    @BindView(R.id.date_wrapper)
    LinearLayout dateWrapper;

    @BindView(R.id.text_and_date)
    FrameLayout textAndDate;

    @BindView(R.id.main_layout)
    LinearLayout mainLayout;

    @BindView(R.id.main_frame)
    FrameLayout mainFrame;

    @BindView(R.id.date)
    TextView tvDate;

    @Nullable
    @BindView(R.id.status)
    ImageView status;

    private int layoutResId;

    private StorageService storageService;

    private ConversationService conversationService;

    private GroupConversationService groupConversationService;

    public ConversationFileView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int[] attrsArray = {
                    android.R.attr.layout
            };
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            layoutResId = array.getResourceId(0, R.layout.merge_conversation_file_item_destination);
            array.recycle();
        }
        storageService = Dependencies.INSTANCE.getStorageService();
        conversationService = Dependencies.INSTANCE.getConversationService();
        groupConversationService = Dependencies.INSTANCE.getGroupConversationService();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), layoutResId, this);
        ButterKnife.bind(this);
    }

    public void display(final Message message) {
        setUpTime(message);
        setMessageStatus(message);
        controlBtn.setText(Utils.readableFileSize(message.getFileModel().getFileSize()));
        setUpImage(message);
        if (message.getFileModel().getIsUploaded() == 0) {
            compressFile(message);
        }
        if (!message.getSelf().equals(message.getDestination())) {
            mainLayout.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        if (message.getSelf().equals(message.getDestination())) {
            message.setMessageStatus(Status.SEEN.getValue());
            setMessageStatus(message);
        }
        if (message.getMessage() != null)
            caption.setText(message.getMessage());
    }

    public void display(Channel channel, final GroupMessage message, String self) {
        setUpTime(message);
        setMessageStatus(message);
        controlBtn.setText(Utils.readableFileSize(message.getFileModel().getFileSize()));
        setUpImage(message, channel, self);
        if (message.getFileModel().getIsUploaded() == 0) {
            compressFile(channel, message);
        }

        if (self.equals(message.getAuthor().getUid())) {
            mainLayout.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        if (!self.equals(message.getAuthor().getUid())) {
            if (nameInGroup != null) {
                nameInGroup.setVisibility(VISIBLE);
            }
            if (!TextUtils.isEmpty(message.getAuthor().getShortName())) {
                if (nameInGroupTv != null) {
                    nameInGroupTv.setText(message.getAuthor().getShortName());
                }
            } else {
                if (nameInGroupTv != null) {
                    nameInGroupTv.setText(message.getAuthor().getPhone());
                }
            }

        }
        if (message.getMessage() != null)
            caption.setText(message.getMessage());
    }

    private void setMessageStatus(Message message) {
        if (status != null) {
            if (Status.SEEN.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_read));
            } else if (Status.DELIVERED.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(Utils.changeDrawableColor(getContext(), R.drawable.msg_status_client_received, R.color.white));
            } else if (Status.SENT.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(Utils.changeDrawableColor(getContext(), R.drawable.msg_status_server_receive, R.color.white));
            } else {
                status.setImageDrawable(Utils.changeDrawableColor(getContext(), R.drawable.msg_status_gray_waiting_2, R.color.white));
            }
        }
    }

    private void setMessageStatus(GroupMessage message) {
        if (status != null) {
            if (Status.SEEN.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_read));
            } else if (Status.DELIVERED.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(Utils.changeDrawableColor(getContext(), R.drawable.msg_status_client_received, R.color.white));
            } else if (Status.SENT.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(Utils.changeDrawableColor(getContext(), R.drawable.msg_status_server_receive, R.color.white));
            } else {
                status.setImageDrawable(Utils.changeDrawableColor(getContext(), R.drawable.msg_status_gray_waiting_2, R.color.white));
            }
        }
    }

    private void setUpImage(Message message) {
        File file;
        if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
            file = new File(Utils.getImagePath() + "/" + message.getFileModel().getFileName());
        } else {
            file = new File(Utils.getImageSentPath() + "/" + message.getFileModel().getFileName());
        }
        if (file.exists()) {
            setImage(file);
            controlFrame.setVisibility(GONE);
        } else if (message.getFileModel().getFileUri() != null) {
            File file1 = new File(Utils.getRealPathFromURI(getContext(), Uri.parse(message.getFileModel().getFileUri())));
            setImage(file1);
        } else if (!file.exists() && message.getFileModel().getFileUri() == null) {
            controlFrame.setVisibility(VISIBLE);
            controlBtn.setVisibility(VISIBLE);
            controlBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_download, 0, 0, 0);
        } else {
            controlFrame.setVisibility(VISIBLE);
            controlBtn.setVisibility(VISIBLE);
            Bitmap bm = ThumbnailUtils.createVideoThumbnail(message.getFileModel().getFileUrl(),
                    MediaStore.Images.Thumbnails.MICRO_KIND);
            image.setImageBitmap(bm);
        }
        controlBtn.setOnClickListener(view -> {
            if (!file.exists() || message.getSelf().equalsIgnoreCase(message.getDestination())) {
                downloadFile(message);
            } else {
                uploadImage(message);
            }
        });
    }


    private void setUpImage(GroupMessage message, Channel channel, String self) {
        File file;
        if (!self.equalsIgnoreCase(message.getAuthor().getUid())) {
            file = new File(Utils.getImagePath() + "/" + message.getFileModel().getFileName());
        } else {
            file = new File(Utils.getImageSentPath() + "/" + message.getFileModel().getFileName());
        }
        if (file.exists()) {
            setImage(file);
            controlFrame.setVisibility(GONE);
        } else if (message.getFileModel().getFileUri() != null) {
            File file1 = new File(Utils.getRealPathFromURI(getContext(), Uri.parse(message.getFileModel().getFileUri())));
            setImage(file1);
        } else if (!file.exists() && message.getFileModel().getFileUri() == null) {
            controlFrame.setVisibility(VISIBLE);
            controlBtn.setVisibility(VISIBLE);
            controlBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_download, 0, 0, 0);
        } else {
            controlFrame.setVisibility(VISIBLE);
            controlBtn.setVisibility(VISIBLE);
            Bitmap bm = ThumbnailUtils.createVideoThumbnail(message.getFileModel().getFileUrl(),
                    MediaStore.Images.Thumbnails.MICRO_KIND);
            image.setImageBitmap(bm);
        }

        controlBtn.setOnClickListener(view -> {
            if (!file.exists() || !self.equalsIgnoreCase(message.getAuthor().getUid())) {
                downloadFile(message, channel, self);
            } else {
                uploadImage(channel, message);
            }
        });
    }

    private void setUpTime(Message message) {
        final String timestamp = message.getTimestamp();
        String time = Utils.getTime(message.getTimestamp());
        String date = Utils.getDate(message.getTimestamp());
        String today = Utils.getCurrentTimestamp();
        String yesterday = Utils.getYesterdayTimeStamp();

        String[] time1 = date.split("/");
        String[] time2 = today.split("/");
        String[] time3 = yesterday.split("/");
        if ((time1[2] + time1[1] + time1[0]).equals(time2[0] + time2[1] + time2[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_today));
        } else if ((time1[2] + time1[1] + time1[0]).equals(time3[0] + time3[1] + time3[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_yesterday));
        } else {
            if (dateTextView != null)
                dateTextView.setText(Utils.getDateString(timestamp));
        }
        tvDate.setText(time);
    }

    private void setUpTime(GroupMessage message) {
        final String timestamp = message.getTimestamp();
        String time = Utils.getTime(message.getTimestamp());
        String date = Utils.getDate(message.getTimestamp());
        String today = Utils.getCurrentTimestamp();
        String yesterday = Utils.getYesterdayTimeStamp();

        String[] time1 = date.split("/");
        String[] time2 = today.split("/");
        String[] time3 = yesterday.split("/");
        if ((time1[2] + time1[1] + time1[0]).equals(time2[0] + time2[1] + time2[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_today));
        } else if ((time1[2] + time1[1] + time1[0]).equals(time3[0] + time3[1] + time3[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_yesterday));
        } else {
            if (dateTextView != null)
                dateTextView.setText(Utils.getDateString(timestamp));
        }
        tvDate.setText(time);
    }

    private void compressFile(Message message) {
        controlBtn.setVisibility(GONE);
        showProgressBar(true);
        new ImageCompress(filePath -> {
            File file = new File(filePath);
            Uri photo = Uri.fromFile(file);
            FileModel fileModel = message.getFileModel();
            fileModel.setFileUri(photo.toString());
            fileModel.setFileName(file.getName());
            fileModel.setIsUploaded(1);
            fileModel.setFileSize(file.length());
            message.setFileModel(fileModel);
            uploadImage(message);
        }).execute(new File(Uri.parse(message.getFileModel().getFileUri()).getPath()).getAbsolutePath());
    }

    private void compressFile(Channel channel, GroupMessage message) {
        controlBtn.setVisibility(GONE);
        showProgressBar(true);
        new ImageCompress(filePath -> {
            File file = new File(filePath);
            Uri photo = Uri.fromFile(file);
            FileModel fileModel = message.getFileModel();
            fileModel.setFileUri(photo.toString());
            fileModel.setFileName(file.getName());
            fileModel.setIsUploaded(1);
            fileModel.setFileSize(file.length());
            message.setFileModel(fileModel);
            uploadImage(channel, message);
        }).execute(new File(Uri.parse(message.getFileModel().getFileUri()).getPath()).getAbsolutePath());
    }

    private void uploadImage(final Message chatModel) {
        controlBtn.setVisibility(GONE);
        StorageReference storageRef = storageService.getChatImageReference();
        Uri photoURI = Uri.parse(chatModel.getFileModel().getFileUri());
        StorageReference imageGalleryRef = storageRef.child(chatModel.getFileModel().getFileName());
        UploadTask uploadTask = imageGalleryRef.putFile(photoURI);
        showProgressBar(true);
        uploadTask
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                        showProgressBar(false);
                        controlFrame.setVisibility(VISIBLE);
                        controlBtn.setVisibility(VISIBLE);
                        chatModel.setMessageStatus(Status.WAITING.getValue());
                        setMessageStatus(chatModel);

                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        FileModel fileModel = chatModel.getFileModel();
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        fileModel.setFileUrl(downloadUrl.toString());
                        fileModel.setFileSize(taskSnapshot.getTotalByteCount());
                        chatModel.setFileModel(fileModel);
                        chatModel.setMessageStatus(Status.SENT.getValue());
                        conversationService.sendMessage(chatModel.getSelf(), chatModel);
                        setMessageStatus(chatModel);
                        final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
                        exec.schedule(new Runnable() {
                            @Override
                            public void run() {
                                chatModel.setMessageStatus(Status.DELIVERED.getValue());
                                setMessageStatus(chatModel);
                            }
                        }, 1, TimeUnit.SECONDS);
                        showProgressBar(false);
                        controlFrame.setVisibility(GONE);
                    }
                });
    }

    private void uploadImage(Channel channel, final GroupMessage chatModel) {
        controlBtn.setVisibility(GONE);
        StorageReference storageRef = storageService.getChatImageReference();
        Uri photoURI = Uri.parse(chatModel.getFileModel().getFileUri());
        StorageReference imageGalleryRef = storageRef.child(chatModel.getFileModel().getFileName());
        UploadTask uploadTask = imageGalleryRef.putFile(photoURI);
        showProgressBar(true);
        uploadTask
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                        showProgressBar(false);
                        controlFrame.setVisibility(VISIBLE);
                        controlBtn.setVisibility(VISIBLE);
                        chatModel.setMessageStatus(Status.WAITING.getValue());
                        setMessageStatus(chatModel);

                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        FileModel fileModel = chatModel.getFileModel();
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        fileModel.setFileUrl(downloadUrl.toString());
                        fileModel.setFileSize(taskSnapshot.getTotalByteCount());
                        chatModel.setFileModel(fileModel);
                        chatModel.setMessageStatus(Status.SENT.getValue());
                        groupConversationService.sendMessage(channel, chatModel);
                        setMessageStatus(chatModel);
                        final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
                        exec.schedule(new Runnable() {
                            @Override
                            public void run() {
                                chatModel.setMessageStatus(Status.DELIVERED.getValue());
                                setMessageStatus(chatModel);
                            }
                        }, 1, TimeUnit.SECONDS);
                        showProgressBar(false);
                        controlFrame.setVisibility(GONE);
                    }
                });
    }

    private void showProgressBar(boolean isVisible) {
        progressBar.setVisibility(isVisible ? VISIBLE : GONE);
    }

    private void downloadFile(final Message messageModel) {
        showProgressBar(true);
        controlBtn.setVisibility(GONE);
        StorageReference storageRef = storageService.getChatImageReference();
        StorageReference imageRef = storageRef.child(messageModel.getFileModel().getFileName());
        File file;
        if (messageModel.getSelf().equalsIgnoreCase(messageModel.getDestination())) {
            file = new File(Utils.getImagePath() + "/" + messageModel.getFileModel().getFileName());
        } else {
            file = new File(Utils.getImageSentPath() + "/" + messageModel.getFileModel().getFileName());
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!file.exists()) {
            return;
        }
        imageRef.getFile(file)
                .addOnFailureListener(e -> {
                    showProgressBar(false);
                    controlBtn.setVisibility(VISIBLE);

                }).addOnSuccessListener(taskSnapshot -> {
            showProgressBar(false);
            setUpImage(messageModel);
        }).addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
            progressBar.setProgress((int) progress);

        });
    }

    private void downloadFile(final GroupMessage message, Channel channel, String self) {
        showProgressBar(true);
        controlBtn.setVisibility(GONE);
        StorageReference storageRef = storageService.getChatImageReference();
        StorageReference imageRef = storageRef.child(message.getFileModel().getFileName());
        File file;
        if (!self.equalsIgnoreCase(message.getAuthor().getUid())) {
            file = new File(Utils.getImagePath() + "/" + message.getFileModel().getFileName());
        } else {
            file = new File(Utils.getImageSentPath() + "/" + message.getFileModel().getFileName());
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!file.exists()) {
            return;
        }
        imageRef.getFile(file)
                .addOnFailureListener(e -> {
                    showProgressBar(false);
                    controlBtn.setVisibility(VISIBLE);

                }).addOnSuccessListener(taskSnapshot -> {
            showProgressBar(false);
            setUpImage(message, channel, self);
        }).addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
            progressBar.setProgress((int) progress);

        });
    }

    private void setImage(File file) {
        Glide.with(getContext())
                .load(Uri.fromFile(file))
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        image.setImageBitmap(resource);
                    }
                });
    }

    public FrameLayout getMainFrame() {
        return mainFrame;
    }


}
