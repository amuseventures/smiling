package com.superdev.smiling.conversation.view;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.superdev.smiling.conversation.data_model.Chat;
import com.superdev.smiling.conversation.data_model.Message;

import java.io.File;
import java.util.List;

/**
 * Created by marco on 29/07/16.
 */

public interface ConversationDisplayer {

    void display(Chat chat, String user);

    void addToDisplay(Message message, String user);

    void setupToolbar(String user, String image, long lastSeen);

    void showTyping();

    void hideTyping();

    void attach(ConversationActionListener conversationInteractionListener);

    void detach(ConversationActionListener conversationInteractionListener);

    void enableInteraction();

    void disableInteraction();

    void clearSelectedItem();

    void setDestination(String destination);

    void onMessageDeleted(Message message);

    void inflateMenu(MenuInflater inflater, Menu menu);

    void updateMessageState(String messageStatus, String messageKey);

    interface ConversationActionListener {

        void onUpPressed();

        void onMessageLengthChanged(int messageLength);

        void onSubmitMessage(String message);

        void onDocumentsPressed();

        void onCameraPressed();

        void onGalleryPressed();

        void onAudioPressed();

        void onLocationPressed();

        void onContactPressed();

        void onMessageSelected(View view, Message message);

        void onImageSelected(View view, Message message);

        void onVideoSelected(View view, Message message);

        void onMapViewSelected(Message message);

        void onDocViewSelected(Message message);

        void showCabMenu(List<Message> messageList);

        void hideCabMenu();

        void onContactAddBtn(Message message);

        void onContactMsgSelected(Message message);

        void showAttachment();

        void onSubmitVoice(File audioFile);
    }

}
