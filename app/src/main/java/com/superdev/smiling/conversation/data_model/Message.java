package com.superdev.smiling.conversation.data_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;
import com.superdev.smiling.MimeTypeEnum;
import com.superdev.smiling.Utils;

import java.util.UUID;

/**
 * Created by marco on 03/07/16.
 */

public class Message implements Parcelable {

    private String messageId;

    private String sender;

    private String destination;

    private String message;

    private String timestamp;

    private String msgType;

    private String messageKey;

    @Exclude
    private String self;

    private FileModel fileModel;

    private MapModel mapModel;

    private DocModel docModel;

    private AudioModel audioModel;

    private VoiceModel voiceModel;

    private VideoModel videoModel;

    private ContactModel contactModel;

    private String messageStatus;

    public Message() {
    }

    public Message(String sender, String destination, String message, String msgType, String messageStatus) {
        this.messageId = UUID.randomUUID().toString();
        this.sender = sender;
        this.destination = destination;
        this.message = message;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.messageStatus = messageStatus;
    }

    public Message(String sender, String destination, String message, FileModel fileModel, String msgType, String messageStatus) {
        this.messageId = UUID.randomUUID().toString();
        this.sender = sender;
        this.destination = destination;
        this.fileModel = fileModel;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.message = message;
        this.messageStatus = messageStatus;
    }

    public Message(String sender, String destination, String message, MapModel mapModel, String msgType, String messageStatus) {
        this.messageId = UUID.randomUUID().toString();
        this.sender = sender;
        this.destination = destination;
        this.mapModel = mapModel;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.message = message;
        this.messageStatus = messageStatus;
    }

    public Message(String sender, String destination, String message, DocModel docModel, String msgType, String messageStatus) {
        this.messageId = UUID.randomUUID().toString();
        this.sender = sender;
        this.destination = destination;
        this.docModel = docModel;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.message = message;
        this.messageStatus = messageStatus;
    }

    public Message(String sender, String destination, String message, AudioModel audioModel, String msgType, String messageStatus) {
        this.messageId = UUID.randomUUID().toString();
        this.sender = sender;
        this.destination = destination;
        this.audioModel = audioModel;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.message = message;
        this.messageStatus = messageStatus;
    }

    public Message(String sender, String destination, String message, VoiceModel voiceModel, String msgType, String messageStatus) {
        this.messageId = UUID.randomUUID().toString();
        this.sender = sender;
        this.destination = destination;
        this.voiceModel = voiceModel;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.message = message;
        this.messageStatus = messageStatus;
    }

    public Message(String sender, String destination, String message, VideoModel videoModel, String msgType, String messageStatus) {
        this.messageId = UUID.randomUUID().toString();
        this.sender = sender;
        this.destination = destination;
        this.videoModel = videoModel;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.message = message;
        this.messageStatus = messageStatus;
    }

    public Message(String sender, String destination, String message, ContactModel contactModel, String msgType, String messageStatus) {
        this.messageId = UUID.randomUUID().toString();
        this.sender = sender;
        this.destination = destination;
        this.contactModel = contactModel;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.message = message;
        this.messageStatus = messageStatus;
    }

    protected Message(Parcel in) {
        messageId = in.readString();
        sender = in.readString();
        destination = in.readString();
        message = in.readString();
        timestamp = in.readString();
        msgType = in.readString();
        self = in.readString();
        fileModel = in.readParcelable(FileModel.class.getClassLoader());
        mapModel = in.readParcelable(MapModel.class.getClassLoader());
        docModel = in.readParcelable(DocModel.class.getClassLoader());
        audioModel = in.readParcelable(AudioModel.class.getClassLoader());
        videoModel = in.readParcelable(VideoModel.class.getClassLoader());
        contactModel = in.readParcelable(ContactModel.class.getClassLoader());
        //messageStatus = in.readString();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public FileModel getFileModel() {
        return fileModel;
    }

    public void setFileModel(FileModel fileModel) {
        this.fileModel = fileModel;
    }

    public MapModel getMapModel() {
        return mapModel;
    }

    public void setMapModel(MapModel mapModel) {
        this.mapModel = mapModel;
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getSender() {
        return sender;
    }

    public String getDestination() {
        return destination;
    }

    public String getMessage() {
        return message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public DocModel getDocModel() {
        return docModel;
    }

    public void setDocModel(DocModel docModel) {
        this.docModel = docModel;
    }

    public VideoModel getVideoModel() {
        return videoModel;
    }

    public void setVideoModel(VideoModel videoModel) {
        this.videoModel = videoModel;
    }

    public ContactModel getContactModel() {
        return contactModel;
    }

    public void setContactModel(ContactModel contactModel) {
        this.contactModel = contactModel;
    }

    public String getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Message message = (Message) o;

        return this.message != null && this.message.equals(message.message)
                && timestamp != null && timestamp.equals(message.timestamp);
    }

    @Exclude
    public boolean isMmsType() {
        return msgType.equalsIgnoreCase(MimeTypeEnum.IMAGE.getValue());
    }

    @Exclude
    public boolean isLocationType() {
        return msgType.equals(MimeTypeEnum.LOCATION.getValue());
    }

    @Exclude
    public boolean isVideoType() {
        return msgType.equals(MimeTypeEnum.VIDEO.getValue());
    }


    @Exclude
    public boolean isAudioType() {
        return msgType.equals(MimeTypeEnum.AUDIO.getValue());
    }

    @Exclude
    public boolean isVoiceType() {
        return msgType.equals(MimeTypeEnum.VOICE.getValue());
    }

    @Exclude
    public boolean isContactType() {
        return msgType.equals(MimeTypeEnum.CONTACT.getValue());
    }

    @Exclude
    public boolean isDocumentType() {
        return msgType.equals(MimeTypeEnum.DOCUMENT.getValue());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public AudioModel getAudioModel() {
        return audioModel;
    }

    public void setAudioModel(AudioModel audioModel) {
        this.audioModel = audioModel;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(messageId);
        parcel.writeString(sender);
        parcel.writeString(destination);
        parcel.writeString(message);
        parcel.writeString(timestamp);
        parcel.writeString(msgType);
        parcel.writeString(self);
        parcel.writeParcelable(fileModel, i);
        parcel.writeParcelable(mapModel, i);
        parcel.writeParcelable(docModel, i);
        parcel.writeParcelable(audioModel, i);
        parcel.writeParcelable(videoModel, i);
        //parcel.writeString(messageStatus);
    }

    public VoiceModel getVoiceModel() {
        return voiceModel;
    }

    public void setVoiceModel(VoiceModel voiceModel) {
        this.voiceModel = voiceModel;
    }
}
