package com.superdev.smiling.conversation.data_model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by gautam on 9/13/17.
 */

public class ContactModel implements Parcelable {

    private String userId;

    private String name;

    private String contactImage;

    private String contactUri;

    private String contactNumber;

    private String isRegistered;


    public ContactModel() {
    }

    public ContactModel(String userId, String name, String contactImage, String contactUri, String contactNumber, String isRegistered) {
        this.userId = userId;
        this.name = name;
        this.contactImage = contactImage;
        this.contactUri = contactUri;
        this.contactNumber = contactNumber;
        this.isRegistered = isRegistered;
    }


    public ContactModel(Parcel in) {
        userId = in.readString();
        name = in.readString();
        contactImage = in.readString();
        contactUri = in.readString();
        contactNumber = in.readString();
        isRegistered = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(name);
        dest.writeString(contactImage);
        dest.writeString(contactUri);
        dest.writeString(contactNumber);
        dest.writeString(isRegistered);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ContactModel> CREATOR = new Creator<ContactModel>() {
        @Override
        public ContactModel createFromParcel(Parcel in) {
            return new ContactModel(in);
        }

        @Override
        public ContactModel[] newArray(int size) {
            return new ContactModel[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(String isRegistered) {
        this.isRegistered = isRegistered;
    }

    public String getContactImage() {
        return contactImage;
    }

    public String getContactUri() {
        return contactUri;
    }

    public void setContactImage(String contactImage) {
        this.contactImage = contactImage;
    }

    public void setContactUri(String contactUri) {
        this.contactUri = contactUri;
    }
}
