package com.superdev.smiling.conversation.database;

import android.app.Notification;

import com.superdev.smiling.conversation.data_model.Chat;
import com.superdev.smiling.conversation.data_model.Message;

import rx.Observable;

/**
 * Created by marco on 29/07/16.
 */

public interface ConversationDatabase {

    Observable<Message> observeAddMessage(String self, String destination);

    Observable<String> observeUpdateMessage(String self, String destination, String messageKey);

    Observable<Message> observeLastMessage(String self, String destination);

    Observable<Chat> observeChat(String self, String destination);

    Observable<Chat> observeMedia(String self, String destination);

    Observable<Boolean> removeChat(String self, String destination, String key);

    void sendMessage(String user, Message message);

    Observable<Boolean> observeTyping(String self, String destination);

    void setTyping(String self, String destination, Boolean value);

    void updateMessage(Message message);

    Observable<Chat> observeAllMedia(String self, String destination);

    Observable<Chat> observeAllDocuments(String self, String destination);
}
