package com.superdev.smiling.conversation.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.conversation.data_model.Status;
import com.superdev.smiling.conversation.service.ConversationService;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.groupconversation.service.GroupConversationService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by marco on 29/07/16.
 */

public class ConversationMessageView extends LinearLayout {

    @Nullable
    @BindView(R.id.dateTextView)
    TextView dateTextView;

    @Nullable
    @BindView(R.id.name_in_group_tv)
    EmojiconTextView nameInGroupTv;

    @Nullable
    @BindView(R.id.pushname_in_group_tv)
    EmojiconTextView pushnameInGroupTv;

    @Nullable
    @BindView(R.id.name_in_group)
    LinearLayout nameInGroup;

    @Nullable
    @BindView(R.id.quoted_message_holder)
    FrameLayout quotedMessageHolder;

    @BindView(R.id.web_page_preview_holder)
    FrameLayout webPagePreviewHolder;

    @BindView(R.id.message_text)
    TextView messageText;

    @BindView(R.id.date)
    TextView tvDate;

    @Nullable
    @BindView(R.id.status)
    ImageView status;

    @BindView(R.id.main_layout)
    LinearLayout mainLayout;

    @BindView(R.id.main_frame)
    FrameLayout mainFrame;

    private ConversationService conversationService;

    private GroupConversationService groupConversationService;

    private int layoutResId;

    public ConversationMessageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int[] attrsArray = {
                    android.R.attr.layout
            };
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            layoutResId = array.getResourceId(0, R.layout.merge_conversation_message_item_destination);
            array.recycle();
        }
        conversationService = Dependencies.INSTANCE.getConversationService();
        groupConversationService = Dependencies.INSTANCE.getGroupConversationService();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), layoutResId, this);
        ButterKnife.bind(this);
    }

    public void display(final Message message) {
        final String timestamp = message.getTimestamp();
        if (dateTextView != null)
            dateTextView.setText(Utils.getDateString(timestamp));
        messageText.setText(String.format("%s", message.getMessage()));
        tvDate.setText(Utils.getTime(timestamp));
        setMessageStatus(message);
        if (Status.WAITING.getValue().equalsIgnoreCase(message.getMessageStatus())) {
            message.setMessageStatus(Status.SENT.getValue());
            conversationService.sendMessage(message.getSelf(), message);
            setMessageStatus(message);
            final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
            exec.schedule(new Runnable() {
                @Override
                public void run() {
                    message.setMessageStatus(Status.DELIVERED.getValue());
                    setMessageStatus(message);
                }
            }, 1, TimeUnit.SECONDS);
        }
        if (message.getSelf().equalsIgnoreCase(message.getDestination()) && !message.getMessageStatus().equalsIgnoreCase(Status.SEEN.getValue())) {
            message.setMessageStatus(Status.SEEN.getValue());
            conversationService.updateMessage(message);
            setMessageStatus(message);
        }

        if (!message.getSelf().equals(message.getDestination())) {
            mainLayout.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        if (message.getSelf().equals(message.getDestination())) {
            message.setMessageStatus(Status.SEEN.getValue());
            setMessageStatus(message);
        }
    }

    public void display(Channel channel, final GroupMessage groupMessage, String self) {
        final String timestamp = groupMessage.getTimestamp();
        if (dateTextView != null)
            dateTextView.setText(Utils.getDateString(timestamp));
        messageText.setText(String.format("%s ", groupMessage.getMessage()));
        tvDate.setText(Utils.getTime(timestamp));
        setMessageStatus(groupMessage);
        if (Status.WAITING.getValue().equalsIgnoreCase(groupMessage.getMessageStatus())) {
            groupMessage.setMessageStatus(Status.SENT.getValue());
            groupConversationService.sendMessage(channel, groupMessage);
            setMessageStatus(groupMessage);
            final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
            exec.schedule(new Runnable() {
                @Override
                public void run() {
                    groupMessage.setMessageStatus(Status.DELIVERED.getValue());
                    setMessageStatus(groupMessage);
                }
            }, 1, TimeUnit.SECONDS);
        }
        if (self.equals(groupMessage.getAuthor().getUid())) {
            mainLayout.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        if (!self.equals(groupMessage.getAuthor().getUid())) {
            if (nameInGroup != null) {
                nameInGroup.setVisibility(VISIBLE);
            }
            if (!TextUtils.isEmpty(groupMessage.getAuthor().getShortName())) {
                if (nameInGroupTv != null) {
                    nameInGroupTv.setText(groupMessage.getAuthor().getShortName());
                }
            } else {
                if (nameInGroupTv != null) {
                    nameInGroupTv.setText(groupMessage.getAuthor().getPhone());
                }
            }
        }

    }

    private void setMessageStatus(Message message) {
        if (status != null) {
            if (Status.SEEN.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_read));
            } else if (Status.DELIVERED.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_received));
            } else if (Status.SENT.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_server_receive));
            } else {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_gray_waiting_2));
            }
        }
    }

    private void setMessageStatus(GroupMessage message) {
        if (status != null) {
            if (Status.SEEN.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_read));
            } else if (Status.DELIVERED.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_received));
            } else if (Status.SENT.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_server_receive));
            } else {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_gray_waiting_2));
            }
        }
    }

    public FrameLayout getMainFrame() {
        return mainFrame;
    }
}
