package com.superdev.smiling.conversation.data_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

/**
 * Created by Bounyavong on 10/2/2017.
 */

public class VoiceModel implements Parcelable {

    private String voiceName;
    private String voiceUrl;
    private long voiceSize;
    private String mimeType;
    private int isUploaded;

    @Exclude
    private String voiceLocalUrl;

    public VoiceModel() {

    }

    public VoiceModel(String voiceName, String voiceUrl, long voiceSize, String mimeType, int isUploaded, String voiceLocalUrl) {
        this.voiceName = voiceName;
        this.voiceUrl = voiceUrl;
        this.voiceSize = voiceSize;
        this.mimeType = mimeType;
        this.isUploaded = isUploaded;
        this.voiceLocalUrl = voiceLocalUrl;
    }

    protected VoiceModel(Parcel in) {
        voiceName = in.readString();
        voiceUrl = in.readString();
        voiceSize = in.readLong();
        mimeType = in.readString();
        isUploaded = in.readInt();
        voiceLocalUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(voiceName);
        dest.writeString(voiceUrl);
        dest.writeLong(voiceSize);
        dest.writeString(mimeType);
        dest.writeInt(isUploaded);
        dest.writeString(voiceLocalUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VoiceModel> CREATOR = new Creator<VoiceModel>() {
        @Override
        public VoiceModel createFromParcel(Parcel in) {
            return new VoiceModel(in);
        }

        @Override
        public VoiceModel[] newArray(int size) {
            return new VoiceModel[size];
        }
    };

    public String getVoiceName() {
        return voiceName;
    }

    public void setVoiceName(String voiceName) {
        this.voiceName = voiceName;
    }

    public String getVoiceUrl() {
        return voiceUrl;
    }

    public void setVoiceUrl(String voiceUrl) {
        this.voiceUrl = voiceUrl;
    }

    public long getVoiceSize() {
        return voiceSize;
    }

    public void setVoiceSize(long voiceSize) {
        this.voiceSize = voiceSize;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getVoiceLocalUrl() {
        return voiceLocalUrl;
    }

    public void setVoiceLocalUrl(String voiceLocalUrl) {
        this.voiceLocalUrl = voiceLocalUrl;
    }

    public void setIsUploaded(int isUploaded) {
        this.isUploaded = isUploaded;
    }

    public int getIsUploaded() {
        return isUploaded;
    }
}