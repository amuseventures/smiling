package com.superdev.smiling.conversation.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;

/**
 * Created by marco on 29/07/16.
 */

public class ConversationMapViewHolder extends RecyclerView.ViewHolder {

    private final ConversationMapView conversationMapView;

    public ConversationMapViewHolder(ConversationMapView conversationMapView) {
        super(conversationMapView);
        this.conversationMapView = conversationMapView;
    }

    public void bind(final Message message, final ConversationMapViewListener conversationMapViewListener) {
        conversationMapView.display(message);
        conversationMapView.mainLayout.setOnClickListener(view -> conversationMapViewListener.onClick(view, message));

        conversationMapView.mainLayout.setOnLongClickListener(view -> {
            conversationMapViewListener.onLongClick(view, message);
            return false;
        });
    }

    public void bind(Channel channel, final GroupMessage message,String self, final GroupConversationMapViewListener conversationMapViewListener) {
        conversationMapView.display(channel, message,  self);
        conversationMapView.mainLayout.setOnClickListener(view -> conversationMapViewListener.onClick(view, message));

        conversationMapView.mainLayout.setOnLongClickListener(view -> {
            conversationMapViewListener.onLongClick(view, message);
            return false;
        });
    }

    public interface ConversationMapViewListener {

        void onClick(View view, Message message);

        void onLongClick(View view, Message message);
    }

    public interface GroupConversationMapViewListener {

        void onClick(View view, GroupMessage message);

        void onLongClick(View view, GroupMessage message);
    }

    public FrameLayout getMainFrame() {
        return conversationMapView.getMainFrame();
    }
}
