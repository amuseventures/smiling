package com.superdev.smiling.conversation.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;

/**
 * Created by marco on 29/07/16.
 */

public class ConversationAudioViewHolder extends RecyclerView.ViewHolder {

    private final ConversationAudioView conversationAudioView;

    public ConversationAudioViewHolder(ConversationAudioView conversationAudioView) {
        super(conversationAudioView);
        this.conversationAudioView = conversationAudioView;
    }

    public void bind(final Message message, final ConversationAudioViewListener conversationAudioViewListener) {
        conversationAudioView.display(message);
        conversationAudioView.mainLayout.setOnClickListener(view -> conversationAudioViewListener.onClick(view, message));

        conversationAudioView.mainLayout.setOnLongClickListener(view -> {
            conversationAudioViewListener.onLongClick(view, message);
            return false;
        });
    }

    public interface ConversationAudioViewListener {
        void onClick(View view, Message message);

        void onLongClick(View view, Message message);
    }

    public void bind(Channel channel, final GroupMessage message, String self, final GroupConversationAudioViewListener conversationAudioViewListener) {
        conversationAudioView.display(channel, message, self);
        conversationAudioView.mainLayout.setOnClickListener(view -> conversationAudioViewListener.onClick(view, message));

        conversationAudioView.mainLayout.setOnLongClickListener(view -> {
            conversationAudioViewListener.onLongClick(view, message);
            return false;
        });
    }

    public interface GroupConversationAudioViewListener {
        void onClick(View view, GroupMessage message);

        void onLongClick(View view, GroupMessage message);
    }

    public FrameLayout getMainFrame() {
        return conversationAudioView.getMainFrame();
    }

    public ConversationAudioView getConversationAudioView() {
        return conversationAudioView;
    }
}
