package com.superdev.smiling.conversation.data_model;

import android.os.Parcel;
import android.os.ParcelFormatException;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

/**
 * Created by gautam on 8/17/17.
 */

public class FileModel implements Parcelable {

    private String fileType;

    private String fileUrl;

    private String fileName;

    private long fileSize;
    @Exclude
    private String fileUri;

    private int isUploaded;


    public FileModel() {
    }

    public FileModel(String fileType, String fileUrl, String fileName, long fileSize, String fileUri, int isUploaded) {
        this.fileType = fileType;
        this.fileUrl = fileUrl;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileUri = fileUri;
        this.isUploaded = isUploaded;
    }

    public FileModel(Parcel in) {
        this.fileType = in.readString();
        this.fileUrl = in.readString();
        this.fileName = in.readString();
        this.fileSize = in.readLong();
        this.isUploaded = in.readInt();
    }

    @Exclude
    public String getFileUri() {
        return fileUri;
    }

    @Exclude
    public void setFileUri(String fileUri) {
        this.fileUri = fileUri;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public int getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(int isUploaded) {
        this.isUploaded = isUploaded;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        try {
            parcel.writeString(this.fileType);
            parcel.writeString(this.fileUrl);
            parcel.writeString(this.fileName);
            parcel.writeLong(this.fileSize);
            parcel.writeInt(this.isUploaded);
        } catch (ParcelFormatException e) {
            e.printStackTrace();
        }
    }

    public static final Creator CREATOR = new Creator() {
        public FileModel createFromParcel(Parcel in) {
            return new FileModel(in);
        }

        public FileModel[] newArray(int size) {
            return new FileModel[size];
        }
    };
}
