package com.superdev.smiling.conversation.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;

/**
 * Created by marco on 29/07/16.
 */

public class ConversationMessageViewHolder extends RecyclerView.ViewHolder {

    private final ConversationMessageView conversationMessageView;

    public ConversationMessageViewHolder(ConversationMessageView messageView) {
        super(messageView);
        this.conversationMessageView = messageView;
    }

    public void bind(Message message, ConversationMessageListener conversationMessageListener) {
        conversationMessageView.display(message);

        conversationMessageView.mainLayout.setOnClickListener(view -> conversationMessageListener.onClick(view, message));

        conversationMessageView.mainLayout.setOnLongClickListener(view -> {
            conversationMessageListener.onLongClick(view, message);
            return false;
        });
    }

    public void bind(Channel channel, GroupMessage message,String self,  GroupConversationMessageListener conversationMessageListener) {
        conversationMessageView.display(channel, message, self);

        conversationMessageView.mainLayout.setOnClickListener(view -> conversationMessageListener.onClick(view, message));

        conversationMessageView.mainLayout.setOnLongClickListener(view -> {
            conversationMessageListener.onLongClick(view, message);
            return false;
        });
    }


    public interface ConversationMessageListener {

        void onClick(View view, Message message);

        void onLongClick(View view, Message message);

    }

    public interface GroupConversationMessageListener {

        void onClick(View view, GroupMessage message);

        void onLongClick(View view, GroupMessage message);

    }

    public FrameLayout getMainFrame() {
        return conversationMessageView.getMainFrame();
    }

}
