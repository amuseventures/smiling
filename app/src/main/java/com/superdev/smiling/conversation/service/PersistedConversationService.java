package com.superdev.smiling.conversation.service;

import com.superdev.smiling.conversation.data_model.Chat;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.conversation.database.ConversationDatabase;
import com.superdev.smiling.database.DatabaseResult;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by marco on 29/07/16.
 */

public class PersistedConversationService implements ConversationService {

    private final ConversationDatabase conversationDatabase;

    public PersistedConversationService(ConversationDatabase conversationDatabase) {
        this.conversationDatabase = conversationDatabase;
    }

    @Override
    public Observable<Message> syncMessages(String self, String destination) {
        return conversationDatabase.observeAddMessage(self, destination);
    }

    @Override
    public Observable<DatabaseResult<Chat>> syncMedia(String self, String destination) {
        return conversationDatabase.observeMedia(self, destination)
                .map(DatabaseResult::new)
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    @Override
    public Observable<DatabaseResult<Chat>> syncAllImages(String self, String destination) {
        return conversationDatabase.observeAllMedia(self, destination)
                .map(DatabaseResult::new)
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    @Override
    public Observable<DatabaseResult<Chat>> syncAllDocuments(String self, String destination) {
        return conversationDatabase.observeAllDocuments(self, destination)
                .map(DatabaseResult::new)
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }


    @Override
    public Observable<DatabaseResult<Chat>> getChat(String self, String destination) {
        return conversationDatabase.observeChat(self, destination)
                .map(asDatabaseResult())
                .onErrorReturn(DatabaseResult.<Chat>errorAsDatabaseResult());
    }

    @Override
    public Observable<Boolean> removeChat(String self, String destination, String key) {
        return conversationDatabase.removeChat(self, destination, key);
    }

    private static Func1<Chat, DatabaseResult<Chat>> asDatabaseResult() {
        return DatabaseResult::new;
    }

    @Override
    public void sendMessage(String user, Message message) {
        conversationDatabase.sendMessage(user, message);
    }

    @Override
    public void updateMessage(Message message) {
        conversationDatabase.updateMessage(message);
    }

    @Override
    public Observable<Boolean> getTyping(String self, String destination) {
        return conversationDatabase.observeTyping(self, destination);
    }

    @Override
    public Observable<String> observeMessageStatus(String self, String destination, String messageKey) {
        return conversationDatabase.observeUpdateMessage(self, destination, messageKey);
    }

    @Override
    public void setTyping(String self, String destination, Boolean value) {
        conversationDatabase.setTyping(self, destination, value);
    }
}
