package com.superdev.smiling.conversation.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.ContactModel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.conversation.data_model.Status;
import com.superdev.smiling.conversation.service.ConversationService;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.groupconversation.service.GroupConversationService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by gautam on 9/13/17.
 */

public class ConversationContactView extends FrameLayout {

    @Nullable
    @BindView(R.id.dateTextView)
    TextView dateTextView;

    @Nullable
    @BindView(R.id.name_in_group_tv)
    EmojiconTextView nameInGroupTv;

    @Nullable
    @BindView(R.id.pushname_in_group_tv)
    EmojiconTextView pushnameInGroupTv;

    @Nullable
    @BindView(R.id.name_in_group)
    LinearLayout nameInGroup;

    @Nullable
    @BindView(R.id.quoted_message_holder)
    FrameLayout quotedMessageHolder;

    @BindView(R.id.picture)
    CircleImageView picture;

    @BindView(R.id.vcard_text)
    EmojiconTextView vcardText;

    @BindView(R.id.date_wrapper)
    LinearLayout dateWrapper;

    @BindView(R.id.contact_card)
    LinearLayout contactCard;

    @BindView(R.id.button_div)
    View buttonDiv;

    @BindView(R.id.msg_contact_btn)
    EmojiconTextView msgContactBtn;

    @BindView(R.id.button_vert_div)
    View buttonVertDiv;

    @BindView(R.id.add_contact_btn)
    EmojiconTextView addContactBtn;

    @BindView(R.id.main_layout)
    LinearLayout mainLayout;

    @BindView(R.id.main_frame)
    FrameLayout mainFrame;

    @BindView(R.id.date)
    TextView tvDate;

    @Nullable
    @BindView(R.id.status)
    ImageView status;


    private int layoutResId;


    private ConversationService conversationService;

    private GroupConversationService groupConversationService;


    public ConversationContactView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
        conversationService = Dependencies.INSTANCE.getConversationService();
        groupConversationService = Dependencies.INSTANCE.getGroupConversationService();
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int[] attrsArray = {
                    android.R.attr.layout
            };
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            layoutResId = array.getResourceId(0, R.layout.merge_conversation_contact_item_destination_other_date);
            array.recycle();
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), layoutResId, this);
        ButterKnife.bind(this);
    }

    public FrameLayout getMainFrame() {
        return mainFrame;
    }

    public LinearLayout getMainLayout() {
        return mainLayout;
    }

    public void display(Message message, ConversationContactViewHolder.ContactSelectionListener contactSelectionListener) {
        ContactModel contactModel = message.getContactModel();
        setUpTime(message);
        setMessageStatus(message);
        if (contactModel.getContactImage() != null) {
            Utils.loadImageElseBlack(contactModel.getContactImage(), picture, getContext(),R.drawable.avatar_contact_large);
        } else if (contactModel.getContactUri() != null) {
            try {
                Bitmap bp = MediaStore.Images.Media
                        .getBitmap(getContext().getContentResolver(),
                                Uri.parse(contactModel.getContactUri()));
                picture.setImageBitmap(bp);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.loadImageElseBlack(contactModel.getContactImage(), picture, getContext(),R.drawable.avatar_contact_large);
            }
        } else {
            Utils.loadImageElseBlack(contactModel.getContactImage(), picture, getContext(),R.drawable.avatar_contact_large);
        }
        if (contactModel.getName() != null) {
            vcardText.setText(contactModel.getName());
        } else {
            vcardText.setText(contactModel.getContactNumber());
        }

        if (!message.getSelf().equals(message.getDestination())) {
            mainLayout.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
            if (contactModel.getIsRegistered() != null && contactModel.getIsRegistered().equalsIgnoreCase("1")) {
                msgContactBtn.setVisibility(VISIBLE);
                addContactBtn.setVisibility(VISIBLE);
                buttonVertDiv.setVisibility(VISIBLE);
            } else {
                msgContactBtn.setVisibility(GONE);
                addContactBtn.setVisibility(VISIBLE);
                buttonVertDiv.setVisibility(GONE);
            }
        } else {
            if (contactModel.getIsRegistered() != null && contactModel.getIsRegistered().equalsIgnoreCase("1")) {
                msgContactBtn.setVisibility(VISIBLE);
                addContactBtn.setVisibility(GONE);
                buttonVertDiv.setVisibility(GONE);
                addContactBtn.setText(R.string.send_message_to_contact_button);
            } else {
                msgContactBtn.setVisibility(GONE);
                addContactBtn.setVisibility(VISIBLE);
                buttonVertDiv.setVisibility(VISIBLE);
                addContactBtn.setText(R.string.invite);
            }
        }

        msgContactBtn.setOnClickListener(view -> {
            contactSelectionListener.onMsgBtnClick(message);
            if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
                message.setMessageStatus(Status.SEEN.getValue());
                conversationService.updateMessage(message);
                setMessageStatus(message);
            }

        });

        addContactBtn.setOnClickListener(view -> {
            contactSelectionListener.onAddBtnClick(message);
        });

        if (Status.WAITING.getValue().equalsIgnoreCase(message.getMessageStatus())) {
            conversationService.sendMessage(message.getSelf(), message);
            message.setMessageStatus(Status.SENT.getValue());
            setMessageStatus(message);
            final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
            exec.schedule(new Runnable() {
                @Override
                public void run() {
                    message.setMessageStatus(Status.DELIVERED.getValue());
                }
            }, 1, TimeUnit.SECONDS);
        }

        if (message.getSelf().equals(message.getDestination())) {
            message.setMessageStatus(Status.SEEN.getValue());
            setMessageStatus(message);
        }
    }

    public void display(Channel channel, GroupMessage message, String self, ConversationContactViewHolder.GroupContactSelectionListener contactSelectionListener) {
        ContactModel contactModel = message.getContactModel();
        setUpTime(message);
        setMessageStatus(message);
        if (contactModel.getContactImage() != null) {
            Utils.loadImageElseBlack(contactModel.getContactImage(), picture, getContext(),R.drawable.avatar_contact_large);
        } else if (contactModel.getContactUri() != null) {
            try {
                Bitmap bp = MediaStore.Images.Media
                        .getBitmap(getContext().getContentResolver(),
                                Uri.parse(contactModel.getContactUri()));
                picture.setImageBitmap(bp);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.loadImageElseBlack(contactModel.getContactImage(), picture, getContext(),R.drawable.avatar_contact_large);
            }
        } else {
            Utils.loadImageElseBlack(contactModel.getContactImage(), picture, getContext(),R.drawable.avatar_contact_large);
        }
        if (contactModel.getName() != null) {
            vcardText.setText(contactModel.getName());
        } else {
            vcardText.setText(contactModel.getContactNumber());
        }

        if (self.equals(message.getAuthor().getUid())) {
            mainLayout.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        if (!self.equalsIgnoreCase(message.getAuthor().getUid())) {
            if (contactModel.getIsRegistered() != null && contactModel.getIsRegistered().equalsIgnoreCase("1")) {
                msgContactBtn.setVisibility(VISIBLE);
                addContactBtn.setVisibility(VISIBLE);
                buttonVertDiv.setVisibility(VISIBLE);
            } else {
                msgContactBtn.setVisibility(GONE);
                addContactBtn.setVisibility(VISIBLE);
                buttonVertDiv.setVisibility(GONE);
            }
        } else {
            if (contactModel.getIsRegistered() != null && contactModel.getIsRegistered().equalsIgnoreCase("1")) {
                msgContactBtn.setVisibility(VISIBLE);
                addContactBtn.setVisibility(GONE);
                buttonVertDiv.setVisibility(GONE);
                addContactBtn.setText(R.string.send_message_to_contact_button);
            } else {
                msgContactBtn.setVisibility(GONE);
                addContactBtn.setVisibility(VISIBLE);
                buttonVertDiv.setVisibility(VISIBLE);
                addContactBtn.setText(R.string.invite);
            }
        }

        msgContactBtn.setOnClickListener(view -> {
            contactSelectionListener.onMsgBtnClick(message);
            if (!message.getSelf().equalsIgnoreCase(message.getAuthor().getUid())) {
                message.setMessageStatus(Status.SEEN.getValue());
                //conversationService.updateMessage(message);
                setMessageStatus(message);
            }
        });

        addContactBtn.setOnClickListener(view -> {
            contactSelectionListener.onAddBtnClick(message);
        });

        if (Status.WAITING.getValue().equalsIgnoreCase(message.getMessageStatus())) {
            message.setMessageStatus(Status.SENT.getValue());
            groupConversationService.sendMessage(channel, message);
            setMessageStatus(message);
            final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
            exec.schedule(new Runnable() {
                @Override
                public void run() {
                    message.setMessageStatus(Status.DELIVERED.getValue());
                    setMessageStatus(message);
                }
            }, 1, TimeUnit.SECONDS);
        }

        if (!self.equals(message.getAuthor().getUid())) {
            if (nameInGroup != null) {
                nameInGroup.setVisibility(VISIBLE);
            }
            if (!TextUtils.isEmpty(message.getAuthor().getShortName())) {
                if (nameInGroupTv != null) {
                    nameInGroupTv.setText(message.getAuthor().getShortName());
                }
            } else {
                if (nameInGroupTv != null) {
                    nameInGroupTv.setText(message.getAuthor().getPhone());
                }
            }
        }
    }

    private void setMessageStatus(Message message) {
        if (status != null) {
            if (Status.SEEN.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_read));
            } else if (Status.DELIVERED.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_received));
            } else if (Status.SENT.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_server_receive));
            } else {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_gray_waiting_2));
            }
        }
    }

    private void setMessageStatus(GroupMessage message) {
        if (status != null) {
            if (Status.SEEN.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_read));
            } else if (Status.DELIVERED.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_received));
            } else if (Status.SENT.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_server_receive));
            } else {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_gray_waiting_2));
            }
        }
    }

    private void setUpTime(Message message) {
        final String timestamp = message.getTimestamp();
        String time = Utils.getTime(message.getTimestamp());
        String date = Utils.getDate(message.getTimestamp());
        String today = Utils.getCurrentTimestamp();
        String yesterday = Utils.getYesterdayTimeStamp();

        String[] time1 = date.split("/");
        String[] time2 = today.split("/");
        String[] time3 = yesterday.split("/");
        if ((time1[2] + time1[1] + time1[0]).equals(time2[0] + time2[1] + time2[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_today));
        } else if ((time1[2] + time1[1] + time1[0]).equals(time3[0] + time3[1] + time3[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_yesterday));
        } else {
            if (dateTextView != null)
                dateTextView.setText(Utils.getDateString(timestamp));
        }
        tvDate.setText(time);
    }

    private void setUpTime(GroupMessage message) {
        final String timestamp = message.getTimestamp();
        String time = Utils.getTime(message.getTimestamp());
        String date = Utils.getDate(message.getTimestamp());
        String today = Utils.getCurrentTimestamp();
        String yesterday = Utils.getYesterdayTimeStamp();

        String[] time1 = date.split("/");
        String[] time2 = today.split("/");
        String[] time3 = yesterday.split("/");
        if ((time1[2] + time1[1] + time1[0]).equals(time2[0] + time2[1] + time2[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_today));
        } else if ((time1[2] + time1[1] + time1[0]).equals(time3[0] + time3[1] + time3[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_yesterday));
        } else {
            if (dateTextView != null)
                dateTextView.setText(Utils.getDateString(timestamp));
        }
        tvDate.setText(time);
    }
}
