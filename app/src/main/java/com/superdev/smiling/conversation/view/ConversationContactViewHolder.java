package com.superdev.smiling.conversation.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;

/**
 * Created by gautam on 9/14/17.
 */

public class ConversationContactViewHolder extends RecyclerView.ViewHolder {

    private ConversationContactView contactView;

    public ConversationContactViewHolder(ConversationContactView itemView) {
        super(itemView);
        this.contactView = itemView;
    }

    public void bind(final Message message, final ContactSelectionListener contactSelectionListener) {
        contactView.display(message, contactSelectionListener);
        contactView.mainLayout.setOnClickListener(view -> contactSelectionListener.onClick(view, message));

        contactView.mainLayout.setOnLongClickListener(view -> {
            contactSelectionListener.onLongClick(view, message);
            return false;
        });
    }

    public void bind(Channel channel, final GroupMessage message,String self, final GroupContactSelectionListener contactSelectionListener) {
        contactView.display(channel, message,self, contactSelectionListener);
        contactView.mainLayout.setOnClickListener(view -> contactSelectionListener.onClick(view, message));

        contactView.mainLayout.setOnLongClickListener(view -> {
            contactSelectionListener.onLongClick(view, message);
            return false;
        });
    }

    public interface ContactSelectionListener {

        void onClick(View view, Message message);

        void onLongClick(View view, Message message);

        void onAddBtnClick(Message message);

        void onMsgBtnClick(Message message);
    }

    public interface GroupContactSelectionListener {

        void onClick(View view, GroupMessage message);

        void onLongClick(View view, GroupMessage message);

        void onAddBtnClick(GroupMessage message);

        void onMsgBtnClick(GroupMessage message);
    }

    public FrameLayout getMainFrame() {
        return contactView.getMainFrame();
    }
}
