package com.superdev.smiling.conversation.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.conversation.data_model.Status;
import com.superdev.smiling.conversation.service.ConversationService;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.groupconversation.service.GroupConversationService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by gautam on 9/5/17.
 */

public class ConversationMapView extends LinearLayout {

    @Nullable
    @BindView(R.id.dateTextView)
    TextView dateTextView;

    @BindView(R.id.map_image)
    SimpleDraweeView mapImage;

    @BindView(R.id.tv_address)
    TextView tvAddress;

    @BindView(R.id.main_layout)
    LinearLayout mainLayout;

    @BindView(R.id.main_frame)
    FrameLayout mainFrame;

    @BindView(R.id.date)
    TextView tvDate;

    @Nullable
    @BindView(R.id.status)
    ImageView status;

    @Nullable
    @BindView(R.id.name_in_group_tv)
    EmojiconTextView nameInGroupTv;

    @Nullable
    @BindView(R.id.pushname_in_group_tv)
    EmojiconTextView pushnameInGroupTv;

    @Nullable
    @BindView(R.id.name_in_group)
    LinearLayout nameInGroup;

    private int layoutResId;

    private ConversationService conversationService;

    private GroupConversationService groupConversationService;

    public ConversationMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
        conversationService = Dependencies.INSTANCE.getConversationService();
        groupConversationService = Dependencies.INSTANCE.getGroupConversationService();
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int[] attrsArray = {
                    android.R.attr.layout
            };
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            layoutResId = array.getResourceId(0, R.layout.merge_conversation_map_item_destination);
            array.recycle();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), layoutResId, this);
        ButterKnife.bind(this);
    }

    public void display(final Message message) {
        setTime(message);
        setMessageStatus(message);
        tvAddress.setText(message.getMapModel().getAddress());
        try {
            ImageRequest imgReq = ImageRequestBuilder.newBuilderWithSource(Uri.parse(Utils.localMap(message.getMapModel().getLatitude(), message.getMapModel().getLongitude())))
                    .setProgressiveRenderingEnabled(false)
                    .build();
            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(imgReq)
                    .setOldController(mapImage.getController())
                    .build();
            RoundingParams roundingParams = RoundingParams.fromCornersRadius(10f);
            mapImage.getHierarchy().setRoundingParams(roundingParams);
            mapImage.setController(controller);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Status.WAITING.getValue().equalsIgnoreCase(message.getMessageStatus())) {
            message.setMessageStatus(Status.SENT.getValue());
            conversationService.sendMessage(message.getSelf(), message);
            setMessageStatus(message);
            final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
            exec.schedule(new Runnable() {
                @Override
                public void run() {
                    message.setMessageStatus(Status.DELIVERED.getValue());
                    setMessageStatus(message);
                }
            }, 1, TimeUnit.SECONDS);
        }

        if (!message.getSelf().equals(message.getDestination())) {
            mainLayout.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        if (message.getSelf().equals(message.getDestination())) {
            message.setMessageStatus(Status.SEEN.getValue());
            setMessageStatus(message);
            
        }
    }

    public void display(Channel channel, final GroupMessage message, String self) {
        setTime(message);
        setMessageStatus(message);
        tvAddress.setText(message.getMapModel().getAddress());
        try {
            ImageRequest imgReq = ImageRequestBuilder.newBuilderWithSource(Uri.parse(Utils.localMap(message.getMapModel().getLatitude(), message.getMapModel().getLongitude())))
                    .setProgressiveRenderingEnabled(false)
                    .build();
            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(imgReq)
                    .setOldController(mapImage.getController())
                    .build();
            RoundingParams roundingParams = RoundingParams.fromCornersRadius(10f);
            mapImage.getHierarchy().setRoundingParams(roundingParams);
            mapImage.setController(controller);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Status.WAITING.getValue().equalsIgnoreCase(message.getMessageStatus())) {
            message.setMessageStatus(Status.SENT.getValue());
            groupConversationService.sendMessage(channel, message);
            setMessageStatus(message);
            final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
            exec.schedule(new Runnable() {
                @Override
                public void run() {
                    message.setMessageStatus(Status.DELIVERED.getValue());
                    setMessageStatus(message);
                }
            }, 1, TimeUnit.SECONDS);
        }
        if (self.equals(message.getAuthor().getUid())) {
            mainLayout.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        if ( ! self.equals(message.getAuthor().getUid())) {
            if (nameInGroup != null) {
                nameInGroup.setVisibility(VISIBLE);
            }
            if (!TextUtils.isEmpty(message.getAuthor().getShortName())) {
                if (nameInGroupTv != null) {
                    nameInGroupTv.setText(message.getAuthor().getShortName());
                }
            } else {
                if (nameInGroupTv != null) {
                    nameInGroupTv.setText(message.getAuthor().getPhone());
                }
            }
        }
    }

    private void setTime(Message message) {
        final String timestamp = message.getTimestamp();
        String time = Utils.getTime(message.getTimestamp());
        String date = Utils.getDate(message.getTimestamp());
        String today = Utils.getCurrentTimestamp();
        String yesterday = Utils.getYesterdayTimeStamp();

        String[] time1 = date.split("/");
        String[] time2 = today.split("/");
        String[] time3 = yesterday.split("/");
        if ((time1[2] + time1[1] + time1[0]).equals(time2[0] + time2[1] + time2[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_today));
        } else if ((time1[2] + time1[1] + time1[0]).equals(time3[0] + time3[1] + time3[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_yesterday));
        } else {
            if (dateTextView != null)
                dateTextView.setText(Utils.getDateString(timestamp));
        }
        tvDate.setText(time);
    }

    private void setTime(GroupMessage message) {
        final String timestamp = message.getTimestamp();
        String time = Utils.getTime(message.getTimestamp());
        String date = Utils.getDate(message.getTimestamp());
        String today = Utils.getCurrentTimestamp();
        String yesterday = Utils.getYesterdayTimeStamp();

        String[] time1 = date.split("/");
        String[] time2 = today.split("/");
        String[] time3 = yesterday.split("/");
        if ((time1[2] + time1[1] + time1[0]).equals(time2[0] + time2[1] + time2[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_today));
        } else if ((time1[2] + time1[1] + time1[0]).equals(time3[0] + time3[1] + time3[2])) {
            if (dateTextView != null)
                dateTextView.setText(getContext().getString(R.string.conversations_conversation_item_yesterday));
        } else {
            if (dateTextView != null)
                dateTextView.setText(Utils.getDateString(timestamp));
        }
        tvDate.setText(time);
    }

    private void setMessageStatus(Message message) {
        if (status != null) {
            if (Status.SEEN.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_read));
            } else if (Status.DELIVERED.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_received));
            } else if (Status.SENT.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_server_receive));
            } else {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_gray_waiting_2));
            }
        }
    }

    private void setMessageStatus(GroupMessage message) {
        if (status != null) {
            if (Status.SEEN.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_read));
            } else if (Status.DELIVERED.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_client_received));
            } else if (Status.SENT.getValue().equalsIgnoreCase(message.getMessageStatus())) {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_server_receive));
            } else {
                status.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.msg_status_gray_waiting_2));
            }
        }
    }

    public FrameLayout getMainFrame() {
        return mainFrame;
    }
}
