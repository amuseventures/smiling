package com.superdev.smiling.conversation.presenter;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.webkit.MimeTypeMap;

import com.google.android.gms.common.api.GoogleApiClient;
import com.superdev.smiling.MimeTypeEnum;
import com.superdev.smiling.Utils;
import com.superdev.smiling.conversation.data_model.AudioModel;
import com.superdev.smiling.conversation.data_model.ContactModel;
import com.superdev.smiling.conversation.data_model.DocModel;
import com.superdev.smiling.conversation.data_model.FileModel;
import com.superdev.smiling.conversation.data_model.MapModel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.conversation.data_model.Status;
import com.superdev.smiling.conversation.data_model.VideoModel;
import com.superdev.smiling.conversation.data_model.VoiceModel;
import com.superdev.smiling.conversation.service.ConversationService;
import com.superdev.smiling.conversation.view.ConversationDisplayer;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.link.LinkFactory;
import com.superdev.smiling.navigation.AndroidConversationViewNavigator;
import com.superdev.smiling.navigation.ConversationNavigator;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.service.UserService;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;

import static com.superdev.smiling.Constants.DESTINATION_ID;
import static com.superdev.smiling.Constants.MAIN_DIRECTORY;
import static com.superdev.smiling.Constants.SENDER_ID;
import static com.superdev.smiling.Constants.SMILE_AUDIO;
import static com.superdev.smiling.Constants.SMILE_DOCUMENTS;
import static com.superdev.smiling.Constants.SMILE_MEDIA_DIRECTORY;
import static com.superdev.smiling.Constants.SMILE_SENT;

/**
 * s
 * Created by marco on 29/07/16.
 */

public class ConversationPresenter {

    private final ConversationService conversationService;

    private final ConversationDisplayer conversationDisplayer;

    private final UserService userService;

    private final String self;

    private final String destination;

    private final AndroidConversationViewNavigator navigator;

    private Subscription subscription;

    private Subscription chatSubscription;

    private Subscription typingSubscription;

    private ArrayList<Subscription> messageStatusSubcriptionList;

    private GoogleApiClient mGoogleApiClient;
    private LinkFactory linkFactory;

    private User destinationUser;

    public ConversationPresenter(
            LinkFactory linkFactory,
            ConversationService conversationService,
            ConversationDisplayer conversationDisplayer,
            UserService userService,
            String self,
            String destination,
            AndroidConversationViewNavigator navigator,
            GoogleApiClient mGoogleApiClient
    ) {
        this.linkFactory = linkFactory;
        this.conversationService = conversationService;
        this.conversationDisplayer = conversationDisplayer;
        this.userService = userService;
        this.self = self;
        this.destination = destination;
        this.navigator = navigator;
        this.mGoogleApiClient = mGoogleApiClient;
    }

    public void startPresenting() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        navigator.attach(onImageSelectedListener);
        conversationDisplayer.attach(actionListener);
        conversationDisplayer.disableInteraction();
        conversationDisplayer.setDestination(destination);
        Subscriber conversationSubscriber = new Subscriber<DatabaseResult<User>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onNext(DatabaseResult<User> userDatabaseResult) {
                if (!userDatabaseResult.isSuccess())
                    return;
                User user = userDatabaseResult.getData();
                conversationDisplayer.setupToolbar(user.getName(), user.getImage(), user.getLastSeen());
                destinationUser = user;
                chatSubscription = conversationService.syncMessages(self, destination)
                        .subscribe(new Subscriber<Message>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onNext(Message message) {
                                message.setMessageStatus(Status.DELIVERED.getValue());
                                conversationDisplayer.addToDisplay(message, self);
                                if (messageStatusSubcriptionList == null) {
                                    messageStatusSubcriptionList = new ArrayList<>();
                                }
                                Subscription subscription = conversationService.observeMessageStatus(self, destination, message.getMessageKey())
                                        .subscribe(new Subscriber<String>() {
                                            @Override
                                            public void onCompleted() {

                                            }

                                            @Override
                                            public void onError(Throwable throwable) {

                                            }

                                            @Override
                                            public void onNext(String key) {
                                                if (key.equalsIgnoreCase(Status.SEEN.getValue())) {
                                                    conversationDisplayer.updateMessageState(key, message.getMessageKey());
                                                }
                                            }
                                        });
                                messageStatusSubcriptionList.add(subscription);
                            }

                        });
            }
        };

        subscription = userService.getUser(destination)
                .subscribe(conversationSubscriber);

        typingSubscription = conversationService.getTyping(self, destination)
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        conversationService.setTyping(self, destination, false);
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean)
                            conversationDisplayer.showTyping();
                        else
                            conversationDisplayer.hideTyping();
                    }
                });
    }


    public void stopPresenting() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        conversationDisplayer.detach(actionListener);
        conversationService.setTyping(self, destination, false);
        subscription.unsubscribe();
        if (typingSubscription != null)
            typingSubscription.unsubscribe();
        if (chatSubscription != null)
            chatSubscription.unsubscribe();
        if (messageStatusSubcriptionList != null) {
            for (int i = 0; i < messageStatusSubcriptionList.size(); i++) {
                messageStatusSubcriptionList.get(i).unsubscribe();
                messageStatusSubcriptionList.clear();
            }
        }
    }

    public void detach() {
        navigator.detach(onImageSelectedListener);
    }

    private boolean userIsAuthenticated() {
        return self != null;
    }

    private final ConversationDisplayer.ConversationActionListener actionListener = new ConversationDisplayer.ConversationActionListener() {

        @Override
        public void onUpPressed() {
            navigator.toParent();
        }

        @Override
        public void onMessageLengthChanged(int messageLength) {
            if (userIsAuthenticated() && messageLength > 0) {
                conversationDisplayer.enableInteraction();
                conversationService.setTyping(self, destination, true);
            } else {
                conversationDisplayer.disableInteraction();
                conversationService.setTyping(self, destination, false);
            }
        }

        @Override
        public void onSubmitMessage(String message) {
            conversationDisplayer.addToDisplay(new Message(self, destination, message, "sms", Status.WAITING.getValue()), self);
        }

        @Override
        public void onSubmitVoice(File audioFile) {
            String fileExtension
                    = MimeTypeMap.getFileExtensionFromUrl(audioFile.getPath());
            final String mimeType
                    = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
            VoiceModel voiceModel = new VoiceModel(audioFile.getName(), null, audioFile.length(), mimeType, 0, audioFile.getPath());
            conversationDisplayer.addToDisplay(new Message(self, destination, "", voiceModel, MimeTypeEnum.VOICE.getValue(), Status.WAITING.getValue()), self);
        }

        @Override
        public void onDocumentsPressed() {
            navigator.onDocumentsPressed();
        }

        @Override
        public void onCameraPressed() {
            navigator.onCameraPressed();
        }

        @Override
        public void onGalleryPressed() {
            navigator.onGalleryPressed();
        }

        @Override
        public void onAudioPressed() {
            navigator.onAudioPressed();
        }

        @Override
        public void onLocationPressed() {
            navigator.onLocationPressed();
        }

        @Override
        public void onContactPressed() {
            navigator.onContactPressed();
        }

        @Override
        public void onMessageSelected(View view, Message message) {
        }

        @Override
        public void onImageSelected(View view, Message message) {
            navigator.toFullScreenImageActivity(view, message);
        }

        @Override
        public void onVideoSelected(View view, Message message) {

        }

        @Override
        public void onMapViewSelected(Message message) {
            navigator.toOpenGoogleMaps(message);
        }

        @Override
        public void onDocViewSelected(Message message) {
            navigator.openDocument(message);
        }

        @Override
        public void showCabMenu(List<Message> messagesList) {
            navigator.showCabMenu(destinationUser, messagesList);
        }

        @Override
        public void hideCabMenu() {
            navigator.hideMenu();
        }

        @Override
        public void onContactAddBtn(Message message) {
            if (message.getContactModel() != null && message.getContactModel().getIsRegistered() != null &&
                    message.getContactModel().getIsRegistered().equalsIgnoreCase("1")) {
                navigator.toAddContact(message.getContactModel());
            } else {
                navigator.toSmsActivity(linkFactory.inviteLinkFrom(self).toString(), message.getContactModel());
            }
        }

        @Override
        public void onContactMsgSelected(Message message) {
            if (message.getContactModel() != null && message.getContactModel().getIsRegistered() != null &&
                    message.getContactModel().getIsRegistered().equalsIgnoreCase("1")) {
                Bundle bundle = new Bundle();
                bundle.putString(SENDER_ID, self);
                bundle.putString(DESTINATION_ID, message.getContactModel().getUserId());
                navigator.toSelectedConversation(bundle);
            } else {
                navigator.toSmsActivity(linkFactory.inviteLinkFrom(self).toString(), message.getContactModel());
            }
        }

        @Override
        public void showAttachment() {
            navigator.showBottomSheet();
        }
    };

    private final AndroidConversationViewNavigator.OnImageSelectedListener onImageSelectedListener = new ConversationNavigator.OnImageSelectedListener() {
        @Override
        public void onImageSelected(Uri photoURI, String message) {
            String fileExtension
                    = MimeTypeMap.getFileExtensionFromUrl(photoURI.toString());
            final String mimeType
                    = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
            if (!mimeType.contains(MimeTypeEnum.VIDEO.getValue())) {
                File file = new File(photoURI.getPath());
                FileModel fileModel = new FileModel(mimeType, null, file.getName(), file.length(), photoURI.toString(), 0);
                conversationDisplayer.addToDisplay(new Message(self, destination, message, fileModel, MimeTypeEnum.IMAGE.getValue(), Status.WAITING.getValue()), self);
            } else {
                File file = new File(photoURI.getPath());
                VideoModel videoModel = new VideoModel(mimeType, null, file.getName(), file.length(), photoURI.toString(), 0);
                conversationDisplayer.addToDisplay(new Message(self, destination, message, videoModel, MimeTypeEnum.VIDEO.getValue(), Status.WAITING.getValue()), self);
            }
        }

        @Override
        public void onMapSelected(MapModel mapModel) {
            conversationDisplayer.addToDisplay(new Message(self, destination, "", mapModel, MimeTypeEnum.LOCATION.getValue(), Status.WAITING.getValue()), self);
        }

        @Override
        public void onDocSelected(DocModel docModel) {
            File oldFile = new File(docModel.getDocLocalUrl());
            File newFile = new File(getDocumentFilename(oldFile));
            Utils.writeFile(oldFile, newFile);
            docModel.setDocLocalUrl(newFile.getPath());
            docModel.setDocName(newFile.getName());
            conversationDisplayer.addToDisplay(new Message(self, destination, "", docModel, MimeTypeEnum.DOCUMENT.getValue(), Status.WAITING.getValue()), self);
        }


        @SuppressWarnings("ALL")
        @Override
        public void onAudioSelected(AudioModel audioModel) {
            File oldFile = new File(audioModel.getAudioLocalUrl());
            File newFile = new File(getAudioFilename(oldFile));
            Utils.writeFile(oldFile, newFile);
            audioModel.setAudioLocalUrl(newFile.getPath());
            audioModel.setAudioName(newFile.getName());
            conversationDisplayer.addToDisplay(new Message(self, destination, "", audioModel, MimeTypeEnum.AUDIO.getValue(), Status.WAITING.getValue()), self);
        }

        @Override
        public void onContactSelected(ContactModel contactModel) {
            conversationDisplayer.addToDisplay(new Message(self, destination, "", contactModel, MimeTypeEnum.CONTACT.getValue(), Status.WAITING.getValue()), self);
        }

        @Override
        public void onCabFinished() {
            conversationDisplayer.clearSelectedItem();
        }

        @Override
        public void onMessageDeleted(boolean deleteMedia, Message message) {
            if (deleteMedia)
                Utils.deleteFile(message);
            conversationDisplayer.onMessageDeleted(message);
        }
    };


    private String getDocumentFilename(File file) {
        String path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_DOCUMENTS, SMILE_SENT);
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMddhhmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "DOC-" + timeStamp + "." + droidninja.filepicker.utils.Utils.getFileExtension(file));
        return mediaFile.getAbsolutePath();
    }

    private String getAudioFilename(File file) {
        String path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_AUDIO, SMILE_SENT);
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMddhhmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "AUD-" + timeStamp + "." + droidninja.filepicker.utils.Utils.getFileExtension(file));
        return mediaFile.getAbsolutePath();
    }
}
