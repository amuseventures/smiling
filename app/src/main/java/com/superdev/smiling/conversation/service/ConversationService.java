package com.superdev.smiling.conversation.service;

import com.superdev.smiling.conversation.data_model.Chat;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.database.DatabaseResult;

import rx.Observable;

/**
 * Created by marco on 29/07/16.
 */

public interface ConversationService {

    Observable<Message> syncMessages(String self, String destination);

    Observable<DatabaseResult<Chat>> syncMedia(String self, String destination);

    Observable<DatabaseResult<Chat>> syncAllImages(String self, String destination);

    Observable<DatabaseResult<Chat>> syncAllDocuments(String self, String destination);

    Observable<String> observeMessageStatus(String self, String destination, String messageKey);

    Observable<DatabaseResult<Chat>> getChat(String self, String destination);

    Observable<Boolean> removeChat(String self, String destination, String key);

    void sendMessage(String user, Message message);

    Observable<Boolean> getTyping(String self, String destination);

    void setTyping(String self, String destination, Boolean value);

    void updateMessage(Message message);
}


