package com.superdev.smiling.conversation.data_model;

public enum Status {

    WAITING("WAITING"),

    SENT("SENT"),

    DELIVERED("DELIVERED"),

    SEEN("SEEN");

    private String value;

    Status(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
