package com.superdev.smiling.conversation.view;

import android.content.Context;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.conversation.data_model.Chat;
import com.superdev.smiling.conversation.data_model.Message;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by marco on 29/07/16.
 */

public class ConversationView extends CoordinatorLayout implements ConversationDisplayer {

    private final ConversationMessageAdapter conversationMessageAdapter;

    @BindView(R.id.profileImageView)
    CircleImageView profileImageView;

    @BindView(R.id.nameTextView)
    EmojiconTextView nameTextView;

    @BindView(R.id.lastSeenTextView)
    EmojiconTextView lastSeenTextView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.cab_stub)
    ViewStub cabStub;

    @BindView(R.id.messageRecyclerView)
    RecyclerView messageRecyclerView;

    @BindView(R.id.iv_smiley)
    ImageView ivSmiley;

    @BindView(R.id.messageEditText)
    EmojiconEditText messageEditText;

    @BindView(R.id.iv_attach)
    ImageView ivAttach;

    @BindView(R.id.compose_layout)
    LinearLayout composeLayout;

    @BindView(R.id.sendButton)
    ImageButton sendButton;

    @BindView(R.id.recordButton)
    ImageButton recordButton;

    @BindView(R.id.typingTextView)
    TextView typingTextView;

    @BindView(R.id.conversation)
    RelativeLayout conversation;

    @BindView(R.id.messageLayout)
    LinearLayout messageLayout;

    @BindView(R.id.audioLayout)
    RelativeLayout audioLayout;

    @BindView(R.id.timeTextView)
    TextView timeTextView;

    private boolean hidden = true;

    private static ConversationActionListener actionListener;

    private String destination;

    private boolean bState = true;

    private boolean isSpeakButtonLongPressed = false;

    private boolean recordStarted = false;

    private MediaRecorder mediaRecorder;

    private File audioFile;

    private int sec_count = 0;

    private CountDownTimer countDownTimer;

    public ConversationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        //setOrientation(VERTICAL);
        conversationMessageAdapter = new ConversationMessageAdapter(context,LayoutInflater.from(context));
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_conversation_view, this);
        ButterKnife.bind(this);
        View rootView = this.getRootView();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setStackFromEnd(true);
        messageRecyclerView.setLayoutManager(layoutManager);
        messageRecyclerView.setAdapter(conversationMessageAdapter);
        composeLayout = findViewById(R.id.compose_layout);
        ivAttach.setOnClickListener(attachmentListener);

        EmojIconActions emojIcon = new EmojIconActions(getContext(), rootView, messageEditText, ivSmiley);
        emojIcon.ShowEmojIcon();
    }

    @Override
    public void display(Chat chat, String user) {
        conversationMessageAdapter.update(chat, user);
        int lastMessagePosition = conversationMessageAdapter.getItemCount() == 0 ? 0 : conversationMessageAdapter.getItemCount() - 1;
        messageRecyclerView.scrollToPosition(lastMessagePosition);
    }

    @Override
    public void addToDisplay(Message message, String user) {
        conversationMessageAdapter.add(message, user);
        int lastMessagePosition = conversationMessageAdapter.getItemCount() == 0 ? 0 : conversationMessageAdapter.getItemCount() - 1;
        messageRecyclerView.scrollToPosition(lastMessagePosition);
    }

    @Override
    public void updateMessageState(String messageStatus, String messageKey) {
        conversationMessageAdapter.updateMessageStatus(messageStatus, messageKey);
    }

    @Override
    public void setupToolbar(String user, String image, long lastSeen) {

        Utils.loadImageElseWhite(image, profileImageView, getContext(),R.drawable.avatar_contact_large);
        nameTextView.setText(user);
        if (lastSeen == 0)
            lastSeenTextView.setText(R.string.chat_toolbar_lastseen_online);
        else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/HH/mm", Locale.US);
            Date resultdate = new Date(lastSeen);
            String timestamp = sdf.format(resultdate);
            String today = Utils.getCurrentTimestamp();
            String[] time1 = timestamp.split("/");
            String[] time2 = today.split("/");
            if ((time1[0] + time1[1] + time1[2]).equals(time2[0] + time2[1] + time2[2])) {
                String s = getResources().getString(R.string.chat_toolbar_lastseen_offline_today);
                lastSeenTextView.setText(s.replace("time", time1[3] + ":" + time1[4]));
            } else {
                String s = getResources().getString(R.string.chat_toolbar_lastseen_offline);
                s = s.replace("date", time1[2] + "/" + time1[1]);
                s = s.replace("time", time1[3] + ":" + time1[4]);
                lastSeenTextView.setText(s);
            }
        }
        typingTextView.setText(String.format("%s %s", user, getResources().getString(R.string.chat_textview_typing)));
    }

    @Override
    public void showTyping() {
        typingTextView.setVisibility(VISIBLE);
    }

    @Override
    public void hideTyping() {
        typingTextView.setVisibility(GONE);
    }

    @Override
    public void attach(ConversationActionListener conversationInteractionListener) {
        actionListener = conversationInteractionListener;
        messageEditText.addTextChangedListener(textWatcher);
        sendButton.setOnClickListener(submitClickListener);
        recordButton.setOnLongClickListener(audioLongClickListener);
        recordButton.setOnTouchListener(audioButtonReleaseListener);
        toolbar.setNavigationOnClickListener(navigationClickListener);
        conversationMessageAdapter.attach(conversationInteractionListener);
    }

    @Override
    public void detach(ConversationActionListener conversationInteractionListener) {
        sendButton.setOnClickListener(null);
        messageEditText.removeTextChangedListener(textWatcher);
        toolbar.setOnMenuItemClickListener(null);
        actionListener = null;
        conversationMessageAdapter.detach(conversationInteractionListener);
    }

    @Override
    public void enableInteraction() {
        sendButton.setEnabled(true);
    }

    @Override
    public void disableInteraction() {
        sendButton.setEnabled(false);
    }


    @Override
    public void clearSelectedItem() {
        conversationMessageAdapter.getSelectedMessagesList().clear();
        conversationMessageAdapter.notifyDataSetChanged();
    }

    @Override
    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Override
    public void onMessageDeleted(Message message) {
        conversationMessageAdapter.removeData(message);
    }

    @Override
    public void inflateMenu(MenuInflater menuInflater, Menu menu) {
        menuInflater.inflate(R.menu.main, menu);
    }

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(s.toString().trim().length() != 0) {
                sendButton.setVisibility(VISIBLE);
                recordButton.setVisibility(GONE);
                bState = false;
            } else {
                sendButton.setVisibility(GONE);
                recordButton.setVisibility(VISIBLE);
                bState = true;
            }
            if(actionListener == null)
                return;
            actionListener.onMessageLengthChanged(s.toString().trim().length());
        }
    };

    private final OnClickListener submitClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            actionListener.onSubmitMessage(messageEditText.getText().toString().trim());
            messageEditText.setText("");
        }
    };

    private final OnLongClickListener audioLongClickListener = view -> {
        if( bState && ! isSpeakButtonLongPressed) {
            isSpeakButtonLongPressed = true;
            final Animation animShow;
            animShow = AnimationUtils.loadAnimation( getContext(), R.anim.slide_in_left);
            audioLayout.startAnimation(animShow);
            audioLayout.setVisibility(VISIBLE);
            messageLayout.setVisibility(GONE);
            startRecording();
        }
        return false;
    };

    private void startRecording() {
        if(recordStarted)
            return;
        if(Utils.getAudioPath() == null)
            return;
        File dir = new File(Utils.getVoiceSentPath());
        if( ! dir.mkdirs()) {
            Log.d("smiling", "Directory is already exist");
        }
        audioFile = null;
        try {
            mediaRecorder = new MediaRecorder();
            audioFile = File.createTempFile("record", ".mp3", dir);
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setAudioSamplingRate(8000);
            mediaRecorder.setAudioEncodingBitRate(12200);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mediaRecorder.setOutputFile(audioFile.getAbsolutePath());
            mediaRecorder.prepare();
            mediaRecorder.start();
            recordStarted = true;
            setTimer();
        } catch (Exception e) {
            e.printStackTrace();
            if (audioFile != null) audioFile.delete();
        }
    }

    private void stopRecording(boolean bSend) {
        if(mediaRecorder == null && ! recordStarted) {
            isSpeakButtonLongPressed = false;
            return;
        }
        try {
            mediaRecorder.stop();
            mediaRecorder.reset();
            mediaRecorder.release();
        } catch (RuntimeException e) {
            e.printStackTrace();
            if(audioFile != null && audioFile.exists())
                audioFile.delete();
            final Animation animShow;
            animShow = AnimationUtils.loadAnimation( getContext(), R.anim.slide_in_right);
            messageLayout.startAnimation(animShow);
            messageLayout.setVisibility(VISIBLE);
            audioLayout.setVisibility(GONE);
            isSpeakButtonLongPressed = false;
            recordStarted = false;
            if(countDownTimer != null)
                countDownTimer.cancel();
        }
        if(bSend && actionListener != null && audioFile != null && audioFile.exists()) {
            actionListener.onSubmitVoice(audioFile);
        } else {
            Toast.makeText(getContext(), "Voice message is removed.", Toast.LENGTH_LONG).show();
            if(audioFile != null && audioFile.exists())
                audioFile.delete();
        }
        final Animation animShow;
        animShow = AnimationUtils.loadAnimation( getContext(), R.anim.slide_in_right);
        messageLayout.startAnimation(animShow);
        messageLayout.setVisibility(VISIBLE);
        audioLayout.setVisibility(GONE);
        isSpeakButtonLongPressed = false;
        recordStarted = false;
    }

    private void setTimer() {
        sec_count = 0;
        if(countDownTimer != null)
            countDownTimer.cancel();
        countDownTimer =  new CountDownTimer(7000000, 1000) {
            public void onTick(long millisUntilFinished) {
                sec_count ++;
                int min = sec_count / 60;
                int sec = sec_count - min * 60;
                timeTextView.setText(String.format("%02d : %02d", min, sec));
            }

            public void onFinish() {
            }
        }.start();
    }

    float dX, dY, originX, originY;

    private final OnTouchListener audioButtonReleaseListener = (view, motionEvent) -> {
        view.onTouchEvent(motionEvent);
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            if (isSpeakButtonLongPressed) {
                stopRecording(true);
                view.animate()
                        .x(originX)
                        .y(originY)
                        .setDuration(0)
                        .start();
            }
        } else if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            dX = view.getX() - motionEvent.getRawX();
            dY = view.getY() - motionEvent.getRawY();
            originX = view.getX();
            originY = 18;
        } else if(motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
            if(isSpeakButtonLongPressed) {
                view.animate()
                        .x(motionEvent.getRawX() + dX)
                        .y(originY)
                        .setDuration(0)
                        .start();
                if(motionEvent.getRawX() + dX < 100) {
                    if (isSpeakButtonLongPressed) {
                        stopRecording(false);
                        view.animate()
                                .x(originX)
                                .y(originY)
                                .setDuration(0)
                                .start();
                    }
                }
            }
        }
        return false;
    };

    private final OnClickListener attachmentListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            actionListener.showAttachment();
        }
    };


    private final OnClickListener navigationClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if(actionListener != null)
                actionListener.onUpPressed();
        }
    };

    public static class BottomDialog extends BottomSheetDialogFragment {

        @BindView(R.id.documents_img_btn)
        ImageButton documentsImgBtn;

        @BindView(R.id.tv_documents)
        TextView tvDocuments;

        @BindView(R.id.ll_documents)
        LinearLayout llDocuments;

        @BindView(R.id.camera_img_btn)
        ImageButton cameraImgBtn;

        @BindView(R.id.tv_camera)
        TextView tvCamera;

        @BindView(R.id.ll_camera)
        LinearLayout llCamera;

        @BindView(R.id.gallery_img_btn)
        ImageButton galleryImgBtn;

        @BindView(R.id.tv_gallery)
        TextView tvGallery;

        @BindView(R.id.ll_gallery)
        LinearLayout llGallery;

        @BindView(R.id.audio_img_button)
        ImageButton audioImgButton;

        @BindView(R.id.tv_audio)
        TextView tvAudio;

        @BindView(R.id.ll_audio)
        LinearLayout llAudio;

        @BindView(R.id.location_img_btn)
        ImageButton locationImgBtn;

        @BindView(R.id.tv_location)
        TextView tvLocation;

        @BindView(R.id.ll_location)
        LinearLayout llLocation;

        @BindView(R.id.contact_img_btn)
        ImageButton contactImgBtn;

        @BindView(R.id.tv_contact)
        TextView tvContact;

        @BindView(R.id.ll_contact)
        LinearLayout llContact;

        @BindView(R.id.reveal_items)
        LinearLayout revealItems;

        Unbinder unbinder;

        public static BottomDialog newInstance() {
            Bundle args = new Bundle();
            BottomDialog fragment = new BottomDialog();
            fragment.setArguments(args);
            return fragment;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.media_attach_menu, container, false);
            unbinder = ButterKnife.bind(this, view);
            llDocuments.setOnClickListener(imgButtonListener);
            llCamera.setOnClickListener(imgButtonListener);
            llGallery.setOnClickListener(imgButtonListener);
            llAudio.setOnClickListener(imgButtonListener);
            llLocation.setOnClickListener(imgButtonListener);
            llContact.setOnClickListener(imgButtonListener);
            return view;
        }


        public final OnClickListener imgButtonListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.ll_documents:
                        actionListener.onDocumentsPressed();
                        break;
                    case R.id.ll_camera:
                        actionListener.onCameraPressed();
                        break;
                    case R.id.ll_gallery:
                        actionListener.onGalleryPressed();
                        break;
                    case R.id.ll_audio:
                        actionListener.onAudioPressed();
                        break;
                    case R.id.ll_location:
                        actionListener.onLocationPressed();
                        break;
                    case R.id.ll_contact:
                        actionListener.onContactPressed();
                        break;
                }
                dismiss();
            }
        };

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            unbinder.unbind();
        }
    }
}


