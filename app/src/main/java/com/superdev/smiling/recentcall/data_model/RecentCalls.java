package com.superdev.smiling.recentcall.data_model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by gautam on 9/6/17.
 */

public class RecentCalls {

    private final List<RecentCall> recentCallLs;

    public RecentCalls(List<RecentCall> recentCallLs) {
        this.recentCallLs = recentCallLs;
    }

    public List<RecentCall> getRecentCallLs() {
        return recentCallLs;
    }

    public void remove(RecentCall recent) {
        if (recentCallLs.contains(recent)) {
            recentCallLs.remove(recent);
        }
    }

    public int size() {
        return recentCallLs.size();
    }

    public RecentCall getRecentCallAt(int position) {
        return recentCallLs.get(position);
    }

    public RecentCalls storedByTimestamp() {
        List<RecentCall> sortedList = new ArrayList<>(recentCallLs);
        Collections.sort(sortedList, byTimestamp());
        return new RecentCalls(sortedList);
    }


    private static Comparator<? super RecentCall> byTimestamp() {
        return (Comparator<RecentCall>) (o1, o2) -> new Date(o2.getCallTime()).compareTo(new Date(o1.getCallTime()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RecentCalls recentCalls1 = (RecentCalls) o;

        return recentCallLs.equals(recentCalls1.recentCallLs);

    }

    @Override
    public int hashCode() {
        return recentCallLs.hashCode();
    }

    @Override
    public String toString() {
        return "RecentCalls{" +
                "recentCall=" + recentCallLs +
                '}';
    }
}
