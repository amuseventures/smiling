package com.superdev.smiling.recentcall.view;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.superdev.smiling.R;
import com.superdev.smiling.recentcall.data_model.RecentCalls;
import com.superdev.smiling.views.EmptyRecyclerView;
import com.superdev.smiling.views.TextViewWithImages;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by gautam on 9/10/17.
 */

public class RecentCallsView extends LinearLayout implements RecentCallDisplayer {

    private final RecentCallAdapter recentCallAdapter;

    @BindView(R.id.welcome_chats_message)
    TextViewWithImages view;

    @BindView(R.id.btn_float)
    FloatingActionButton btnFloat;

    private RecentCallAdapter recentCallFilteredAdapter;

    private UserInteractionListener usersInteractionListener;

    @BindView(R.id.conversationsRecyclerView)
    EmptyRecyclerView conversations;

    public RecentCallsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
        recentCallAdapter = new RecentCallAdapter(LayoutInflater.from(context));
        recentCallFilteredAdapter = new RecentCallAdapter(LayoutInflater.from(context));
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_conversation_list_view, this);
        ButterKnife.bind(this);
        view.setText(R.string.welcome_calls_message);
        btnFloat.setImageResource(R.drawable.ic_new_call_tip);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        conversations.setLayoutManager(layoutManager);
        conversations.setAdapter(recentCallAdapter);
        conversations.setEmptyView(view);
    }

    @Override
    public void display(RecentCalls recentCalls) {
        recentCallAdapter.update(recentCalls);
    }

    @Override
    public void attach(UserInteractionListener userInteractionListener) {
        this.usersInteractionListener = userInteractionListener;
        recentCallAdapter.attach(userInteractionListener);
        recentCallFilteredAdapter.attach(usersInteractionListener);
    }

    @Override
    public void detach(UserInteractionListener userInteractionListener) {
        this.usersInteractionListener = null;
        recentCallAdapter.detach(userInteractionListener);
        recentCallFilteredAdapter.detach(userInteractionListener);
    }

    @Override
    public void filter(String text) {
        if (text.equals(""))
            conversations.setAdapter(recentCallAdapter);
        else {
            recentCallFilteredAdapter.update(recentCallAdapter.getRecentCalls());
            recentCallFilteredAdapter.filter(text);
            conversations.setAdapter(recentCallFilteredAdapter);
        }
    }

    @OnClick(R.id.btn_float)
    public void onViewClicked() {
        usersInteractionListener.onFloatBtnSelected();
    }
}
