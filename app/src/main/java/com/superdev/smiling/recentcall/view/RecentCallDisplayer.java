package com.superdev.smiling.recentcall.view;

import android.view.View;

import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.recentcall.data_model.RecentCalls;
import com.superdev.smiling.user.data_model.User;

/**
 * Created by gautam on 9/10/17.
 */

public interface RecentCallDisplayer {

    void display(RecentCalls recentCalls);

    void attach(UserInteractionListener userInteractionListener);

    void detach(UserInteractionListener userInteractionListener);

    void filter(String text);

    interface UserInteractionListener{

        void onRecentCallSelected(RecentCall recentCall);

        void onCallSelected(RecentCall recentCall);

        void onImageSelected(View view, User user);

        void onFloatBtnSelected();
    }
}
