package com.superdev.smiling.recentcall.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.user.data_model.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

import static com.superdev.smiling.Constants.AUDIO_TYPE;
import static com.superdev.smiling.Constants.VIDEO_TYPE;

/**
 * Created by gautam on 9/10/17.
 */

public class RecentCallView extends FrameLayout {


    @BindView(R.id.contact_photo)
    CircleImageView contactPhoto;

    @BindView(R.id.contact_selector)
    FrameLayout contactSelector;

    @BindView(R.id.contact_name)
    EmojiconTextView contactName;

    @BindView(R.id.call_type_icon)
    ImageView callTypeIcon;

    @BindView(R.id.count)
    TextView count;

    @BindView(R.id.date_time)
    TextView dateTime;

    @BindView(R.id.call)
    ImageView call;

    @BindView(R.id.call_row_container)
    RelativeLayout callRowContainer;

    private int layoutResId;

    public RecentCallView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int[] attrsArray = {
                    android.R.attr.layout
            };
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            layoutResId = array.getResourceId(0, R.layout.merge_recent_call_item_view);
            array.recycle();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), layoutResId, this);
        ButterKnife.bind(this);
    }

    public void display(RecentCall recentCall) {
        if (!TextUtils.isEmpty(recentCall.getUserContactEntity().getName()) && !TextUtils.isEmpty(recentCall.getUserContactEntity().getUid())) {
            Dependencies.INSTANCE.getUserService().getUser(recentCall.getUserContactEntity().getUid())
                    .subscribe(userDatabaseResult -> {
                        if (userDatabaseResult.isSuccess()) {
                            if (!TextUtils.isEmpty(recentCall.getUserContactEntity().getName())) {
                                User user = userDatabaseResult.getData();
                                Utils.loadImageElseBlack(user.getImage(), contactPhoto, getContext(), R.drawable.avatar_contact_large);
                                if (!TextUtils.isEmpty(user.getName())) {
                                    contactName.setText(user.getName());
                                } else {
                                    contactName.setText(user.getPhone());
                                }
                            }
                        }
                    });
        } else {
            if (recentCall.getUserContactEntity() != null)
                Utils.loadImageElseBlack(recentCall.getUserContactEntity().getImage(), contactPhoto, getContext(), R.drawable.avatar_contact_large);
            if (recentCall.getUserContactEntity() != null) {
                if (!TextUtils.isEmpty(recentCall.getUserContactEntity().getName())) {
                    contactName.setText(recentCall.getUserContactEntity().getName());
                } else {
                    contactName.setText(recentCall.getUserContactEntity().getPhone());
                }
            }
        }

        switch (recentCall.getType()) {
            case Constants.TYPE_IN:
                callTypeIcon.setImageResource(R.drawable.call_inc);
                break;
            case Constants.TYPE_OUT:
                callTypeIcon.setImageResource(R.drawable.call_out);
                break;
            case Constants.TYPE_MISSED:
                callTypeIcon.setImageResource(R.drawable.call_missed);
                break;
        }

        switch (recentCall.getCallType()) {
            case AUDIO_TYPE:
                call.setImageResource(R.drawable.ic_action_call);
                break;
            case VIDEO_TYPE:
                call.setImageResource(R.drawable.ic_action_videocall);
                break;
        }

        dateTime.setText(Utils.formatDateCallLog(getContext(), recentCall.getCallTime()));
    }
}
