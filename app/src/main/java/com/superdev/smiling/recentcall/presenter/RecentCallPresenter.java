package com.superdev.smiling.recentcall.presenter;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.superdev.smiling.Constants;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.navigation.AndroidConversationsNavigator;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.recentcall.data_model.RecentCalls;
import com.superdev.smiling.recentcall.service.RecentCallService;
import com.superdev.smiling.recentcall.view.RecentCallDisplayer;
import com.superdev.smiling.sinch.SinchService;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.service.UserService;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by gautam on 9/10/17.
 */

public class RecentCallPresenter {

    private static final String SENDER = "sender";
    private static final String DESTINATION = "destination";

    private final RecentCallDisplayer recentCallDisplayer;
    private final AndroidConversationsNavigator navigator;
    private final PhoneLoginService loginService;
    private final RecentCallService recentCallService;
    private final UserService userService;

    private Subscription loginSubscription;
    private Subscription recentCallSubscription;


    private SinchService.SinchServiceInterface mSinchService;

    private User self;

    public RecentCallPresenter(
            UserService userService,
            RecentCallDisplayer recentCallDisplayer,
            AndroidConversationsNavigator navigator,
            PhoneLoginService loginService,
            RecentCallService recentCallService) {
        this.userService = userService;
        this.recentCallDisplayer = recentCallDisplayer;
        this.navigator = navigator;
        this.loginService = loginService;
        this.recentCallService = recentCallService;
    }


    public void startPresenting() {
        recentCallDisplayer.attach(interactionListener);
        final Subscriber recentCallSubscriber = new Subscriber<RecentCalls>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(RecentCalls recentCalls) {
                recentCallDisplayer.display(recentCalls);
            }
        };

        Subscriber userSubscriber = new Subscriber<Authentication>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(Authentication authentication) {
                recentCallSubscription = recentCallService.syncRecentCalls()
                        .subscribe(recentCallSubscriber);
            }
        };

        loginSubscription = loginService.getAuthentication()
                .filter(successfullyAuthenticated())
                .doOnNext(new Action1<Authentication>() {
                    @Override
                    public void call(Authentication authentication) {
                        self = authentication.getUser();
                    }
                })
                .subscribe(userSubscriber);
    }

    public void setSinchService(SinchService.SinchServiceInterface mSinchService) {
        this.mSinchService = mSinchService;
    }

    public void stopPresenting() {
        recentCallDisplayer.detach(interactionListener);
        loginSubscription.unsubscribe();
        if (recentCallSubscription != null)
            recentCallSubscription.unsubscribe();
    }

    private final RecentCallDisplayer.UserInteractionListener interactionListener = new RecentCallDisplayer.UserInteractionListener() {
        @Override
        public void onRecentCallSelected(RecentCall recentCall) {

        }

        @Override
        public void onCallSelected(RecentCall recentCall) {
            if (recentCall.getCallType() == Constants.AUDIO_TYPE) {
                navigator.placeCall(mSinchService, recentCall.getUserContactEntity());
            } else {
                navigator.placeVideoCall(mSinchService, recentCall.getUserContactEntity());
            }
        }

        @Override
        public void onImageSelected(View view, User user) {
            Bundle bundle = new Bundle();
            if (user.getUid() != null) {
                bundle.putString(SENDER, self.getUid());
                bundle.putString(DESTINATION, user.getUid());
            } else {
                bundle.putParcelable(Constants.FIREBASE_USERS, user);
            }
            navigator.openQuickContact(bundle);
        }

        @Override
        public void onFloatBtnSelected() {
            navigator.toUserPickerForCall();
        }
    };

    private Func1<Authentication, Boolean> successfullyAuthenticated() {
        return new Func1<Authentication, Boolean>() {
            @Override
            public Boolean call(Authentication authentication) {
                return authentication.isSuccess();
            }
        };
    }

    public void filterUsers(String text) {
        recentCallDisplayer.filter(text);
    }

}
