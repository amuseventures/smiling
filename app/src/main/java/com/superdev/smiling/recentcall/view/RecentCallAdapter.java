package com.superdev.smiling.recentcall.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.superdev.smiling.R;
import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.recentcall.data_model.RecentCalls;
import com.superdev.smiling.user.data_model.User;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by gautam on 9/10/17.
 */

public class RecentCallAdapter extends RecyclerView.Adapter<RecentCallViewHolder> {

    private RecentCalls recentCalls = new RecentCalls(new ArrayList<RecentCall>());
    private RecentCallDisplayer.UserInteractionListener interactionListener;
    private final LayoutInflater inflater;

    RecentCallAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void update(RecentCalls recentCalls) {
        this.recentCalls = recentCalls;
        this.recentCalls = recentCalls.storedByTimestamp();
        notifyDataSetChanged();
    }

    public RecentCalls getRecentCalls() {
        return recentCalls;
    }

    public void filter(String text) {
        Iterator<RecentCall> it = recentCalls.getRecentCallLs().iterator();
        while (it.hasNext()) {
            if (!text.toLowerCase().contains(it.next().getUserContactEntity().getName().toLowerCase()) || !text.toLowerCase().contains(it.next().getUserContactEntity().getPhone()))
                it.remove();
        }
        notifyDataSetChanged();
    }

    @Override
    public RecentCallViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecentCallViewHolder((RecentCallView) inflater.inflate(R.layout.recent_call_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecentCallViewHolder holder, int position) {
        holder.bind(recentCalls.getRecentCallAt(position), clickListener);
    }

    public void attach(RecentCallDisplayer.UserInteractionListener userInteractionListener) {
        this.interactionListener = userInteractionListener;
    }

    public void detach(RecentCallDisplayer.UserInteractionListener userInteractionListener) {
        this.interactionListener = null;
    }

    @Override
    public int getItemCount() {
        return recentCalls.size();
    }

    @Override
    public long getItemId(int position) {
        return recentCalls.getRecentCallAt(position).hashCode();
    }

    private final RecentCallViewHolder.RecentCallSelectionListener clickListener = new RecentCallViewHolder.RecentCallSelectionListener() {
        @Override
        public void onRecentCallSelected(RecentCall recentCall) {
            interactionListener.onRecentCallSelected(recentCall);
        }

        @Override
        public void onCallSelected(RecentCall recentCall) {
            interactionListener.onCallSelected(recentCall);
        }

        @Override
        public void onImageSelected(View view, User user) {
            interactionListener.onImageSelected(view, user);
        }
    };
}
