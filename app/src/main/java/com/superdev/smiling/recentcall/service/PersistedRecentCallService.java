package com.superdev.smiling.recentcall.service;

import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.recentcall.data_model.RecentCalls;
import com.superdev.smiling.recentcall.database.RecentCallDatabase;

import rx.Observable;

/**
 * Created by gautam on 9/10/17.
 */

public class PersistedRecentCallService implements RecentCallService {

    private final RecentCallDatabase recentCallDatabase;


    public PersistedRecentCallService(RecentCallDatabase recentCallDatabase){
        this.recentCallDatabase = recentCallDatabase;
    }


    @Override
    public Observable<RecentCalls> syncRecentCalls() {
        return recentCallDatabase.observeRecentCalls();
    }

    @Override
    public void setRecentCall(RecentCall recentCall) {
        recentCallDatabase.setRecentCall(recentCall);
    }
}
