package com.superdev.smiling.recentcall.data_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.superdev.smiling.user.data_model.User;

/**
 * Created by gautam on 9/6/17.
 */

public class RecentCall implements Parcelable {

    private String callId;

    private User userContactEntity;

    private long callTime;

    private long callEndTime;

    private int totalDuration;

    private int callType;

    private int type;

    private int callEndCause;

    public RecentCall() {

    }

    public RecentCall(Parcel in) {
        callId = in.readString();
        userContactEntity = in.readParcelable(User.class.getClassLoader());
        callTime = in.readLong();
        callEndTime = in.readLong();
        totalDuration = in.readInt();
        callType = in.readInt();
        type = in.readInt();
        callEndCause = in.readInt();
    }

    public static final Creator<RecentCall> CREATOR = new Creator<RecentCall>() {
        @Override
        public RecentCall createFromParcel(Parcel in) {
            return new RecentCall(in);
        }

        @Override
        public RecentCall[] newArray(int size) {
            return new RecentCall[size];
        }
    };

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public User getUserContactEntity() {
        return userContactEntity;
    }

    public void setUserContactEntity(User userContactEntity) {
        this.userContactEntity = userContactEntity;
    }

    public long getCallTime() {
        if (String.valueOf(callTime).length() < 13)
            return callTime * 1000;
        else
            return callTime;
    }

    public void setCallTime(long callTime) {
        this.callTime = callTime;
    }

    public long getCallEndTime() {
        return callEndTime;
    }

    public void setCallEndTime(long callEndTime) {
        this.callEndTime = callEndTime;
    }

    public int getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(int totalDuration) {
        this.totalDuration = totalDuration;
    }

    public int getCallType() {
        return callType;
    }

    public void setCallType(int callType) {
        this.callType = callType;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCallEndCause() {
        return callEndCause;
    }

    public void setCallEndCause(int callEndCause) {
        this.callEndCause = callEndCause;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(callId);
        parcel.writeParcelable(userContactEntity, i);
        parcel.writeLong(callTime);
        parcel.writeLong(callEndTime);
        parcel.writeInt(totalDuration);
        parcel.writeInt(callType);
        parcel.writeInt(type);
        parcel.writeInt(callEndCause);
    }
}
