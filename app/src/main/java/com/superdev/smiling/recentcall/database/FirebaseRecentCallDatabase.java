package com.superdev.smiling.recentcall.database;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Remember;
import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.recentcall.data_model.RecentCalls;
import com.superdev.smiling.rx.FirebaseObservableListeners;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by gautam on 9/10/17.
 */

public class FirebaseRecentCallDatabase implements RecentCallDatabase {

    private final DatabaseReference recentCallDB;
    private final FirebaseObservableListeners firebaseObservableListeners;


    public FirebaseRecentCallDatabase(FirebaseDatabase firebaseDatabase, FirebaseObservableListeners firebaseObservableListeners) {
        recentCallDB = firebaseDatabase.getReference(Constants.FIREBASE_RECENT_CALLS);
        this.firebaseObservableListeners = firebaseObservableListeners;
    }


    @Override
    public Observable<RecentCalls> observeRecentCalls() {
        return firebaseObservableListeners.listenToValueEvents(recentCallDB.child(Remember.getString(Constants.FIREBASE_USER_ID, "")), toRecentCalls());
    }

    @Override
    public void setRecentCall(RecentCall recentCall) {
        recentCallDB.child(Remember.getString(Constants.FIREBASE_USER_ID, "")).push().setValue(recentCall);
    }

    private Func1<DataSnapshot, RecentCalls> toRecentCalls() {
        return new Func1<DataSnapshot, RecentCalls>() {
            @Override
            public RecentCalls call(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                List<RecentCall> recentCallList = new ArrayList<>();
                for (DataSnapshot child : children) {
                    RecentCall recentCall = child.getValue(RecentCall.class);
                    recentCallList.add(recentCall);
                }
                return new RecentCalls(recentCallList);
            }
        };
    }
}
