package com.superdev.smiling.recentcall.service;

import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.recentcall.data_model.RecentCalls;

import rx.Observable;


/**
 * Created by gautam on 9/10/17.
 */

public interface RecentCallService {

    Observable<RecentCalls> syncRecentCalls();

    void setRecentCall(RecentCall recentCall);
}
