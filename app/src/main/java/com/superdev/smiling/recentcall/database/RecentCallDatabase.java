package com.superdev.smiling.recentcall.database;

import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.recentcall.data_model.RecentCalls;

import rx.Observable;

/**
 * Created by gautam on 9/10/17.
 */

public interface RecentCallDatabase {

    Observable<RecentCalls> observeRecentCalls();

    void setRecentCall(RecentCall recentCall);
}
