package com.superdev.smiling.recentcall.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.user.data_model.User;

/**
 * Created by gautam on 9/10/17.
 */

public class RecentCallViewHolder extends RecyclerView.ViewHolder {

    RecentCallView recentCallView;

    public RecentCallViewHolder(RecentCallView itemView) {
        super(itemView);
        this.recentCallView = itemView;
    }

    public void bind(final RecentCall recentCall, final RecentCallSelectionListener listener) {
        recentCallView.display(recentCall);

        recentCallView.call.setOnClickListener(view -> listener.onCallSelected(recentCall));

        recentCallView.contactPhoto.setOnClickListener(view -> listener.onImageSelected(view, recentCall.getUserContactEntity()));
    }

    public interface RecentCallSelectionListener {

        void onRecentCallSelected(RecentCall recentCall);

        void onCallSelected(RecentCall recentCall);

        void onImageSelected(View view, User user);

    }

}
