package com.superdev.smiling.utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Lincoln on 05/05/16.
 */
public class PrefManager {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;
    private static final String PREF_NAME = "sharedPreference";
    private static final String LANGUAGE_SETTINGS = "language_settings";
    public PrefManager(Context context) {
        this._context = context;
        int PRIVATE_MODE = 0;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLanguageSettings(int languageSettings) {
        editor.putInt(LANGUAGE_SETTINGS, languageSettings);
        editor.commit();
    }

    public int getLanguageSettings() {
        return pref.getInt(LANGUAGE_SETTINGS, -1);
    }

}

