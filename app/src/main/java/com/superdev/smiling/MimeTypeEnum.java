package com.superdev.smiling;

/**
 * Created by gautam on 9/4/17.
 */

public enum MimeTypeEnum {

    IMAGE("image"),
    VIDEO("video"),
    AUDIO("audio"),
    LOCATION("location"),
    DOCUMENT("pdf"),
    CONTACT("vcard"),
    SMS("sms"),
    VOICE("voice"),
    MEMBER("member"),
    REMOVE("remove");

    private String value;

    MimeTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
