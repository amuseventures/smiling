package com.superdev.smiling.main;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.quickblox.sample.core.utils.SharedPrefsHelper;
import com.quickblox.users.model.QBUser;
import com.sinch.android.rtc.SinchError;
import com.superdev.smiling.BuildConfig;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.link.FirebaseDynamicLinkFactory;
import com.superdev.smiling.main.presenter.MainPresenter;
import com.superdev.smiling.main.view.MainDisplayer;
import com.superdev.smiling.navigation.AndroidMainNavigator;
import com.superdev.smiling.quick.service.CallService;
import com.superdev.smiling.quick.service.QuickService;
import com.superdev.smiling.sinch.SinchBaseActivity;
import com.superdev.smiling.sinch.SinchService;

import java.io.IOException;

/**
 * Created by marco on 14/08/16.
 */

public class MainActivity extends SinchBaseActivity implements SinchService.StartFailedListener {

    private MainPresenter presenter;
    private AndroidMainNavigator navigator;
    private MainDisplayer mainDisplayer;
    private static final String TAG = "MainActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainDisplayer = findViewById(R.id.mainView);
        Toolbar toolbar = findViewById(R.id.toolbar);
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        setSupportActionBar(toolbar);
        setTitle(R.string.app_name);
        Remember.putString(Constants.FIREBASE_USERS_FCM, FirebaseInstanceId.getInstance().getToken());
        FirebaseDynamicLinkFactory firebaseDynamicLinkFactory = new FirebaseDynamicLinkFactory(
                getResources().getString(R.string.dynamic_link_url),
                getResources().getString(R.string.deepLinkBaseUrl),
                BuildConfig.APPLICATION_ID
        );
        navigator = new AndroidMainNavigator(this);
        navigator.addDrawerToggle(drawerLayout, toolbar);
        presenter = new MainPresenter(
                firebaseDynamicLinkFactory,
                Dependencies.INSTANCE.getPhoneLoginService(),
                Dependencies.INSTANCE.getUserService(),
                mainDisplayer,
                Dependencies.INSTANCE.getMainService(),
                Dependencies.INSTANCE.getMessagingService(),
                navigator,
                Dependencies.INSTANCE.getFirebaseToken(),
                this
        );
        setToken();
        //subscribeToTopic("115795163");
        sharedPrefsHelper = SharedPrefsHelper.getInstance();
        if (sharedPrefsHelper.hasQbUser()) {
            startLoginService(sharedPrefsHelper.getQbUser());
        } else {
            startService(new Intent(this, QuickService.class));
        }
    }

    @Override
    protected void onServiceConnected() {
        if (getSinchServiceInterface().isStarted()) {

        } else {
            getSinchServiceInterface().setStartListener(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getSinchServiceInterface() != null && !getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(Remember.getString(Constants.FIREBASE_USERS_PHONE, ""));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!navigator.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        if (!presenter.onBackPressed())
            if (!navigator.onBackPressed()) {
                super.onBackPressed();
            }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.stopPresenting();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mainDisplayer.inflateMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_profile) {
            navigator.toProfile();
        } else if (item.getItemId() == R.id.action_new_group) {
            navigator.toNewGroupCreate();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStartFailed(SinchError error) {

    }

    @Override
    public void onStarted() {
//        Toast.makeText(this, "Started", Toast.LENGTH_SHORT).show();
    }

    private void setToken() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference tokenRef = FirebaseDatabase.getInstance().getReference(Constants.FIREBASE_FCM);
        tokenRef.child(firebaseUser.getUid() + "/" + Constants.FIREBASE_FCM_TOKEN).setValue(Remember.getString(Constants.FIREBASE_USERS_FCM, ""));
        tokenRef.child(firebaseUser.getUid() + "/" + Constants.FIREBASE_FCM_ENABLED).setValue(Boolean.TRUE.toString());
    }

    private void subscribeToTopic(String topic) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                GcmPubSub pubSub = GcmPubSub.getInstance(MainActivity.this);
                try {
                    pubSub.subscribe(Remember.getString(Constants.FIREBASE_USERS_FCM, ""), "/topics/" + topic, null);
                    Log.d(TAG, "subscribe: ");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    protected void startLoginService(QBUser qbUser) {
        CallService.start(this, qbUser);
    }
}

