package com.superdev.smiling.main.view;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel_list.ChannelListFragment;
import com.superdev.smiling.conversation_list.ConversationListFragment;
import com.superdev.smiling.main.MainActivity;
import com.superdev.smiling.user.data_model.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by marco on 16/08/16.
 */

public class MainView extends CoordinatorLayout implements MainDisplayer {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.search_view)
    MaterialSearchView searchView;

    @BindView(R.id.container)
    FrameLayout container;

    @BindView(R.id.nav_view)
    NavigationView navView;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.tabs)
    TabLayout tabs;

    @BindView(R.id.viewpager)
    ViewPager viewpager;

    private CircleImageView profileImageView;

    private EmojiconTextView nameTextView;

    private DrawerActionListener drawerActionListener;

    private NavigationActionListener navigationActionListener;

    private SearchActionListener searchActionListener;

    public MainView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_main_view, this);
        ButterKnife.bind(this);

        View headerLayout = navView.getHeaderView(0);
        profileImageView = headerLayout.findViewById(R.id.profileImageView);
        nameTextView = headerLayout.findViewById(R.id.nameTextView);
        showPagerView();
    }

    private void showPagerView() {
        TabPageAdapter adapter = new TabPageAdapter(getContext(), ((MainActivity) getContext()).getSupportFragmentManager());
        adapter.addFragment(ConversationListFragment.newInstance(), "CHATS");
        adapter.addFragment(ChannelListFragment.newInstance(), "GROUPS");
        viewpager.setAdapter(adapter);
        viewpager.setOffscreenPageLimit(2);
        tabs.setupWithViewPager(viewpager);
    }

    @Override
    public void attach(final DrawerActionListener drawerActionListener, NavigationActionListener navigationActionListener, SearchActionListener searchActionListener) {
        this.drawerActionListener = drawerActionListener;
        this.navigationActionListener = navigationActionListener;
        this.searchActionListener = searchActionListener;

        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        navView.setNavigationItemSelectedListener(navigationItemSelectedListener);
        profileImageView.setOnClickListener(headerClickListener);
        nameTextView.setOnClickListener(headerClickListener);
        toolbar.setNavigationOnClickListener(navigationClickListener);
    }

    @Override
    public void detach(DrawerActionListener drawerActionListener, NavigationActionListener navigationActionListener, SearchActionListener searchActionListener) {
        this.searchActionListener = null;
        navView.setNavigationItemSelectedListener(null);
        profileImageView.setOnClickListener(null);
        nameTextView.setOnClickListener(null);
        toolbar.setNavigationOnClickListener(null);
    }

    @Override
    public void setTitle(String title) {
        toolbar.setTitle(title);
    }

    @Override
    public void setUser(User user) {
        Utils.loadImageElseWhite(user.getImage(), profileImageView, getContext(),R.drawable.avatar_contact_large);
        nameTextView.setText(user.getName());
    }

    @Override
    public void inflateMenu() {
        if (!toolbar.getMenu().hasVisibleItems()) {
            toolbar.inflateMenu(R.menu.fragment_users_itemlist);
            searchView.setOnQueryTextListener(queryTextListener);

            MenuItem item = toolbar.getMenu().findItem(R.id.search_user);
            searchView.setMenuItem(item);
        }
    }

    @Override
    public void clearMenu() {
        toolbar.getMenu().clear();
    }

    @Override
    public boolean onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
            return true;
        }
        return false;
    }

    private MaterialSearchView.OnQueryTextListener queryTextListener = new MaterialSearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            searchActionListener.showFilteredUsers(newText);
            return false;
        }
    };

    @Override
    public void openDrawer() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void closeDrawer() {
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    private final NavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            drawerActionListener.onNavigationItemSelected(item);
            return true;
        }
    };

    private Toolbar.OnMenuItemClickListener menuItemClickListener = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if (item.getItemId() == R.id.action_new_group) {

            }
            return false;
        }
    };


    private final OnClickListener headerClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            drawerActionListener.onHeaderSelected();
        }
    };

    private final OnClickListener navigationClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            navigationActionListener.onHamburgerPressed();
        }
    };

}

