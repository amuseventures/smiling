package com.superdev.smiling.main.presenter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.superdev.smiling.Constants;
import com.superdev.smiling.R;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.link.LinkFactory;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.main.service.CloudMessagingService;
import com.superdev.smiling.main.service.MainService;
import com.superdev.smiling.main.view.MainDisplayer;
import com.superdev.smiling.navigation.AndroidMainNavigator;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.service.UserService;

import java.util.HashMap;
import java.util.Map;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by marco on 16/08/16.
 */

public class MainPresenter {

    private AppCompatActivity activity;

    private final PhoneLoginService loginService;
    private final UserService userService;
    private final MainDisplayer mainDisplayer;
    private final MainService mainService;
    private final CloudMessagingService messagingService;
    private final AndroidMainNavigator navigator;
    private final String token;

    private Subscription loginSubscription;
    private Subscription userSubscription;
    private Subscription messageSubscription;
    private LinkFactory linkFactory;
    private User self;

    public MainPresenter(
            LinkFactory linkFactory,
            PhoneLoginService loginService,
            UserService userService,
            MainDisplayer mainDisplayer,
            MainService mainService,
            CloudMessagingService messagingService,
            AndroidMainNavigator navigator,
            String token,
            AppCompatActivity activity) {
        this.loginService = loginService;
        this.userService = userService;
        this.mainDisplayer = mainDisplayer;
        this.mainService = mainService;
        this.messagingService = messagingService;
        this.navigator = navigator;
        this.token = token;
        this.activity = activity;
        this.linkFactory = linkFactory;
    }

    public void startPresenting() {
        navigator.init();
        mainDisplayer.attach(drawerActionListener, navigationActionListener, searchActionListener);

        final Subscriber userSubscriber = new Subscriber<DatabaseResult<User>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onNext(DatabaseResult<User> userDatabaseResult) {
                if (userDatabaseResult.isSuccess()) {
                    User user = userDatabaseResult.getData();
                    if (user == null || TextUtils.isEmpty(user.getName())) {
                        navigator.toFirstLogin();
                    } else {
                        messageSubscription = messagingService.readToken(user)
                                .subscribe(new Subscriber<String>() {
                                    @Override
                                    public void onCompleted() {

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        e.printStackTrace();
                                    }

                                    @Override
                                    public void onNext(String s) {
                                        if (s == null || !s.equals(token)) {
                                            messageSubscription.unsubscribe();
                                            messagingService.setToken(user);
                                        }
                                    }
                                });
                        mainService.initLastSeen(user);
                        mainDisplayer.setUser(user);
                    }
                } else {
                    navigator.toFirstLogin();
                }
            }
        };
        loginSubscription = loginService.getAuthentication()
                .first().subscribe(new Action1<Authentication>() {
                    @Override
                    public void call(Authentication authentication) {
                        if (authentication.isSuccess()) {
                            self = authentication.getUser();
                            userSubscription = userService.getUser(authentication.getUser().getUid())
                                    .first().subscribe(userSubscriber);
                        } else {
                            navigator.toLogin();
                        }
                    }
                });
    }

    public void stopPresenting() {
        mainDisplayer.detach(drawerActionListener, navigationActionListener, searchActionListener);
        if (loginSubscription != null) loginSubscription.unsubscribe();
        if (userSubscription != null) userSubscription.unsubscribe();
        if (messageSubscription != null) messageSubscription.unsubscribe();
    }

    private final MainDisplayer.SearchActionListener searchActionListener = new MainDisplayer.SearchActionListener() {

        @Override
        public void showFilteredUsers(String text) {
            Intent intent = new Intent("SEARCH");
            intent.putExtra("search", text);
            activity.sendBroadcast(intent);
        }

    };

    private final MainDisplayer.DrawerActionListener drawerActionListener = new MainDisplayer.DrawerActionListener() {

        @Override
        public void onHeaderSelected() {
            navigator.toProfile();
        }

        @Override
        public void onNavigationItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.nav_conversations:
                    navigator.toConversations();
                    mainDisplayer.setTitle(activity.getString(R.string.conversations_toolbar_title));
                    break;
                case R.id.nav_contacts:
                    navigator.toContactList();
                    mainDisplayer.setTitle(activity.getString(R.string.users_toolbar_title));
                    mainDisplayer.inflateMenu();
                    break;
                case R.id.nav_calls:
                    navigator.toRecentCalls();
                    mainDisplayer.setTitle(activity.getString(R.string.calls_toolbar_title));
                    mainDisplayer.clearMenu();
                    break;
                case R.id.lang_settings:
                    navigator.showLanguageSettingsDialog();
                    break;
                case R.id.nav_share:
                    navigator.toShareInvite(linkFactory.inviteLinkFrom(self.getUid()).toString());
                    break;
                case R.id.profile:
                    navigator.toProfile();
                    break;
                case R.id.settings:
                    navigator.toSettings();

                    /*try {
                        mainService.logout();
                        navigator.toLogin();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                case R.id.nav_group:
                    mainDisplayer.setTitle(activity.getString(R.string.groups_title));
                    navigator.toChannels();
                    break;
            }
            mainDisplayer.closeDrawer();
        }

    };

    private final MainDisplayer.NavigationActionListener navigationActionListener = new MainDisplayer.NavigationActionListener() {

        @Override
        public void onHamburgerPressed() {
            mainDisplayer.openDrawer();
        }

    };

    public boolean onBackPressed() {
        return mainDisplayer.onBackPressed();
    }

}
