package com.superdev.smiling;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.format.DateUtils;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.storage.StorageReference;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.conversation_list.view.VerticalImageSpan;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.storage.FirebaseImageLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.superdev.smiling.Constants.MAIN_DIRECTORY;
import static com.superdev.smiling.Constants.SMILE_ANIMATED_GIFS;
import static com.superdev.smiling.Constants.SMILE_AUDIO;
import static com.superdev.smiling.Constants.SMILE_DOCUMENTS;
import static com.superdev.smiling.Constants.SMILE_IMAGES;
import static com.superdev.smiling.Constants.SMILE_MEDIA_DIRECTORY;
import static com.superdev.smiling.Constants.SMILE_PROFILE_PHOTOS;
import static com.superdev.smiling.Constants.SMILE_SENT;
import static com.superdev.smiling.Constants.SMILE_VIDEOS;
import static com.superdev.smiling.Constants.SMILE_VOICE;

/**
 * Created by marco on 13/07/16.
 */

@SuppressWarnings("ALL")
public class Utils {
    private static final String TAG = "Utils";

    public static float density = 1;
    public static int statusBarHeight = 0;
    public static Point displaySize = new Point();

    static {
        density = App.getInstance().getResources().getDisplayMetrics().density;
        checkDisplaySize();
    }

    public static int dp(float value) {
        return (int) Math.ceil(density * value);
    }

    public static final String getGroupDate(long dateTime) {
        Date date = new Date(dateTime);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
        return dateFormat.format(date);
    }

    public static String getCurrentTimestamp() {
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy/MM/dd/HH/mm/ss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().getID()));
        return dateFormatGmt.format(new Date());
    }

    public static String getYesterdayTimeStamp() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Date yesterdayDate = cal.getTime();
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy/MM/dd/HH/mm/ss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().getID()));
        return dateFormatGmt.format(yesterdayDate);
    }

    public static String getTimestamp(String timestamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/HH/mm");
            Date date = sdf.parse(timestamp);
            long currentDate = date.getTime();

            long millis = TimeZone.getDefault().getOffset(currentDate);
            long hour = (millis / (1000 * 60 * 60)) % 24;
            long minutes = (millis / (1000 * 60)) % 60;

            String[] timestampPart = timestamp.split("/");
            long h = Long.parseLong(timestampPart[3]);
            long m = Long.parseLong(timestampPart[4]);
            h += hour;
            h %= 24;
            m += minutes;
            m %= 60;

            String output = h + ":" + m;
            if (h < 10) {
                if (m < 10) {
                    output = "0" + h + ":0" + m;
                } else {
                    output = "0" + h + ":" + m;
                }
            } else if (m < 10) {
                output = h + ":0" + m;
            }
            return output;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDate(String timestamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/HH/mm");
            Date date = sdf.parse(timestamp);
            long currentDate = date.getTime();
            sdf.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().getID()));

            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
            return sdfDate.format(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTime(String timestamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/HH/mm");
            sdf.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().getID()));
            Date date = sdf.parse(timestamp);
            long currentDate = date.getTime();
            //  currentDate += TimeZone.getDefault().getOffset(currentDate);

            SimpleDateFormat sdfDate = new SimpleDateFormat("hh:mm aa", Locale.US);
            return sdfDate.format(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static long getMilliseconds(String timestamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/HH/mm");
            sdf.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().getID()));
            Date date = sdf.parse(timestamp);
            return date.getTime() / 1000;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String getMediaDateTime(String timestamp) {
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/HH/mm");
            sdf.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().getID()));
            Date date = sdf.parse(timestamp);
            long currentDate = date.getTime();
            return DateUtils.getRelativeTimeSpanString(currentDate, calendar.getTimeInMillis(), DateUtils.DAY_IN_MILLIS).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDateString(String timestamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/HH/mm");
            Date date = sdf.parse(timestamp);
            long currentDate = date.getTime();
            sdf.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().getID()));
            //currentDate += TimeZone.getDefault().getOffset(currentDate);

            SimpleDateFormat sdfDate = new SimpleDateFormat("MMMM dd", Locale.US);
            return sdfDate.format(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void loadImageElseBlack(String image, CircleImageView imageView, Context context, int placeHolder) {
        try {
            if (image != null && image.length() > 0) {
                StorageReference ref = Dependencies.INSTANCE.getStorageService().getProfileImageReference(image);
                Glide.with(context)
                        .using(new FirebaseImageLoader())
                        .load(ref)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontTransform()
                        .into(imageView);
            } else {
                Glide.with(context)
                        .load("")
                        .placeholder(R.drawable.avatar_contact_large)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public static void loadImageElseWhite(String image, CircleImageView imageView, Context context, int placeHolder) {

        try {
            if (image != null && image.length() > 0) {
                StorageReference ref = Dependencies.INSTANCE.getStorageService().getProfileImageReference(image);
                Glide.with(context)
                        .using(new FirebaseImageLoader())
                        .load(ref)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontTransform()
                        .into(imageView);
            } else {
                Glide.with(context)
                        .load("")
                        .placeholder(R.drawable.avatar_contact_large)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

    }

    public static void loadImage(String image, ImageView imageView, Context context, int placeHolder) {
        try {
            if (image != null && image.length() > 0) {
                StorageReference ref = Dependencies.INSTANCE.getStorageService().getProfileImageReference(image);
                Glide.with(context)
                        .using(new FirebaseImageLoader())
                        .load(ref)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontTransform()
                        .into(imageView);
            } else {
                Glide.with(context)
                        .load("")
                        .placeholder(R.drawable.avatar_contact_large)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public static void showKeyboard(View view) {
        if (view == null) {
            return;
        }
        try {
            InputMethodManager inputManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }
    }

    public static boolean isKeyboardShowed(View view) {
        if (view == null) {
            return false;
        }
        try {
            InputMethodManager inputManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            return inputManager.isActive(view);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }
        return false;
    }

    public static void hideKeyboard(View view) {
        if (view == null) {
            return;
        }
        try {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (!imm.isActive()) {
                return;
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }
    }

    public static String localMap(String latitudeFinal, String longitudeFinal) {
        return "https://maps.googleapis.com/maps/api/staticmap?markers=color:red|" + latitudeFinal + "," + longitudeFinal + "&zoom=14&size=270x200&scale=2&language=en";
    }

    public static String getImageSentPath() {
        String path = Environment.getExternalStorageDirectory() + String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_IMAGES, SMILE_SENT);
        File mediaStorageDir = new File(path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return path;
    }

    public static String getImagePath() {
        String path = Environment.getExternalStorageDirectory() + String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_IMAGES);
        File mediaStorageDir = new File(path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return path;
    }

    public static String getAudioSentPath() {
        String path = Environment.getExternalStorageDirectory() + String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_AUDIO, SMILE_SENT);
        File mediaStorageDir = new File(path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return path;
    }

    public static String getAudioPath() {
        String path = Environment.getExternalStorageDirectory() + String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_AUDIO);
        File mediaStorageDir = new File(path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return path;
    }

    public static String getVoicePath() {
        String path = Environment.getExternalStorageDirectory() + String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_VOICE);
        File mediaStorageDir = new File(path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return path;
    }

    public static String getVoiceSentPath() {
        String path = Environment.getExternalStorageDirectory() + String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_VOICE, SMILE_SENT);
        File mediaStorageDir = new File(path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return path;
    }

    public static String getSentDocumentPath() {
        String path = Environment.getExternalStorageDirectory() + String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_DOCUMENTS, SMILE_SENT);
        File mediaStorageDir = new File(path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return path;
    }

    public static String getDocumentPath() {
        String path = Environment.getExternalStorageDirectory() + String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_DOCUMENTS);
        File mediaStorageDir = new File(path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return path;
    }

    public static String getVideoSentPath() {
        String path = Environment.getExternalStorageDirectory() + String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_VIDEOS, SMILE_SENT);
        File mediaStorageDir = new File(path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return path;
    }

    public static String getVideoPath() {
        String path = Environment.getExternalStorageDirectory() + String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_VIDEOS);
        File mediaStorageDir = new File(path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return path;
    }

    public static String getGifSentPath() {
        String path = Environment.getExternalStorageDirectory() + String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_ANIMATED_GIFS, SMILE_SENT);
        File mediaStorageDir = new File(path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return path;
    }

    public static String getGifPath() {
        String path = Environment.getExternalStorageDirectory() + String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_ANIMATED_GIFS);
        File mediaStorageDir = new File(path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return path;
    }


    public static void writeFile(String type, File fileToWrite, boolean isSentByMe) {
        String path;
        switch (type) {
            case "IMAGE":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_IMAGES, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_IMAGES);
                break;
            case "VIDEO":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_VIDEOS, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_VIDEOS);
                break;
            case "AUDIO":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_AUDIO, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_AUDIO);
                break;
            case "DOCS":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_DOCUMENTS, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_DOCUMENTS);
                break;
            case "GIFS":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_ANIMATED_GIFS, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_ANIMATED_GIFS);
                break;
            default:
                path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_PROFILE_PHOTOS);
                break;
        }
        try {
            File outFile = new File(path, fileToWrite.getName());
            InputStream is = new FileInputStream(fileToWrite);
            OutputStream outstream = new FileOutputStream(outFile);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = is.read(buffer)) > 0) {
                outstream.write(buffer, 0, len);
            }
            outstream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteFile(Message message) {
        String path;
        String fileName;
        boolean isSentByMe = !message.getSelf().equalsIgnoreCase(message.getDestination());
        if (message.getMsgType().equalsIgnoreCase(MimeTypeEnum.SMS.getValue())) {
            return;
        }
        switch (message.getMsgType().toUpperCase()) {
            case "IMAGE":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_IMAGES, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_IMAGES);
                fileName = message.getFileModel().getFileName();
                break;
            case "VIDEO":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_VIDEOS, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_VIDEOS);
                fileName = message.getVideoModel().getVideoName();
                break;
            case "AUDIO":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_AUDIO, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_AUDIO);
                fileName = message.getAudioModel().getAudioName();
                break;
            case "VOICE":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_VOICE, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_VOICE);
                fileName = message.getVideoModel().getVideoName();
                break;
            case "DOCS":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_DOCUMENTS, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_DOCUMENTS);
                fileName = message.getDocModel().getDocName();
                break;
            case "GIFS":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_ANIMATED_GIFS, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_ANIMATED_GIFS);
                fileName = "";
                break;
            default:
                path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_PROFILE_PHOTOS);
                fileName = "";
                break;
        }
        try {
            File outFile = new File(Environment.getExternalStorageDirectory() + path, fileName);
            if (outFile.exists()) {
                outFile.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteFile(String self, GroupMessage message) {
        String path;
        String fileName;
        boolean isSentByMe = self.equalsIgnoreCase(message.getAuthor().getUid());
        if (message.getMsgType().equalsIgnoreCase(MimeTypeEnum.SMS.getValue())) {
            return;
        }
        switch (message.getMsgType().toUpperCase()) {
            case "IMAGE":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_IMAGES, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_IMAGES);
                fileName = message.getFileModel().getFileName();
                break;
            case "VIDEO":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_VIDEOS, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_VIDEOS);
                fileName = message.getVideoModel().getVideoName();
                break;
            case "AUDIO":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_AUDIO, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_AUDIO);
                fileName = message.getAudioModel().getAudioName();
                break;
            case "VOICE":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_VOICE, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_VOICE);
                fileName = message.getVideoModel().getVideoName();
                break;
            case "DOCS":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_DOCUMENTS, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_DOCUMENTS);
                fileName = message.getDocModel().getDocName();
                break;
            case "GIFS":
                if (isSentByMe)
                    path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_ANIMATED_GIFS, SMILE_SENT);
                else
                    path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_ANIMATED_GIFS);
                fileName = "";
                break;
            default:
                path = String.format("/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_PROFILE_PHOTOS);
                fileName = "";
                break;
        }
        try {
            File outFile = new File(Environment.getExternalStorageDirectory() + path, fileName);
            if (outFile.exists()) {
                outFile.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String readableFileSize(long size) {
        if (size <= 0) return "0";
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return ((int) (size / Math.pow(1024, digitGroups))) + " " + units[digitGroups];
    }

    public static void writeFile(File oldFile, File newFile) {

        try {
            InputStream is = new FileInputStream(oldFile);
            OutputStream outstream = new FileOutputStream(newFile);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = is.read(buffer)) > 0) {
                outstream.write(buffer, 0, len);
            }
            outstream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getCustomBody(String type, String message, Context context) {
        Drawable audio = context.getResources().getDrawable(R.drawable.msg_status_audio).mutate();
        audio.setBounds(0, 0, audio.getIntrinsicWidth(), audio.getIntrinsicHeight());
        Drawable video = context.getResources().getDrawable(R.drawable.msg_status_video).mutate();
        video.setBounds(0, 0, video.getIntrinsicWidth(), video.getIntrinsicHeight());
        Drawable image = context.getResources().getDrawable(R.drawable.msg_status_cam).mutate();
        image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
        Drawable document = context.getResources().getDrawable(R.drawable.msg_status_doc).mutate();
        document.setBounds(0, 0, document.getIntrinsicWidth(), document.getIntrinsicHeight());
        Drawable contact = context.getResources().getDrawable(R.drawable.msg_status_contact).mutate();
        contact.setBounds(0, 0, contact.getIntrinsicWidth(), contact.getIntrinsicHeight());
        Drawable location = context.getResources().getDrawable(R.drawable.msg_status_location).mutate();
        location.setBounds(0, 0, contact.getIntrinsicWidth(), contact.getIntrinsicHeight());

        ImageSpan audioSpan = new VerticalImageSpan(audio);
        ImageSpan videoSpan = new VerticalImageSpan(video);
        ImageSpan imageSpan = new VerticalImageSpan(image);
        ImageSpan contactSpan = new VerticalImageSpan(contact);
        ImageSpan documentSpan = new VerticalImageSpan(document);
        ImageSpan locationSpan = new VerticalImageSpan(location);

        SpannableString messageString;
        String ldir = "";
        if (type.equalsIgnoreCase(MimeTypeEnum.IMAGE.getValue())) {
            messageString = new SpannableString(ldir + "  Photo");
            messageString.setSpan(imageSpan, ldir.length(), ldir.length() + 1, 0);
        } else if (type.equalsIgnoreCase(MimeTypeEnum.VIDEO.getValue())) {
            messageString = new SpannableString(ldir + "  Video");
            messageString.setSpan(videoSpan, ldir.length(), ldir.length() + 1, 0);
        } else if (type.equalsIgnoreCase(MimeTypeEnum.AUDIO.getValue())) {
            messageString = new SpannableString(ldir + "  Audio");
            messageString.setSpan(audioSpan, ldir.length(), ldir.length() + 1, 0);
        } else if (type.equalsIgnoreCase(MimeTypeEnum.CONTACT.getValue())) {
            messageString = new SpannableString(ldir + "  Contact");
            messageString.setSpan(contactSpan, ldir.length(), ldir.length() + 1, 0);
        } else if (type.equalsIgnoreCase(MimeTypeEnum.LOCATION.getValue())) {
            messageString = new SpannableString(ldir + "  Location");
            messageString.setSpan(locationSpan, ldir.length(), ldir.length() + 1, 0);
        } else if (type.equalsIgnoreCase(MimeTypeEnum.DOCUMENT.getValue())) {
            messageString = new SpannableString(ldir + "  Document");
            messageString.setSpan(documentSpan, ldir.length(), ldir.length() + 1, 0);
        } else {
            messageString = new SpannableString(message);
        }
        return messageString.toString();
    }

    public static String getRealPathFromURI(Context context, Uri contentURI) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static Bitmap writeTextOnBitmap(Context context, int icon, String extension) {
        Bitmap bm1 = null;
        Bitmap newBitmap = null;

        try {
            bm1 = BitmapFactory.decodeResource(context.getResources(), icon);

            Bitmap.Config config = bm1.getConfig();
            if (config == null) {
                config = Bitmap.Config.ARGB_8888;
            }
            newBitmap = Bitmap.createBitmap(bm1.getWidth(), bm1.getHeight(), config);
            Canvas newCanvas = new Canvas(newBitmap);
            newCanvas.drawBitmap(bm1, 0, 0, null);
            Paint paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
            paintText.setColor(Color.WHITE);
            paintText.setTextSize(16);
            paintText.setTextAlign(Paint.Align.CENTER);
            paintText.setStyle(Paint.Style.FILL);

            Rect rectText = new Rect();
            int x = (bm1.getWidth() - rectText.width()) / 2;
            int y = (bm1.getHeight() + rectText.height() + 25) / 2;

            paintText.getTextBounds(extension, 0, extension.length(), rectText);

            newCanvas.drawText(extension,
                    x, y, paintText);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return newBitmap;
    }

    @SuppressLint("SimpleDateFormat")
    public static String formatDateCallLog(Context context, long date) {
        try {
            Calendar rightNow = Calendar.getInstance();
            int day = rightNow.get(Calendar.DAY_OF_YEAR);
            int year = rightNow.get(Calendar.YEAR);
            rightNow.setTimeInMillis(date);
            int dateDay = rightNow.get(Calendar.DAY_OF_YEAR);
            int dateYear = rightNow.get(Calendar.YEAR);

            if (dateDay == day && year == dateYear) {
                return String.format("%s, %s", context.getString(R.string.today), new SimpleDateFormat(context.getString(R.string.formatterDay12H), Locale.US).format(new Date(date)));
            } else if (dateDay + 1 == day && year == dateYear) {
                return String.format("%s %s", context.getString(R.string.YesterdayAt), new SimpleDateFormat(context.getString(R.string.formatterDay12H), Locale.US).format(new Date(date)));
            } else if (Math.abs(System.currentTimeMillis() - date) < 31536000000L) {
                return String.format(context.getString(R.string.formatDateAtTime), new SimpleDateFormat(context.getString(R.string.chatDate), Locale.US).format(new Date(date)), new SimpleDateFormat(context.getString(R.string.formatterDay12H), Locale.US).format(new Date(date)));
            } else {
                return String.format(context.getString(R.string.formatDateAtTime), new SimpleDateFormat(context.getString(R.string.chatFullDate), Locale.US).format(new Date(date)), new SimpleDateFormat(context.getString(R.string.formatterDay12H), Locale.US).format(new Date(date)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "LOC_ERR";
    }

    public static void checkDisplaySize() {
        try {
            WindowManager manager = (WindowManager) App.getInstance().getSystemService(Context.WINDOW_SERVICE);
            if (manager != null) {
                Display display = manager.getDefaultDisplay();
                if (display != null) {
                    if (android.os.Build.VERSION.SDK_INT < 13) {
                        displaySize.set(display.getWidth(), display.getHeight());
                    } else {
                        display.getSize(displaySize);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Drawable changeDrawableColor(Context context, int icon, int newColor) {
        Drawable mDrawable = ContextCompat.getDrawable(context, icon).mutate();
        mDrawable.setColorFilter(new PorterDuffColorFilter(newColor, PorterDuff.Mode.SRC_IN));
        return mDrawable;
    }

    public static String getPath(Context context, Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static String method(String str) {
        if (str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    public static String formatTimespan(int totalMilliSeconds) {
        int totalSeconds = (int) (TimeUnit.MILLISECONDS.toSeconds(totalMilliSeconds));
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }
}
