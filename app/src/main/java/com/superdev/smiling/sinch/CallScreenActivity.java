package com.superdev.smiling.sinch;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallListener;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.Utils;
import com.superdev.smiling.conversation.ConversationActivity;
import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.recentcall.service.RecentCallService;
import com.superdev.smiling.user.data_model.User;

import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.superdev.smiling.Constants.DESTINATION_ID;
import static com.superdev.smiling.Constants.SENDER_ID;

public class CallScreenActivity extends SinchBaseActivity {

    static final String TAG = CallScreenActivity.class.getSimpleName();

    @BindView(R.id.remoteUser)
    TextView remoteUser;

    @BindView(R.id.callState)
    TextView callState;

    @BindView(R.id.callDuration)
    TextView callDuration;

    @BindView(R.id.iv_user_image)
    ImageView ivUserImage;

    @BindView(R.id.iv_speaker)
    ImageView iv_speaker;

    @BindView(R.id.iv_chat)
    ImageView iv_chat;

    @BindView(R.id.iv_mute)
    ImageView iv_mute;

    private AudioPlayer mAudioPlayer;

    private Timer mTimer;

    private UpdateCallDurationTask mDurationTask;

    private String mCallId;

    private User contactInfo;

    private RecentCall recentCall;

    private RecentCallService recentCallService;

    private boolean isSpeakerEnabled = false;

    private boolean isMute = false;

    private static final String GROUP = "OutGoingCall";

    private static int notificationId = 3333;

    private class UpdateCallDurationTask extends TimerTask {

        @Override
        public void run() {
            CallScreenActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateCallDuration();
                }
            });
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.callscreen);
        ButterKnife.bind(this);
        recentCallService = Dependencies.INSTANCE.getRecentCallService();
        mAudioPlayer = new AudioPlayer(this);

        mCallId = getIntent().getStringExtra(SinchService.CALL_ID);

        contactInfo = getIntent().getParcelableExtra(SinchService.CONTACT_INFO);

        recentCall = getIntent().getParcelableExtra(SinchService.RECENT_CALLS);

        if (contactInfo != null) {
            Utils.loadImage(contactInfo.getImage(), ivUserImage, this, R.drawable.avatar_contact_large);
            remoteUser.setText(!TextUtils.isEmpty(contactInfo.getName()) ? contactInfo.getName() : contactInfo.getPhone());
        }
    }

    @Override
    public void onServiceConnected() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.addCallListener(new SinchCallListener());
            callState.setText(R.string.state_calling);
        } else {
            Log.e(TAG, "Started with invalid callId, aborting.");
            //     finish();

        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        endCall();
    }

    @Override
    public void onPause() {
        super.onPause();
        mDurationTask.cancel();
        mTimer.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTimer = new Timer();
        mDurationTask = new UpdateCallDurationTask();
        mTimer.schedule(mDurationTask, 0, 500);
        sendNotification();
    }

    @Override
    public void onBackPressed() {
        // User should exit activity by ending call, not by going back.
    }

    private void endCall() {
        removeNotification();
        if (mAudioPlayer != null)
            mAudioPlayer.stopProgressTone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        } else {
            getSinchServiceInterface().stopClient();
        }
        finish();
    }

    private String formatTimespan(int totalSeconds) {
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }

    private void updateCallDuration() {
        if (getSinchServiceInterface() == null)
            return;
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            callDuration.setText(formatTimespan(call.getDetails().getDuration()));
        }
    }

    private class SinchCallListener implements CallListener {

        @Override
        public void onCallEnded(Call call) {
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended. Reason: " + cause.toString());
            mAudioPlayer.stopProgressTone();
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
            String endMsg = "Call ended: " + call.getDetails().toString();
            if (recentCall != null) {
                recentCall.setCallTime(call.getDetails().getStartedTime());
                recentCall.setTotalDuration(call.getDetails().getDuration());
                recentCall.setCallEndCause(call.getDetails().getEndCause().getValue());
                recentCall.setCallEndTime(call.getDetails().getEndedTime());
                recentCall.setCallId(call.getCallId());
                recentCallService.setRecentCall(recentCall);
            }
            endCall();
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d(TAG, "Call established");
            mAudioPlayer.stopProgressTone();
            callState.setVisibility(View.GONE);
            callDuration.setVisibility(View.VISIBLE);
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
            mAudioPlayer.playProgressTone();
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // no need to implement if you use managed push
        }
    }

    @OnClick(R.id.hangupButton)
    public void onHangupButtonClicked() {
        endCall();
    }

    @OnClick(R.id.iv_speaker)
    public void onIvSpeakerClicked() {
        AudioController audioController = getSinchServiceInterface().getAudioController();
        if (isSpeakerEnabled) {
            audioController.disableSpeaker();
            isSpeakerEnabled = false;
            iv_speaker.clearColorFilter();
        } else {
            audioController.enableSpeaker();
            isSpeakerEnabled = true;
            iv_speaker.setColorFilter(ContextCompat.getColor(this, R.color.red), PorterDuff.Mode.MULTIPLY);
        }
    }

    @OnClick(R.id.iv_chat)
    public void onIvChatClicked() {
        Bundle bundle = new Bundle();
        bundle.putString(SENDER_ID, Remember.getString(Constants.FIREBASE_USER_ID, ""));
        bundle.putString(DESTINATION_ID, contactInfo.getUid());
        Intent msgInent = new Intent(CallScreenActivity.this, ConversationActivity.class);
        msgInent.putExtras(bundle);
        startActivity(msgInent);
    }

    @OnClick(R.id.iv_mute)
    public void onIvMuteClicked() {
        AudioController audioController = getSinchServiceInterface().getAudioController();
        if (isMute) {
            audioController.unmute();
            isMute = false;
            iv_mute.clearColorFilter();
        } else {
            audioController.mute();
            iv_mute.setColorFilter(ContextCompat.getColor(this, R.color.red), PorterDuff.Mode.MULTIPLY);
            isMute = true;
        }

    }

    private void sendNotification() {

        String body = getString(R.string.outgoing_call);
        Intent intent = new Intent(getApplicationContext(), CallScreenActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(SinchService.CALL_ID, mCallId);
        intent.putExtra(SinchService.CONTACT_INFO, contactInfo);
        intent.putExtra(SinchService.RECENT_CALLS, recentCall);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification_small)
                .setContentTitle(contactInfo.getName() + " " + contactInfo.getPhone())
                .setContentText(body)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setTicker(body)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setGroup(GROUP)
                .setOngoing(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    private void removeNotification() {
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notificationId);
    }
}
