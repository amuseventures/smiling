package com.superdev.smiling.sinch;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallListener;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.Utils;
import com.superdev.smiling.conversation.ConversationActivity;
import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.recentcall.service.RecentCallService;
import com.superdev.smiling.user.data_model.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTouch;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.superdev.smiling.Constants.DESTINATION_ID;
import static com.superdev.smiling.Constants.SENDER_ID;

public class IncomingCallScreenActivity extends SinchBaseActivity {

    static final String TAG = IncomingCallScreenActivity.class.getSimpleName();

    @BindView(R.id.iv_user_image)
    CircleImageView ivUserImage;

    @BindView(R.id.tv_voice_call)
    TextView tvVoiceCall;

    @BindView(R.id.remoteUser)
    TextView remoteUser;

    @BindView(R.id.callState)
    TextView callState;

    @BindView(R.id.decline_incoming_call_view)
    ImageView declineIncomingCallView;

    @BindView(R.id.accept_incoming_call_view)
    ImageView acceptIncomingCallView;

    @BindView(R.id.reply_incoming_call_view)
    ImageView replyIncomingCallView;

    float dX, dY;

    private String mCallId;

    private AudioPlayer mAudioPlayer;

    private boolean isOpened = false;

    private Animation translateShake;

    private RecentCall recentCall;

    private boolean mCallType;

    private User contactUser;

    private RecentCallService recentCallService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incoming_audio_call);
        ButterKnife.bind(this);
        recentCallService = Dependencies.INSTANCE.getRecentCallService();
        mAudioPlayer = new AudioPlayer(this);
        mAudioPlayer.playRingtone();

        mCallId = getIntent().getStringExtra(SinchService.CALL_ID);

        mCallType = getIntent().getBooleanExtra(SinchService.CALL_TYPE, false);

        if (getIntent().hasExtra(SinchService.RECENT_CALLS)) {
            recentCall = getIntent().getParcelableExtra(SinchService.RECENT_CALLS);
        } else {
            recentCall = new RecentCall();
        }

        if (getIntent().hasExtra(SinchService.CONTACT_INFO)) {
            contactUser = getIntent().getParcelableExtra(SinchService.CONTACT_INFO);
        }

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT_WATCH) {
            isScreenOn = pm.isInteractive();
        } else {
            isScreenOn = pm.isScreenOn();
        }
        if (!isScreenOn) {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "MyLock");
            wl.acquire(10000);
            wl.release();
        }

        String mRemoteUserId = getIntent().getStringExtra(SinchService.REMOTE_USER_ID);

        if (contactUser != null && contactUser.getName() != null) {
            remoteUser.setText(contactUser.getName());
        } else {
            remoteUser.setText(mRemoteUserId);
        }

        Utils.loadImageElseWhite(contactUser != null && contactUser.getImage() != null ? contactUser.getImage() : null, ivUserImage, this, R.drawable.avatar_contact_large);

        translateShake = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.anim_bounce_shake);
        acceptIncomingCallView.setAnimation(translateShake);
        translateShake.setRepeatCount(Animation.INFINITE);
        translateShake.start();

        if (mCallType) {
            tvVoiceCall.setText(R.string.smile_video_call_title);
        } else {
            tvVoiceCall.setText(R.string.smile_voice_call_title);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @OnTouch(R.id.accept_incoming_call_view)
    boolean onAcceptCallViewSwipe(View view, MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                dX = view.getX() - event.getRawX();
                dY = view.getY() - event.getRawY();
                break;

            case MotionEvent.ACTION_MOVE:
                view.setY(event.getRawY() + dY);
                //view.setX(event.getRawX() + dX);
                if (!isOpened) {
                    translateShake.cancel();
                    if (!mCallType) {
                        answerClicked();
                    } else {
                        acceptVideoCall();
                    }
                    isOpened = true;
                }
                break;

            case MotionEvent.ACTION_UP:
                break;

            default:
                return false;
        }
        return true;
    }


    @OnTouch(R.id.decline_incoming_call_view)
    boolean onDeclineIncomingCallView(View view, MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                dX = view.getX() - event.getRawX();
                dY = view.getY() - event.getRawY();
                break;

            case MotionEvent.ACTION_MOVE:
                view.setY(event.getRawY() + dY);
                //view.setX(event.getRawX() + dX);
                if (!isOpened) {
                    translateShake.cancel();
                    declineClicked();
                    isOpened = true;
                }
                break;

            case MotionEvent.ACTION_UP:
                break;

            default:
                return false;
        }
        return true;
    }

    @OnTouch(R.id.reply_incoming_call_view)
    boolean onReplyDeclineIncomingCallView(View view, MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                dX = view.getX() - event.getRawX();
                dY = view.getY() - event.getRawY();
                break;

            case MotionEvent.ACTION_MOVE:
                view.setY(event.getRawY() + dY);
                //view.setX(event.getRawX() + dX);
                if (!isOpened) {
                    translateShake.cancel();
                    declineClicked();
                    openMesage();
                    isOpened = true;
                }
                break;

            case MotionEvent.ACTION_UP:
                break;

            default:
                return false;
        }
        return true;
    }

    private void openMesage() {
        Bundle bundle = new Bundle();
        bundle.putString(SENDER_ID, Remember.getString(Constants.FIREBASE_USER_ID, ""));
        bundle.putString(DESTINATION_ID, contactUser.getUid());
        Intent msgInent = new Intent(IncomingCallScreenActivity.this, ConversationActivity.class);
        msgInent.putExtras(bundle);
        startActivity(msgInent);
    }


    @Override
    protected void onServiceConnected() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.addCallListener(new SinchCallListener());
            callState.setText(R.string.incoming_call_status);
        } else {
            Log.e(TAG, "Started with invalid callId, aborting");
            finish();
        }
    }

    public void answerClicked() {
        mAudioPlayer.stopRingtone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            Log.d(TAG, "Answering call");
            call.answer();
            Intent intent = new Intent(this, CallScreenActivity.class);
            intent.putExtra(SinchService.CALL_ID, mCallId);
            if (contactUser != null) {
                intent.putExtra(SinchService.CONTACT_INFO, contactUser);
            }
            recentCall.setType(Constants.TYPE_IN);
            intent.putExtra(SinchService.RECENT_CALLS, recentCall);
            startActivity(intent);
        } else {
            finish();
        }
    }

    public void declineClicked() {
        mAudioPlayer.stopRingtone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        finish();
    }

    private class SinchCallListener implements CallListener {

        @Override
        public void onCallEnded(Call call) {
            if (call != null && recentCall != null) {
                recentCall.setType(Constants.TYPE_MISSED);
                recentCall.setCallTime(call.getDetails().getStartedTime());
                recentCall.setCallEndTime(call.getDetails().getEndedTime());
                recentCall.setCallId(call.getCallId());
                recentCall.setCallEndCause(call.getDetails().getEndCause().getValue());
                recentCall.setTotalDuration(call.getDetails().getDuration());
                recentCallService.setRecentCall(recentCall);
            }
            mAudioPlayer.stopRingtone();
            finish();
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d(TAG, "Call established");
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // no need to implement for managed push
        }
    }

    public void acceptVideoCall() {
        mAudioPlayer.stopRingtone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.answer();
            Intent intent = new Intent(this, VideoCallerScreenActivity.class);
            intent.putExtra(SinchService.CALL_ID, mCallId);
            if (contactUser != null) {
                intent.putExtra(SinchService.CONTACT_INFO, contactUser);
            }
            recentCall.setType(Constants.TYPE_IN);
            intent.putExtra(SinchService.RECENT_CALLS, recentCall);
            startActivity(intent);
        }
        finish();
    }
}
