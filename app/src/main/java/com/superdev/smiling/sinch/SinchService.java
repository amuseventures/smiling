package com.superdev.smiling.sinch;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.ClientRegistration;
import com.sinch.android.rtc.NotificationResult;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchClientListener;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallClient;
import com.sinch.android.rtc.calling.CallClientListener;
import com.sinch.android.rtc.video.VideoController;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.Remember;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.service.UserService;

import java.util.Date;

import rx.Observer;

public class SinchService extends Service {

    /*private static final String APP_KEY = "enter-application-key";
    private static final String APP_SECRET = "enter-application-secret";
    private static final String ENVIRONMENT = "sandbox.sinch.com";
*/
    private static final String APP_KEY = "5ff79759-16ec-46ae-b332-a0dd6277e588";
    private static final String APP_SECRET = "vYtOQ8lZOUCA2z4pl0rA9Q==";
    private static final String ENVIRONMENT = "clientapi.sinch.com";

    static final String TAG = SinchService.class.getSimpleName();
    public static final String CALL_ID = "CALL_ID";
    public static final String CALL_TYPE = "CALL_TYPE";
    public static final String CONTACT_INFO = "contact_info";
    public static final String RECENT_CALLS = "recent_calls";
    public static final String REMOTE_USER_ID = "remote_user_id";
    private PersistedSettings mSettings;
    private UserService userService;
    private SinchServiceInterface mSinchServiceInterface = new SinchServiceInterface();
    private SinchClient mSinchClient;

    private StartFailedListener mListener;

    @Override
    public void onCreate() {
        super.onCreate();
        Dependencies.INSTANCE.init(this);
        mSettings = new PersistedSettings(getApplicationContext());
        userService = Dependencies.INSTANCE.getUserService();
    }

    private void createClient(String username) {
        mSinchClient = Sinch.getSinchClientBuilder().context(getApplicationContext()).userId(username)
                .applicationKey(APP_KEY)
                .applicationSecret(APP_SECRET)
                .environmentHost(ENVIRONMENT).build();

        mSinchClient.setSupportCalling(true);
        mSinchClient.setSupportManagedPush(true);

        mSinchClient.checkManifest();

        mSinchClient.addSinchClientListener(new MySinchClientListener());
        mSinchClient.getCallClient().addCallClientListener(new SinchCallClientListener());
    }

    @Override
    public void onDestroy() {
        if (mSinchClient != null && mSinchClient.isStarted()) {
            mSinchClient.terminate();
        }
        super.onDestroy();
    }

    private void start(String username) {
        if (mSinchClient == null) {
            mSettings.setUsername(username);
            createClient(username);
        }
        Log.d(TAG, "Starting SinchClient");
        if (!mSinchClient.isStarted()) {
            mSinchClient.start();
        }
    }

    private void stop() {
        if (mSinchClient != null) {
            mSinchClient.terminate();
            mSinchClient = null;
        }
    }

    private boolean isStarted() {
        return (mSinchClient != null && mSinchClient.isStarted());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mSinchServiceInterface;
    }

    public class SinchServiceInterface extends Binder {

        public Call callUser(String userId) {
            if (mSinchClient == null || !mSinchClient.isStarted()) {
                startClient(Remember.getString(Constants.FIREBASE_USERS_PHONE, ""));
            }
            if (mSinchClient == null) {
                return null;
            } else {
                try {
                    return mSinchClient.getCallClient().callUser(userId);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }

        public String getUserName() {
            return mSinchClient.getLocalUserId();
        }

        public boolean isStarted() {
            return SinchService.this.isStarted();
        }

        public void startClient(String userName) {
            start(userName);
        }

        public void stopClient() {
            stop();
        }

        public void setStartListener(StartFailedListener listener) {
            mListener = listener;
        }

        public Call getCall(String callId) {
            if (mSinchClient == null)
                return null;
            else
                return mSinchClient.getCallClient().getCall(callId);
        }

        public NotificationResult relayRemotePushNotificationPayload(Intent intent) {
            if (mSinchClient == null && !mSettings.getUsername().isEmpty()) {
                createClient(mSettings.getUsername());
            } else if (mSinchClient == null && mSettings.getUsername().isEmpty()) {
                Log.e(TAG, "Can't start a SinchClient as no username is available, unable to relay push.");
                return null;
            }
            return mSinchClient.relayRemotePushNotificationPayload(intent);
        }

        public Call callUserVideo(String userId) {
            if (mSinchClient == null)
                return null;
            return mSinchClient.getCallClient().callUserVideo(userId);
        }

        public Call callConferenceVideo(String conferenceId) {
            if (mSinchClient == null)
                return null;
            return mSinchClient.getCallClient().callConference(conferenceId);
        }

        public VideoController getVideoController() {
            if (!isStarted()) {
                return null;
            }
            return mSinchClient.getVideoController();
        }

        public AudioController getAudioController() {
            if (!isStarted()) {
                return null;
            }
            return mSinchClient.getAudioController();
        }
    }

    public interface StartFailedListener {

        void onStartFailed(SinchError error);

        void onStarted();
    }

    private class MySinchClientListener implements SinchClientListener {

        @Override
        public void onClientFailed(SinchClient client, SinchError error) {
            if (mListener != null) {
                mListener.onStartFailed(error);
            }
            mSinchClient.terminate();
            mSinchClient = null;
        }

        @Override
        public void onClientStarted(SinchClient client) {
            Log.d(TAG, "SinchClient started");
            if (mListener != null) {
                mListener.onStarted();
            }
        }

        @Override
        public void onClientStopped(SinchClient client) {
            Log.d(TAG, "SinchClient stopped");
        }

        @Override
        public void onLogMessage(int level, String area, String message) {
            switch (level) {
                case Log.DEBUG:
                    Log.d(area, message);
                    break;
                case Log.ERROR:
                    Log.e(area, message);
                    break;
                case Log.INFO:
                    Log.i(area, message);
                    break;
                case Log.VERBOSE:
                    Log.v(area, message);
                    break;
                case Log.WARN:
                    Log.w(area, message);
                    break;
            }
        }

        @Override
        public void onRegistrationCredentialsRequired(SinchClient client,
                                                      ClientRegistration clientRegistration) {
        }
    }

    private class SinchCallClientListener implements CallClientListener {

        @Override
        public void onIncomingCall(CallClient callClient, Call call) {
            if (call == null)
                return;
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference userContactDB = firebaseDatabase.getReference(Constants.FIREBASE_CONTACTS).child(Remember.getString(Constants.FIREBASE_USERS_PHONE, ""));
            userContactDB.child(call.getRemoteUserId())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Log.d(TAG, "onDataChange: " + dataSnapshot);
                            User user = dataSnapshot.getValue(User.class);
                            RecentCall recentCallEntity = new RecentCall();
                            Intent intent = new Intent(SinchService.this, IncomingCallScreenActivity.class);
                            if (!call.getDetails().isVideoOffered()) {
                                recentCallEntity.setCallType(Constants.AUDIO_TYPE);
                            } else {
                                recentCallEntity.setCallType(Constants.VIDEO_TYPE);
                            }
                            if (user != null) {
                                recentCallEntity.setUserContactEntity(user);
                                recentCallEntity.setCallId(call.getCallId());
                                recentCallEntity.setCallTime(new Date().getTime());
                                recentCallEntity.setType(Constants.TYPE_IN);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra(CALL_TYPE, call.getDetails().isVideoOffered());
                                intent.putExtra(RECENT_CALLS, recentCallEntity);
                                intent.putExtra(CALL_ID, call.getCallId());
                                intent.putExtra(CONTACT_INFO, user);
                                SinchService.this.startActivity(intent);
                            } else {
                                userService.getContact(call.getRemoteUserId())
                                        .subscribe(new Observer<DatabaseResult<User>>() {
                                            @Override
                                            public void onCompleted() {

                                            }

                                            @Override
                                            public void onError(Throwable throwable) {
                                                call.hangup();
                                            }

                                            @Override
                                            public void onNext(DatabaseResult<User> userDatabaseResult) {
                                                if (userDatabaseResult.isSuccess()) {
                                                    User remoteUser = userDatabaseResult.getData();
                                                    User newuser = new User(remoteUser.getUid(), "", "", remoteUser.getPhone());
                                                    recentCallEntity.setUserContactEntity(newuser);
                                                    recentCallEntity.setCallId(call.getCallId());
                                                    recentCallEntity.setCallTime(new Date().getTime());
                                                    recentCallEntity.setType(Constants.TYPE_IN);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    intent.putExtra(CALL_TYPE, call.getDetails().isVideoOffered());
                                                    intent.putExtra(RECENT_CALLS, recentCallEntity);
                                                    intent.putExtra(CALL_ID, call.getCallId());
                                                    intent.putExtra(CONTACT_INFO, newuser);
                                                    SinchService.this.startActivity(intent);
                                                } else {
                                                    call.hangup();
                                                }
                                            }
                                        });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.d(TAG, "onCancelled: " + databaseError);
                            Crashlytics.logException(databaseError.toException());
                            call.hangup();
                        }
                    });
        }
    }


    private class PersistedSettings {

        private SharedPreferences mStore;

        private static final String PREF_KEY = "Sinch";

        PersistedSettings(Context context) {
            mStore = context.getSharedPreferences(PREF_KEY, MODE_PRIVATE);
        }

        String getUsername() {
            return mStore.getString("Username", "");
        }

        void setUsername(String username) {
            SharedPreferences.Editor editor = mStore.edit();
            editor.putString("Username", username);
            editor.apply();
        }
    }
}
