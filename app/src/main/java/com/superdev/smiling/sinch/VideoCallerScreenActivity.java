package com.superdev.smiling.sinch;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallState;
import com.sinch.android.rtc.video.VideoCallListener;
import com.sinch.android.rtc.video.VideoController;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.conversation.ConversationActivity;
import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.recentcall.service.RecentCallService;
import com.superdev.smiling.user.data_model.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.superdev.smiling.Constants.DESTINATION_ID;
import static com.superdev.smiling.Constants.SENDER_ID;

public class VideoCallerScreenActivity extends SinchBaseActivity {

    static final String ADDED_LISTENER = "addedListener";

    static final String TAG = VideoCallerScreenActivity.class.getSimpleName();

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.hangupButton)
    FloatingActionButton hangupButton;

    @BindView(R.id.iv_switch_camera)
    ImageView ivSwitchCamera;

    @BindView(R.id.iv_speaker)
    ImageView iv_speaker;

    @BindView(R.id.iv_chat)
    ImageView iv_chat;


    @BindView(R.id.iv_mic)
    ImageView ivMic;

    @BindView(R.id.localVideo)
    LinearLayout localVideo;

    @BindView(R.id.remoteVideo)
    LinearLayout remoteVideo;

    private String mCallId;

    private AudioPlayer mAudioPlayer;

    private boolean mAddedListener = false;

    private boolean mLocalVideoViewAdded = false;

    private boolean mRemoteVideoViewAdded = false;

    private boolean isMute = false;

    private User contactInfo;

    private RecentCall recentCall;

    private RecentCallService recentCallService;

    private boolean isExpendedMyVideo = true;

    private boolean isSpeakerEnabled = false;

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean(ADDED_LISTENER, mAddedListener);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mAddedListener = savedInstanceState.getBoolean(ADDED_LISTENER);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_caller_screen);
        ButterKnife.bind(this);
        recentCallService = Dependencies.INSTANCE.getRecentCallService();
        mAudioPlayer = new AudioPlayer(this);
        mCallId = getIntent().getStringExtra(SinchService.CALL_ID);
        contactInfo = getIntent().getParcelableExtra(SinchService.CONTACT_INFO);
        recentCall = getIntent().getParcelableExtra(SinchService.RECENT_CALLS);
        localVideo.setOnTouchListener(touchListener);
        localVideo.setOnClickListener(clickListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @OnClick({R.id.hangupButton, R.id.iv_mic, R.id.iv_speaker, R.id.iv_chat})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.hangupButton:
                endCall();
                break;
            case R.id.iv_mic:
                if (getSinchServiceInterface() == null)
                    return;
                AudioController audioController = getSinchServiceInterface().getAudioController();
                if (!isMute) {
                    audioController.mute();
                    isMute = true;
                    ivMic.setColorFilter(ContextCompat.getColor(this, R.color.red), PorterDuff.Mode.MULTIPLY);
                } else {
                    audioController.unmute();
                    isMute = false;
                    ivMic.clearColorFilter();
                }

                break;
            case R.id.iv_speaker:
                audioController = getSinchServiceInterface().getAudioController();
                if (isSpeakerEnabled) {
                    audioController.disableSpeaker();
                    isSpeakerEnabled = false;
                    iv_speaker.clearColorFilter();
                } else {
                    audioController.enableSpeaker();
                    isSpeakerEnabled = true;
                    iv_speaker.setColorFilter(ContextCompat.getColor(this, R.color.red), PorterDuff.Mode.MULTIPLY);
                }
                break;
            case R.id.iv_chat:
                Bundle bundle = new Bundle();
                bundle.putString(SENDER_ID, Remember.getString(Constants.FIREBASE_USER_ID, ""));
                bundle.putString(DESTINATION_ID, contactInfo.getUid());
                Intent msgInent = new Intent(VideoCallerScreenActivity.this, ConversationActivity.class);
                msgInent.putExtras(bundle);
                startActivity(msgInent);
                break;

        }
    }

    private void updateUI() {
        if (getSinchServiceInterface() == null) {
            return;
        }
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            if (call.getState() == CallState.ESTABLISHED) {
                isExpendedMyVideo = false;
            }
            if (call.getDetails().isVideoOffered()) {
                addLocalView();
                if (call.getState() == CallState.ESTABLISHED) {
                    addRemoteView();
                }
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        removeVideoViews();
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUI();
    }

    @Override
    public void onBackPressed() {
        // User should exit activity by ending call, not by going back.
    }

    private void endCall() {
        if (mAudioPlayer != null) {
            mAudioPlayer.stopProgressTone();
        }
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        finish();
    }

    private void addLocalView() {
        if (getSinchServiceInterface() == null) {
            return; //early
        }
        final VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            if( ! isExpendedMyVideo) {
                try {
                    localVideo.addView(vc.getLocalView());
                } catch (Exception e) {
                    e.printStackTrace();
                    if(vc.getLocalView().getParent() != null) {
                        ((ViewGroup)vc.getLocalView().getParent()).removeView(vc.getLocalView());
                    }
                    localVideo.addView(vc.getLocalView());
                }
            }
            else {
                try {
                    remoteVideo.addView(vc.getLocalView());
                } catch (Exception e) {
                    e.printStackTrace();
                    if(vc.getLocalView().getParent() != null) {
                        ((ViewGroup)vc.getLocalView().getParent()).removeView(vc.getLocalView());
                    }
                    remoteVideo.addView(vc.getLocalView());
                }
            }
            mLocalVideoViewAdded = true;
            ivSwitchCamera.setOnClickListener(view -> vc.toggleCaptureDevicePosition());
        }
    }

    private void addRemoteView() {
        if ( getSinchServiceInterface() == null) {
            return; //early
        }
        final VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            if( ! isExpendedMyVideo) {
                try {
                    remoteVideo.addView(vc.getRemoteView());
                } catch (Exception e) {
                    e.printStackTrace();
                    if(vc.getRemoteView().getParent() != null) {
                        ((ViewGroup)vc.getRemoteView().getParent()).removeView(vc.getRemoteView());
                    }
                    remoteVideo.addView(vc.getRemoteView());
                }
            }
            else {
                try {
                    localVideo.addView(vc.getRemoteView());
                } catch (Exception e) {
                    e.printStackTrace();
                    if(vc.getRemoteView().getParent() != null) {
                        ((ViewGroup)vc.getRemoteView().getParent()).removeView(vc.getRemoteView());
                    }
                    localVideo.addView(vc.getRemoteView());
                    localVideo.requestLayout();
                    localVideo.invalidate();
                }
            }
            mRemoteVideoViewAdded = true;
        }
    }

    @Override
    public void onServiceConnected() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            if (!mAddedListener) {
                call.addCallListener(new SinchCallListener());
                mAddedListener = true;
            }
        } else {
            Log.e(TAG, "Started with invalid callId, aborting.");
            finish();
        }

        updateUI();
    }


    private void removeVideoViews() {
        if (getSinchServiceInterface() == null) {
            return; // early
        }

        VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            try {
                remoteVideo.removeView(vc.getRemoteView());
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            try {
                localVideo.removeView(vc.getLocalView());
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            mLocalVideoViewAdded = false;
            mRemoteVideoViewAdded = false;
        }
    }

    private class SinchCallListener implements VideoCallListener {

        @Override
        public void onCallEnded(Call call) {
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended. Reason: " + cause.toString());
            if (recentCall != null) {
                recentCall.setCallTime(call.getDetails().getStartedTime());
                recentCall.setTotalDuration(call.getDetails().getDuration());
                recentCall.setCallEndCause(call.getDetails().getEndCause().getValue());
                recentCall.setCallEndTime(call.getDetails().getEndedTime());
                recentCall.setCallId(call.getCallId());
                recentCallService.setRecentCall(recentCall);
            }

            if (mAudioPlayer != null) {
                mAudioPlayer.stopProgressTone();
            }
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
            endCall();
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d(TAG, "Call established");
            updateUI();
            if (mAudioPlayer != null) {
                mAudioPlayer.stopProgressTone();
            }
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            Log.d(TAG, "Call offered video: " + call.getDetails().isVideoOffered());
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
            if (mAudioPlayer != null) {
                mAudioPlayer.playProgressTone();
            }
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // Send a push through your push provider here, e.g. GCM

        }

        @Override
        public void onVideoTrackAdded(Call call) {
            Log.d(TAG, "Video track added");
            addRemoteView();
        }

        @Override
        public void onVideoTrackPaused(Call call) {

        }

        @Override
        public void onVideoTrackResumed(Call call) {

        }
    }

    float dX, dY;

    private final View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            view.onTouchEvent(motionEvent);
            if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                dX = view.getX() - motionEvent.getRawX();
                dY = view.getY() - motionEvent.getRawY();
            } else if(motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                view.animate()
                        .x(motionEvent.getRawX() + dX)
                        .y(motionEvent.getRawY() + dY)
                        .setDuration(0)
                        .start();
            }
            return false;
        }
    };

    private final View.OnClickListener clickListener = view -> {

    };
}
