package com.superdev.smiling.navigation;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.channel.ChannelsActivity;
import com.superdev.smiling.channel_list.ChannelListFragment;
import com.superdev.smiling.conversation_list.ConversationListFragment;
import com.superdev.smiling.firstlogin.UserFirstLoginActivity;
import com.superdev.smiling.group.GroupContactPickActivity;
import com.superdev.smiling.phonelogin.PhoneLoginActivity;
import com.superdev.smiling.profile.UserProfileActivity;
import com.superdev.smiling.recentcall.RecentCallFragment;
import com.superdev.smiling.settings.SettingsActivity;
import com.superdev.smiling.user.UsersFragment;
import com.superdev.smiling.utility.PrefManager;

import java.util.Locale;

import static android.app.Activity.RESULT_OK;

/**
 * Created by marco on 16/08/16.
 */

public class AndroidMainNavigator implements MainNavigator {

    private static final String TAG = AndroidMainNavigator.class.getSimpleName();
    private static final int REQUEST_INVITE = 1;

    private boolean doubleBackToExitPressedOnce = false;
    private boolean firstOpen = true;

    private final AppCompatActivity activity;

    public AndroidMainNavigator(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void attach() {

    }

    @Override
    public void detach() {

    }

    @Override
    public void init() {
        if (firstOpen) {
            this.toConversations();
            firstOpen = false;
        }
    }

    @Override
    public void toConversations() {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, ConversationListFragment.newInstance())
                .commit();

    }

    @Override
    public void toRecentCalls() {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, RecentCallFragment.newInstance())
                .commit();
    }

    @Override
    public void showLanguageSettingsDialog() {
        final CharSequence[] items = {"English", "\u200E" + "پښتو", "\u200E" + "فارسی", "\u200E" + "عربی"};
        final int[] nLang = {-1};
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.languageSettings);
        builder.setSingleChoiceItems(items, -1,
                (dialog, item) -> nLang[0] = item);
        builder.setPositiveButton("Yes", (dialog, id) -> {
            PrefManager prefManager = new PrefManager(activity);
            prefManager.setLanguageSettings(nLang[0]);
            activity.recreate();
        });
        builder.setNegativeButton("No", (dialog, id) -> {

        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void toSettings() {
        activity.startActivity(new Intent(activity, SettingsActivity.class));
    }

    @Override
    public void toContactList() {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, UsersFragment.newInstance())
                .commit();
    }

    public void toInvite() {
        /*Intent intent = new AppInviteInvitation.IntentBuilder(activity.getString(R.string.main_invite_title))
                .setMessage(activity.getString(R.string.main_invite_message))
                .setCallToActionText(activity.getString(R.string.main_invite_cta))
                .build();
        activity.startActivityForResult(intent, REQUEST_INVITE);*/
    }

    @Override
    public void toNewGroupCreate() {
        activity.startActivity(new Intent(activity, GroupContactPickActivity.class));
    }

    @Override
    public void toProfile() {
        activity.startActivity(new Intent(activity, UserProfileActivity.class));
    }

    public void toChannels() {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, ChannelListFragment.newInstance())
                .commit();
        //activity.startActivity(new Intent(activity, ChannelsActivity.class));
    }


    @Override
    public void toFirstLogin() {
        activity.startActivity(new Intent(activity, UserFirstLoginActivity.class));
        activity.finish();
    }

    @Override
    public void toLogin() {
        Dependencies.INSTANCE.clearDependecies();
        activity.startActivity(new Intent(activity, PhoneLoginActivity.class));
        activity.finish();
    }

    @Override
    public void toShareInvite(String sharingLink) {
        String sharingMessage = String.format(Locale.getDefault(), activity.getString(R.string.tell_a_friend_sms), sharingLink);
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, sharingMessage);
        activity.startActivity(Intent.createChooser(sharingIntent, sharingMessage));
    }

    @Override
    public Boolean onBackPressed() {
        if (doubleBackToExitPressedOnce)
            activity.finishAffinity();

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(activity, R.string.main_toast_exit_message, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
        return true;
    }

    @Override
    public void addDrawerToggle(DrawerLayout drawerLayout, Toolbar toolbar) {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                activity, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_INVITE) {
            if (resultCode == RESULT_OK) {
                // Use Firebase Measurement to log that invitation was sent.
                Bundle payload = new Bundle();
                payload.putString(FirebaseAnalytics.Param.VALUE, "inv_sent");

                // Check how many invitations were sent and log.
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                Log.d(TAG, "Invitations sent: " + ids.length);
            } else {
                // Use Firebase Measurement to log that invitation was not sent
                Bundle payload = new Bundle();
                payload.putString(FirebaseAnalytics.Param.VALUE, "inv_not_sent");

                // Sending failed or it was canceled, show failure message to the user
                Log.d(TAG, "Failed to send invitation.");
            }
            return true;
        } else
            return false;
    }
}
