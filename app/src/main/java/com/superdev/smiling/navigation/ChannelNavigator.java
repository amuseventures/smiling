package com.superdev.smiling.navigation;

import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.AudioModel;
import com.superdev.smiling.conversation.data_model.ContactModel;
import com.superdev.smiling.conversation.data_model.DocModel;
import com.superdev.smiling.conversation.data_model.MapModel;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.user.data_model.User;

/**
 * Created by gautam on 10/2/17.
 */

public interface ChannelNavigator extends Navigator {

    void toChannel(Channel channel);

    void toChannels();

    void toCreateChannel();

    void toLogin();

    void attach(OnImageSelectedListener imageSelectedListener);

    void detach(OnImageSelectedListener imageSelectedListener);

    void toMembersOf(Channel channel);

    void toParent();

    void toChannelWithClearedHistory(Channel channel);

    void toShareInvite(String sharingLink);

    void toSelectChannelImage();

    void onDocumentsPressed();

    void onCameraPressed();

    void onGalleryPressed();

    void onAudioPressed();

    void onLocationPressed();

    void onContactPressed();

    void toOpenGoogleMaps(GroupMessage message);

    void openDocument(String self, GroupMessage message);

    void toFullScreenImageActivity(String self, View view, GroupMessage message);

    void openDeleteChannelDialog(String uid, Channel channel);


    interface OnImageSelectedListener {

        void onImageSelected(Uri photoUri, String message);

        void onMapSelected(MapModel message);

        void onDocSelected(DocModel docModel);

        void onAudioSelected(AudioModel audioModel);

        void onContactSelected(ContactModel contactModel);

        void onCabFinished();

        void onMessageDeleted(boolean deleteMedia, GroupMessage message);

        void onImageSelected(byte[] imageBytes, Bitmap selectedImage);

        void onProfileRemove();

        void onUserLogin();

        void onUserLoginError();
    }
}
