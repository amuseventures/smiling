package com.superdev.smiling.navigation;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;

/**
 * Created by marco on 16/08/16.
 */

public interface MainNavigator {

    void attach();

    void detach();

    void init();

    void toConversations();

    void toRecentCalls();

    void toContactList();

    void toInvite();

    void toNewGroupCreate();

    void toProfile();

    void toFirstLogin();

    void toLogin();

    void toShareInvite(String sharingLink);

    Boolean onBackPressed();

    void addDrawerToggle(DrawerLayout drawerLayout, Toolbar toolbar);

    void showLanguageSettingsDialog();

    void toSettings();
}
