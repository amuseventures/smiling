package com.superdev.smiling.navigation;

import android.app.Activity;
import android.content.Intent;

import com.superdev.smiling.main.MainActivity;
import com.superdev.smiling.phonelogin.PhoneLoginActivity;

/**
 * Created by marco on 27/07/16.
 */

public class AndroidNavigator implements Navigator {

    private final Activity activity;

    public AndroidNavigator(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void toLogin() {
        Intent intent = new Intent(activity, PhoneLoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

    @Override
    public void toMainActivity() {
        activity.startActivity(new Intent(activity, MainActivity.class));
        activity.finish();

    }

    @Override
    public void toParent() {
        activity.finish();
    }

    @Override
    public void toShareInvite(String sharingLink) {

    }

}
