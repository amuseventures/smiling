package com.superdev.smiling.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.sinch.android.rtc.calling.Call;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.conversation.ConversationActivity;
import com.superdev.smiling.conversation_list.QuickContactActivity;
import com.superdev.smiling.conversation_list.data_model.Conversation;
import com.superdev.smiling.conversation_list.service.ConversationListService;
import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.sinch.CallScreenActivity;
import com.superdev.smiling.sinch.SinchService;
import com.superdev.smiling.sinch.VideoCallerScreenActivity;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.userpicker.ContactUserPickerActivity;
import com.superdev.smiling.userpicker.UserPickerActivity;

import java.util.Date;
import java.util.Locale;

import static com.superdev.smiling.Constants.AUDIO_TYPE;
import static com.superdev.smiling.Constants.TYPE_OUT;
import static com.superdev.smiling.Constants.VIDEO_TYPE;


/**
 * Created by marco on 31/07/16.
 */

public class AndroidConversationsNavigator implements Navigator {

    private final AppCompatActivity activity;

    private final Navigator navigator;

    private ConversationListService conversationListService;

    public AndroidConversationsNavigator(AppCompatActivity activity, Navigator navigator) {
        this.activity = activity;
        this.navigator = navigator;
        conversationListService = Dependencies.INSTANCE.getConversationListService();
    }

    @Override
    public void toLogin() {
        activity.onBackPressed();
    }

    @Override
    public void toMainActivity() {

    }

    @Override
    public void toParent() {

    }

    @Override
    public void toShareInvite(String sharingLink) {

    }

    public void placeCall(SinchService.SinchServiceInterface mSinchServiceInterface, User user) {
        if (mSinchServiceInterface != null) {
            RecentCall recentCallEntity = new RecentCall();
            recentCallEntity.setCallTime(new Date().getTime());
            recentCallEntity.setCallType(AUDIO_TYPE);
            recentCallEntity.setType(TYPE_OUT);
            recentCallEntity.setUserContactEntity(user);
            recentCallEntity.setTotalDuration(0);
            Call call = mSinchServiceInterface.callUser(user.getPhone());
            if (call == null) {
                return;
            }
            String callId = call.getCallId();
            recentCallEntity.setCallId(callId);
            Intent callScreen = new Intent(activity, CallScreenActivity.class);
            callScreen.putExtra(SinchService.CALL_ID, callId);
            callScreen.putExtra(SinchService.CONTACT_INFO, user);
            callScreen.putExtra(SinchService.RECENT_CALLS, recentCallEntity);
            activity.startActivity(callScreen);
        }
    }

    public void placeVideoCall(SinchService.SinchServiceInterface mSinchServiceInterface, User user) {
        if (mSinchServiceInterface != null) {
            RecentCall recentCallEntity = new RecentCall();
            recentCallEntity.setCallTime(new Date().getTime());
            recentCallEntity.setCallType(VIDEO_TYPE);
            recentCallEntity.setType(TYPE_OUT);
            recentCallEntity.setUserContactEntity(user);
            recentCallEntity.setTotalDuration(0);
            if (user == null) {
                Toast.makeText(activity, R.string.error_call, Toast.LENGTH_LONG).show();
                return;
            }
            Call call = mSinchServiceInterface.callUserVideo(user.getPhone());
            if (call == null) {
                return;
            }
            String callId = call.getCallId();
            recentCallEntity.setCallId(callId);
            Intent callScreen = new Intent(activity, VideoCallerScreenActivity.class);
            callScreen.putExtra(SinchService.CALL_ID, callId);
            callScreen.putExtra(SinchService.CONTACT_INFO, user);
            callScreen.putExtra(SinchService.RECENT_CALLS, recentCallEntity);
            activity.startActivity(callScreen);
        }
    }

    public void toSelectedConversation(Bundle bundle) {
        Intent intent = new Intent(activity, ConversationActivity.class);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public void openQuickContact(Bundle bundle) {
        Intent intent = new Intent(activity, QuickContactActivity.class);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public void setContactData(User user) {
        Intent intent = new Intent();
        intent.putExtra(Constants.CONTACT_PICK_EXTRA, user);
        activity.setResult(Activity.RESULT_OK, intent);
        activity.finish();
    }

    public void openDeleteConversationDialog(String selfId, Conversation conversation) {
        View view = LayoutInflater.from(activity).inflate(R.layout.delete_media_messages_dialog, null, false);
        TextView message = view.findViewById(R.id.groupMessage);
        AppCompatCheckBox deleteMedia = view.findViewById(R.id.delete_media);
        message.setText(String.format(Locale.getDefault(), activity.getString(R.string.delete_contact_dialog_title), conversation.getName()));
        new MaterialDialog.Builder(activity)
                .customView(view, false)
                .positiveText(R.string.delete_info)
                .negativeText(R.string.cancel)
                .onPositive((dialog1, which) -> {
                    User user = new User();
                    user.setUid(selfId);
                    conversationListService.removeValue(user)
                            .subscribe(aBoolean -> {
                                if (!aBoolean) {
                                    Intent intent = new Intent(Constants.INTENT_ACTION_REFRESH_LIST);
                                    intent.putExtra(Constants.CONVERSATION_EXTRA, conversation);
                                    activity.sendBroadcast(intent);
                                }
                            });

                })
                .onNegative((dialog1, which) -> dialog1.dismiss())
                .show();
    }

    public void toUserPickerForMessage() {
        Intent intent = new Intent(activity, UserPickerActivity.class);
        activity.startActivity(intent);
    }

    public void toUserPickerForCall() {
        Intent intent = new Intent(activity, ContactUserPickerActivity.class);
        activity.startActivity(intent);
    }
}
