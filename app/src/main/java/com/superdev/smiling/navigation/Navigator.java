package com.superdev.smiling.navigation;

/**
 * Created by marco on 27/07/16.
 */

public interface Navigator {

    void toLogin();

    void toMainActivity();

    void toParent();

    void toShareInvite(String sharingLink);

}
