package com.superdev.smiling.navigation;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.sinch.SinchService;
import com.superdev.smiling.user.data_model.User;

/**
 * Created by marco on 09/09/16.
 */

public interface ProfileNavigator extends Navigator {

    void showImagePicker();

    void showRemoveDialog();

    void showNameChangeActivity(String name);

    void showImagePreview(View view, User user);

    void attach(ProfileDialogListener dialogListener);

    void detach(ProfileDialogListener dialogListener);

    void placeCall(SinchService.SinchServiceInterface mSinchServiceInterface, User user);

    void placeVideoCall(SinchService.SinchServiceInterface mSinchServiceInterface, User user);

    void toSelectedConversation(Bundle bundle);

    void toStatusActivity(String status);

    void toFullScreenImage(View view, Message message);

    void toMediaActivity(String sender, String destination);

    interface ProfileDialogListener {

        void onNameSelected(String text);

        void onRemoveSelected();

        void onImageSelected(byte[] imageBytes, Bitmap bitmap);

        void onProfileRemove();


    }

}
