package com.superdev.smiling.navigation;

import android.net.Uri;
import android.view.View;

import com.superdev.smiling.conversation.data_model.AudioModel;
import com.superdev.smiling.conversation.data_model.ContactModel;
import com.superdev.smiling.conversation.data_model.DocModel;
import com.superdev.smiling.conversation.data_model.MapModel;
import com.superdev.smiling.conversation.data_model.Message;

/**
 * Created by gautam on 9/2/17.
 */

public interface ConversationNavigator extends Navigator {

    void onDocumentsPressed();

    void onCameraPressed();

    void onGalleryPressed();

    void onAudioPressed();

    void onLocationPressed();

    void onContactPressed();

    void toOpenGoogleMaps(Message message);

    void openDocument(Message message);

    void attach(OnImageSelectedListener imageSelectedListener);

    void detach(OnImageSelectedListener imageSelectedListener);

    void toFullScreenImageActivity(View view, Message message);

    interface OnImageSelectedListener {

        void onImageSelected(Uri photoUri, String message);

        void onMapSelected(MapModel message);

        void onDocSelected(DocModel docModel);

        void onAudioSelected(AudioModel audioModel);

        void onContactSelected(ContactModel contactModel);

        void onCabFinished();

        void onMessageDeleted(boolean deleteMedia, Message message);
    }
}
