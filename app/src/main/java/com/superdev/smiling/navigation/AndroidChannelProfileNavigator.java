package com.superdev.smiling.navigation;

import android.app.ActivityOptions;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.superdev.smiling.Constants;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.common.intents.IntentConstants;
import com.superdev.smiling.conversation.ConversationActivity;
import com.superdev.smiling.conversation.FullScreenImageActivity;
import com.superdev.smiling.group.GroupContactPickActivity;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.groupmedia.GroupMediaActivity;
import com.superdev.smiling.login.LoginActivity;
import com.superdev.smiling.main.MainActivity;
import com.superdev.smiling.profile.ChangeNameActivity;
import com.superdev.smiling.profile.ProfileActivity;
import com.superdev.smiling.profile.ProfileImageViewActivity;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.utility.ImageCompressionAsyncTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by gautam on 13/10/17.
 */

public class AndroidChannelProfileNavigator implements ChannelProfileNavigator {

    private static final int REQUEST_CODE_PROFILE_PHOTO = 2;

    private static final int REQUEST_CODE_CHANGE_NAME = 3;

    private final AppCompatActivity activity;

    private ChannelProfileNavigator.ChannelProfileDialogListener dialogListener;

    public AndroidChannelProfileNavigator(AppCompatActivity activity) {
        this.activity = activity;
    }


    @Override
    public void toLogin() {
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public void toMainActivity() {
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public void toParent() {
        activity.onBackPressed();
    }

    @Override
    public void toShareInvite(String sharingLink) {

    }

    @Override
    public void showImagePicker() {
        showProfilePhotoIntent();
    }

    @Override
    public void showParticipantsInfoDialog(User user, Channel channel) {
        boolean owner = channel.getOwner().equalsIgnoreCase(Remember.getString(Constants.FIREBASE_USER_ID, ""));
        ArrayList<String> optionList = new ArrayList<>();
        optionList.add(String.format(activity.getString(R.string.message_to), user.getName().split(" ")[0]));
        optionList.add(String.format(activity.getString(R.string.view_to), user.getName().split(" ")[0]));
//        if (owner && !channel.getOwner().equalsIgnoreCase(user.getUid())) {
//            optionList.add(activity.getString(R.string.make_contact_group_admin));
//        }
        if (owner && !channel.getOwner().equalsIgnoreCase(user.getUid())) {
            optionList.add(String.format(activity.getString(R.string.remove_to), user.getName().split(" ")[0]));
        }
        new MaterialDialog.Builder(activity)
                .items(optionList)
                .itemsCallback((dialog, view, which, text) -> {
                    Bundle bundle = new Bundle();
                    switch (which) {
                        case 0:

                            bundle.putString(Constants.SENDER_ID, Remember.getString(Constants.FIREBASE_USER_ID, ""));
                            bundle.putString(Constants.DESTINATION_ID, user.getUid());
                            toSelectedConversation(bundle);
                            break;
                        case 1:
                            bundle.putString(Constants.SENDER_ID, Remember.getString(Constants.FIREBASE_USER_ID, ""));
                            bundle.putString(Constants.DESTINATION_ID, user.getUid());
                            toProfileActivity(bundle);
                            break;
                        /*case 2:
                            if (text.toString().equalsIgnoreCase(activity.getString(R.string.make_contact_group_admin))) {

                            } else {

                            }
                            break;*/
                        case 2:
                            if (text.toString().contains("Remove")) {
                                showParticipantRemoveDialog(user, channel);
                            } else {

                            }
                            break;
                    }
                })
                .show();
    }

    @Override
    public void showNameChangeActivity(String name) {
        Intent intent = new Intent(activity, ChangeNameActivity.class);
        intent.putExtra(Constants.FIREBASE_USERS_NAME, name);
        activity.startActivityForResult(intent, REQUEST_CODE_CHANGE_NAME);
    }

    @Override
    public void showImagePreview(View view, Channel channel) {
        if (channel != null) {
            Intent intent = new Intent(activity, ProfileImageViewActivity.class);
            intent.putExtra(Constants.FIREBASE_USERS_NAME, channel.getName());
            intent.putExtra(Constants.FIREBASE_USERS_IMAGE, channel.getImage());
            CircleImageView circleImageView = view.findViewById(R.id.photo_btn);
            ImageView squaredImageView = view.findViewById(R.id.image);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                activity.startActivity(intent);
            } else {
                ActivityOptions options = ActivityOptions
                        .makeSceneTransitionAnimation(activity, circleImageView != null ? circleImageView : squaredImageView, activity.getString(R.string.transition_photo));
                // start the new activity
                activity.startActivity(intent, options.toBundle());
            }
            //close(true);
        }
    }

    @Override
    public void attach(ChannelProfileDialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }

    @Override
    public void detach(ChannelProfileDialogListener dialogListener) {
        this.dialogListener = null;
    }

    @Override
    public void toSelectedConversation(Bundle bundle) {
        Intent intent = new Intent(activity, ConversationActivity.class);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    private void toProfileActivity(Bundle bundle) {
        Intent intent = new Intent(activity, ProfileActivity.class);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    private void showProfilePhotoIntent() {
        final ArrayList<Intent> intentList = new ArrayList<>();
        //Gallery
        Intent i1 = new Intent(IntentConstants.PHOTO_FROM_GALLERY_ACTION);
        i1.setComponent(new ComponentName(activity, IntentConstants.PHOTO_GALLERY_PACKAGE_NAME));
        intentList.add(i1);

        //Camera
        Intent i2 = new Intent(IntentConstants.PHOTO_FROM_CAMERA_ACTION);
        i2.setComponent(new ComponentName(activity, IntentConstants.PHOTO_CAMERA_PACKAGE_NAME));
        intentList.add(i2);

        //None
        Intent i3 = new Intent(IntentConstants.PHOTO_NONE_ACTION);
        i3.setComponent(new ComponentName(activity, IntentConstants.PHOTO_NONE_PACKAGE_NAME));
        intentList.add(i3);

        if (intentList.isEmpty())
            Toast.makeText(activity, "No apps can perform this action", Toast.LENGTH_LONG).show();
        else {
            final Intent chooserIntent = Intent.createChooser(intentList.remove(intentList.size() - 1), "Profile Photo");//this removes 'Always' & 'Only once' buttons
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toArray(new Parcelable[intentList.size()]));
            activity.startActivityForResult(chooserIntent, REQUEST_CODE_PROFILE_PHOTO);
        }
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PROFILE_PHOTO) {
            if (resultCode == RESULT_OK) {
                if (data == null)
                    return false;
                try {
                    final Uri selectedUri = Uri.fromFile(new File(data.getExtras().getString(MediaStore.EXTRA_OUTPUT)));
                    final InputStream imageStream = activity.getContentResolver().openInputStream(selectedUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                    new ImageCompressionAsyncTask() {
                        @Override
                        protected void onPostExecute(byte[] imageBytes) {
                            dialogListener.onImageSelected(imageBytes, selectedImage);
                        }
                    }.execute(Utils.getRealPathFromURI(activity, selectedUri));

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == RESULT_CANCELED) {
                if (data == null || data.getAction() == null)
                    return false;
                if (data.getAction().equalsIgnoreCase(IntentConstants.PHOTO_NONE_ACTION)) {
                    dialogListener.onProfileRemove();
                }
            }
        } else if (requestCode == REQUEST_CODE_CHANGE_NAME && resultCode == RESULT_OK) {
            String name = data.getStringExtra(Constants.FIREBASE_USERS_NAME);
            dialogListener.onNameSelected(name);
        }
        return true;
    }

    @Override
    public void showParticipantRemoveDialog(User user, Channel channel) {
        boolean owner = channel.getOwner().equalsIgnoreCase(Remember.getString(Constants.FIREBASE_USER_ID, ""));
        new MaterialDialog.Builder(activity)
                .content(String.format(Locale.getDefault(), activity.getString(R.string.remove_participant_dialog_title), user.getName(), channel.getName()))
                .positiveText(R.string.ok_title)
                .negativeText(R.string.cancel_title)
                .onPositive((materialDialog, dialogAction) -> {
                    if (owner) {
                        dialogListener.onExitGroup(user, channel);
                    } else {
                        Toast.makeText(activity, R.string.admin_cannot_exit, Toast.LENGTH_SHORT).show();
                    }
                    materialDialog.dismiss();
                })
                .onNegative((materialDialog, dialogAction) -> {
                    materialDialog.dismiss();
                }).show();
    }

    public void showExitDialog(User user, Channel channel) {
        new MaterialDialog.Builder(activity)
                .content(String.format(Locale.getDefault(), activity.getString(R.string.exit_group_dialog_title), user.getName()))
                .positiveText(R.string.ok_title)
                .negativeText(R.string.cancel_title)
                .onPositive((materialDialog, dialogAction) -> {
                    dialogListener.onExitGroup(user, channel);
                    materialDialog.dismiss();
                })
                .onNegative((materialDialog, dialogAction) -> {
                    materialDialog.dismiss();
                }).show();
    }

    @Override
    public void toAddMembers(Channel channel) {
        Intent intent = new Intent(activity, GroupContactPickActivity.class);
        intent.putExtra(Constants.FIREBASE_CHANNELS, channel);
        activity.startActivity(intent);
    }

    @Override
    public void toFullScreenImage(View view, GroupMessage message) {
        Intent intent = new Intent(activity, FullScreenImageActivity.class);
        intent.putExtra(Constants.GROUP_MESSAGE_EXTRA, message);
        intent.putExtra(Constants.SENDER_ID, Remember.getString(Constants.FIREBASE_USER_ID, ""));
        View sharedView = view.findViewById(R.id.media_thumbs);
        String transitionName = activity.getString(R.string.transition_photo);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, sharedView, transitionName);
        activity.startActivity(intent, options.toBundle());
    }

    public void toAllGroupMediaActivity(Channel channel) {
        Intent intent = new Intent(activity, GroupMediaActivity.class);
        intent.putExtra(Constants.FIREBASE_CHANNELS, channel);
        activity.startActivity(intent);
    }
}
