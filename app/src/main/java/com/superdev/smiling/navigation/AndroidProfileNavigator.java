package com.superdev.smiling.navigation;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.sinch.android.rtc.calling.Call;
import com.superdev.smiling.Constants;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.Utils;
import com.superdev.smiling.common.intents.IntentConstants;
import com.superdev.smiling.conversation.ConversationActivity;
import com.superdev.smiling.conversation.FullScreenImageActivity;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.media.MediaActivity;
import com.superdev.smiling.profile.ChangeNameActivity;
import com.superdev.smiling.profile.ProfileImageViewActivity;
import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.sinch.CallScreenActivity;
import com.superdev.smiling.sinch.SinchService;
import com.superdev.smiling.sinch.VideoCallerScreenActivity;
import com.superdev.smiling.status.StatusActivity;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.utility.ImageCompressionAsyncTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.superdev.smiling.Constants.AUDIO_TYPE;
import static com.superdev.smiling.Constants.TYPE_OUT;
import static com.superdev.smiling.Constants.VIDEO_TYPE;

/**
 * Created by marco on 09/09/16.
 */

public class AndroidProfileNavigator implements ProfileNavigator {

    private static final int REQUEST_CODE_PROFILE_PHOTO = 2;

    private static final int REQUEST_CODE_CHANGE_NAME = 3;

    private final AppCompatActivity activity;

    private ProfileDialogListener dialogListener;

    public AndroidProfileNavigator(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void showImagePicker() {
        showProfilePhotoIntent();
    }

    private void showProfilePhotoIntent() {
        final ArrayList<Intent> intentList = new ArrayList<>();
        //Gallery
        Intent i1 = new Intent(IntentConstants.PHOTO_FROM_GALLERY_ACTION);
        i1.setComponent(new ComponentName(activity, IntentConstants.PHOTO_GALLERY_PACKAGE_NAME));
        intentList.add(i1);

        //Camera
        Intent i2 = new Intent(IntentConstants.PHOTO_FROM_CAMERA_ACTION);
        i2.setComponent(new ComponentName(activity, IntentConstants.PHOTO_CAMERA_PACKAGE_NAME));
        intentList.add(i2);

        //None
        Intent i3 = new Intent(IntentConstants.PHOTO_NONE_ACTION);
        i3.setComponent(new ComponentName(activity, IntentConstants.PHOTO_NONE_PACKAGE_NAME));
        intentList.add(i3);

        if (intentList.isEmpty())
            Toast.makeText(activity, "No apps can perform this action", Toast.LENGTH_LONG).show();
        else {
            final Intent chooserIntent = Intent.createChooser(intentList.remove(intentList.size() - 1), "Profile Photo");//this removes 'Always' & 'Only once' buttons
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toArray(new Parcelable[intentList.size()]));
            activity.startActivityForResult(chooserIntent, REQUEST_CODE_PROFILE_PHOTO);
        }
    }

    @Override
    public void showRemoveDialog() {

    }

    @Override
    public void showNameChangeActivity(String name) {
        Intent intent = new Intent(activity, ChangeNameActivity.class);
        intent.putExtra(Constants.FIREBASE_USERS_NAME, name);
        activity.startActivityForResult(intent, REQUEST_CODE_CHANGE_NAME);
    }

    @Override
    public void showImagePreview(View view, User user) {
        if (user != null) {
            Intent intent = new Intent(activity, ProfileImageViewActivity.class);
            intent.putExtra(Constants.FIREBASE_USERS_NAME, user.getName());
            intent.putExtra(Constants.FIREBASE_USERS_IMAGE, user.getImage());
            CircleImageView circleImageView = view.findViewById(R.id.photo_btn);
            ImageView squaredImageView = view.findViewById(R.id.image);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                activity.startActivity(intent);
            } else {
                ActivityOptions options = ActivityOptions
                        .makeSceneTransitionAnimation(activity, circleImageView != null ? circleImageView : squaredImageView, activity.getString(R.string.transition_photo));
                // start the new activity
                activity.startActivity(intent, options.toBundle());
            }
            //close(true);
        }
    }


    @Override
    public void attach(ProfileDialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }

    @Override
    public void detach(ProfileDialogListener dialogListener) {
        this.dialogListener = null;
    }


    @Override
    public void toLogin() {

    }

    @Override
    public void toMainActivity() {

    }

    @Override
    public void toParent() {
        activity.supportFinishAfterTransition();
    }

    @Override
    public void toShareInvite(String sharingLink) {

    }

    @SuppressLint("StaticFieldLeak")
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PROFILE_PHOTO) {
            if (resultCode == RESULT_OK) {
                if (data == null)
                    return false;
                try {
                    final Uri selectedUri = Uri.fromFile(new File(data.getExtras().getString(MediaStore.EXTRA_OUTPUT)));
                    final InputStream imageStream = activity.getContentResolver().openInputStream(selectedUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                    new ImageCompressionAsyncTask() {
                        @Override
                        protected void onPostExecute(byte[] imageBytes) {
                            dialogListener.onImageSelected(imageBytes, selectedImage);
                        }
                    }.execute(Utils.getRealPathFromURI(activity, selectedUri));

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == RESULT_CANCELED) {
                if (data == null || data.getAction() == null)
                    return false;
                if (data.getAction().equalsIgnoreCase(IntentConstants.PHOTO_NONE_ACTION)) {
                    dialogListener.onProfileRemove();
                }
            }
        } else if (requestCode == REQUEST_CODE_CHANGE_NAME && resultCode == RESULT_OK) {
            String name = data.getStringExtra(Constants.FIREBASE_USERS_NAME);
            dialogListener.onNameSelected(name);
        }
        return true;
    }

    @Override
    public void placeCall(SinchService.SinchServiceInterface mSinchServiceInterface, User user) {
        if (mSinchServiceInterface != null) {
            RecentCall recentCallEntity = new RecentCall();
            recentCallEntity.setCallTime(new Date().getTime());
            recentCallEntity.setCallType(AUDIO_TYPE);
            recentCallEntity.setType(TYPE_OUT);
            recentCallEntity.setUserContactEntity(user);
            recentCallEntity.setTotalDuration(0);
            Call call = mSinchServiceInterface.callUser(user.getPhone());
            if (call == null)
                return;
            String callId = call.getCallId();
            recentCallEntity.setCallId(callId);
            Intent callScreen = new Intent(activity, CallScreenActivity.class);
            callScreen.putExtra(SinchService.CALL_ID, callId);
            callScreen.putExtra(SinchService.CONTACT_INFO, user);
            callScreen.putExtra(SinchService.RECENT_CALLS, recentCallEntity);
            activity.startActivity(callScreen);
        }
    }

    @Override
    public void placeVideoCall(SinchService.SinchServiceInterface mSinchServiceInterface, User user) {
        if (mSinchServiceInterface != null) {
            RecentCall recentCallEntity = new RecentCall();
            recentCallEntity.setCallTime(new Date().getTime());
            recentCallEntity.setCallType(VIDEO_TYPE);
            recentCallEntity.setType(TYPE_OUT);
            recentCallEntity.setUserContactEntity(user);
            recentCallEntity.setTotalDuration(0);
            Call call = mSinchServiceInterface.callUserVideo(user.getPhone());
            if (call == null)
                return;
            String callId = call.getCallId();
            recentCallEntity.setCallId(callId);
            Intent callScreen = new Intent(activity, VideoCallerScreenActivity.class);
            callScreen.putExtra(SinchService.CALL_ID, callId);
            callScreen.putExtra(SinchService.CONTACT_INFO, user);
            callScreen.putExtra(SinchService.RECENT_CALLS, recentCallEntity);
            activity.startActivity(callScreen);
        }
    }

    @Override
    public void toSelectedConversation(Bundle bundle) {
        Intent intent = new Intent(activity, ConversationActivity.class);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    @Override
    public void toStatusActivity(String status) {
        Intent intent = new Intent(activity, StatusActivity.class);
        intent.putExtra(Constants.FIREBASE_USER_STATUS, status);
        activity.startActivity(intent);
    }

    @Override
    public void toFullScreenImage(View view, Message message) {
        Intent intent = new Intent(activity, FullScreenImageActivity.class);
        intent.putExtra(Constants.MESSAGE_EXTRA, message);
        View sharedView = view.findViewById(R.id.media_thumbs);
        String transitionName = activity.getString(R.string.transition_photo);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, sharedView, transitionName);
        activity.startActivity(intent, options.toBundle());
    }

    @Override
    public void toMediaActivity(String sender, String destination) {
        Intent intent = new Intent(activity, MediaActivity.class);
        intent.putExtra(Constants.SENDER_ID, sender);
        intent.putExtra(Constants.DESTINATION_ID, destination);
        activity.startActivity(intent);
    }

}
