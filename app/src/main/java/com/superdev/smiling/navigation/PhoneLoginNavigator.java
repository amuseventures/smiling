package com.superdev.smiling.navigation;

/**
 * Created by gautam on 9/2/17.
 */

public interface PhoneLoginNavigator extends Navigator {

    void attach(LoginResultListener loginResultListener);

    void detach(LoginResultListener loginResultListener);


    interface LoginResultListener {

        void startLoginWithPhoneNumber(String phoneNumber);

        void verifyPhoneWithCredentials();

        void resendVerificationCode(String phoneNumber);

    }
}
