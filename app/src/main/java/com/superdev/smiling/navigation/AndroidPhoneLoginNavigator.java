package com.superdev.smiling.navigation;

import android.support.v7.app.AppCompatActivity;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

/**
 * Created by gautam on 9/2/17.
 */

public class AndroidPhoneLoginNavigator implements PhoneLoginNavigator {

    private static final int RC_SIGN_IN = 242;

    private final AppCompatActivity activity;

    private final Navigator navigator;

    private LoginResultListener loginResultListener;

    public AndroidPhoneLoginNavigator(AppCompatActivity activity, Navigator navigator) {
        this.activity = activity;
        this.navigator = navigator;
    }

    @Override
    public void toLogin() {

    }

    @Override
    public void toMainActivity() {

    }

    @Override
    public void toParent() {

    }

    @Override
    public void toShareInvite(String sharingLink) {

    }

    @Override
    public void attach(LoginResultListener loginResultListener) {

    }

    @Override
    public void detach(LoginResultListener loginResultListener) {

    }


    private final PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            e.printStackTrace();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
        }
    };
}
