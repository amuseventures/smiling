package com.superdev.smiling.navigation;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.user.data_model.User;

/**
 * Created by gautam on 13/10/17.
 */

public interface ChannelProfileNavigator extends Navigator {

    void showImagePicker();

    void showParticipantsInfoDialog(User user, Channel channelOwner);

    void showNameChangeActivity(String name);

    void showImagePreview(View view, Channel channel);

    void attach(ChannelProfileDialogListener dialogListener);

    void detach(ChannelProfileDialogListener dialogListener);

    void toSelectedConversation(Bundle bundle);

    void showParticipantRemoveDialog(User user, Channel channel);

    void toAddMembers(Channel channel);

    void toFullScreenImage(View view, GroupMessage message);

    interface ChannelProfileDialogListener {

        void onNameSelected(String text);

        void onExitGroup(User user, Channel channel);

        void onImageSelected(byte[] imageBytes, Bitmap bitmap);

        void onProfileRemove();
    }

}
