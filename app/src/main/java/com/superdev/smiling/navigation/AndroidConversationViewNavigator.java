package com.superdev.smiling.navigation;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialcab.MaterialCab;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.superdev.smiling.BuildConfig;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.Utils;
import com.superdev.smiling.contactpicker.ContactPickerActivity;
import com.superdev.smiling.conversation.ConversationActivity;
import com.superdev.smiling.conversation.FullScreenImageActivity;
import com.superdev.smiling.conversation.TextAndImageSendActivity;
import com.superdev.smiling.conversation.data_model.AudioModel;
import com.superdev.smiling.conversation.data_model.ContactModel;
import com.superdev.smiling.conversation.data_model.DocModel;
import com.superdev.smiling.conversation.data_model.MapModel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.conversation.service.ConversationService;
import com.superdev.smiling.conversation.view.ConversationView;
import com.superdev.smiling.main.MainActivity;
import com.superdev.smiling.user.data_model.User;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import droidninja.filepicker.utils.FileUtils;

import static android.app.Activity.RESULT_OK;

/**
 * Created by gautam on 9/2/17.
 */

public class AndroidConversationViewNavigator implements ConversationNavigator, MaterialCab.Callback {

    private final AppCompatActivity activity;

    private static final int IMAGE_CAMERA_REQUEST = 2;

    private static final int IMAGE_PREVIEW_REQUEST = 3;

    private static final int LOCATION_PICKER_REQUEST = 4;

    private static final int CONTACT_PICKER_REQUEST = 6;

    private Toast toast;

    private File filePathImageCamera;

    private OnImageSelectedListener onImageSelectedListener;

    private String destination;

    private MaterialCab cab;

    private User destinationUser;

    private String senderId;

    private List<Message> messageList;

    private ConversationService conversationService;

    public AndroidConversationViewNavigator(AppCompatActivity activity, String senderId, String destination) {
        this.activity = activity;
        this.destination = destination;
        this.senderId = senderId;
        conversationService = Dependencies.INSTANCE.getConversationService();
    }

    public AppCompatActivity getActivity() {
        return activity;
    }

    @Override
    public void onDocumentsPressed() {
        FilePickerBuilder.getInstance().setMaxCount(1)
                .addFileSupport("", new String[]{".pdf", ".doc", ".txt", ".ppt", ".xls"})
                .setActivityTheme(R.style.AppThemeActionBar)
                .pickFile(activity);
    }

    @Override
    public void onCameraPressed() {
        String photoName = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
        filePathImageCamera = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), photoName + ".jpg");
        Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri photoURI = FileProvider.getUriForFile(activity,
                BuildConfig.APPLICATION_ID + ".provider",
                filePathImageCamera);
        it.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        activity.startActivityForResult(it, IMAGE_CAMERA_REQUEST);
    }

    @Override
    public void onGalleryPressed() {
        FilePickerBuilder.getInstance().setMaxCount(1)
                .showFolderView(true)
                .enableImagePicker(true)
                .showGifs(true)
                .enableVideoPicker(true)
                .setActivityTheme(R.style.AppThemeActionBar)
                .pickPhoto(activity);
    }

    @Override
    public void onAudioPressed() {
        FilePickerBuilder.getInstance().setMaxCount(1)
                .enableAudioPicker(true)
                .addFileSupport("", new String[]{".mp3", ".aac", ".wav", ".amr", ".ogg"})
                .setActivityTheme(R.style.AppThemeActionBar)
                .pickFile(activity);
    }

    @Override
    public void onLocationPressed() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            activity.startActivityForResult(builder.build(activity), LOCATION_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onContactPressed() {
        Intent intent = new Intent(activity, ContactPickerActivity.class);
        activity.startActivityForResult(intent, CONTACT_PICKER_REQUEST);
    }

    @Override
    public void toOpenGoogleMaps(Message message) {
        String uri = "https://www.google.com/maps/search/?api=1&query=" + message.getMapModel().getLatitude() + "," + message.getMapModel().getLongitude()
                + "&query_place_id=" + message.getMapModel().getPlaceId() + "&z=17&hl=en";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setPackage("com.google.android.apps.maps");
        try {
            activity.startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            try {
                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                activity.startActivity(unrestrictedIntent);
            } catch (ActivityNotFoundException innerEx) {
                Toast.makeText(activity, "Please install a map application", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void openDocument(Message message) {
        File file;
        if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
            file = new File(Utils.getDocumentPath() + "/" + message.getDocModel().getDocName());
        } else {
            file = new File(Utils.getSentDocumentPath() + "/" + message.getDocModel().getDocName());
        }
        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, message.getDocModel().getMimeType());
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            try {
                activity.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(activity,
                        "No Application Available to View PDF",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void attach(OnImageSelectedListener imageSelectedListener) {
        this.onImageSelectedListener = imageSelectedListener;
    }

    @Override
    public void detach(OnImageSelectedListener imageSelectedListener) {
        this.onImageSelectedListener = null;
    }

    @Override
    public void toFullScreenImageActivity(View view, Message message) {
        Intent intent = new Intent(activity, FullScreenImageActivity.class);
        intent.putExtra(Constants.MESSAGE_EXTRA, message);
        View sharedView = view.findViewById(R.id.image);
        String transitionName = activity.getString(R.string.transition_photo);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, sharedView, transitionName);
        activity.startActivity(intent, options.toBundle());
    }

    @Override
    public void toLogin() {

    }

    @Override
    public void toMainActivity() {
        Intent intent =  new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
    }

    public void toSelectedConversation(Bundle bundle) {
        Intent intent = new Intent(activity, ConversationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public void showBottomSheet() {
        ConversationView.BottomDialog dialog = ConversationView.BottomDialog.newInstance();
        dialog.show(activity.getSupportFragmentManager(), "Bottom");
    }

    @Override
    public void toParent() {
        if (cab != null && cab.isActive()) {
            cab.finish();
        } else {
            toMainActivity();
        }
    }

    @Override
    public void toShareInvite(String sharingLink) {

    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
            case IMAGE_CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    Intent intent1 = new Intent(activity, TextAndImageSendActivity.class);
                    intent1.putExtra("image", Utils.getRealPathFromURI(activity, Uri.fromFile(filePathImageCamera)));
                    intent1.putExtra("destination", destination);
                    activity.startActivityForResult(intent1, IMAGE_PREVIEW_REQUEST);
                }
                break;
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (intent == null) {
                    return false;
                }
                if (resultCode != Activity.RESULT_OK)
                    return false;
                ArrayList<String> photoPaths = new ArrayList<>();
                photoPaths.addAll(intent.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));

                Intent intent1 = new Intent(activity, TextAndImageSendActivity.class);
                try {
                    intent1.putExtra("image", photoPaths.get(0));
                    intent1.putExtra("destination", destination);
                    activity.startActivityForResult(intent1, IMAGE_PREVIEW_REQUEST);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
                break;
            case FilePickerConst.REQUEST_CODE_DOC:
                if (intent == null) {
                    return false;
                }
                if (resultCode != Activity.RESULT_OK)
                    return false;
                ArrayList<String> docPaths = new ArrayList<>();
                docPaths.addAll(intent.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                for (String path : docPaths) {
                    File file = new File(path);
                    String fileExtension
                            = MimeTypeMap.getFileExtensionFromUrl(path);
                    final String mimeType
                            = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
                    DocModel docModel = new DocModel(file.getName(), file.getName(), FileUtils.getFileType(path).toString(), null, file.length(), mimeType, path, 0);
                    onImageSelectedListener.onDocSelected(docModel);
                }
                break;
            case IMAGE_PREVIEW_REQUEST:
                if (resultCode == RESULT_OK) {
                    String filePath = intent.getStringExtra("FILE_PATH");
                    String message = intent.getStringExtra("message");
                    onImageSelectedListener.onImageSelected(Uri.fromFile(new File(filePath)), message);
                }
                break;
            case LOCATION_PICKER_REQUEST:
                if (resultCode == RESULT_OK) {
                    Place place = PlacePicker.getPlace(activity, intent);
                    String placeId = place.getId();
                    String latitude = String.valueOf(place.getLatLng().latitude);
                    String longitude = String.valueOf(place.getLatLng().longitude);
                    String address = String.format("%s", place.getAddress());
                    MapModel mapModel = new MapModel(latitude, longitude, address, placeId);
                    onImageSelectedListener.onMapSelected(mapModel);
                }
                break;
            case FilePickerConst.REQUEST_CODE_AUD:
                if (intent == null) {
                    return false;
                }

                if (resultCode != Activity.RESULT_OK)
                    return false;
                ArrayList<String> audPaths = new ArrayList<>();
                audPaths.addAll(intent.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                for (String path : audPaths) {
                    File file = new File(path);
                    String fileExtension
                            = MimeTypeMap.getFileExtensionFromUrl(path);
                    final String mimeType
                            = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
                    AudioModel audioModel = new AudioModel(file.getName(), null, file.length(), mimeType, 0, file.getPath());
                    onImageSelectedListener.onAudioSelected(audioModel);
                }
                break;
            case CONTACT_PICKER_REQUEST:
                if (intent == null) {
                    return false;
                }
                if (resultCode != Activity.RESULT_OK) {
                    return false;
                }
                User contactUser = intent.getParcelableExtra(Constants.CONTACT_PICK_EXTRA);
                ContactModel contactModel = new ContactModel(contactUser.getUid(), contactUser.getName(), contactUser.getImage(), contactUser.getImageUri(), contactUser.getPhone(), contactUser.getRegister());
                onImageSelectedListener.onContactSelected(contactModel);
                break;
            default:
                return false;
        }
        return true;
    }

    public void showCabMenu(User destinationUser, List<Message> messages) {
        if (cab == null) {
            cab = new MaterialCab(activity, R.id.cab_stub)
                    .setMenu(R.menu.conversation_cab_menu).start(this);
        } else if (!cab.isActive()) {
            cab.reset().setMenu(R.menu.conversation_cab_menu).start(this);
        }
        this.destinationUser = destinationUser;
        messageList = messages;
        cab.setTitleRes(R.string.x_selected, messages.size());
    }

    public void hideMenu() {
        if (cab != null) {
            cab.finish();
        }
        onImageSelectedListener.onCabFinished();
    }

    @Override
    public boolean onCabCreated(MaterialCab cab, Menu menu) {
        if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
            try {
                Field field = menu.getClass().getDeclaredField("mOptionalIconsVisible");
                field.setAccessible(true);
                field.setBoolean(menu, true);
            } catch (Exception ignored) {
                ignored.printStackTrace();
            }
        }
        return true; // allow creation
    }

    @Override
    public boolean onCabItemClicked(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_delete) {
            if (messageList != null && !messageList.isEmpty()) {
                openDeleteConversationDialog(messageList);
            }
        }
        return true;
    }

    public void openDeleteConversationDialog(List<Message> messagesList) {
        View view = LayoutInflater.from(activity).inflate(R.layout.delete_media_messages_dialog, null, false);
        TextView message = view.findViewById(R.id.groupMessage);
        AppCompatCheckBox deleteMedia = view.findViewById(R.id.delete_media);
        message.setText(String.format(activity.getString(R.string.delete_conversation_title), destinationUser.getName()));
        new MaterialDialog.Builder(activity)
                .customView(view, false)
                .positiveText(R.string.delete_info)
                .negativeText(R.string.cancel)
                .onPositive((dialog1, which) -> {
                    for (Message msg : messagesList) {
                        conversationService.removeChat(Remember.getString(Constants.FIREBASE_USER_ID, ""), destination, msg.getMessageKey())
                                .subscribe(aBoolean -> {
                                    if (aBoolean) {
                                        onImageSelectedListener.onMessageDeleted(deleteMedia.isChecked(), msg);
                                    }
                                });
                    }
                    hideMenu();
                })
                .onNegative((dialog1, which) -> dialog1.dismiss())
                .show();

    }

    @Override
    public boolean onCabFinished(@NonNull MaterialCab cab) {
        onImageSelectedListener.onCabFinished();
        return true; // allow destruction
    }


    private void showToast(String text) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(activity, text, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void toAddContact(ContactModel contactModel) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

        intent.putExtra(ContactsContract.Intents.Insert.NAME, contactModel.getName());
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, contactModel.getContactNumber());
        activity.startActivity(intent);
    }

    public void toSmsActivity(String url, ContactModel contactModel) {
        String uri = "smsto:" + contactModel.getContactNumber();
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(uri));
        intent.putExtra("sms_body", String.format(Locale.getDefault(), activity.getString(R.string.tell_a_friend_sms), url));
        intent.putExtra("compose_mode", true);
        activity.startActivity(intent);
    }
}
