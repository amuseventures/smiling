package com.superdev.smiling.link;

import com.superdev.smiling.user.data_model.User;

import java.net.URI;

public interface LinkFactory {

    URI inviteLinkFrom(String userId);

}