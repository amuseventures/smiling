package com.superdev.smiling.link;

import android.net.Uri;

import com.superdev.smiling.user.data_model.User;

import java.net.URI;

public class FirebaseDynamicLinkFactory implements LinkFactory {

    public static final String SENDER = "sender";
    private final String dynamicLinkDomain;
    private final String deepLinkBaseUrl;
    private final String androidPackageName;

    public FirebaseDynamicLinkFactory(String dynamicLinkDomain, String deepLinkBaseUrl, String androidPackageName) {
        this.dynamicLinkDomain = dynamicLinkDomain;
        this.deepLinkBaseUrl = deepLinkBaseUrl;
        this.androidPackageName = androidPackageName;
    }

    @Override
    public URI inviteLinkFrom(String userId) {
        Uri uri = Uri.parse(dynamicLinkDomain)
                .buildUpon()
                .appendQueryParameter("link", welcomeDeepLinkFromUser(userId).toString())
                .appendQueryParameter("apn", androidPackageName)
                .build();
        return URI.create(uri.toString());
    }

    private Uri welcomeDeepLinkFromUser(String userId) {
        return Uri.parse(deepLinkBaseUrl)
                .buildUpon()
                .appendPath("welcome")
                .appendQueryParameter(SENDER, userId)
                .build();
    }

}
