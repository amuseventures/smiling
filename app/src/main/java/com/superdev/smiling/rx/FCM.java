package com.superdev.smiling.rx;

import android.util.Log;

import com.sinch.gson.Gson;
import com.sinch.gson.GsonBuilder;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Remember;
import com.superdev.smiling.Utils;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class FCM {
    final static private String FCM_URL = "https://fcm.googleapis.com/fcm/send";

    /**
     * Method to send push notification to Android FireBased Cloud messaging
     * Server.
     *
     * @param tokenId Generated and provided from Android Client Developer
     * @param message which contains actual information.
     */

    public static void sendFCMNotification(String tokenId, String title, String message, GroupMessage groupMessage) {
        try {
            URL url = new URL(FCM_URL);
            HttpURLConnection conn;
            conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "key=" + "AIzaSyBIhDrVy-eE9AKmt36aweo_Y1Nv-Izi4mU");
            conn.setRequestProperty("Content-Type", "application/json");
            JSONObject infoJson = new JSONObject();

            Gson gson = new GsonBuilder().create();

            infoJson.put("title", title);
            infoJson.put("body", message);
            infoJson.put("group", gson.toJson(groupMessage));

            JSONObject json = new JSONObject();
            json.put("to", tokenId.trim());
            json.put("data", infoJson);

            System.out.println("json :" + json.toString());
            System.out.println("infoJson :" + infoJson.toString());
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(json.toString());
            wr.flush();
            int status = 0;
            if (null != conn) {
                status = conn.getResponseCode();
            }
            if (status != 0) {
                if (status == 200) {
                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));
                    System.out.println("Android Notification Response : " + reader.readLine());
                } else if (status == 401) {
                    System.out.println("Notification Response : TokenId : " + tokenId + " Error occurred :");
                } else if (status == 501) {
                    System.out.println("Notification Response : [ errorCode=ServerError ] TokenId : " + tokenId);
                } else if (status == 503) {
                    System.out.println("Notification Response : FCM Service is Unavailable TokenId :" + tokenId);
                }
            }
        } catch (MalformedURLException mlfexception) {
            System.out.println("Error occurred while sending push Notification!.." + mlfexception.getMessage());
        } catch (Exception mlfexception) {
            System.out.println("Reading URL, Error occurred while sending push Notification !.." + mlfexception.getMessage());
        }
    }


    public static void sendCallNotification(String topicName, String message) {
        try {
            URL url = new URL(FCM_URL);
            HttpURLConnection conn;
            conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "key=" + "AIzaSyBIhDrVy-eE9AKmt36aweo_Y1Nv-Izi4mU");
            conn.setRequestProperty("Content-Type", "application/json");
            JSONObject infoJson = new JSONObject();

            infoJson.put("message", message);
            Log.d("Message", message);
            infoJson.put(Constants.FIREBASE_USER_ID, Remember.getString(Constants.FIREBASE_USER_ID, ""));
            JSONObject json = new JSONObject();
            json.put("to", "/topics/" + topicName);
            json.put("data", infoJson);

            System.out.println("json :" + json.toString());
            System.out.println("infoJson :" + infoJson.toString());
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(json.toString());
            wr.flush();
            int status = 0;
            if (null != conn) {
                status = conn.getResponseCode();
            }
            if (status != 0) {
                if (status == 200) {
                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));
                    System.out.println("Android Notification Response : " + reader.readLine());
                } else if (status == 401) {
                    System.out.println("Notification Response : TokenId : " + topicName + " Error occurred :");
                } else if (status == 501) {
                    System.out.println("Notification Response : [ errorCode=ServerError ] TokenId : " + topicName);
                } else if (status == 503) {
                    System.out.println("Notification Response : FCM Service is Unavailable TokenId :" + topicName);
                }
            }
        } catch (MalformedURLException mlfexception) {
            System.out.println("Error occurred while sending push Notification!.." + mlfexception.getMessage());
        } catch (Exception mlfexception) {
            System.out.println("Reading URL, Error occurred while sending push Notification !.." + mlfexception.getMessage());
        }
    }


    static void sendFCMNotificationMulti(List<String> putIds2, String tokenId, String server_key, String message) {
        try {
            URL url = new URL(FCM_URL);
            HttpURLConnection conn;
            conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "key=" + server_key);
            conn.setRequestProperty("Content-Type", "application/json");

            JSONArray regId = null;
            JSONObject objData = null;
            JSONObject data = null;
            JSONObject notif = null;

            regId = new JSONArray();
            for (int i = 0; i < putIds2.size(); i++) {
                regId.put(putIds2.get(i));
            }
            data = new JSONObject();
            data.put("message", message);
            notif = new JSONObject();
            notif.put("title", "Alankit Universe");
            notif.put("text", message);

            objData = new JSONObject();
            objData.put("registration_ids", regId);
            objData.put("data", data);
            objData.put("notification", notif);
            System.out.println("!_@rj@_group_PASS:>" + objData.toString());


            System.out.println("json :" + objData.toString());
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write(objData.toString());
            wr.flush();
            int status = 0;
            if (null != conn) {
                status = conn.getResponseCode();
            }
            if (status != 0) {

                if (status == 200) {
                    //SUCCESS message
                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));
                    System.out.println("Android Notification Response : " +
                            reader.readLine());
                } else if (status == 401) {
                    System.out.println("Notification Response : TokenId : " + tokenId + " Error occurred :");
                } else if (status == 501) {
                    System.out.println("Notification Response : [ errorCode=ServerError ] TokenId :" + tokenId);
                } else if (status == 503) {
                    System.out.println("Notification Response : FCM Service is Unavailable TokenId :" + tokenId);
                }
            }
        } catch (MalformedURLException mlfexception) {
            System.out.println("Error occurred while sending push Notification!.." +
                    mlfexception.getMessage());
        } catch (IOException mlfexception) {
            System.out.println("Reading URL, Error occurred while sending push Notification !.." + mlfexception.getMessage());
        } catch (Exception exception) {
            System.out.println("Error occurred while sending push Notification!.." +
                    exception.getMessage());
        }

    }
}