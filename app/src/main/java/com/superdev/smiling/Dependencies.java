package com.superdev.smiling;

import android.content.Context;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.superdev.smiling.analytics.Analytics;
import com.superdev.smiling.analytics.ErrorLogger;
import com.superdev.smiling.analytics.FirebaseAnalyticsAnalytics;
import com.superdev.smiling.analytics.FirebaseConfig;
import com.superdev.smiling.analytics.FirebaseErrorLogger;
import com.superdev.smiling.channel.database.FirebaseChannelsDatabase;
import com.superdev.smiling.channel.presenter.Config;
import com.superdev.smiling.channel.service.ChannelService;
import com.superdev.smiling.channel.service.PersistedChannelService;
import com.superdev.smiling.conversation.database.FirebaseConversationDatabase;
import com.superdev.smiling.conversation.service.ConversationService;
import com.superdev.smiling.conversation.service.PersistedConversationService;
import com.superdev.smiling.conversation_list.database.FirebaseConversationListDatabase;
import com.superdev.smiling.conversation_list.service.ConversationListService;
import com.superdev.smiling.conversation_list.service.PersistedConversationListService;
import com.superdev.smiling.global.database.FirebaseGlobalDatabase;
import com.superdev.smiling.global.service.GlobalService;
import com.superdev.smiling.global.service.PersistedGlobalService;
import com.superdev.smiling.group.database.FirebaseGroupDatabase;
import com.superdev.smiling.group.service.GroupService;
import com.superdev.smiling.group.service.PersistedGroupService;
import com.superdev.smiling.groupconversation.database.FirebaseGroupConversationDatabase;
import com.superdev.smiling.groupconversation.service.GroupConversationService;
import com.superdev.smiling.groupconversation.service.PersistedConversationGroupService;
import com.superdev.smiling.login.database.FirebaseAuthDatabase;
import com.superdev.smiling.login.service.FirebaseLoginService;
import com.superdev.smiling.login.service.LoginService;
import com.superdev.smiling.main.database.FirebaseCloudMessagingDatabase;
import com.superdev.smiling.main.service.CloudMessagingService;
import com.superdev.smiling.main.service.FirebaseCloudMessagingService;
import com.superdev.smiling.main.service.MainService;
import com.superdev.smiling.main.service.PersistedMainService;
import com.superdev.smiling.phonelogin.database.FirebasePhoneAuthDatabase;
import com.superdev.smiling.phonelogin.service.FirebasePhoneLoginService;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.profile.service.ChannelProfileService;
import com.superdev.smiling.profile.service.PersistedChannelProfileService;
import com.superdev.smiling.profile.service.FirebaseProfileService;
import com.superdev.smiling.profile.service.ProfileService;
import com.superdev.smiling.recentcall.database.FirebaseRecentCallDatabase;
import com.superdev.smiling.recentcall.service.PersistedRecentCallService;
import com.superdev.smiling.recentcall.service.RecentCallService;
import com.superdev.smiling.registration.service.FirebaseRegistrationService;
import com.superdev.smiling.registration.service.RegistrationService;
import com.superdev.smiling.rx.FirebaseObservableListeners;
import com.superdev.smiling.storage.FirebaseStorageService;
import com.superdev.smiling.storage.StorageService;
import com.superdev.smiling.user.database.FirebaseUserDatabase;
import com.superdev.smiling.user.service.PersistedUserService;
import com.superdev.smiling.user.service.UserService;

/**
 * Created by marco on 27/07/16.
 */

public enum Dependencies {
    INSTANCE;

    private ErrorLogger errorLogger;
    private Analytics analytics;
    private RegistrationService registrationService;
    private LoginService loginService;
    private PhoneLoginService phoneLoginService;
    private ConversationListService conversationListService;
    private ConversationService conversationService;
    private GlobalService globalService;
    private UserService userService;
    private MainService mainService;
    private RecentCallService recentCallService;
    private ProfileService profileService;
    private ChannelProfileService channelProfileService;
    private CloudMessagingService messagingService;
    private StorageService storageService;
    private String firebaseToken;
    private ChannelService channelService;
    private Config config;
    private GroupService groupService;
    private GroupConversationService groupConversationService;
    private boolean setPersistence = false;
    private GcmPubSub gcmPubSub;

    public void init(Context context) {
        if (needsInitialisation()) {
            Context appContext = context.getApplicationContext();
            //FirebaseApp firebaseApp = FirebaseApp.initializeApp(appContext, FirebaseOptions.fromResource(appContext), "ChatFire");
            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            firebaseToken = Remember.getString(Constants.FIREBASE_USERS_FCM, "");
            if (!setPersistence) {
                FirebaseDatabase.getInstance().setPersistenceEnabled(true);
                setPersistence = true;
            }
            gcmPubSub = GcmPubSub.getInstance(context);
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
            FirebaseObservableListeners firebaseObservableListeners = new FirebaseObservableListeners();
            FirebaseUserDatabase userDatabase = new FirebaseUserDatabase(firebaseDatabase, firebaseObservableListeners);
            FirebaseRecentCallDatabase recentCallDatabase = new FirebaseRecentCallDatabase(firebaseDatabase, firebaseObservableListeners);
            FirebaseCloudMessagingDatabase messagingDatabase = new FirebaseCloudMessagingDatabase(firebaseDatabase, firebaseObservableListeners, firebaseToken);
            FirebaseConversationDatabase conversationDatabase = new FirebaseConversationDatabase(firebaseDatabase, firebaseObservableListeners);
            FirebaseConversationListDatabase conversationListDatabase = new FirebaseConversationListDatabase(firebaseDatabase, firebaseObservableListeners);
            FirebaseGroupDatabase groupDatabase = new FirebaseGroupDatabase(firebaseDatabase, firebaseObservableListeners);
            FirebaseGroupConversationDatabase groupConversationDatabase = new FirebaseGroupConversationDatabase(firebaseDatabase, firebaseObservableListeners);

            analytics = new FirebaseAnalyticsAnalytics(FirebaseAnalytics.getInstance(appContext));
            errorLogger = new FirebaseErrorLogger();
            loginService = new FirebaseLoginService(new FirebaseAuthDatabase(firebaseAuth), messagingDatabase);
            phoneLoginService = new FirebasePhoneLoginService(new FirebasePhoneAuthDatabase(firebaseAuth), messagingDatabase);
            registrationService = new FirebaseRegistrationService(firebaseAuth);
            conversationService = new PersistedConversationService(conversationDatabase);
            globalService = new PersistedGlobalService(new FirebaseGlobalDatabase(firebaseDatabase, firebaseObservableListeners));
            userService = new PersistedUserService(userDatabase);
            conversationListService = new PersistedConversationListService(conversationListDatabase, conversationDatabase, userDatabase);
            mainService = new PersistedMainService(firebaseAuth, userDatabase, messagingDatabase);
            profileService = new FirebaseProfileService(firebaseAuth);
            channelProfileService = new PersistedChannelProfileService(new FirebaseChannelsDatabase(firebaseDatabase, firebaseObservableListeners));
            recentCallService = new PersistedRecentCallService(recentCallDatabase);
            messagingService = new FirebaseCloudMessagingService(messagingDatabase);
            storageService = new FirebaseStorageService(firebaseStorage, firebaseObservableListeners);
            channelService = new PersistedChannelService(new FirebaseChannelsDatabase(firebaseDatabase, firebaseObservableListeners), userDatabase, gcmPubSub);
            config = FirebaseConfig.newInstance().init(errorLogger);
            groupService = new PersistedGroupService(groupDatabase);
            groupConversationService = new PersistedConversationGroupService(groupConversationDatabase);
        }
    }

    public void clearDependecies() {
        gcmPubSub = null;
        loginService = null;
        phoneLoginService = null;
        conversationListService = null;
        conversationService = null;
        userService = null;
        recentCallService = null;
        config = null;
        analytics = null;
        channelService = null;
        channelProfileService = null;
    }

    private boolean needsInitialisation() {
        return loginService == null || conversationListService == null || conversationService == null || registrationService == null
                || userService == null || recentCallService == null || analytics == null || errorLogger == null || channelProfileService == null || gcmPubSub == null;
    }

    public Analytics getAnalytics() {
        return analytics;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void initFirebaseToken() {
        firebaseToken = Remember.getString(Constants.FIREBASE_USERS_FCM, "");
    }

    public MainService getMainService() {
        return mainService;
    }

    public CloudMessagingService getMessagingService() {
        return messagingService;
    }

    public GlobalService getGlobalService() {
        return globalService;
    }

    public ConversationService getConversationService() {
        return conversationService;
    }

    public ConversationListService getConversationListService() {
        return conversationListService;
    }

    public RegistrationService getRegistrationService() {
        return registrationService;
    }

    public LoginService getLoginService() {
        return loginService;
    }

    public ProfileService getProfileService() {
        return profileService;
    }

    public UserService getUserService() {
        return userService;
    }

    public StorageService getStorageService() {
        return storageService;
    }

    public PhoneLoginService getPhoneLoginService() {
        return phoneLoginService;
    }

    public RecentCallService getRecentCallService() {
        return recentCallService;
    }

    public ChannelService getChannelService() {
        return channelService;
    }

    public Config getConfig() {
        return config;
    }

    public ErrorLogger getErrorLogger() {
        return errorLogger;
    }

    public GroupService getGroupService() {
        return groupService;
    }

    public GroupConversationService getGroupConversationService() {
        return groupConversationService;
    }

    public GcmPubSub getGcmPubSub() {
        return gcmPubSub;
    }

    public ChannelProfileService getChannelProfileService() {
        return channelProfileService;
    }
}
