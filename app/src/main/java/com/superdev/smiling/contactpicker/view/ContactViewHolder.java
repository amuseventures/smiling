package com.superdev.smiling.contactpicker.view;

import android.support.v7.widget.RecyclerView;

import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.view.UserView;

/**
 * Created by gautam on 9/13/17.
 */

public class ContactViewHolder extends RecyclerView.ViewHolder {

    private UserView userView;

    public ContactViewHolder(UserView itemView) {
        super(itemView);
        this.userView = itemView;
    }

    public void bind(User user, ContactSelectionListener listener) {
        userView.displayContact(user);
        userView.setOnClickListener(view -> listener.onContactPick(user));
    }

    interface ContactSelectionListener {

        void onContactPick(User user);
    }
}
