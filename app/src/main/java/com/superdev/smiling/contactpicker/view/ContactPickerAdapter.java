package com.superdev.smiling.contactpicker.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.superdev.smiling.R;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;
import com.superdev.smiling.user.view.UserView;
import com.superdev.smiling.user.view.UsersDisplayer;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by gautam on 9/13/17.
 */

public class ContactPickerAdapter extends RecyclerView.Adapter<ContactViewHolder> {

    private Users users = new Users(new ArrayList<User>());
    private UsersDisplayer.UserInteractionListener usersInteractionListener;
    private final LayoutInflater inflater;


    ContactPickerAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void update(Users users) {
        this.users = users;
        this.users = users.sortedByName();
        notifyDataSetChanged();
    }

    public void filter(String text) {
        Iterator<User> it = users.getUsers().iterator();
        while (it.hasNext()) {
            if (!it.next().getName().toLowerCase().contains(text.toLowerCase()))
                it.remove();
        }
        notifyDataSetChanged();
    }

    public Users getUsers() {
        return users;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContactViewHolder((UserView) inflater.inflate(R.layout.users_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        holder.bind(users.getUserAt(position), clickListener);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public long getItemId(int position) {
        return users.getUserAt(position).hashCode();
    }

    public void attach(UsersDisplayer.UserInteractionListener userInteractionListener) {
        this.usersInteractionListener = userInteractionListener;
    }

    public void detach(UsersDisplayer.UserInteractionListener userInteractionListener) {
        this.usersInteractionListener = null;
    }

    private final ContactViewHolder.ContactSelectionListener clickListener = new ContactViewHolder.ContactSelectionListener() {

        @Override
        public void onContactPick(User user) {
            usersInteractionListener.onContactSelected(user);
        }
    };
}
