package com.superdev.smiling.contactpicker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.contactpicker.presenter.ContactPresenter;
import com.superdev.smiling.navigation.AndroidConversationsNavigator;
import com.superdev.smiling.navigation.AndroidNavigator;

public class ContactPickerActivity extends AppCompatActivity {

    private ContactPresenter presenter;

    private AndroidConversationsNavigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_picker);
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        navigator = new AndroidConversationsNavigator(this, new AndroidNavigator(this));
        presenter = new ContactPresenter(
                findViewById(R.id.contactView),
                navigator,
                Dependencies.INSTANCE.getPhoneLoginService(),
                Dependencies.INSTANCE.getUserService()
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.stopPresenting();
    }
}
