package com.superdev.smiling.contactpicker.view;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.superdev.smiling.R;
import com.superdev.smiling.user.data_model.Users;
import com.superdev.smiling.user.view.UsersDisplayer;
import com.superdev.smiling.views.EmptyRecyclerView;
import com.superdev.smiling.views.TextViewWithImages;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by gautam on 9/13/17.
 */

public class ContactsView extends LinearLayout implements UsersDisplayer {

    private final ContactPickerAdapter contactUserAdapter;

    @BindView(R.id.tvTitle)
    EmojiconTextView tvTitle;

    @BindView(R.id.tvSubTitle)
    EmojiconTextView tvSubTitle;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private UserInteractionListener usersInteractionListener;

    @BindView(R.id.conversationsRecyclerView)
    EmptyRecyclerView conversations;

    @BindView(R.id.welcome_chats_message)
    TextViewWithImages view;

    @BindView(R.id.btn_float)
    FloatingActionButton btnFloat;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    public ContactsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
        contactUserAdapter = new ContactPickerAdapter(LayoutInflater.from(context));
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_user_picker_list_view, this);
        ButterKnife.bind(this);
        tvTitle.setText(R.string.select_contacts);
        btnFloat.setVisibility(GONE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        conversations.setLayoutManager(layoutManager);
        conversations.setAdapter(contactUserAdapter);
    }

    @Override
    public void display(Users users) {
        contactUserAdapter.update(users);
        if (users.getUsers() != null && !users.getUsers().isEmpty())
            tvSubTitle.setText(String.format(Locale.getDefault(), getContext().getString(R.string.total_contacts), users.getUsers().size()));
        else
            tvSubTitle.setText(R.string.no_contacts_found);
    }

    @Override
    public void attach(UserInteractionListener userInteractionListener) {
        this.usersInteractionListener = userInteractionListener;
        contactUserAdapter.attach(this.usersInteractionListener);
    }

    @Override
    public void detach(UserInteractionListener userInteractionListener) {
        this.usersInteractionListener = null;
        contactUserAdapter.detach(userInteractionListener);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(GONE);
    }

    @Override
    public void filter(String text) {

    }
}

