package com.superdev.smiling.group.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.views.SelectionCheckView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by gautam on 9/28/17.
 */

public class GroupUserView extends FrameLayout {

    @BindView(R.id.contact_photo)
    CircleImageView contactPhoto;

    @BindView(R.id.selection_check)
    SelectionCheckView selectionCheck;

    @BindView(R.id.chat_able_contacts_row_name)
    EmojiconTextView chatAbleContactsRowName;

    @BindView(R.id.chat_able_contacts_row_status)
    EmojiconTextView chatAbleContactsRowStatus;

    public GroupUserView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_group_user_view, this);
        ButterKnife.bind(this);
    }

    public void display(GroupPickView.SelectableUser user) {
        Utils.loadImageElseWhite(user.user.getImage(), contactPhoto, getContext(),R.drawable.avatar_contact_large);
        chatAbleContactsRowName.setText(user.user.getName());
        chatAbleContactsRowStatus.setText(user.user.getStatus());
        selectionCheck.setChecked(user.isSelected);
    }
}
