package com.superdev.smiling.group;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.group.presenter.GroupPresenter;
import com.superdev.smiling.navigation.AndroidChannelNavigator;
import com.superdev.smiling.navigation.AndroidNavigator;
import com.superdev.smiling.navigation.ChannelNavigator;

public class GroupContactPickActivity extends AppCompatActivity {

    private GroupPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_contact_pic);
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        Channel channel = getIntent().getParcelableExtra(Constants.FIREBASE_CHANNELS);
        AndroidChannelNavigator channelNavigator = new AndroidChannelNavigator(this, new AndroidNavigator(this));
        presenter = new GroupPresenter(Dependencies.INSTANCE.getGroupService(), Dependencies.INSTANCE.getPhoneLoginService(), Dependencies.INSTANCE.getChannelService()
                , findViewById(R.id.group_pick_view), channel, channelNavigator, Dependencies.INSTANCE.getErrorLogger(),
                Dependencies.INSTANCE.getAnalytics());
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.stopPresenting();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
