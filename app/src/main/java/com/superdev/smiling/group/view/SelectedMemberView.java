package com.superdev.smiling.group.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.user.data_model.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by gautam on 10/3/17.
 */

public class SelectedMemberView extends FrameLayout {

    @BindView(R.id.contact_row_photo)
    CircleImageView contactRowPhoto;

    @BindView(R.id.contact_name)
    TextView contactName;

    public SelectedMemberView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_selected_member_view, this);
        ButterKnife.bind(this);
    }

    public void display(User user) {
        Utils.loadImageElseWhite(user.getImage(), contactRowPhoto, getContext(), R.drawable.avatar_contact_large);
        contactName.setText(user.getName());
    }
}
