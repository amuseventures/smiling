package com.superdev.smiling.group.service;

import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;

import rx.Observable;


/**
 * Created by gautam on 9/28/17.
 */

public interface GroupService {

    Observable<DatabaseResult<Users>> getAllUsers();

    Observable<User> getUser(String userId);

}
