package com.superdev.smiling.group.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.superdev.smiling.R;
import com.superdev.smiling.user.data_model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gautam on 10/3/17.
 */

public class SelectedMemberAdapter extends RecyclerView.Adapter<SelectedMemberViewHolder> {

    private List<User> userList = new ArrayList<>();
    private final LayoutInflater inflater;

    public SelectedMemberAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
        setHasStableIds(true);
    }

    public void update(List<User> users) {
        this.userList = users;
        notifyDataSetChanged();
    }

    public List<User> getUserList() {
        return userList;
    }

    @Override
    public SelectedMemberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SelectedMemberViewHolder((SelectedMemberView) inflater.inflate(R.layout.seleted_member_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(SelectedMemberViewHolder holder, int position) {
        User user = userList.get(position);
        holder.bind(user);
    }

    @Override
    public long getItemId(int position) {
        return userList.get(position).getUid().hashCode();
    }


    @Override
    public int getItemCount() {
        return userList.size();
    }
}
