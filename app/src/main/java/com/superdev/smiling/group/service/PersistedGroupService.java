package com.superdev.smiling.group.service;


import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.group.database.GroupDatabase;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;

import rx.Observable;
import rx.functions.Func1;


public class PersistedGroupService implements GroupService {

    private final GroupDatabase groupDatabase;

    public PersistedGroupService(GroupDatabase groupDatabase) {
        this.groupDatabase = groupDatabase;
    }

    @Override
    public Observable<DatabaseResult<Users>> getAllUsers() {
        return groupDatabase.observeUsers()
                .map(DatabaseResult::new)
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    @Override
    public Observable<User> getUser(String userId) {
        return groupDatabase.observeUser(userId);
    }

}
