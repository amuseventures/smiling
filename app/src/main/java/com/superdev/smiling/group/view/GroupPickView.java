package com.superdev.smiling.group.view;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.superdev.smiling.R;
import com.superdev.smiling.group.displayer.GroupDisplayer;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by gautam on 9/28/17.
 */

public class GroupPickView extends CoordinatorLayout implements GroupDisplayer {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.users_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.tvTitle)
    EmojiconTextView tvTitle;

    @BindView(R.id.tvSubTitle)
    EmojiconTextView tvSubTitle;

    @BindView(R.id.float_btn)
    FloatingActionButton floatBtn;

    private GroupUserAdapter groupUserAdapter;

    private List<SelectableUser> selectableUsers;

    private SelectionListener selectionListener;

    public GroupPickView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_group_pick_view, this);
        ButterKnife.bind(this);
        tvTitle.setText(R.string.new_group);
        tvSubTitle.setText(R.string.add_paticipants);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void attach(SelectionListener selectionListener) {
        this.selectionListener = selectionListener;
        groupUserAdapter = new GroupUserAdapter(selectionListener, LayoutInflater.from(getContext()));
        recyclerView.setAdapter(groupUserAdapter);
        floatBtn.setOnClickListener(onClickListener);
    }

    @Override
    public void detach(SelectionListener selectionListener) {
        this.selectionListener = null;
        floatBtn.setOnClickListener(null);
    }

    @Override
    public void display(Users users) {
        selectableUsers = toSelectableUsers(users);
        groupUserAdapter.update(selectableUsers);
    }

    @Override
    public void showFailure() {
        Toast.makeText(getContext(), "Cannot add users to the channel", Toast.LENGTH_LONG).show();
    }

    @Override
    public void displaySelectedUsers(Users selectedUsers) {
        List<SelectableUser> usersWithUpdatedSelection = new ArrayList<>(selectableUsers.size());
        for (SelectableUser selectableUser : selectableUsers) {
            boolean isSelected = isUserSelected(selectedUsers, selectableUser);
            usersWithUpdatedSelection.add(new SelectableUser(selectableUser.user, isSelected));
        }
        selectableUsers = usersWithUpdatedSelection;
        groupUserAdapter.update(selectableUsers);
        tvSubTitle.setText(selectedUsers.size() + " Selected");
    }


    private boolean isUserSelected(Users selectedUsers, SelectableUser selectableUser) {
        boolean foundMatch = false;
        for (User selectedUser : selectedUsers.getUsers()) {
            if (selectedUser.equals(selectableUser.user)) {
                foundMatch = true;
                break;
            }
        }
        return foundMatch;
    }

    class SelectableUser {
        public final User user;
        public boolean isSelected;

        SelectableUser(User user, boolean isSelected) {
            this.user = user;
            this.isSelected = isSelected;
        }
    }

    private List<SelectableUser> toSelectableUsers(Users users) {
        List<SelectableUser> selectableUsers = new ArrayList<>(users.size());
        for (User user : users.getUsers()) {
            selectableUsers.add(new SelectableUser(user, false));
        }
        return selectableUsers;
    }

    private OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            selectionListener.onCompleteClicked();
        }
    };
}
