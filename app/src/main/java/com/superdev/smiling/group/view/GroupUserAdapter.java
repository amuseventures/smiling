package com.superdev.smiling.group.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.superdev.smiling.R;
import com.superdev.smiling.group.displayer.GroupDisplayer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gautam on 10/3/17.
 */

class GroupUserAdapter extends RecyclerView.Adapter<GroupUserViewHolder> {

    private List<GroupPickView.SelectableUser> users = new ArrayList<>();
    private final GroupDisplayer.SelectionListener selectionListener;
    private final LayoutInflater inflater;

    GroupUserAdapter(GroupDisplayer.SelectionListener selectionListener, LayoutInflater inflater) {
        this.selectionListener = selectionListener;
        this.inflater = inflater;
        setHasStableIds(true);
    }

    public void update(List<GroupPickView.SelectableUser> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    @Override
    public GroupUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GroupUserViewHolder((GroupUserView) inflater.inflate(R.layout.group_user_view, parent, false));
    }

    @Override
    public void onBindViewHolder(GroupUserViewHolder holder, int position) {
        final GroupPickView.SelectableUser user = users.get(position);
        holder.bind(user, groupUserSelectedListener);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public long getItemId(int position) {
        return users.get(position).user.getUid().hashCode();
    }

    private GroupUserViewHolder.GroupUserSelectedListener groupUserSelectedListener = new GroupUserViewHolder.GroupUserSelectedListener() {
        @Override
        public void onGroupUserSelected(GroupPickView.SelectableUser selectableUser) {
            selectionListener.onUserSelected(selectableUser.user);
        }

        @Override
        public void onGroupUserDeselected(GroupPickView.SelectableUser selectableUser) {
            selectionListener.onUserDeselected(selectableUser.user);
        }
    };

}