package com.superdev.smiling.group.view;

import android.support.v7.widget.RecyclerView;

import com.superdev.smiling.user.data_model.User;

/**
 * Created by gautam on 10/3/17.
 */

public class SelectedMemberViewHolder extends RecyclerView.ViewHolder {

    private SelectedMemberView itemView;

    public SelectedMemberViewHolder(SelectedMemberView itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    public void bind(User user) {
        itemView.display(user);
    }
}
