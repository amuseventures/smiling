package com.superdev.smiling.group.displayer;

import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;

/**
 * Created by gautam on 9/28/17.
 */

public interface GroupDisplayer {

    void attach(SelectionListener selectionListener);

    void detach(SelectionListener selectionListener);

    void display(Users users);

    void showFailure();

    void displaySelectedUsers(Users selectedUsers);

    interface SelectionListener {

        void onUserSelected(User user);

        void onUserDeselected(User user);

        void onCompleteClicked();
    }
}
