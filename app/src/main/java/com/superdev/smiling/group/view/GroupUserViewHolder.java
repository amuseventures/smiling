package com.superdev.smiling.group.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by gautam on 10/3/17.
 */

public class GroupUserViewHolder extends RecyclerView.ViewHolder {

    private final GroupUserView groupUserView;

    public GroupUserViewHolder(GroupUserView groupUserView) {
        super(groupUserView);
        this.groupUserView = groupUserView;
    }

    public void bind(final GroupPickView.SelectableUser selectableUser, final GroupUserSelectedListener listener) {
        groupUserView.display(selectableUser);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectableUser.isSelected) {
                    listener.onGroupUserDeselected(selectableUser);
                } else{
                    listener.onGroupUserSelected(selectableUser);
                }
            }
        });
    }

    interface GroupUserSelectedListener {

        void onGroupUserSelected(GroupPickView.SelectableUser selectableUser);

        void onGroupUserDeselected(GroupPickView.SelectableUser selectableUser);
    }
}
