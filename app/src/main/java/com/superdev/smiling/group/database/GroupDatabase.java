package com.superdev.smiling.group.database;

import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;

import rx.Observable;


/**
 * Created by gautam on 9/28/17.
 */

public interface GroupDatabase {

    Observable<Users> observeUsers();

    Observable<User> readUserFrom(String userId);

    Observable<User> observeUser(String userId);
}
