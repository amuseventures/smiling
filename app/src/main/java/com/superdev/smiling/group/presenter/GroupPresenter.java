package com.superdev.smiling.group.presenter;

import android.os.AsyncTask;

import com.superdev.smiling.MimeTypeEnum;
import com.superdev.smiling.analytics.Analytics;
import com.superdev.smiling.analytics.ErrorLogger;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.channel.service.ChannelService;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.group.displayer.GroupDisplayer;
import com.superdev.smiling.group.service.GroupService;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.navigation.AndroidChannelNavigator;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.rx.FCM;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;

import java.util.ArrayList;

import rx.Observer;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by gautam on 9/28/17.
 */

public class GroupPresenter {

    private final GroupService userService;
    private final ChannelService channelService;
    private final GroupDisplayer groupDisplayer;
    private final Channel channel;
    private final AndroidChannelNavigator navigator;
    private final ErrorLogger errorLogger;
    private final Analytics analytics;
    private Subscription subscriptions;
    private final PhoneLoginService phoneLoginService;
    private ArrayList<User> selectedUser = new ArrayList<>();
    private User currentUser;

    public GroupPresenter(GroupService userService,
                          PhoneLoginService phoneLoginService,
                          ChannelService channelService,
                          GroupDisplayer groupDisplayer,
                          Channel channel,
                          AndroidChannelNavigator navigator,
                          ErrorLogger errorLogger,
                          Analytics analytics) {
        this.userService = userService;
        this.phoneLoginService = phoneLoginService;
        this.channelService = channelService;
        this.groupDisplayer = groupDisplayer;
        this.channel = channel;
        this.navigator = navigator;
        this.errorLogger = errorLogger;
        this.analytics = analytics;
    }

    public void startPresenting() {
        groupDisplayer.attach(selectionListener);

        subscriptions = userService.getAllUsers().subscribe(new Observer<DatabaseResult<Users>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onNext(DatabaseResult<Users> usersDatabaseResult) {
                if (usersDatabaseResult.isSuccess()) {
                    Users users = usersDatabaseResult.getData();
                    groupDisplayer.display(users);
                }
            }
        });

        subscriptions = phoneLoginService.getAuthentication()
                .subscribe(authentication -> {
                    if (authentication.isSuccess()) {
                        userService.getUser(authentication.getUser().getUid())
                                .subscribe(user -> {
                                    currentUser = user;
                                });
                    }
                });

        if (channel != null) {
            subscriptions = channelService.getOwnersOfChannel(channel)
                    .subscribe(new Observer<DatabaseResult<Users>>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(DatabaseResult<Users> databaseResult) {
                            if (databaseResult.isSuccess()) {
                                groupDisplayer.displaySelectedUsers(databaseResult.getData());
                            } else {
                                errorLogger.reportError(databaseResult.getFailure(), "Cannot fetch channel owners");
                                groupDisplayer.showFailure();
                            }
                        }
                    });
        }
    }

    public void stopPresenting() {
        groupDisplayer.detach(selectionListener);
        subscriptions.unsubscribe();
    }

    private GroupDisplayer.SelectionListener selectionListener = new GroupDisplayer.SelectionListener() {
        @Override
        public void onUserSelected(final User user) {
            if (channel != null) {
                analytics.trackAddChannelOwner(channel.getName(), user.getUid());
            }
            selectedUser.add(user);
            groupDisplayer.displaySelectedUsers(new Users(selectedUser));
            //

        }

        @Override
        public void onUserDeselected(User user) {
            if (channel != null) {
                analytics.trackRemoveChannelOwner(channel.getName(), user.getUid());
            }
            selectedUser.remove(user);
            groupDisplayer.displaySelectedUsers(new Users(selectedUser));
            /*channelService.removeOwnerFromPrivateChannel(channel, user)
                    .subscribe(updateOnActionResult());*/
        }

        @Override
        public void onCompleteClicked() {
            if (channel != null) {
                for (User member : selectedUser) {
                    channelService.addOwnerToPrivateChannel(channel, member)
                            .subscribe(userDatabaseResult -> {
                                if (!userDatabaseResult.isSuccess()) {
                                    errorLogger.reportError(userDatabaseResult.getFailure(), "Cannot update channel owners");
                                    //newChannelDisplayer.showFailure();
                                } else {
                                    new AsyncTask<Void, Void, Void>() {

                                        @Override
                                        protected Void doInBackground(Void... voids) {
                                            GroupMessage group = new GroupMessage(member, channel, String.format("%1s added you", currentUser.getName()),
                                                    MimeTypeEnum.MEMBER.getValue(), com.superdev.smiling.conversation.data_model.Status.SEEN.getValue());
                                            FCM.sendFCMNotification(member.getFcm(), channel.getName(), String.format("%1s added you in %2s group", currentUser.getName(), channel.getName()), group);
                                            return null;
                                        }
                                    }.execute();
                                }
                            });
                }
                navigator.finishActivity();
            } else {
                navigator.createNewChanel(selectedUser);
            }
        }
    };

    private Observer<DatabaseResult<User>> updateOnActionResult() {
        return new Observer<DatabaseResult<User>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(DatabaseResult<User> userDatabaseResult) {
                if (!userDatabaseResult.isSuccess()) {
                    errorLogger.reportError(userDatabaseResult.getFailure(), "Cannot update channel owners");
                    groupDisplayer.showFailure();
                }
            }
        };
    }
}
