package com.superdev.smiling.group.database;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.superdev.smiling.Constants;
import com.superdev.smiling.rx.FirebaseObservableListeners;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by gautam on 10/3/17.
 */

public class FirebaseGroupDatabase implements GroupDatabase {

    private final DatabaseReference usersDB;

    private final DatabaseReference userContactDB;

    private final FirebaseObservableListeners firebaseObservableListeners;

    private FirebaseUser firebaseUser;

    private FirebaseAuth firebaseAuth;

    public FirebaseGroupDatabase(FirebaseDatabase firebaseDatabase, FirebaseObservableListeners firebaseObservableListeners) {
        usersDB = firebaseDatabase.getReference(Constants.FIREBASE_USERS);
        userContactDB = firebaseDatabase.getReference(Constants.FIREBASE_CONTACTS);
        this.firebaseObservableListeners = firebaseObservableListeners;
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
    }
    @Override
    public Observable<Users> observeUsers() {
        if (firebaseUser == null) {
            if (firebaseAuth == null) {
                firebaseAuth = FirebaseAuth.getInstance();
            }
            firebaseUser = firebaseAuth.getCurrentUser();
        }
        DatabaseReference databaseReference = userContactDB.child(firebaseUser.getPhoneNumber());
        databaseReference.orderByChild("register").equalTo("1");
        return firebaseObservableListeners.listenToSingleValueEvents(databaseReference, mappingOfRegisterUser());
    }

    private Func1<DataSnapshot, Users> mappingOfRegisterUser() {
        return dataSnapshot -> {
            Iterable<DataSnapshot> children = dataSnapshot.getChildren();
            List<User> users = new ArrayList<>();
            for (DataSnapshot child : children) {
                User message = child.getValue(User.class);
                if (message != null && firebaseUser != null && message.getUid() != null && !firebaseUser.getUid().equals(message.getUid()) && message.getRegister().equalsIgnoreCase("1")) {
                    users.add(message);
                }
            }
            return new Users(users);
        };
    }

    @Override
    public Observable<User> readUserFrom(String userId) {
        return firebaseObservableListeners.listenToValueEvents(usersDB.child(userId), as(User.class));
    }

    @Override
    public Observable<User> observeUser(String userId) {
        return firebaseObservableListeners.listenToValueEvents(usersDB.child(userId), as(User.class));
    }

    private <T> Func1<DataSnapshot, T> as(final Class<T> tClass) {
        return dataSnapshot -> dataSnapshot.getValue(tClass);
    }
}
