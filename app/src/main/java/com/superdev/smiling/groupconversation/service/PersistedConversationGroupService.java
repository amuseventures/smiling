package com.superdev.smiling.groupconversation.service;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.groupconversation.data_model.GroupChat;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.groupconversation.database.GroupConversationDatabase;

import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by gautam on 10/3/17.
 */

public class PersistedConversationGroupService implements GroupConversationService {

    private final GroupConversationDatabase groupConversationDatabase;

    public PersistedConversationGroupService(GroupConversationDatabase groupConversationDatabase) {
        this.groupConversationDatabase = groupConversationDatabase;
    }

    @Override
    public Observable<DatabaseResult<GroupChat>> getChat(final Channel channel) {
        return groupConversationDatabase.observeChat(channel)
                .map(asDatabaseResult())
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    @Override
    public Observable<DatabaseResult<GroupChat>> getGroupMedia(Channel channel) {
        return groupConversationDatabase.observeMedia(channel)
                .map(asDatabaseResult())
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    @Override
    public Observable<DatabaseResult<GroupChat>> getAllGroupImageMedia(Channel channel) {
        return groupConversationDatabase.observeAllGroupImages(channel)
                .map(asDatabaseResult())
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    @Override
    public Observable<DatabaseResult<GroupChat>> getAllGroupDocument(Channel channel) {
        return groupConversationDatabase.observeAllGroupDocuments(channel)
                .map(asDatabaseResult())
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    private static Func1<GroupChat, DatabaseResult<GroupChat>> asDatabaseResult() {
        return DatabaseResult::new;
    }


    private static Func1<GroupMessage, DatabaseResult<GroupMessage>> asLastMessageResult() {
        return DatabaseResult::new;
    }


    @Override
    public Observable<List<String>> getUserIds(Channel channel) {
        return groupConversationDatabase.getUserIdsFromChannel(channel);
    }

    @Override
    public Observable<List<String>> getUserKey(Channel channel) {
        return groupConversationDatabase.getUserKeyFromChannel(channel);
    }

    @Override
    public void sendMessage(Channel channel, GroupMessage groupMessage) {
        groupConversationDatabase.sendMessage(channel, groupMessage);
    }

    @Override
    public Observable<Boolean> removeChat(Channel channel, String key) {
        return groupConversationDatabase.removeChat(channel, key);
    }

    @Override
    public boolean exitGroup(Channel channel, String userId, List<String> ownerList) {
        return groupConversationDatabase.exitGroup(channel, userId, ownerList);
    }

    @Override
    public Observable<Boolean> getTyping(Channel channel, String userId) {
        return null;
    }

    @Override
    public void setTyping(Channel channel, String userId, Boolean value) {

    }

    @Override
    public Observable<DatabaseResult<GroupMessage>> getLastMessageFor(Channel channel) {
        return groupConversationDatabase.observeLastMessage(channel)
                .map(asLastMessageResult())
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }
}
