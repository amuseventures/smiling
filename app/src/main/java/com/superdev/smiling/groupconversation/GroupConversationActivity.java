package com.superdev.smiling.groupconversation;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.quickblox.users.model.QBUser;
import com.superdev.smiling.BaseActivity;
import com.superdev.smiling.BuildConfig;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.groupconversation.presenter.GroupConversationPresenter;
import com.superdev.smiling.groupconversation.view.GroupConversationDisplayer;
import com.superdev.smiling.link.FirebaseDynamicLinkFactory;
import com.superdev.smiling.navigation.AndroidChannelConversationNavigator;
import com.superdev.smiling.navigation.AndroidNavigator;
import com.superdev.smiling.profile.ChannelProfileActivity;

import de.hdodenhof.circleimageview.CircleImageView;

public class GroupConversationActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {

    public static Intent createIntentFor(Context context, Channel channel) {
        Intent intent = new Intent(context, GroupConversationActivity.class);
        intent.putExtra(Constants.FIREBASE_CHANNELS, channel);
        return intent;
    }

    public static boolean isInForeground;

    private GroupConversationPresenter presenter;

    private GroupConversationDisplayer displayer;

    private AndroidChannelConversationNavigator navigator;

    private QBUser userForSave;

    private Channel channel;

    private boolean bcalledExitGroup = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_conversation);
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        channel = getIntent().getParcelableExtra(Constants.FIREBASE_CHANNELS);
        navigator = new AndroidChannelConversationNavigator(this, new AndroidNavigator(this), channel);
        displayer = findViewById(R.id.group_conversation_view);
        GoogleApiClient mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, this)
                .build();

        FirebaseDynamicLinkFactory firebaseDynamicLinkFactory = new FirebaseDynamicLinkFactory(
                getResources().getString(R.string.dynamic_link_url),
                getResources().getString(R.string.deepLinkBaseUrl),
                BuildConfig.APPLICATION_ID
        );

        presenter = new GroupConversationPresenter(
                channel,
                Dependencies.INSTANCE.getPhoneLoginService(),
                Dependencies.INSTANCE.getGroupConversationService(),
                displayer,
                Dependencies.INSTANCE.getUserService(),
                navigator,
                Dependencies.INSTANCE.getAnalytics(),
                Dependencies.INSTANCE.getErrorLogger(),
                mGoogleApiClient,
                firebaseDynamicLinkFactory,
                requestExecutor);

        findViewById(R.id.toolbar).setOnClickListener(view -> {
            Intent intent = new Intent(this, ChannelProfileActivity.class);
            intent.putExtra(Constants.FIREBASE_CHANNELS, channel);
            CircleImageView circleImageView = view.findViewById(R.id.profileImageView);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                startActivity(intent);
            } else {
                ActivityOptions options = ActivityOptions
                        .makeSceneTransitionAnimation(this, circleImageView, getString(R.string.transition_photo));
                // start the new activity
                startActivity(intent, options.toBundle());
            }
        });
        presenter.startPresenting();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        isInForeground = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isInForeground = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!navigator.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        displayer.inflateMenu(getMenuInflater(), menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            navigator.toParent();
        } else if (item.getItemId() == R.id.action_attach) {
            navigator.showBottomSheet();
        }else if (item.getItemId()==R.id.action_group_call){
            navigator.toOpponents();
        }
        /*else if (item.getItemId() == R.id.action_exit_group) {
            Dependencies.INSTANCE.getGroupConversationService().getUserKey(channel)
                    .subscribe(new Observer<List<String>>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(List<String> strings) {
                            if (!bcalledExitGroup) {
                                bcalledExitGroup = true;
                                if (navigator.exitGroup(channel, Remember.getString(Constants.FIREBASE_USER_ID, ""), strings))
                                    finish();

                            }
                        }
                    });
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.stopPresenting();
    }
}
