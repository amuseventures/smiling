package com.superdev.smiling.groupconversation.presenter;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;

import com.google.android.gms.common.api.GoogleApiClient;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.sample.core.utils.SharedPrefsHelper;
import com.quickblox.sample.core.utils.Toaster;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.superdev.smiling.App;
import com.superdev.smiling.Constants;
import com.superdev.smiling.MimeTypeEnum;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.analytics.Analytics;
import com.superdev.smiling.analytics.ErrorLogger;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.AudioModel;
import com.superdev.smiling.conversation.data_model.ContactModel;
import com.superdev.smiling.conversation.data_model.DocModel;
import com.superdev.smiling.conversation.data_model.FileModel;
import com.superdev.smiling.conversation.data_model.MapModel;
import com.superdev.smiling.conversation.data_model.Status;
import com.superdev.smiling.conversation.data_model.VideoModel;
import com.superdev.smiling.conversation.data_model.VoiceModel;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.groupconversation.data_model.GroupChat;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.groupconversation.service.GroupConversationService;
import com.superdev.smiling.groupconversation.view.GroupConversationDisplayer;
import com.superdev.smiling.link.LinkFactory;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.navigation.AndroidChannelConversationNavigator;
import com.superdev.smiling.navigation.ChannelNavigator;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.quickutils.Consts;
import com.superdev.smiling.quickutils.QBEntityCallbackImpl;
import com.superdev.smiling.quickutils.QBResRequestExecutor;
import com.superdev.smiling.quickutils.UsersUtils;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.service.UserService;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Func2;

import static android.support.constraint.Constraints.TAG;
import static com.superdev.smiling.Constants.MAIN_DIRECTORY;
import static com.superdev.smiling.Constants.SMILE_AUDIO;
import static com.superdev.smiling.Constants.SMILE_DOCUMENTS;
import static com.superdev.smiling.Constants.SMILE_MEDIA_DIRECTORY;
import static com.superdev.smiling.Constants.SMILE_SENT;

/**
 * Created by gautam on 10/3/17.
 */

public class GroupConversationPresenter {

    private final PhoneLoginService loginService;

    private final GroupConversationService conversationService;

    private final GroupConversationDisplayer conversationDisplayer;

    private final UserService userService;

    private final AndroidChannelConversationNavigator navigator;

    private final ErrorLogger errorLogger;

    private final Analytics analytics;

    private final Channel channel;

    private final GoogleApiClient mGoogleApiClient;

    private final QBResRequestExecutor requestExecutor;

    private Subscription subscription;

    private LinkFactory linkFactory;

    private User user;

    private QBUser userForSave;


    public GroupConversationPresenter(
            Channel channel,
            PhoneLoginService loginService,
            GroupConversationService conversationService,
            GroupConversationDisplayer conversationDisplayer,
            UserService userService,
            AndroidChannelConversationNavigator navigator,
            Analytics analytics,
            ErrorLogger errorLogger,
            GoogleApiClient mGoogleApiClient,
            LinkFactory linkFactory,
            QBResRequestExecutor requestExecutor) {
        this.channel = channel;
        this.loginService = loginService;
        this.conversationService = conversationService;
        this.conversationDisplayer = conversationDisplayer;
        this.userService = userService;
        this.navigator = navigator;
        this.analytics = analytics;
        this.errorLogger = errorLogger;
        this.mGoogleApiClient = mGoogleApiClient;
        this.linkFactory = linkFactory;
        this.requestExecutor = requestExecutor;
    }

    public void startPresenting() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        navigator.attach(imageSelectedListener);
        conversationDisplayer.attach(actionListener);
        conversationDisplayer.disableInteraction();
        subscription = Observable.combineLatest(conversationService.getChat(channel), loginService.getAuthentication(), asPair())
                .subscribe(new Observer<Pair>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Pair pair) {
                        if (pair.auth.isSuccess()) {
                            userService.getUser(pair.auth.getUser().getUid())
                                    .subscribe(userDatabaseResult -> {
                                        if (userDatabaseResult.isSuccess()) {
                                            user = userDatabaseResult.getData();
                                            createUserWithEnteredData();
                                            displayChat(pair);
                                        }
                                    });
                        } else {
                            errorLogger.reportError(pair.auth.getFailure(), "Not logged in when opening chat");
                            navigator.toLogin();
                        }
                    }
                });

        subscription = conversationService.getUserIds(channel)
                .subscribe(new Observer<List<String>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<String> strings) {
                        StringBuilder builder = new StringBuilder();
                        for (String nameString : strings) {
                            String[] name = nameString.split(" ");
                            builder.append(name[0]).append(", ");
                        }
                        String members = builder.toString();
                        conversationDisplayer.setupToolbar(channel.getName(), channel.getImage(), Utils.method(members.trim()));
                    }
                });
    }

    private void displayChat(Pair pair) {
        if (pair.chatResult.isSuccess()) {
            conversationDisplayer.display(channel, pair.chatResult.getData(), user);
        } else {
            errorLogger.reportError(pair.chatResult.getFailure(), "Cannot open chat");
            navigator.toChannels();
        }
    }

    private boolean userIsAuthenticated() {
        return user != null;
    }


    private final GroupConversationDisplayer.ConversationActionListener actionListener = new GroupConversationDisplayer.ConversationActionListener() {
        @Override
        public void onUpPressed() {
            navigator.toParent();
        }

        @Override
        public void onManageOwnersClicked() {

        }

        @Override
        public void onMessageLengthChanged(int messageLength) {
            if (userIsAuthenticated() && messageLength > 0) {
                conversationDisplayer.enableInteraction();
            } else {
                conversationDisplayer.disableInteraction();
            }
        }


        @Override
        public void onSubmitMessage(String message) {
            conversationDisplayer.addToDisplay(channel, new GroupMessage(user, channel, message, "sms", Status.WAITING.getValue()), user);
        }

        @Override
        public void onSubmitVoice(File audioFile) {
            String fileExtension
                    = MimeTypeMap.getFileExtensionFromUrl(audioFile.getPath());
            final String mimeType
                    = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
            VoiceModel voiceModel = new VoiceModel(audioFile.getName(), null, audioFile.length(), mimeType, 0, audioFile.getPath());
            conversationDisplayer.addToDisplay(channel, new GroupMessage(user, channel, "", voiceModel, MimeTypeEnum.VOICE.getValue(), Status.WAITING.getValue()), user);
        }

        @Override
        public void onDocumentsPressed() {
            navigator.onDocumentsPressed();
        }

        @Override
        public void onCameraPressed() {
            navigator.onCameraPressed();
        }

        @Override
        public void onGalleryPressed() {
            navigator.onGalleryPressed();
        }

        @Override
        public void onAudioPressed() {
            navigator.onAudioPressed();
        }

        @Override
        public void onLocationPressed() {
            navigator.onLocationPressed();
        }

        @Override
        public void onContactPressed() {
            navigator.onContactPressed();
        }

        @Override
        public void onMessageSelected(View view, GroupMessage message) {
        }

        @Override
        public void onImageSelected(String self, View view, GroupMessage message) {
            navigator.toFullScreenImageActivity(self, view, message);
        }

        @Override
        public void onVideoSelected(View view, GroupMessage message) {
        }

        @Override
        public void onMapViewSelected(GroupMessage message) {
            navigator.toOpenGoogleMaps(message);
        }

        @Override
        public void onDocViewSelected(GroupMessage message) {
            navigator.openDocument(user.getUid(), message);
        }

        @Override
        public void showCabMenu(List<GroupMessage> groupMessageList) {
            navigator.showCabMenu(groupMessageList);
        }

        @Override
        public void hideCabMenu() {
            navigator.hideMenu();
        }

        @Override
        public void onContactAddBtn(GroupMessage message) {
            if (message.getContactModel() != null && message.getContactModel().getIsRegistered() != null &&
                    message.getContactModel().getIsRegistered().equalsIgnoreCase("1")) {
                navigator.toAddContact(message.getContactModel());
            } else {
                navigator.toSmsActivity(linkFactory.inviteLinkFrom(user.getUid()).toString(), message.getContactModel());
            }
        }

        @Override
        public void onContactMsgSelected(GroupMessage message) {
            if (message.getContactModel() != null && message.getContactModel().getIsRegistered() != null &&
                    message.getContactModel().getIsRegistered().equalsIgnoreCase("1")) {
                Bundle bundle = new Bundle();
                /*bundle.putString(SENDER_ID, self);
                bundle.putString(DESTINATION_ID, message.getContactModel().getUserId());
                navigator.toSelectedConversation(bundle);*/
            } else {
                navigator.toSmsActivity(linkFactory.inviteLinkFrom(user.getUid()).toString(), message.getContactModel());
            }
        }

        @Override
        public void showAttachment() {
            navigator.showBottomSheet();
        }
    };

    private final ChannelNavigator.OnImageSelectedListener imageSelectedListener = new ChannelNavigator.OnImageSelectedListener() {
        @Override
        public void onImageSelected(Uri photoUri, String message) {
            String fileExtension
                    = MimeTypeMap.getFileExtensionFromUrl(photoUri.toString());
            final String mimeType
                    = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
            if (!mimeType.contains(MimeTypeEnum.VIDEO.getValue())) {
                File file = new File(photoUri.getPath());
                FileModel fileModel = new FileModel(mimeType, null, file.getName(), file.length(), photoUri.toString(), 0);
                conversationDisplayer.addToDisplay(channel, new GroupMessage(user, channel, message, fileModel, MimeTypeEnum.IMAGE.getValue(), Status.WAITING.getValue()), user);
            } else {
                File file = new File(photoUri.getPath());
                VideoModel videoModel = new VideoModel(mimeType, null, file.getName(), file.length(), photoUri.toString(), 0);
                conversationDisplayer.addToDisplay(channel, new GroupMessage(user, channel, message, videoModel, MimeTypeEnum.VIDEO.getValue(), Status.WAITING.getValue()), user);
            }
        }

        @Override
        public void onMapSelected(MapModel mapModel) {
            conversationDisplayer.addToDisplay(channel, new GroupMessage(user, channel, "", mapModel, MimeTypeEnum.LOCATION.getValue(), Status.WAITING.getValue()), user);
        }

        @Override
        public void onDocSelected(DocModel docModel) {
            File oldFile = new File(docModel.getDocLocalUrl());
            File newFile = new File(getDocumentFilename(oldFile));
            Utils.writeFile(oldFile, newFile);
            docModel.setDocLocalUrl(newFile.getPath());
            docModel.setDocName(newFile.getName());
            conversationDisplayer.addToDisplay(channel, new GroupMessage(user, channel, "", docModel, MimeTypeEnum.DOCUMENT.getValue(), Status.WAITING.getValue()), user);
        }

        @Override
        public void onAudioSelected(AudioModel audioModel) {
            File oldFile = new File(audioModel.getAudioLocalUrl());
            File newFile = new File(getAudioFilename(oldFile));
            Utils.writeFile(oldFile, newFile);
            audioModel.setAudioLocalUrl(newFile.getPath());
            audioModel.setAudioName(newFile.getName());
            conversationDisplayer.addToDisplay(channel, new GroupMessage(user, channel, "", audioModel, MimeTypeEnum.AUDIO.getValue(), Status.WAITING.getValue()), user);
        }

        @Override
        public void onContactSelected(ContactModel contactModel) {
            conversationDisplayer.addToDisplay(channel, new GroupMessage(user, channel, "", contactModel, MimeTypeEnum.CONTACT.getValue(), Status.WAITING.getValue()), user);
        }

        @Override
        public void onCabFinished() {

        }

        @Override
        public void onMessageDeleted(boolean deleteMedia, GroupMessage message) {
            if (deleteMedia)
                Utils.deleteFile(user.getUid(), message);
            conversationDisplayer.onMessageDeleted(message);
        }

        @Override
        public void onImageSelected(byte[] imageBytes, Bitmap selectedImage) {

        }

        @Override
        public void onProfileRemove() {

        }

        @Override
        public void onUserLogin() {
            saveUserData(userForSave);
            signInCreatedUser(userForSave, false);
        }

        @Override
        public void onUserLoginError() {
            Toaster.longToast("Error in login in call service");
        }
    };

    private static class Pair {

        final DatabaseResult<GroupChat> chatResult;
        public final Authentication auth;

        private Pair(DatabaseResult<GroupChat> chatResult, Authentication auth) {
            this.chatResult = chatResult;
            this.auth = auth;
        }
    }

    private static Func2<DatabaseResult<GroupChat>, Authentication, Pair> asPair() {
        return Pair::new;
    }

    public void stopPresenting() {
        navigator.detach(imageSelectedListener);
        conversationDisplayer.detach(actionListener);
        subscription.unsubscribe();
    }

    private String getDocumentFilename(File file) {
        String path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_DOCUMENTS, SMILE_SENT);
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMddhhmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "DOC-" + timeStamp + "." + droidninja.filepicker.utils.Utils.getFileExtension(file));
        return mediaFile.getAbsolutePath();
    }

    private String getAudioFilename(File file) {
        String path = String.format("/%s/%s/%s/%s/", MAIN_DIRECTORY, SMILE_MEDIA_DIRECTORY, SMILE_AUDIO, SMILE_SENT);
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), path);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMddhhmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "AUD-" + timeStamp + "." + droidninja.filepicker.utils.Utils.getFileExtension(file));
        return mediaFile.getAbsolutePath();
    }

    private void startSignUpNewUser(final QBUser newUser, boolean update) {
        requestExecutor.signUpNewUser(newUser, new QBEntityCallback<QBUser>() {
                    @Override
                    public void onSuccess(QBUser result, Bundle params) {
                        loginToChat(result);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        if (e.getHttpStatusCode() == Consts.ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
                            signInCreatedUser(newUser, update);
                        } else {
                            Toaster.longToast(R.string.sign_up_error);
                        }
                    }
                }
        );
    }

    private void saveUserData(QBUser qbUser) {
        SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
        sharedPrefsHelper.save(Consts.PREF_CURREN_ROOM_NAME, qbUser.getTags().getItemsAsString());
        sharedPrefsHelper.saveQbUser(qbUser);
    }

    private void removeAllUserData(final QBUser user) {
        requestExecutor.deleteCurrentUser(user.getId(), new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                UsersUtils.removeUserData(App.getInstance().getApplicationContext());
                createUserWithEnteredData();
            }

            @Override
            public void onError(QBResponseException e) {
                Toaster.longToast(R.string.sign_up_error);
            }
        });
    }

    private void signInCreatedUser(final QBUser user, final boolean deleteCurrentUser) {
        requestExecutor.signInUser(user, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser result, Bundle params) {
                if (deleteCurrentUser) {
                    removeAllUserData(result);
                }
            }

            @Override
            public void onError(QBResponseException responseException) {
                Toaster.longToast(R.string.sign_up_error);
            }
        });
    }

    private void createUserWithEnteredData() {
        createQBUserWithCurrentData(user, channel);
    }

    private void createQBUserWithCurrentData(User user, Channel channel) {
        QBUser tempUser = SharedPrefsHelper.getInstance().getQbUser();
        if (tempUser != null) {
            QBUsers.signIn(tempUser)
                    .performAsync(new QBEntityCallback<QBUser>() {
                        @Override
                        public void onSuccess(QBUser loggedUser, Bundle bundle) {
                            if (loggedUser != null) {
                                StringifyArrayList<String> userTags = tempUser.getTags();
                                if (userTags == null) {
                                    userTags = new StringifyArrayList<>();
                                }
                                String chatRoomName = "cha" + channel.getUid().replace("-", "");
                                if (!TextUtils.isEmpty(user.getName()) && !TextUtils.isEmpty(chatRoomName)) {
                                    userTags.add(chatRoomName);
                                    loggedUser.setTags(userTags);
                                }
                                QBUsers.updateUser(loggedUser).performAsync(new QBEntityCallback<QBUser>() {
                                    @Override
                                    public void onSuccess(QBUser user, Bundle args) {
                                        Log.i(TAG, ">>> User: " + user);
                                        saveUserData(user);
                                    }

                                    @Override
                                    public void onError(QBResponseException errors) {
                                        errors.printStackTrace();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onError(QBResponseException e) {

                        }
                    });
        } else {
            QBUser qbUser = null;
            String chatRoomName = "cha" + channel.getUid().replace("-", "");
            if (!TextUtils.isEmpty(user.getName()) && !TextUtils.isEmpty(chatRoomName)) {
                StringifyArrayList<String> userTags = new StringifyArrayList<>();
                userTags.add(chatRoomName);

                qbUser = new QBUser();
                qbUser.setFullName(user.getName());
                qbUser.setLogin(user.getUid());
                qbUser.setPassword(Consts.DEFAULT_USER_PASSWORD);
                qbUser.setCustomData(user.getImage());
                qbUser.setPhone(user.getPhone());
                qbUser.setTags(userTags);
                startSignUpNewUser(qbUser, true);
            }
        }
    }

    private void loginToChat(final QBUser qbUser) {
        qbUser.setPassword(Consts.DEFAULT_USER_PASSWORD);
        userForSave = qbUser;
        navigator.startLoginService(qbUser);
    }
}
