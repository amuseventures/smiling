package com.superdev.smiling.groupconversation.data_model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by gautam on 10/6/17.
 */

public class GroupChat {

    private final List<GroupMessage> groupMessages;

    public GroupChat(List<GroupMessage> groupMessages) {
        this.groupMessages = groupMessages;
    }

    public int size() {
        return groupMessages.size();
    }

    public GroupMessage get(int position) {
        return groupMessages.get(position);
    }

    public void addMessage(GroupMessage groupMessage) {
        if (!this.groupMessages.contains(groupMessage))
            this.groupMessages.add(groupMessage);
    }

    public void remove(GroupMessage m) {
        if (this.groupMessages.contains(m)) {
            this.groupMessages.remove(m);
        }
    }

    public List<GroupMessage> getGroupMessages() {
        return groupMessages;
    }

    public GroupChat sortedByDate() {
        List<GroupMessage> sortedList = new ArrayList<>(groupMessages);
        Collections.sort(sortedList, byDate());
        return new GroupChat(sortedList);
    }

    private static Comparator<? super GroupMessage> byDate() {
        return new Comparator<GroupMessage>() {
            @Override
            public int compare(GroupMessage o1, GroupMessage o2) {
                long time1 = Long.parseLong(o1.getTimestamp().replace("/", ""));
                long time2 = Long.parseLong(o2.getTimestamp().replace("/", ""));
                return time1 < time2 ? -1 : time1 > time2 ? 1 : 0;
            }
        };
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GroupChat chat = (GroupChat) o;

        return groupMessages != null ? groupMessages.equals(chat.groupMessages) : chat.groupMessages == null;

    }

    @Override
    public int hashCode() {
        return groupMessages != null ? groupMessages.hashCode() : 0;
    }
}
