package com.superdev.smiling.groupconversation.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.analytics.DeveloperError;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.view.ConversationAudioView;
import com.superdev.smiling.conversation.view.ConversationAudioViewHolder;
import com.superdev.smiling.conversation.view.ConversationContactView;
import com.superdev.smiling.conversation.view.ConversationContactViewHolder;
import com.superdev.smiling.conversation.view.ConversationDocView;
import com.superdev.smiling.conversation.view.ConversationDocViewHolder;
import com.superdev.smiling.conversation.view.ConversationFileView;
import com.superdev.smiling.conversation.view.ConversationFileViewHolder;
import com.superdev.smiling.conversation.view.ConversationMapView;
import com.superdev.smiling.conversation.view.ConversationMapViewHolder;
import com.superdev.smiling.conversation.view.ConversationMessageView;
import com.superdev.smiling.conversation.view.ConversationMessageViewHolder;
import com.superdev.smiling.conversation.view.ConversationVideoView;
import com.superdev.smiling.conversation.view.ConversationVideoViewHolder;
import com.superdev.smiling.conversation.view.ConversationVoiceView;
import com.superdev.smiling.conversation.view.ConversationVoiceViewHolder;
import com.superdev.smiling.groupconversation.data_model.GroupChat;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.groupconversation.view.GroupConversationDisplayer;
import com.superdev.smiling.user.data_model.User;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.superdev.smiling.Utils.formatTimespan;

/**
 * Created by gautam on 10/3/17.
 */

@SuppressWarnings("ALL")
public class GroupMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Handler.Callback, SeekBar.OnSeekBarChangeListener {

    private static final int VIEW_TYPE_MESSAGE_THIS_USER = 0;
    private static final int VIEW_TYPE_MESSAGE_OTHER_USERS = 1;
    private static final int VIEW_TYPE_MESSAGE_THIS_USER_OTHER_DATE = 3;
    private static final int VIEW_TYPE_MESSAGE_OTHER_USERS_OTHER_DATE = 4;

    private static final int VIEW_TYPE_FILE_THIS_USER = 5;
    private static final int VIEW_TYPE_FILE_OTHER_USERS = 6;
    private static final int VIEW_TYPE_FILE_THIS_USER_OTHER_DATE = 7;
    private static final int VIEW_TYPE_FILE_OTHER_USERS_OTHER_DATE = 8;

    private static final int VIEW_TYPE_MAP_THIS_USER = 9;
    private static final int VIEW_TYPE_MAP_OTHER_USERS = 10;
    private static final int VIEW_TYPE_MAP_THIS_USER_OTHER_DATE = 11;
    private static final int VIEW_TYPE_MAP_OTHER_USERS_OTHER_DATE = 12;

    private static final int VIEW_TYPE_DOC_THIS_USER = 13;
    private static final int VIEW_TYPE_DOC_OTHER_USERS = 14;
    private static final int VIEW_TYPE_DOC_THIS_USER_OTHER_DATE = 15;
    private static final int VIEW_TYPE_DOC_OTHER_USERS_OTHER_DATE = 16;

    private static final int VIEW_TYPE_AUDIO_THIS_USER = 17;
    private static final int VIEW_TYPE_AUDIO_OTHER_USERS = 18;
    private static final int VIEW_TYPE_AUDIO_THIS_USER_OTHER_DATE = 19;
    private static final int VIEW_TYPE_AUDIO_OTHER_USERS_OTHER_DATE = 20;

    private static final int VIEW_TYPE_VIDEO_THIS_USER = 21;
    private static final int VIEW_TYPE_VIDEO_OTHER_USERS = 22;
    private static final int VIEW_TYPE_VIDEO_THIS_USER_OTHER_DATE = 23;
    private static final int VIEW_TYPE_VIDEO_OTHER_USERS_OTHER_DATE = 24;

    private static final int VIEW_TYPE_CONTACT_THIS_USER = 25;
    private static final int VIEW_TYPE_CONTACT_OTHER_USERS = 26;
    private static final int VIEW_TYPE_CONTACT_THIS_USER_OTHER_DATE = 27;
    private static final int VIEW_TYPE_CONTACT_OTHER_USERS_OTHER_DATE = 28;

    private static final int VIEW_TYPE_VOICE_THIS_USER = 29;
    private static final int VIEW_TYPE_VOICE_OTHER_USERS = 30;
    private static final int VIEW_TYPE_VOICE_THIS_USER_OTHER_DATE = 31;
    private static final int VIEW_TYPE_VOICE_OTHER_USERS_OTHER_DATE = 32;

    private User user;
    private GroupChat chat = new GroupChat(new ArrayList<GroupMessage>());
    private String self;
    private final LayoutInflater inflater;
    private List<GroupMessage> selectedMessagesList;
    private boolean disableOnClick = false;
    private Channel channel;

    private GroupConversationDisplayer.ConversationActionListener conversationActionListener;

    private ConversationVoiceViewHolder voicePlayingHolder;

    private ConversationAudioViewHolder audioPlayingHolder;

    private final Context context;

    private static final int MSG_UPDATE_SEEK_BAR = 1845;

    private static final int MSG_UPDATEA_AUDIO_SEEK_BAR = 1846;

    private MediaPlayer mediaPlayer;

    private Handler uiUpdateHandler;

    private int playingPosition;

    public GroupMessageAdapter(Context context, LayoutInflater inflater) {
        this.inflater = inflater;
        setHasStableIds(true);
        this.context = context;
        selectedMessagesList = new ArrayList<>();
        uiUpdateHandler = new Handler(this);
        playingPosition = -1;
    }


    public void removeData(GroupMessage message) {
        int pos = this.chat.getGroupMessages().indexOf(message);
        this.chat.remove(message);
        if (pos > -1) {
            notifyItemRemoved(pos);
        } else {
            notifyDataSetChanged();
        }
    }

    void clearSelected() {
        selectedMessagesList.clear();
        notifyDataSetChanged();
    }

    private void toggleSelected(GroupMessage message, int position) {
        final boolean newState = !selectedMessagesList.contains(message);
        if (newState) {
            selectedMessagesList.add(message);
        } else {
            selectedMessagesList.remove(message);
        }
        notifyItemChanged(position);
    }

    public void update(Channel channel, GroupChat chat, User user) {
        this.channel = channel;
        this.chat = chat;
        this.user = user;
        this.self = user.getUid();
        notifyDataSetChanged();
    }

    public void add(Channel channel, GroupMessage message, User user) {
        this.channel = channel;
        this.self = user.getUid();
        message.setSelf(user.getUid());
        this.user = user;
        this.chat.addMessage(message);
        notifyDataSetChanged();
    }

    public List<GroupMessage> getSelectedMessagesList() {
        return selectedMessagesList;
    }

    private boolean alreadyInAdapter(GroupMessage newMsg) {
        boolean alreadyInAdapter = false;
        for (GroupMessage msg : this.chat.getGroupMessages()) {
            if (msg.getMessageId().equals(newMsg.getMessageId())) {
                alreadyInAdapter = true;
                break;
            }
        }
        return alreadyInAdapter;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ConversationMessageView messageView;
        ConversationFileView fileView;
        ConversationMapView mapView;
        ConversationDocView docView;
        ConversationAudioView audioView;
        ConversationVideoView videoView;
        ConversationContactView contactView;
        ConversationVoiceView voiceView;
        if (viewType == VIEW_TYPE_MESSAGE_THIS_USER) {
            messageView = (ConversationMessageView) inflater.inflate(R.layout.conversation_message_item_destination_view, parent, false);
            return new ConversationMessageViewHolder(messageView);
        } else if (viewType == VIEW_TYPE_MESSAGE_OTHER_USERS) {
            messageView = (ConversationMessageView) inflater.inflate(R.layout.conversation_message_item_sender_view, parent, false);
            return new ConversationMessageViewHolder(messageView);
        } else if (viewType == VIEW_TYPE_MESSAGE_THIS_USER_OTHER_DATE) {
            messageView = (ConversationMessageView) inflater.inflate(R.layout.conversation_message_item_destination_other_date_view, parent, false);
            return new ConversationMessageViewHolder(messageView);
        } else if (viewType == VIEW_TYPE_MESSAGE_OTHER_USERS_OTHER_DATE) {
            messageView = (ConversationMessageView) inflater.inflate(R.layout.conversation_message_item_sender_other_date_view, parent, false);
            return new ConversationMessageViewHolder(messageView);
        } else if (viewType == VIEW_TYPE_FILE_THIS_USER) {
            fileView = (ConversationFileView) inflater.inflate(R.layout.conversation_file_item_destination_view, parent, false);
            return new ConversationFileViewHolder(fileView);
        } else if (viewType == VIEW_TYPE_FILE_OTHER_USERS) {
            fileView = (ConversationFileView) inflater.inflate(R.layout.conversation_file_item_sender_view, parent, false);
            return new ConversationFileViewHolder(fileView);
        } else if (viewType == VIEW_TYPE_FILE_THIS_USER_OTHER_DATE) {
            fileView = (ConversationFileView) inflater.inflate(R.layout.conversation_file_item_destination_other_date_view, parent, false);
            return new ConversationFileViewHolder(fileView);
        } else if (viewType == VIEW_TYPE_FILE_OTHER_USERS_OTHER_DATE) {
            fileView = (ConversationFileView) inflater.inflate(R.layout.conversation_file_item_sender_other_date_view, parent, false);
            return new ConversationFileViewHolder(fileView);
        } else if (viewType == VIEW_TYPE_MAP_THIS_USER) {
            mapView = (ConversationMapView) inflater.inflate(R.layout.conversation_map_item_destination_view, parent, false);
            return new ConversationMapViewHolder(mapView);
        } else if (viewType == VIEW_TYPE_MAP_OTHER_USERS) {
            mapView = (ConversationMapView) inflater.inflate(R.layout.conversation_map_item_sender_view, parent, false);
            return new ConversationMapViewHolder(mapView);
        } else if (viewType == VIEW_TYPE_MAP_THIS_USER_OTHER_DATE) {
            mapView = (ConversationMapView) inflater.inflate(R.layout.conversation_map_item_destination_other_date_view, parent, false);
            return new ConversationMapViewHolder(mapView);
        } else if (viewType == VIEW_TYPE_MAP_OTHER_USERS_OTHER_DATE) {
            mapView = (ConversationMapView) inflater.inflate(R.layout.conversation_map_item_sender_other_date_view, parent, false);
            return new ConversationMapViewHolder(mapView);
        } else if (viewType == VIEW_TYPE_DOC_THIS_USER) {
            docView = (ConversationDocView) inflater.inflate(R.layout.conversation_doc_item_destination_view, parent, false);
            return new ConversationDocViewHolder(docView);
        } else if (viewType == VIEW_TYPE_DOC_OTHER_USERS) {
            docView = (ConversationDocView) inflater.inflate(R.layout.conversation_doc_item_sender_view, parent, false);
            return new ConversationDocViewHolder(docView);
        } else if (viewType == VIEW_TYPE_DOC_THIS_USER_OTHER_DATE) {
            docView = (ConversationDocView) inflater.inflate(R.layout.conversation_doc_item_destination_other_date_view, parent, false);
            return new ConversationDocViewHolder(docView);
        } else if (viewType == VIEW_TYPE_DOC_OTHER_USERS_OTHER_DATE) {
            docView = (ConversationDocView) inflater.inflate(R.layout.conversation_doc_item_sender_other_date_view, parent, false);
            return new ConversationDocViewHolder(docView);
        } else if (viewType == VIEW_TYPE_AUDIO_THIS_USER) {
            audioView = (ConversationAudioView) inflater.inflate(R.layout.conversation_audio_item_destination_view, parent, false);
            return new ConversationAudioViewHolder(audioView);
        } else if (viewType == VIEW_TYPE_AUDIO_OTHER_USERS) {
            audioView = (ConversationAudioView) inflater.inflate(R.layout.conversation_audio_item_sender_view, parent, false);
            return new ConversationAudioViewHolder(audioView);
        } else if (viewType == VIEW_TYPE_AUDIO_THIS_USER_OTHER_DATE) {
            audioView = (ConversationAudioView) inflater.inflate(R.layout.conversation_audio_item_destination_other_date_view, parent, false);
            return new ConversationAudioViewHolder(audioView);
        } else if (viewType == VIEW_TYPE_AUDIO_OTHER_USERS_OTHER_DATE) {
            audioView = (ConversationAudioView) inflater.inflate(R.layout.conversation_audio_item_sender_other_date_view, parent, false);
            return new ConversationAudioViewHolder(audioView);
        } else if (viewType == VIEW_TYPE_VOICE_THIS_USER) {
            voiceView = (ConversationVoiceView) inflater.inflate(R.layout.conversation_voice_item_destination_view, parent, false);
            return new ConversationVoiceViewHolder(voiceView);
        } else if (viewType == VIEW_TYPE_VOICE_OTHER_USERS) {
            voiceView = (ConversationVoiceView) inflater.inflate(R.layout.conversation_voice_item_sender_view, parent, false);
            return new ConversationVoiceViewHolder(voiceView);
        } else if (viewType == VIEW_TYPE_VOICE_THIS_USER_OTHER_DATE) {
            voiceView = (ConversationVoiceView) inflater.inflate(R.layout.conversation_voice_item_destination_other_date_view, parent, false);
            return new ConversationVoiceViewHolder(voiceView);
        } else if (viewType == VIEW_TYPE_VOICE_OTHER_USERS_OTHER_DATE) {
            voiceView = (ConversationVoiceView) inflater.inflate(R.layout.conversation_voice_item_sender_other_date_view, parent, false);
            return new ConversationVoiceViewHolder(voiceView);
        } else if (viewType == VIEW_TYPE_VIDEO_THIS_USER) {
            videoView = (ConversationVideoView) inflater.inflate(R.layout.conversation_video_item_destination_view, parent, false);
            return new ConversationVideoViewHolder(videoView);
        } else if (viewType == VIEW_TYPE_VIDEO_OTHER_USERS) {
            videoView = (ConversationVideoView) inflater.inflate(R.layout.conversation_video_item_sender_view, parent, false);
            return new ConversationVideoViewHolder(videoView);
        } else if (viewType == VIEW_TYPE_VIDEO_THIS_USER_OTHER_DATE) {
            videoView = (ConversationVideoView) inflater.inflate(R.layout.conversation_video_item_destination_other_date_view, parent, false);
            return new ConversationVideoViewHolder(videoView);
        } else if (viewType == VIEW_TYPE_VIDEO_OTHER_USERS_OTHER_DATE) {
            videoView = (ConversationVideoView) inflater.inflate(R.layout.conversation_video_item_sender_other_date_view, parent, false);
            return new ConversationVideoViewHolder(videoView);
        } else if (viewType == VIEW_TYPE_CONTACT_THIS_USER) {
            contactView = (ConversationContactView) inflater.inflate(R.layout.conversation_contact_item_destination_view, parent, false);
            return new ConversationContactViewHolder(contactView);
        } else if (viewType == VIEW_TYPE_CONTACT_OTHER_USERS) {
            contactView = (ConversationContactView) inflater.inflate(R.layout.conversation_contact_item_sender_view, parent, false);
            return new ConversationContactViewHolder(contactView);
        } else if (viewType == VIEW_TYPE_CONTACT_THIS_USER_OTHER_DATE) {
            contactView = (ConversationContactView) inflater.inflate(R.layout.conversation_contact_item_destination_other_date_view, parent, false);
            return new ConversationContactViewHolder(contactView);
        } else if (viewType == VIEW_TYPE_CONTACT_OTHER_USERS_OTHER_DATE) {
            contactView = (ConversationContactView) inflater.inflate(R.layout.conversation_contact_item_sender_other_date_view, parent, false);
            return new ConversationContactViewHolder(contactView);
        } else {
            throw new DeveloperError("chat message error");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ConversationMessageViewHolder) {
            ConversationMessageViewHolder holder = (ConversationMessageViewHolder) viewHolder;
            holder.bind(channel, chat.get(position), self, conversationMessageListener);
            if (getSelectedMessagesList().contains(chat.get(position))) {
                holder.getMainFrame().setForeground(new ColorDrawable(R.color.list_item_selected));
            } else {
                holder.getMainFrame().setForeground(new ColorDrawable(0));
            }
        } else if (viewHolder instanceof ConversationMapViewHolder) {
            ConversationMapViewHolder holder = (ConversationMapViewHolder) viewHolder;
            holder.bind(channel, chat.get(position), self, conversationMapListener);
            if (getSelectedMessagesList().contains(chat.get(position))) {
                holder.getMainFrame().setForeground(new ColorDrawable(R.color.list_item_selected));
            } else {
                holder.getMainFrame().setForeground(new ColorDrawable(0));
            }
        } else if (viewHolder instanceof ConversationDocViewHolder) {
            ConversationDocViewHolder holder = (ConversationDocViewHolder) viewHolder;
            holder.bind(channel, chat.get(position), self, conversationDocViewListener);
            if (getSelectedMessagesList().contains(chat.get(position))) {
                holder.getMainFrame().setForeground(new ColorDrawable(R.color.list_item_selected));
            } else {
                holder.getMainFrame().setForeground(new ColorDrawable(0));
            }
        } else if (viewHolder instanceof ConversationAudioViewHolder) {
            ConversationAudioViewHolder holder = (ConversationAudioViewHolder) viewHolder;
            GroupMessage audioMessage = chat.get(position);
            holder.bind(channel, audioMessage, self, conversationAudioViewListener);

            holder.getConversationAudioView().audioSeekbar.setOnSeekBarChangeListener(this);
            if (position == playingPosition) {
                audioPlayingHolder = holder;
                // this view holder corresponds to the currently playing audio cell
                // update its view to show playing progress
                updateAudioPlayingView();
            } else {
                // and this one corresponds to non playing
                updateNonAudioPlayingView(holder);
            }

            holder.getConversationAudioView().controlBtnPlay.setOnClickListener(view -> {
                File file;
                if (!self.equalsIgnoreCase(audioMessage.getAuthor().getUid())) {
                    file = new File(Utils.getAudioPath() + "/" + audioMessage.getAudioModel().getAudioName());
                } else {
                    file = new File(Utils.getAudioSentPath() + "/" + audioMessage.getAudioModel().getAudioName());
                }
                if (file.exists() && audioMessage.getAudioModel().getIsUploaded() == 1) {
                    if (holder.getAdapterPosition() == playingPosition) {
                        // toggle between play/pause of audio
                        if (mediaPlayer.isPlaying()) {
                            mediaPlayer.pause();
                        } else {
                            mediaPlayer.start();
                        }
                    } else {
                        // start another audio playback
                        playingPosition = holder.getAdapterPosition();
                        if (mediaPlayer != null) {
                            if (null != audioPlayingHolder) {
                                updateNonAudioPlayingView(audioPlayingHolder);
                            }
                            mediaPlayer.release();
                        }
                        audioPlayingHolder = holder;
                        startMediaPlayer(Uri.fromFile(file));
                    }
                    updateAudioPlayingView();
                }
            });

            if (getSelectedMessagesList().contains(audioMessage)) {
                holder.getMainFrame().setForeground(new ColorDrawable(R.color.list_item_selected));
            } else {
                holder.getMainFrame().setForeground(new ColorDrawable(0));
            }

        } else if (viewHolder instanceof ConversationVoiceViewHolder) {
            ConversationVoiceViewHolder holder = (ConversationVoiceViewHolder) viewHolder;
            GroupMessage voiceMessage = chat.get(position);
            holder.bind(channel, voiceMessage, self, conversationVoiceViewListener);

            holder.getConversationVoiceView().audioSeekbar.setOnSeekBarChangeListener(this);
            if (position == playingPosition) {
                voicePlayingHolder = holder;
                // this view holder corresponds to the currently playing audio cell
                // update its view to show playing progress
                updatePlayingView();
            } else {
                // and this one corresponds to non playing
                updateNonPlayingView(holder);
            }

            holder.getConversationVoiceView().controlBtnPlay.setOnClickListener(view -> {
                File file;
                if (!self.equalsIgnoreCase(voiceMessage.getAuthor().getUid())) {
                    file = new File(Utils.getVoicePath() + "/" + voiceMessage.getVoiceModel().getVoiceName());
                } else {
                    file = new File(Utils.getVoiceSentPath() + "/" + voiceMessage.getVoiceModel().getVoiceName());
                }
                if (file.exists() && voiceMessage.getVoiceModel().getIsUploaded() == 1) {
                    if (holder.getAdapterPosition() == playingPosition) {
                        // toggle between play/pause of audio
                        if (mediaPlayer.isPlaying()) {
                            mediaPlayer.pause();
                        } else {
                            mediaPlayer.start();
                        }
                    } else {
                        // start another audio playback
                        playingPosition = holder.getAdapterPosition();
                        if (mediaPlayer != null) {
                            if (null != voicePlayingHolder) {
                                updateNonPlayingView(voicePlayingHolder);
                            }
                            mediaPlayer.release();
                        }
                        voicePlayingHolder = holder;
                        startMediaPlayer(Uri.fromFile(file));
                    }
                    updatePlayingView();
                }
            });

            if (getSelectedMessagesList().contains(voiceMessage)) {
                holder.getMainFrame().setForeground(new ColorDrawable(R.color.list_item_selected));
            } else {
                holder.getMainFrame().setForeground(new ColorDrawable(0));
            }
        } else if (viewHolder instanceof ConversationVideoViewHolder) {
            ConversationVideoViewHolder holder = (ConversationVideoViewHolder) viewHolder;
            holder.bind(channel, chat.get(position), self, conversationVideoViewListener);
            if (getSelectedMessagesList().contains(chat.get(position))) {
                holder.getMainFrame().setForeground(new ColorDrawable(R.color.list_item_selected));
            } else {
                holder.getMainFrame().setForeground(new ColorDrawable(0));
            }
        } else if (viewHolder instanceof ConversationContactViewHolder) {
            ConversationContactViewHolder holder = (ConversationContactViewHolder) viewHolder;
            holder.bind(channel, chat.get(position), self, contactSelectionListener);
            if (getSelectedMessagesList().contains(chat.get(position))) {
                holder.getMainFrame().setForeground(new ColorDrawable(R.color.list_item_selected));
            } else {
                holder.getMainFrame().setForeground(new ColorDrawable(0));
            }
        } else {
            ConversationFileViewHolder holder = (ConversationFileViewHolder) viewHolder;
            holder.bind(channel, chat.get(position), self, conversationFileViewListener);
            if (getSelectedMessagesList().contains(chat.get(position))) {
                holder.getMainFrame().setForeground(new ColorDrawable(R.color.list_item_selected));
            } else {
                holder.getMainFrame().setForeground(new ColorDrawable(0));
            }
        }
    }

    @Override
    public int getItemCount() {
        return chat.size();
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(chat.get(position).getTimestamp().replace("/", ""));
    }

    @Override
    public int getItemViewType(int position) {
        GroupMessage message = chat.get(position);
        if (message.isMmsType()) {
            return imageViewType(position);
        } else if (message.isLocationType()) {
            return locationViewType(position);
        } else if (message.isDocumentType()) {
            return documentViewType(position);
        } else if (message.isVoiceType()) {
            return voiceViewType(position);
        } else if (message.isAudioType()) {
            return audioViewType(position);
        } else if (message.isVideoType()) {
            return videoViewType(position);
        } else if (message.isContactType()) {
            return contactViewType(position);
        } else {
            return messageViewType(position);
        }
    }

    public void attach(GroupConversationDisplayer.ConversationActionListener conversationActionListener) {
        this.conversationActionListener = conversationActionListener;
    }

    public void detach(GroupConversationDisplayer.ConversationActionListener conversationActionListener) {
        this.conversationActionListener = null;
        stopPlayer();
    }

    private ConversationMessageViewHolder.GroupConversationMessageListener conversationMessageListener = new ConversationMessageViewHolder.GroupConversationMessageListener() {

        @Override
        public void onClick(View view, GroupMessage message) {
            if (!disableOnClick)
                conversationActionListener.onMessageSelected(view, message);
            else {
                toggleSelected(message, chat.getGroupMessages().indexOf(message));
                if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                    conversationActionListener.hideCabMenu();
                    disableOnClick = false;
                    return;
                }
                disableOnClick = true;
                conversationActionListener.showCabMenu(getSelectedMessagesList());
                return;
            }
        }

        @Override
        public void onLongClick(View view, GroupMessage message) {
            toggleSelected(message, chat.getGroupMessages().indexOf(message));
            if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                conversationActionListener.hideCabMenu();
                disableOnClick = false;
                return;
            }
            disableOnClick = true;
            conversationActionListener.showCabMenu(getSelectedMessagesList());
        }
    };

    private ConversationFileViewHolder.GroupConversationFileViewListener conversationFileViewListener = new ConversationFileViewHolder.GroupConversationFileViewListener() {
        @Override
        public void onClick(View view, GroupMessage message) {
            if (!disableOnClick)
                conversationActionListener.onImageSelected(user.getUid(), view, message);
            else {
                toggleSelected(message, chat.getGroupMessages().indexOf(message));
                if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                    conversationActionListener.hideCabMenu();
                    disableOnClick = false;
                    return;
                }
                disableOnClick = true;
                conversationActionListener.showCabMenu(getSelectedMessagesList());
                return;
            }
        }

        @Override
        public void onLongClick(View view, GroupMessage message) {
            toggleSelected(message, chat.getGroupMessages().indexOf(message));
            if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                conversationActionListener.hideCabMenu();
                disableOnClick = false;
                return;
            }
            disableOnClick = true;
            conversationActionListener.showCabMenu(getSelectedMessagesList());
        }
    };

    private ConversationMapViewHolder.GroupConversationMapViewListener conversationMapListener = new ConversationMapViewHolder.GroupConversationMapViewListener() {
        @Override
        public void onClick(View view, GroupMessage message) {
            if (!disableOnClick)
                conversationActionListener.onMapViewSelected(message);
            else {
                toggleSelected(message, chat.getGroupMessages().indexOf(message));
                if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                    conversationActionListener.hideCabMenu();
                    disableOnClick = false;
                    return;
                }
                disableOnClick = true;
                conversationActionListener.showCabMenu(getSelectedMessagesList());
                return;
            }
        }

        @Override
        public void onLongClick(View view, GroupMessage message) {
            toggleSelected(message, chat.getGroupMessages().indexOf(message));
            if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                conversationActionListener.hideCabMenu();
                disableOnClick = false;
                return;
            }
            disableOnClick = true;
            conversationActionListener.showCabMenu(getSelectedMessagesList());
        }
    };

    private ConversationDocViewHolder.GroupConversationDocViewListener conversationDocViewListener = new ConversationDocViewHolder.GroupConversationDocViewListener() {
        @Override
        public void onClick(View view, GroupMessage message) {
            if (!disableOnClick)
                conversationActionListener.onDocViewSelected(message);
            else {
                toggleSelected(message, chat.getGroupMessages().indexOf(message));
                if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                    conversationActionListener.hideCabMenu();
                    disableOnClick = false;
                    return;
                }
                disableOnClick = true;
                conversationActionListener.showCabMenu(getSelectedMessagesList());
                return;
            }
        }

        @Override
        public void onLongClick(View view, GroupMessage message) {
            toggleSelected(message, chat.getGroupMessages().indexOf(message));
            if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                conversationActionListener.hideCabMenu();
                disableOnClick = false;
                return;
            }
            disableOnClick = true;
            conversationActionListener.showCabMenu(getSelectedMessagesList());
        }
    };

    private ConversationAudioViewHolder.GroupConversationAudioViewListener conversationAudioViewListener = new ConversationAudioViewHolder.GroupConversationAudioViewListener() {
        @Override
        public void onClick(View view, GroupMessage message) {
            if (!disableOnClick) {

            } else {
                toggleSelected(message, chat.getGroupMessages().indexOf(message));
                if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                    conversationActionListener.hideCabMenu();
                    disableOnClick = false;
                    return;
                }
                disableOnClick = true;
                conversationActionListener.showCabMenu(getSelectedMessagesList());
                return;
            }
        }

        @Override
        public void onLongClick(View view, GroupMessage message) {
            toggleSelected(message, chat.getGroupMessages().indexOf(message));
            if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                conversationActionListener.hideCabMenu();
                disableOnClick = false;
                return;
            }
            disableOnClick = true;
            conversationActionListener.showCabMenu(getSelectedMessagesList());
        }
    };

    private ConversationVoiceViewHolder.GroupConversationVoiceViewListener conversationVoiceViewListener = new ConversationVoiceViewHolder.GroupConversationVoiceViewListener() {
        @Override
        public void onClick(View view, GroupMessage message) {
            if (!disableOnClick) {
            } else {
                toggleSelected(message, chat.getGroupMessages().indexOf(message));
                if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                    conversationActionListener.hideCabMenu();
                    disableOnClick = false;
                    return;
                }
                disableOnClick = true;
                conversationActionListener.showCabMenu(getSelectedMessagesList());
                return;
            }
        }

        @Override
        public void onLongClick(View view, GroupMessage message) {
            toggleSelected(message, chat.getGroupMessages().indexOf(message));
            if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                conversationActionListener.hideCabMenu();
                disableOnClick = false;
                return;
            }
            disableOnClick = true;
            conversationActionListener.showCabMenu(getSelectedMessagesList());
        }
    };

    private ConversationVideoViewHolder.GroupConversationVideoViewListener conversationVideoViewListener = new ConversationVideoViewHolder.GroupConversationVideoViewListener() {

        @Override
        public void onClick(View view, GroupMessage message) {
            if (!disableOnClick)
                conversationActionListener.onVideoSelected(view, message);
            else {
                toggleSelected(message, chat.getGroupMessages().indexOf(message));
                if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                    conversationActionListener.hideCabMenu();
                    disableOnClick = false;
                    return;
                }
                disableOnClick = true;
                conversationActionListener.showCabMenu(getSelectedMessagesList());
            }
        }

        @Override
        public void onLongClick(View view, GroupMessage message) {
            toggleSelected(message, chat.getGroupMessages().indexOf(message));
            if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                conversationActionListener.hideCabMenu();
                disableOnClick = false;
                return;
            }
            disableOnClick = true;
            conversationActionListener.showCabMenu(getSelectedMessagesList());
        }
    };

    private ConversationContactViewHolder.GroupContactSelectionListener contactSelectionListener = new ConversationContactViewHolder.GroupContactSelectionListener() {

        @Override
        public void onClick(View view, GroupMessage message) {
            if (!disableOnClick) {

            } else {
                toggleSelected(message, chat.getGroupMessages().indexOf(message));
                if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                    conversationActionListener.hideCabMenu();
                    disableOnClick = false;
                    return;
                }
                disableOnClick = true;
                conversationActionListener.showCabMenu(getSelectedMessagesList());
            }
        }

        @Override
        public void onLongClick(View view, GroupMessage message) {
            toggleSelected(message, chat.getGroupMessages().indexOf(message));
            if (getSelectedMessagesList() == null || getSelectedMessagesList().isEmpty()) {
                conversationActionListener.hideCabMenu();
                disableOnClick = false;
                return;
            }
            disableOnClick = true;
            conversationActionListener.showCabMenu(getSelectedMessagesList());
        }

        @Override
        public void onAddBtnClick(GroupMessage message) {
            if (!disableOnClick) {
                conversationActionListener.onContactAddBtn(message);
            }
        }

        @Override
        public void onMsgBtnClick(GroupMessage message) {
            if (!disableOnClick) {
                conversationActionListener.onContactMsgSelected(message);
            }
        }
    };


    private int messageViewType(int position) {
        try {
            String[] date1 = Utils.getDate(chat.get(position - 1).getTimestamp()).split("/");
            String[] date2 = Utils.getDate(chat.get(position).getTimestamp()).split("/");
            String concatDate1 = date1[0] + date1[1] + date1[2];
            String concatDate2 = date2[0] + date2[1] + date2[2];
            if (!concatDate1.equals(concatDate2)) {
                return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_MESSAGE_THIS_USER_OTHER_DATE : VIEW_TYPE_MESSAGE_OTHER_USERS_OTHER_DATE;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_MESSAGE_THIS_USER_OTHER_DATE : VIEW_TYPE_MESSAGE_OTHER_USERS_OTHER_DATE;
        }

        return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_MESSAGE_THIS_USER : VIEW_TYPE_MESSAGE_OTHER_USERS;
    }

    private int imageViewType(int position) {
        try {
            String[] date1 = Utils.getDate(chat.get(position - 1).getTimestamp()).split("/");
            String[] date2 = Utils.getDate(chat.get(position).getTimestamp()).split("/");
            String concatDate1 = date1[0] + date1[1] + date1[2];
            String concatDate2 = date2[0] + date2[1] + date2[2];
            if (!concatDate1.equals(concatDate2)) {
                return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_FILE_THIS_USER_OTHER_DATE : VIEW_TYPE_FILE_OTHER_USERS_OTHER_DATE;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_FILE_THIS_USER_OTHER_DATE : VIEW_TYPE_FILE_OTHER_USERS_OTHER_DATE;
        }
        return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_FILE_THIS_USER : VIEW_TYPE_FILE_OTHER_USERS;
    }

    private int videoViewType(int position) {
        try {
            String[] date1 = Utils.getDate(chat.get(position - 1).getTimestamp()).split("/");
            String[] date2 = Utils.getDate(chat.get(position).getTimestamp()).split("/");
            String concatDate1 = date1[0] + date1[1] + date1[2];
            String concatDate2 = date2[0] + date2[1] + date2[2];
            if (!concatDate1.equals(concatDate2)) {
                return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_VIDEO_THIS_USER_OTHER_DATE : VIEW_TYPE_VIDEO_OTHER_USERS_OTHER_DATE;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_VIDEO_THIS_USER_OTHER_DATE : VIEW_TYPE_VIDEO_OTHER_USERS_OTHER_DATE;
        }

        return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_VIDEO_THIS_USER : VIEW_TYPE_VIDEO_OTHER_USERS;
    }

    private int audioViewType(int position) {
        try {
            String[] date1 = Utils.getDate(chat.get(position - 1).getTimestamp()).split("/");
            String[] date2 = Utils.getDate(chat.get(position).getTimestamp()).split("/");
            String concatDate1 = date1[0] + date1[1] + date1[2];
            String concatDate2 = date2[0] + date2[1] + date2[2];
            if (!concatDate1.equals(concatDate2)) {
                return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_AUDIO_THIS_USER_OTHER_DATE : VIEW_TYPE_AUDIO_OTHER_USERS_OTHER_DATE;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_AUDIO_THIS_USER_OTHER_DATE : VIEW_TYPE_AUDIO_OTHER_USERS_OTHER_DATE;
        }

        return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_AUDIO_THIS_USER : VIEW_TYPE_AUDIO_OTHER_USERS;
    }

    private int locationViewType(int position) {
        try {
            String[] date1 = Utils.getDate(chat.get(position - 1).getTimestamp()).split("/");
            String[] date2 = Utils.getDate(chat.get(position).getTimestamp()).split("/");
            String concatDate1 = date1[0] + date1[1] + date1[2];
            String concatDate2 = date2[0] + date2[1] + date2[2];
            if (!concatDate1.equals(concatDate2)) {
                return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_MAP_THIS_USER_OTHER_DATE : VIEW_TYPE_MAP_OTHER_USERS_OTHER_DATE;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_MAP_THIS_USER_OTHER_DATE : VIEW_TYPE_MAP_OTHER_USERS_OTHER_DATE;
        }

        return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_MAP_THIS_USER : VIEW_TYPE_MAP_OTHER_USERS;
    }

    private int voiceViewType(int position) {
        try {
            String[] date1 = Utils.getDate(chat.get(position - 1).getTimestamp()).split("/");
            String[] date2 = Utils.getDate(chat.get(position).getTimestamp()).split("/");
            String concatDate1 = date1[0] + date1[1] + date1[2];
            String concatDate2 = date2[0] + date2[1] + date2[2];
            if (!concatDate1.equals(concatDate2)) {
                return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_VOICE_THIS_USER_OTHER_DATE : VIEW_TYPE_VOICE_OTHER_USERS_OTHER_DATE;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_VOICE_THIS_USER_OTHER_DATE : VIEW_TYPE_VOICE_OTHER_USERS_OTHER_DATE;
        }

        return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_VOICE_THIS_USER : VIEW_TYPE_VOICE_OTHER_USERS;
    }

    private int contactViewType(int position) {
        try {
            String[] date1 = Utils.getDate(chat.get(position - 1).getTimestamp()).split("/");
            String[] date2 = Utils.getDate(chat.get(position).getTimestamp()).split("/");
            String concatDate1 = date1[0] + date1[1] + date1[2];
            String concatDate2 = date2[0] + date2[1] + date2[2];
            if (!concatDate1.equals(concatDate2)) {
                return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_CONTACT_THIS_USER_OTHER_DATE : VIEW_TYPE_CONTACT_OTHER_USERS_OTHER_DATE;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_CONTACT_THIS_USER_OTHER_DATE : VIEW_TYPE_CONTACT_OTHER_USERS_OTHER_DATE;
        }

        return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_CONTACT_THIS_USER : VIEW_TYPE_CONTACT_OTHER_USERS;
    }

    private int documentViewType(int position) {
        try {
            String[] date1 = Utils.getDate(chat.get(position - 1).getTimestamp()).split("/");
            String[] date2 = Utils.getDate(chat.get(position).getTimestamp()).split("/");
            String concatDate1 = date1[0] + date1[1] + date1[2];
            String concatDate2 = date2[0] + date2[1] + date2[2];
            if (!concatDate1.equals(concatDate2)) {
                return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_DOC_THIS_USER_OTHER_DATE : VIEW_TYPE_DOC_OTHER_USERS_OTHER_DATE;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_DOC_THIS_USER_OTHER_DATE : VIEW_TYPE_DOC_OTHER_USERS_OTHER_DATE;
        }
        return !chat.get(position).getAuthor().getUid().equals(user.getUid()) ? VIEW_TYPE_DOC_THIS_USER : VIEW_TYPE_DOC_OTHER_USERS;
    }

    /**
     * Changes the view to non playing state
     * - icon is changed to play arrow
     * - seek bar disabled
     * - remove seek bar updater, if needed
     *
     * @param holder ViewHolder whose state is to be chagned to non playing
     */
    private void updateNonPlayingView(ConversationVoiceViewHolder holder) {
        if (holder == null)
            return;
        if (holder == voicePlayingHolder) {
            uiUpdateHandler.removeMessages(MSG_UPDATE_SEEK_BAR);
        }
        holder.getConversationVoiceView().audioSeekbar.setEnabled(false);
        holder.getConversationVoiceView().audioSeekbar.setProgress(0);
        holder.getConversationVoiceView().controlBtnPlay.setImageResource(R.drawable.inline_audio_play);
    }

    private void updateNonAudioPlayingView(ConversationAudioViewHolder holder) {
        if (holder == null)
            return;
        if (holder == audioPlayingHolder) {
            uiUpdateHandler.removeMessages(MSG_UPDATEA_AUDIO_SEEK_BAR);
        }
        holder.getConversationAudioView().audioSeekbar.setEnabled(false);
        holder.getConversationAudioView().audioSeekbar.setProgress(0);
        holder.getConversationAudioView().controlBtnPlay.setImageResource(R.drawable.inline_audio_play);
    }

    /**
     * Changes the view to playing state
     * - icon is changed to pause
     * - seek bar enabled
     * - start seek bar updater, if needed
     */
    private void updatePlayingView() {
        if (voicePlayingHolder == null)
            return;
        voicePlayingHolder.getConversationVoiceView().audioSeekbar.setMax(mediaPlayer.getDuration());
        voicePlayingHolder.getConversationVoiceView().audioSeekbar.setProgress(mediaPlayer.getCurrentPosition());
        voicePlayingHolder.getConversationVoiceView().duration.setText(formatTimespan(mediaPlayer.getCurrentPosition()));
        voicePlayingHolder.getConversationVoiceView().audioSeekbar.setEnabled(true);
        if (mediaPlayer.isPlaying()) {
            uiUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 100);
            voicePlayingHolder.getConversationVoiceView().controlBtnPlay.setImageResource(R.drawable.inline_audio_pause);
        } else {
            uiUpdateHandler.removeMessages(MSG_UPDATE_SEEK_BAR);
            voicePlayingHolder.getConversationVoiceView().controlBtnPlay.setImageResource(R.drawable.inline_audio_play);
        }
    }

    private void updateAudioPlayingView() {
        if (audioPlayingHolder == null)
            return;
        audioPlayingHolder.getConversationAudioView().audioSeekbar.setMax(mediaPlayer.getDuration());
        audioPlayingHolder.getConversationAudioView().audioSeekbar.setProgress(mediaPlayer.getCurrentPosition());
        audioPlayingHolder.getConversationAudioView().audioSeekbar.setEnabled(true);
        audioPlayingHolder.getConversationAudioView().duration.setText(formatTimespan(mediaPlayer.getCurrentPosition()));
        if (mediaPlayer.isPlaying()) {
            uiUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATEA_AUDIO_SEEK_BAR, 100);
            audioPlayingHolder.getConversationAudioView().controlBtnPlay.setImageResource(R.drawable.inline_audio_pause);
        } else {
            uiUpdateHandler.removeMessages(MSG_UPDATEA_AUDIO_SEEK_BAR);
            audioPlayingHolder.getConversationAudioView().controlBtnPlay.setImageResource(R.drawable.inline_audio_play);
        }
    }

    void stopPlayer() {
        if (null != mediaPlayer) {
            releaseMediaPlayer();
        }
    }

    @Override
    public boolean handleMessage(android.os.Message msg) {
        switch (msg.what) {
            case MSG_UPDATE_SEEK_BAR: {
                voicePlayingHolder.getConversationVoiceView().audioSeekbar.setProgress(mediaPlayer.getCurrentPosition());
                voicePlayingHolder.getConversationVoiceView().duration.setText(formatTimespan(mediaPlayer.getCurrentPosition()));
                uiUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 100);
                return true;
            }
            case MSG_UPDATEA_AUDIO_SEEK_BAR: {
                audioPlayingHolder.getConversationAudioView().audioSeekbar.setProgress(mediaPlayer.getCurrentPosition());
                audioPlayingHolder.getConversationAudioView().duration.setText(formatTimespan(mediaPlayer.getCurrentPosition()));
                uiUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATEA_AUDIO_SEEK_BAR, 100);
                return true;
            }
        }
        return false;
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder instanceof ConversationVoiceViewHolder) {
            if (playingPosition == holder.getAdapterPosition()) {
                // view holder displaying playing audio cell is being recycled
                // change its state to non-playing
                updateNonPlayingView(voicePlayingHolder);
                voicePlayingHolder = null;
            }
        }
        if (holder instanceof ConversationAudioViewHolder) {
            if (playingPosition == holder.getAdapterPosition()) {
                updateNonAudioPlayingView(audioPlayingHolder);
                audioPlayingHolder = null;
            }
        }
    }

    private void startMediaPlayer(Uri audioResId) {
        mediaPlayer = MediaPlayer.create(context, audioResId);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                releaseMediaPlayer();
            }
        });
        mediaPlayer.start();
    }

    private void releaseMediaPlayer() {
        if (null != voicePlayingHolder) {
            updateNonPlayingView(voicePlayingHolder);
            updateNonAudioPlayingView(audioPlayingHolder);
        }
        if (null != audioPlayingHolder) {
            updateNonPlayingView(voicePlayingHolder);
            updateNonAudioPlayingView(audioPlayingHolder);
        }
        mediaPlayer.release();
        mediaPlayer = null;
        playingPosition = -1;
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            if (mediaPlayer != null) {
                mediaPlayer.seekTo(progress);
            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
