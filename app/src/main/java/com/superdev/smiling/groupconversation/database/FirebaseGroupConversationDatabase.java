package com.superdev.smiling.groupconversation.database;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.superdev.smiling.Constants;
import com.superdev.smiling.MimeTypeEnum;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.groupconversation.data_model.GroupChat;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.rx.FirebaseObservableListeners;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by gautam on 10/3/17.
 */

public class FirebaseGroupConversationDatabase implements GroupConversationDatabase {

    private static final int DEFAULT_LIMIT = 1000;

    private final DatabaseReference messagesDB;
    private final DatabaseReference ownersDB;
    private final FirebaseObservableListeners firebaseObservableListeners;
    private final DatabaseReference notificationRef;

    public FirebaseGroupConversationDatabase(FirebaseDatabase firebaseDatabase, FirebaseObservableListeners firebaseObservableListeners) {
        messagesDB = firebaseDatabase.getReference(Constants.FIREBASE_GLOBALMESSAGES);
        ownersDB = firebaseDatabase.getReference(Constants.FIREBASE_OWNERS);
        notificationRef = firebaseDatabase.getReference(Constants.FIREBASE_GROUP_NOTIFICATION).child(Constants.FIREBASE_CHAT);
        this.firebaseObservableListeners = firebaseObservableListeners;
    }

    @Override
    public Observable<GroupChat> observeChat(Channel channel) {
        return firebaseObservableListeners.listenToValueEvents(messagesInChannel(channel).limitToLast(DEFAULT_LIMIT), toChat());
    }

    @Override
    public Observable<GroupChat> observeMedia(Channel channel) {
        DatabaseReference databaseReference = messagesInChannel(channel);
        return firebaseObservableListeners.listenToValueEvents(databaseReference.orderByChild("msgType").equalTo(MimeTypeEnum.IMAGE.getValue()).limitToLast(20), toChat());
    }

    @Override
    public Observable<List<String>> getUserKeyFromChannel(Channel channel) {
        return null;
    }

    @Override
    public Observable<List<String>> getUserIdsFromChannel(Channel channel) {
        return firebaseObservableListeners.listenToValueEvents(usersInChannel(channel), getUserNames());
    }

    private DatabaseReference messagesOfUser(Channel channel) {
        return messagesInChannel(channel).child(Constants.FIREBASE_CHAT_MESSAGES);
    }

    @Override
    public void sendMessage(Channel channel, GroupMessage groupMessage) {
        String senderMsgKey = messagesInChannel(channel).push().getKey();
        groupMessage.setMessageKey(senderMsgKey);
        groupMessage.setChannel(channel);
        messagesInChannel(channel).child(senderMsgKey).setValue(groupMessage);
        notificationRef.push().setValue(groupMessage);
    }

    @Override
    public Observable<GroupMessage> observeAddMessage(Channel channel) {
        return firebaseObservableListeners.listenToAddChildEvents(messagesOfUser(channel), toMessage());
    }

    @Override
    public Observable<GroupMessage> observeLastMessage(Channel channel) {
        return firebaseObservableListeners.listenToSingleValueEvents(messagesInChannel(channel).limitToLast(2), toLastMessage());
    }

    @Override
    public Observable<Boolean> removeChat(Channel channel, String key) {
        return null;
    }

    @Override
    public Observable<Boolean> observeTyping(Channel channel, String userId) {
        return null;
    }

    @Override
    public void setTyping(Channel channel, String userId, Boolean value) {

    }

    @Override
    public boolean exitGroup(Channel channel, String userId, List<String> ownerList) {
        return false;
    }

    @Override
    public Observable<GroupChat> observeAllGroupDocuments(Channel channel) {
        DatabaseReference databaseReference = messagesInChannel(channel);
        return firebaseObservableListeners.listenToValueEvents(databaseReference.orderByChild("msgType").equalTo(MimeTypeEnum.DOCUMENT.getValue()).limitToLast(DEFAULT_LIMIT), toChat());
    }

    @Override
    public Observable<GroupChat> observeAllGroupImages(Channel channel) {
        DatabaseReference databaseReference = messagesInChannel(channel);
        return firebaseObservableListeners.listenToValueEvents(databaseReference.orderByChild("msgType").equalTo(MimeTypeEnum.IMAGE.getValue()).limitToLast(DEFAULT_LIMIT), toChat());
    }

    private DatabaseReference messagesInChannel(Channel channel) {
        return messagesDB.child(channel.getUid());
    }


    private DatabaseReference usersInChannel(Channel channel) {
        return ownersDB.child(channel.getUid());
    }

    private Func1<DataSnapshot, GroupChat> toChat() {
        return dataSnapshot -> {
            Iterable<DataSnapshot> children = dataSnapshot.getChildren();
            List<GroupMessage> groupMessages = new ArrayList<>();
            for (DataSnapshot child : children) {
                GroupMessage groupMessage = child.getValue(GroupMessage.class);
                groupMessages.add(groupMessage);
            }
            return new GroupChat(groupMessages);
        };
    }

    private Func1<DataSnapshot, GroupMessage> toMessage() {
        return new Func1<DataSnapshot, GroupMessage>() {
            @Override
            public GroupMessage call(DataSnapshot dataSnapshot) {
                return dataSnapshot.getValue(GroupMessage.class);
            }
        };
    }

    private Func1<DataSnapshot, GroupMessage> toLastMessage() {
        return new Func1<DataSnapshot, GroupMessage>() {
            @Override
            public GroupMessage call(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                List<GroupMessage> groupMessages = new ArrayList<>();
                for (DataSnapshot child : children) {
                    GroupMessage groupMessage = child.getValue(GroupMessage.class);
                    groupMessages.add(groupMessage);
                }
                return groupMessages.get(groupMessages.size() - 1);
            }
        };
    }

    private static Func1<DataSnapshot, List<String>> getKeys() {
        return new Func1<DataSnapshot, List<String>>() {
            @Override
            public List<String> call(DataSnapshot dataSnapshot) {
                List<String> keys = new ArrayList<>();
                if (dataSnapshot.hasChildren()) {
                    Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                    for (DataSnapshot child : children) {
                        keys.add(child.getKey());
                    }
                }
                return keys;
            }
        };
    }

    private static Func1<DataSnapshot, List<String>> getUserNames() {
        return new Func1<DataSnapshot, List<String>>() {
            @Override
            public List<String> call(DataSnapshot dataSnapshot) {
                List<String> values = new ArrayList<>();
                if (dataSnapshot.hasChildren()) {
                    Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                    for (DataSnapshot child : children) {
                        values.add((String) child.getValue());
                    }
                }
                return values;
            }
        };
    }
}
