package com.superdev.smiling.groupconversation.data_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;
import com.superdev.smiling.MimeTypeEnum;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.AudioModel;
import com.superdev.smiling.conversation.data_model.ContactModel;
import com.superdev.smiling.conversation.data_model.DocModel;
import com.superdev.smiling.conversation.data_model.FileModel;
import com.superdev.smiling.conversation.data_model.MapModel;
import com.superdev.smiling.conversation.data_model.VideoModel;
import com.superdev.smiling.conversation.data_model.VoiceModel;
import com.superdev.smiling.user.data_model.User;

import java.util.UUID;

/**
 * Created by gautam on 10/6/17.
 */

public class GroupMessage implements Parcelable {

    private String messageId;

    private String message;

    private String timestamp;

    private String msgType;

    private String messageKey;

    private Channel channel;

    @Exclude
    private String self;

    private FileModel fileModel;

    private MapModel mapModel;

    public DocModel docModel;

    public AudioModel audioModel;

    private VoiceModel voiceModel;

    private VideoModel videoModel;

    public ContactModel contactModel;

    private String messageStatus;

    private User author;

    public GroupMessage() {

    }


    public GroupMessage(User author, Channel channel, String message, String msgType, String messageStatus) {
        this.author = author;
        this.channel = channel;
        this.messageId = UUID.randomUUID().toString();
        this.message = message;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.messageStatus = messageStatus;
    }

    public GroupMessage(User author, Channel channel, String message, FileModel fileModel, String msgType, String messageStatus) {
        this.author = author;
        this.channel = channel;
        this.messageId = UUID.randomUUID().toString();
        this.fileModel = fileModel;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.message = message;
        this.messageStatus = messageStatus;
    }

    public GroupMessage(User author, Channel channel, String message, MapModel mapModel, String msgType, String messageStatus) {
        this.author = author;
        this.channel = channel;
        this.messageId = UUID.randomUUID().toString();
        this.mapModel = mapModel;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.message = message;
        this.messageStatus = messageStatus;
    }

    public GroupMessage(User author, Channel channel, String message, DocModel docModel, String msgType, String messageStatus) {
        this.author = author;
        this.channel = channel;
        this.messageId = UUID.randomUUID().toString();
        this.docModel = docModel;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.message = message;
        this.messageStatus = messageStatus;
    }

    public GroupMessage(User author, Channel channel, String message, AudioModel audioModel, String msgType, String messageStatus) {
        this.author = author;
        this.channel = channel;
        this.messageId = UUID.randomUUID().toString();
        this.audioModel = audioModel;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.message = message;
        this.messageStatus = messageStatus;
    }

    public GroupMessage(User author, Channel channel, String message, VoiceModel voiceModel, String msgType, String messageStatus) {
        this.author = author;
        this.channel = channel;
        this.messageId = UUID.randomUUID().toString();
        this.voiceModel = voiceModel;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.message = message;
        this.messageStatus = messageStatus;
    }

    public GroupMessage(User author, Channel channel, String message, VideoModel videoModel, String msgType, String messageStatus) {
        this.author = author;
        this.channel = channel;
        this.messageId = UUID.randomUUID().toString();
        this.videoModel = videoModel;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.message = message;
        this.messageStatus = messageStatus;
    }

    public GroupMessage(User author, Channel channel, String message, ContactModel contactModel, String msgType, String messageStatus) {
        this.author = author;
        this.channel = channel;
        this.messageId = UUID.randomUUID().toString();
        this.contactModel = contactModel;
        this.timestamp = Utils.getCurrentTimestamp();
        this.msgType = msgType;
        this.message = message;
        this.messageStatus = messageStatus;
    }

    protected GroupMessage(Parcel in) {
        messageId = in.readString();
        message = in.readString();
        timestamp = in.readString();
        msgType = in.readString();
        messageKey = in.readString();
        channel = in.readParcelable(Channel.class.getClassLoader());
        self = in.readString();
        fileModel = in.readParcelable(FileModel.class.getClassLoader());
        mapModel = in.readParcelable(MapModel.class.getClassLoader());
        docModel = in.readParcelable(DocModel.class.getClassLoader());
        audioModel = in.readParcelable(AudioModel.class.getClassLoader());
        voiceModel = in.readParcelable(VoiceModel.class.getClassLoader());
        videoModel = in.readParcelable(VideoModel.class.getClassLoader());
        contactModel = in.readParcelable(ContactModel.class.getClassLoader());
        //messageStatus = in.readString();
        author = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<GroupMessage> CREATOR = new Creator<GroupMessage>() {
        @Override
        public GroupMessage createFromParcel(Parcel in) {
            return new GroupMessage(in);
        }

        @Override
        public GroupMessage[] newArray(int size) {
            return new GroupMessage[size];
        }
    };

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public FileModel getFileModel() {
        return fileModel;
    }

    public void setFileModel(FileModel fileModel) {
        this.fileModel = fileModel;
    }

    public MapModel getMapModel() {
        return mapModel;
    }

    public void setMapModel(MapModel mapModel) {
        this.mapModel = mapModel;
    }

    @Exclude
    public String getSelf() {
        return self;
    }

    @Exclude
    public void setSelf(String self) {
        this.self = self;
    }

    public String getMessage() {
        return message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public DocModel getDocModel() {
        return docModel;
    }

    public void setDocModel(DocModel docModel) {
        this.docModel = docModel;
    }

    public VideoModel getVideoModel() {
        return videoModel;
    }

    public void setVideoModel(VideoModel videoModel) {
        this.videoModel = videoModel;
    }

    public ContactModel getContactModel() {
        return contactModel;
    }

    public void setContactModel(ContactModel contactModel) {
        this.contactModel = contactModel;
    }

    public String getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GroupMessage groupMessage = (GroupMessage) o;

        return this.message != null && this.message.equals(groupMessage.message)
                && timestamp != null && timestamp.equals(groupMessage.timestamp);
    }

    @Exclude
    public boolean isMmsType() {
        return msgType.equalsIgnoreCase(MimeTypeEnum.IMAGE.getValue());
    }

    @Exclude
    public boolean isLocationType() {
        return msgType.equals(MimeTypeEnum.LOCATION.getValue());
    }

    @Exclude
    public boolean isVideoType() {
        return msgType.equals(MimeTypeEnum.VIDEO.getValue());
    }


    @Exclude
    public boolean isAudioType() {
        return msgType.equals(MimeTypeEnum.AUDIO.getValue());
    }

    @Exclude
    public boolean isVoiceType() {
        return msgType.equals(MimeTypeEnum.VOICE.getValue());
    }

    @Exclude
    public boolean isContactType() {
        return msgType.equals(MimeTypeEnum.CONTACT.getValue());
    }

    @Exclude
    public boolean isDocumentType() {
        return msgType.equals(MimeTypeEnum.DOCUMENT.getValue());
    }

    public AudioModel getAudioModel() {
        return audioModel;
    }

    public void setAudioModel(AudioModel audioModel) {
        this.audioModel = audioModel;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public VoiceModel getVoiceModel() {
        return voiceModel;
    }

    public void setVoiceModel(VoiceModel voiceModel) {
        this.voiceModel = voiceModel;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(messageId);
        parcel.writeString(message);
        parcel.writeString(timestamp);
        parcel.writeString(msgType);
        parcel.writeString(messageKey);
        parcel.writeParcelable(channel, i);
        parcel.writeString(self);
        parcel.writeParcelable(fileModel, i);
        parcel.writeParcelable(mapModel, i);
        parcel.writeParcelable(docModel, i);
        parcel.writeParcelable(audioModel, i);
        parcel.writeParcelable(voiceModel, i);
        parcel.writeParcelable(videoModel, i);
        parcel.writeParcelable(contactModel, i);
        //parcel.writeString(messageStatus);
        parcel.writeParcelable(author, i);
    }
}
