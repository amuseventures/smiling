package com.superdev.smiling.groupconversation.database;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.Chat;
import com.superdev.smiling.groupconversation.data_model.GroupChat;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;

import java.util.List;

import rx.Observable;


/**
 * Created by gautam on 10/3/17.
 */

public interface GroupConversationDatabase {

    Observable<GroupChat> observeChat(Channel channel);

    Observable<GroupChat> observeMedia(Channel channel);

    Observable<List<String>> getUserKeyFromChannel(Channel channel);

    Observable<List<String>> getUserIdsFromChannel(Channel channel);

    void sendMessage(Channel channel, GroupMessage groupMessage);

    Observable<GroupMessage> observeAddMessage(Channel channel);

    Observable<GroupMessage> observeLastMessage(Channel channel);

    Observable<Boolean> removeChat(Channel channel, String key);

    Observable<Boolean> observeTyping(Channel channel, String userId);

    void setTyping(Channel channel, String userId, Boolean value);

    boolean exitGroup(Channel channel, String userId, List<String> ownerList);

    Observable<GroupChat> observeAllGroupDocuments(Channel channel);

    Observable<GroupChat> observeAllGroupImages(Channel channel);
}
