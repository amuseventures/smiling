package com.superdev.smiling.groupconversation.service;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.groupconversation.data_model.GroupChat;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.user.data_model.User;

import java.util.List;

import rx.Observable;


/**
 * Created by gautam on 10/3/17.
 */

public interface GroupConversationService {

    Observable<DatabaseResult<GroupChat>> getChat(Channel channel);

    Observable<DatabaseResult<GroupChat>> getGroupMedia(Channel channel);

    Observable<DatabaseResult<GroupChat>> getAllGroupImageMedia(Channel channel);

    Observable<DatabaseResult<GroupChat>> getAllGroupDocument(Channel channel);


    Observable<List<String>> getUserIds(Channel channel);

    void sendMessage(Channel channel, GroupMessage groupMessage);

    Observable<Boolean> removeChat(Channel channel, String key);

    Observable<Boolean> getTyping(Channel channel, String userId);

    void setTyping(Channel channel, String userId, Boolean value);

    Observable<DatabaseResult<GroupMessage>> getLastMessageFor(Channel channel);

    Observable<List<String>> getUserKey(Channel channel);

    boolean exitGroup(Channel channel, String userId, List<String> ownerList);

}
