package com.superdev.smiling.groupconversation.view;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.groupconversation.data_model.GroupChat;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.user.data_model.User;

import java.io.File;
import java.util.List;

/**
 * Created by gautam on 10/3/17.
 */

public interface GroupConversationDisplayer {

    void display(Channel channel, GroupChat chat, User user);

    void addToDisplay(Channel channel, GroupMessage groupMessage, User user);

    void setupToolbar(String channelName, String image, String membersName);

    void showTyping();

    void hideTyping();

    void attach(GroupConversationDisplayer.ConversationActionListener conversationInteractionListener);

    void detach(GroupConversationDisplayer.ConversationActionListener conversationInteractionListener);

    void enableInteraction();

    void disableInteraction();

    void clearSelectedItem();

    void setDestination(String destination);

    void onMessageDeleted(GroupMessage groupMessage);

    void inflateMenu(MenuInflater inflater, Menu menu);

    interface ConversationActionListener {

        void onUpPressed();

        void onManageOwnersClicked();

        void onMessageLengthChanged(int messageLength);

        void onSubmitMessage(String message);

        void onSubmitVoice(File audioFile);

        void onDocumentsPressed();

        void onCameraPressed();

        void onGalleryPressed();

        void onAudioPressed();

        void onLocationPressed();

        void onContactPressed();

        void onMessageSelected(View view, GroupMessage groupMessage);

        void onImageSelected(String self, View view, GroupMessage groupMessage);

        void onVideoSelected(View view, GroupMessage groupMessage);

        void onMapViewSelected(GroupMessage groupMessage);

        void onDocViewSelected(GroupMessage groupMessage);

        void showCabMenu(List<GroupMessage> groupMessageList);

        void hideCabMenu();

        void onContactAddBtn(GroupMessage groupMessage);

        void onContactMsgSelected(GroupMessage groupMessage);

        void showAttachment();
    }
}
