package com.superdev.smiling.conversation_list;

import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.firebase.storage.StorageReference;
import com.sinch.android.rtc.calling.Call;
import com.superdev.smiling.BuildConfig;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.Utils;
import com.superdev.smiling.conversation.ConversationActivity;
import com.superdev.smiling.conversation_list.view.FloatingChildLayout;
import com.superdev.smiling.conversation_list.view.SchedulingUtils;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.link.FirebaseDynamicLinkFactory;
import com.superdev.smiling.link.LinkFactory;
import com.superdev.smiling.profile.ProfileActivity;
import com.superdev.smiling.profile.ProfileImageViewActivity;
import com.superdev.smiling.recentcall.data_model.RecentCall;
import com.superdev.smiling.sinch.CallScreenActivity;
import com.superdev.smiling.sinch.SinchBaseActivity;
import com.superdev.smiling.sinch.SinchService;
import com.superdev.smiling.sinch.VideoCallerScreenActivity;
import com.superdev.smiling.storage.FirebaseImageLoader;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.service.UserService;

import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import jp.wasabeef.fresco.processors.BlurPostprocessor;
import rx.Observer;
import rx.Subscription;
import rx.functions.Action1;

import static com.superdev.smiling.Constants.AUDIO_TYPE;
import static com.superdev.smiling.Constants.DESTINATION_ID;
import static com.superdev.smiling.Constants.SENDER_ID;
import static com.superdev.smiling.Constants.TYPE_OUT;
import static com.superdev.smiling.Constants.VIDEO_TYPE;

public class QuickContactActivity extends SinchBaseActivity {

    @BindView(R.id.picture)
    ImageView picture;

    @BindView(R.id.name)
    EmojiconTextView name;

    @BindView(R.id.invite_btn)
    TextView inviteBtn;

    @BindView(R.id.message_btn)
    ImageButton messageBtn;

    @BindView(R.id.audio_call_btn)
    ImageButton audioCallBtn;

    @BindView(R.id.video_call_btn)
    ImageButton videoCallBtn;

    @BindView(R.id.info_btn)
    ImageButton infoBtn;

    @BindView(R.id.buttons)
    LinearLayout buttons;

    @BindView(R.id.floating_layout)
    FloatingChildLayout floatingLayout;

    private Subscription userSubscription;

    private String self, destination;
    private LinkFactory linkFactory;
    private User userContact;
    private static final int POST_DRAW_WAIT_DURATION = 60;
    String userName, userImage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quick_contact);
        ButterKnife.bind(this);

        linkFactory = new FirebaseDynamicLinkFactory(
                getResources().getString(R.string.dynamic_link_url),
                getResources().getString(R.string.deepLinkBaseUrl),
                BuildConfig.APPLICATION_ID
        );
        self = getIntent().getStringExtra(Constants.SENDER_ID);
        destination = getIntent().getStringExtra(Constants.DESTINATION_ID);
        UserService userService = Dependencies.INSTANCE.getUserService();
        User userData = null;

        if (getIntent().hasExtra(Constants.FIREBASE_USERS)) {
            userData = getIntent().getParcelableExtra(Constants.FIREBASE_USERS);
        }

        if (getIntent().hasExtra(Constants.FIREBASE_USERS_NAME)) {
            userName = getIntent().getStringExtra(Constants.FIREBASE_USERS_NAME);
            if (!TextUtils.isEmpty(userName)) {
                name.setText(userName);
            }
        }

        if (getIntent().hasExtra(Constants.FIREBASE_USERS_IMAGE)) {
            userImage = getIntent().getStringExtra(Constants.FIREBASE_USERS_IMAGE);
            loadImage(userImage);
        }

        if (self != null && destination != null) {
            userSubscription = userService.getUser(destination)
                    .subscribe(new Action1<DatabaseResult<User>>() {
                        @Override
                        public void call(DatabaseResult<User> userDatabaseResult) {
                            if (userDatabaseResult.isSuccess()) {
                                userContact = userDatabaseResult.getData();
                                name.setText(userDatabaseResult.getData().getName());
                                loadImage(userDatabaseResult.getData().getImage());
                                if (userDatabaseResult.getData().getRegister().equalsIgnoreCase("0")) {
                                    inviteBtn.setVisibility(View.VISIBLE);
                                    buttons.setVisibility(View.GONE);
                                } else {
                                    inviteBtn.setVisibility(View.GONE);
                                    buttons.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    });
        } else if (userData != null) {
            name.setText(userData.getName() != null ? userData.getName() : userData.getPhone());
            if (userData.getRegister() == null || userData.getRegister().equalsIgnoreCase("0")) {
                inviteBtn.setVisibility(View.VISIBLE);
                buttons.setVisibility(View.GONE);
            } else {
                inviteBtn.setVisibility(View.GONE);
                buttons.setVisibility(View.VISIBLE);
            }
            loadImage(userData.getImage());
        }

        floatingLayout.setOnOutsideTouchListener((v, event) -> {
            handleOutsideTouch();
            return true;
        });

        SchedulingUtils.doAfterLayout(floatingLayout, new Runnable() {
            @Override
            public void run() {
                floatingLayout.fadeInBackground();
            }
        });

        SchedulingUtils.doAfterLayout(floatingLayout, new Runnable() {
            @Override
            public void run() {
                floatingLayout.showContent(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
            }
        });
    }

    private void handleOutsideTouch() {
        if (floatingLayout.isContentFullyVisible()) {
            close(true);
        }
    }

    private void close(boolean withAnimation) {

        if (withAnimation) {
            floatingLayout.fadeOutBackground();
            final boolean animated = floatingLayout.hideContent(new Runnable() {
                @Override
                public void run() {
                    SchedulingUtils.doAfterDraw(floatingLayout, new Runnable() {
                        @Override
                        public void run() {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            }, POST_DRAW_WAIT_DURATION);
                        }
                    });
                }
            });
            if (!animated) {
                finish();
            }
        } else {
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (userSubscription != null) {
            userSubscription.unsubscribe();
        }
    }

    @Override
    public void onBackPressed() {
        close(true);
    }

    @OnClick({R.id.invite_btn, R.id.message_btn, R.id.audio_call_btn, R.id.video_call_btn, R.id.info_btn})
    public void onViewClicked(View view) {
        RecentCall recentCallEntity = new RecentCall();
        switch (view.getId()) {
            case R.id.invite_btn:
                String sharingMessage = String.format(Locale.getDefault(), getString(R.string.tell_a_friend_sms), linkFactory.inviteLinkFrom(self).toString());
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, sharingMessage);
                startActivity(Intent.createChooser(sharingIntent, sharingMessage));
                break;
            case R.id.message_btn:
                Bundle bundle = new Bundle();
                bundle.putString(SENDER_ID, self);
                bundle.putString(DESTINATION_ID, destination);
                Intent msgInent = new Intent(QuickContactActivity.this, ConversationActivity.class);
                msgInent.putExtras(bundle);
                startActivity(msgInent);
                break;
            case R.id.audio_call_btn:
                recentCallEntity.setCallTime(new Date().getTime());
                recentCallEntity.setCallType(AUDIO_TYPE);
                recentCallEntity.setType(TYPE_OUT);
                recentCallEntity.setUserContactEntity(userContact);
                recentCallEntity.setTotalDuration(0);

                Call call = getSinchServiceInterface().callUser(userContact.getPhone());
                if (call == null) {
                    getSinchServiceInterface().startClient(Remember.getString(Constants.FIREBASE_USERS_PHONE, ""));
                    return;
                }
                String callId = call.getCallId();
                recentCallEntity.setCallId(callId);
                Intent callScreen = new Intent(QuickContactActivity.this, CallScreenActivity.class);
                callScreen.putExtra(SinchService.CALL_ID, callId);
                callScreen.putExtra(SinchService.CONTACT_INFO, userContact);
                callScreen.putExtra(SinchService.RECENT_CALLS, recentCallEntity);
                startActivity(callScreen);
                break;
            case R.id.video_call_btn:
                recentCallEntity.setCallTime(new Date().getTime());
                recentCallEntity.setCallType(VIDEO_TYPE);
                recentCallEntity.setType(TYPE_OUT);
                recentCallEntity.setUserContactEntity(userContact);
                recentCallEntity.setTotalDuration(0);

                Call videoCall = getSinchServiceInterface().callUserVideo(userContact.getPhone());
                if (videoCall == null) {
                    getSinchServiceInterface().startClient(Remember.getString(Constants.FIREBASE_USERS_PHONE, ""));
                    return;
                }
                String videoCallId = videoCall.getCallId();
                recentCallEntity.setCallId(videoCallId);
                Intent videoCallScreen = new Intent(QuickContactActivity.this, VideoCallerScreenActivity.class);
                videoCallScreen.putExtra(SinchService.CALL_ID, videoCallId);
                videoCallScreen.putExtra(SinchService.CONTACT_INFO, userContact);
                videoCallScreen.putExtra(SinchService.RECENT_CALLS, recentCallEntity);
                startActivity(videoCallScreen);
                break;
            case R.id.info_btn:
                Intent intent = new Intent(this, ProfileActivity.class);
                intent.putExtra(Constants.DESTINATION_ID, destination);
                intent.putExtra(Constants.SENDER_ID, self);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onServiceConnected() {

    }

    @OnClick(R.id.picture)
    public void onPictureViewClicked(View view) {
        if (userContact != null && userContact.getImage() != null) {
            Intent intent = new Intent(this, ProfileImageViewActivity.class);
            intent.putExtra(Constants.FIREBASE_USERS_NAME, userContact.getName());
            intent.putExtra(Constants.FIREBASE_USERS_IMAGE, userContact.getImage());

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                startActivity(intent);
            } else {
                ActivityOptions options = ActivityOptions
                        .makeSceneTransitionAnimation(this, picture, getString(R.string.transition_photo));
                // start the new activity
                startActivity(intent, options.toBundle());
            }
            //close(true);
        }
    }

    private void loadImage(String userImage) {
        try {
            if (userImage != null && userImage.length() > 0) {
                StorageReference ref = Dependencies.INSTANCE.getStorageService().getProfileImageReference(userImage);
                Glide.with(QuickContactActivity.this)
                        .using(new FirebaseImageLoader())
                        .load(ref)
                        .asBitmap()
                        .placeholder(R.drawable.avatar_contact_large)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                picture.setImageBitmap(resource);
                            }
                        });
            } else {
                Glide.with(QuickContactActivity.this)
                        .load(R.drawable.avatar_contact_large)
                        .into(picture);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
