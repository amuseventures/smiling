package com.superdev.smiling.conversation_list.database;

import com.superdev.smiling.user.data_model.User;

import java.util.List;

import rx.Observable;

/**
 * Created by marco on 29/07/16.
 */

public interface ConversationListDatabase {

    Observable<List<String>> observeConversationsFor(User user);

    Observable<Boolean> removeConversation(User user);

    //void deleteConversation(String sender);

}
