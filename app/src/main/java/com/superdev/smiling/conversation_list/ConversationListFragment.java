package com.superdev.smiling.conversation_list;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.SinchBaseFragment;
import com.superdev.smiling.conversation_list.data_model.Conversation;
import com.superdev.smiling.conversation_list.presenter.ConversationListPresenter;
import com.superdev.smiling.navigation.AndroidConversationsNavigator;
import com.superdev.smiling.navigation.AndroidNavigator;

/**
 * Created by marco on 29/07/16.
 */

public class ConversationListFragment extends SinchBaseFragment {

    private ConversationListPresenter presenter;
    private AndroidConversationsNavigator navigator;

    public static ConversationListFragment newInstance() {
        Bundle args = new Bundle();
        ConversationListFragment fragment = new ConversationListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_conversations, container, false);

        navigator = new AndroidConversationsNavigator((AppCompatActivity) getActivity(), new AndroidNavigator(getActivity()));
        presenter = new ConversationListPresenter(
                rootView.findViewById(R.id.conversationsView),
                Dependencies.INSTANCE.getConversationListService(),
                navigator,
                Dependencies.INSTANCE.getPhoneLoginService(),
                Dependencies.INSTANCE.getUserService()
        );

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(Constants.INTENT_ACTION_REFRESH_LIST);
        getActivity().registerReceiver(updateReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(getActivity() == null)
            return;
        try {
            getActivity().unregisterReceiver(updateReceiver);
        } catch (IllegalArgumentException ignored) {
        }
        presenter.stopPresenting();
    }


    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
    }

    @Override
    protected void onServiceDisconnected() {
        super.onServiceDisconnected();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    BroadcastReceiver updateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null && intent.getAction().equalsIgnoreCase(Constants.INTENT_ACTION_REFRESH_LIST)) {
                Conversation conversation = intent.getParcelableExtra(Constants.CONVERSATION_EXTRA);
                presenter.removeData(conversation);
            }
        }
    };
}
