package com.superdev.smiling.conversation_list.presenter;

import android.os.Bundle;

import com.superdev.smiling.Constants;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.conversation_list.data_model.Conversation;
import com.superdev.smiling.conversation_list.service.ConversationListService;
import com.superdev.smiling.conversation_list.view.ConversationListDisplayer;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.navigation.AndroidConversationsNavigator;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.service.UserService;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by marco on 29/07/16.
 */

public class ConversationListPresenter {

    private static final String SENDER = "sender";
    private static final String DESTINATION = "destination";

    private ConversationListDisplayer conversationListDisplayer;
    private ConversationListService conversationListService;
    private AndroidConversationsNavigator navigator;
    private PhoneLoginService loginService;
    private UserService userService;

    private Subscription loginSubscription;
    private Subscription userSubscription;
    private Subscription messageSubscription;

    private List<String> uids;
    private User self;

    public ConversationListPresenter(
            ConversationListDisplayer conversationListDisplayer,
            ConversationListService conversationListService,
            AndroidConversationsNavigator navigator,
            PhoneLoginService loginService,
            UserService userService) {
        this.conversationListDisplayer = conversationListDisplayer;
        this.conversationListService = conversationListService;
        this.navigator = navigator;
        this.loginService = loginService;
        this.userService = userService;
    }

    public void startPresenting() {
        conversationListDisplayer.attach(conversationInteractionListener);

        final Subscriber userSubscriber = new Subscriber<DatabaseResult<User>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onNext(DatabaseResult<User> userDatabaseResult) {
                if (!userDatabaseResult.isSuccess())
                    return;
                User user = userDatabaseResult.getData();
                messageSubscription = conversationListService.getLastMessageFor(self, user)
                        .subscribe(new Observer<DatabaseResult<Message>>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {

                            }

                            @Override
                            public void onNext(DatabaseResult<Message> messageDatabaseResult) {
                                if (messageDatabaseResult.isSuccess()) {
                                    Message message = messageDatabaseResult.getData();
                                    conversationListDisplayer.addToDisplay(
                                            new Conversation(user.getUid(), user.getName(), user.getImage(), message.getMessage(), message.getTimestamp(), message.getMsgType(),
                                                    message.getFileModel(), message.getMapModel(), message.getVideoModel(), message.getAudioModel(), message.getContactModel()));
                                }
                            }
                        });
            }
        };

        Subscriber usersSubscriber = new Subscriber<List<String>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(List<String> strings) {
                uids = new ArrayList<>(strings);
                for (String uid : uids) {
                    userSubscription = userService.getUser(uid)
                            .subscribe(userSubscriber);
                }
            }

        };

        loginSubscription = loginService.getAuthentication()
                .filter(successfullyAuthenticated())
                .doOnNext(new Action1<Authentication>() {
                    @Override
                    public void call(Authentication authentication) {
                        self = authentication.getUser();
                    }
                })
                .flatMap(conversationsForUser())
                .subscribe(usersSubscriber);

    }

    public void removeData(Conversation conversation) {
        conversationListDisplayer.removeData(conversation);
    }

    public void stopPresenting() {
        conversationListDisplayer.detach(conversationInteractionListener);
        loginSubscription.unsubscribe();
        if (userSubscription != null)
            userSubscription.unsubscribe();
        if (messageSubscription != null)
            messageSubscription.unsubscribe();
    }

    private Func1<Authentication, Observable<List<String>>> conversationsForUser() {
        return authentication -> conversationListService.getConversationsFor(self);
    }

    private Func1<Authentication, Boolean> successfullyAuthenticated() {
        return new Func1<Authentication, Boolean>() {
            @Override
            public Boolean call(Authentication authentication) {
                return authentication.isSuccess();
            }
        };
    }

    private final ConversationListDisplayer.ConversationInteractionListener conversationInteractionListener = new ConversationListDisplayer.ConversationInteractionListener() {

        @Override
        public void onConversationSelected(Conversation conversation) {
            Bundle bundle = new Bundle();
            bundle.putString(SENDER, self.getUid());
            bundle.putString(DESTINATION, conversation.getUid());
            navigator.toSelectedConversation(bundle);
        }

        @Override
        public void onUserImageSelected(Conversation conversation) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.FIREBASE_USERS_NAME, conversation.getName());
            bundle.putString(Constants.FIREBASE_USERS_IMAGE, conversation.getImage());
            bundle.putString(SENDER, self.getUid());
            bundle.putString(DESTINATION, conversation.getUid());
            navigator.openQuickContact(bundle);
        }

        @Override
        public void onConversationLongPressed(Conversation conversation) {
            navigator.openDeleteConversationDialog(self.getUid(), conversation);
        }

        @Override
        public void onFloatButtonClick() {
            navigator.toUserPickerForMessage();
        }

    };

    public void setSelf(User self) {
        this.self = self;
    }

}
