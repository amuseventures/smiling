package com.superdev.smiling.conversation_list.view;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.superdev.smiling.R;
import com.superdev.smiling.conversation_list.data_model.Conversation;
import com.superdev.smiling.conversation_list.data_model.Conversations;
import com.superdev.smiling.views.EmptyRecyclerView;
import com.superdev.smiling.views.TextViewWithImages;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by marco on 29/07/16.
 */

public class ConversationListView extends LinearLayout implements ConversationListDisplayer {

    private final ConversationListAdapter conversationListAdapter;

    @BindView(R.id.conversationsRecyclerView)
    EmptyRecyclerView conversations;

    @BindView(R.id.welcome_chats_message)
    TextViewWithImages view;

    @BindView(R.id.btn_float)
    FloatingActionButton btnFloat;

    private Toolbar toolbar;

    private ConversationInteractionListener conversationInteractionListener;

    public ConversationListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
        conversationListAdapter = new ConversationListAdapter(LayoutInflater.from(context));
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_conversation_list_view, this);
        ButterKnife.bind(this);

        view.setText(R.string.welcome_chats_message);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        conversations.setLayoutManager(layoutManager);
        conversations.setAdapter(conversationListAdapter);
        conversations.setEmptyView(view);
        btnFloat.setImageResource(R.drawable.ic_new_message_tip);
    }

    @Override
    public void display(Conversations conversations) {
        conversationListAdapter.update(conversations);
    }

    @Override
    public void addToDisplay(Conversation conversation) {
        conversationListAdapter.add(conversation);
    }

    @Override
    public void removeData(Conversation conversation) {
        conversationListAdapter.removeData(conversation);
    }

    @Override
    public void attach(ConversationInteractionListener conversationInteractionListener) {
        this.conversationInteractionListener = conversationInteractionListener;
        conversationListAdapter.attach(conversationInteractionListener);
    }

    @Override
    public void detach(ConversationInteractionListener conversationInteractionListener) {
        conversationListAdapter.detach(conversationInteractionListener);
        this.conversationInteractionListener = null;
    }

    @OnClick(R.id.btn_float)
    public void onViewClicked() {
     conversationInteractionListener.onFloatButtonClick();
    }
}