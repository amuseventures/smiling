package com.superdev.smiling.conversation_list.view;

import android.view.View;
import android.view.ViewTreeObserver;

public class SchedulingUtils {


    /** Runs a piece of code after the next layout run */
    public static void doAfterLayout(final View view, final Runnable runnable) {
        final ViewTreeObserver.OnGlobalLayoutListener listener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                // Layout pass done, unregister for further events
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                runnable.run();
            }
        };
        view.getViewTreeObserver().addOnGlobalLayoutListener(listener);
    }

    /** Runs a piece of code just before the next draw. */
    public static void doAfterDraw(final View view, final Runnable runnable) {
        final ViewTreeObserver.OnDrawListener listener = new ViewTreeObserver.OnDrawListener() {
            @Override
            public void onDraw() {
                view.getViewTreeObserver().removeOnDrawListener(this);
                runnable.run();
            }
        };
        view.getViewTreeObserver().addOnDrawListener(listener);
    }
}