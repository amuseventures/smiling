package com.superdev.smiling.conversation_list.data_model;

import android.os.Parcel;
import android.os.ParcelFormatException;
import android.os.Parcelable;

import com.superdev.smiling.conversation.data_model.AudioModel;
import com.superdev.smiling.conversation.data_model.ContactModel;
import com.superdev.smiling.conversation.data_model.FileModel;
import com.superdev.smiling.conversation.data_model.MapModel;
import com.superdev.smiling.conversation.data_model.VideoModel;

/**
 * Created by marco on 04/07/16.
 */

public class Conversation implements Parcelable {

    private String uid;
    private String name;
    private String image;
    private String message;
    private String time;
    private String msgType;
    private FileModel fileModel;
    private MapModel mapModel;
    private VideoModel videoModel;
    private AudioModel audioModel;
    private ContactModel contactModel;

    public Conversation() {
    }

    public Conversation(String uid, String name, String image, String message, String time, String msgType, FileModel fileModel, MapModel mapModel, VideoModel videoModel, AudioModel audioModel, ContactModel contactModel) {
        this.uid = uid;
        this.name = name;
        this.image = image;
        this.message = message;
        this.time = time;
        this.msgType = msgType;
        this.fileModel = fileModel;
        this.mapModel = mapModel;
        this.videoModel = videoModel;
        this.audioModel = audioModel;
        this.contactModel = contactModel;
    }

    public Conversation(Parcel in) {
        this.uid = in.readString();
        this.name = in.readString();
        this.image = in.readString();
        this.message = in.readString();
        this.time = in.readString();
        this.msgType = in.readString();
        this.fileModel = in.readParcelable(FileModel.class.getClassLoader());
        this.mapModel = in.readParcelable(MapModel.class.getClassLoader());
        this.videoModel = in.readParcelable(MapModel.class.getClassLoader());
        this.contactModel = in.readParcelable(MapModel.class.getClassLoader());
        this.audioModel = in.readParcelable(MapModel.class.getClassLoader());
    }

    public String getName() {
        return name;
    }

    public String getUid() {
        return uid;
    }

    public String getMessage() {
        return message;
    }

    public String getTime() {
        return time;
    }

    public String getImage() {
        return image;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public MapModel getMapModel() {
        return mapModel;
    }

    public FileModel getFileModel() {
        return fileModel;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setMapModel(MapModel mapModel) {
        this.mapModel = mapModel;
    }

    public void setFileModel(FileModel fileModel) {
        this.fileModel = fileModel;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public ContactModel getContactModel() {
        return contactModel;
    }

    public void setContactModel(ContactModel contactModel) {
        this.contactModel = contactModel;
    }

    public VideoModel getVideoModel() {
        return videoModel;
    }

    public AudioModel getAudioModel() {
        return audioModel;
    }

    public void setVideoModel(VideoModel videoModel) {
        this.videoModel = videoModel;
    }

    public void setAudioModel(AudioModel audioModel) {
        this.audioModel = audioModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Conversation conversation = (Conversation) o;

        return uid != null && uid.equals(conversation.uid);
    }

    @Override
    public int hashCode() {
        int result = uid != null ? uid.hashCode() : 0;
        result = 31 * result;
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        try {
            parcel.writeString(this.uid);
            parcel.writeString(this.name);
            parcel.writeString(this.image);
            parcel.writeString(this.message);
            parcel.writeString(this.time);
            parcel.writeString(this.msgType);
            parcel.writeParcelable(fileModel, i);
            parcel.writeParcelable(mapModel, i);
            parcel.writeParcelable(videoModel, i);
            parcel.writeParcelable(contactModel, i);
            parcel.writeParcelable(audioModel, i);
        } catch (ParcelFormatException e) {
            e.printStackTrace();
        }
    }

    public static final Creator CREATOR = new Creator() {
        public Conversation createFromParcel(Parcel in) {
            return new Conversation(in);
        }

        public Conversation[] newArray(int size) {
            return new Conversation[size];
        }
    };
}