package com.superdev.smiling.conversation_list.service;

import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;

import java.util.List;

import rx.Observable;

/**
 * Created by marco on 29/07/16.
 */

public interface ConversationListService {

    Observable<DatabaseResult<Message>> getLastMessageFor(User self, User destination);

    Observable<List<String>> getConversationsFor(User user);

    Observable<Users> getUsers(List<String> usersId);

    Observable<Boolean> removeValue(User user);

    void deleteConversationBySenderId(String sender);

}
