package com.superdev.smiling.conversation_list.service;

import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.conversation.database.ConversationDatabase;
import com.superdev.smiling.conversation_list.database.ConversationListDatabase;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.groupconversation.data_model.GroupChat;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;
import com.superdev.smiling.user.database.UserDatabase;

import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by marco on 29/07/16.
 */

public class PersistedConversationListService implements ConversationListService {

    private final ConversationListDatabase conversationListDatabase;
    private final ConversationDatabase conversationDatabase;
    private final UserDatabase userDatabase;

    public PersistedConversationListService(ConversationListDatabase conversationListDatabase, ConversationDatabase conversationDatabase, UserDatabase userDatabase) {
        this.conversationListDatabase = conversationListDatabase;
        this.conversationDatabase = conversationDatabase;
        this.userDatabase = userDatabase;
    }

    @Override
    public Observable<DatabaseResult<Message>> getLastMessageFor(User self, User destination) {
        return conversationDatabase.observeLastMessage(self.getUid(), destination.getUid())
                .map(asDatabaseResult())
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }


    private static Func1<Message, DatabaseResult<Message>> asDatabaseResult() {
        return DatabaseResult::new;
    }

    @Override
    public Observable<List<String>> getConversationsFor(User user) {
        return conversationListDatabase.observeConversationsFor(user);
    }

    @Override
    public Observable<Users> getUsers(List<String> usersId) {
        return userDatabase.singleObserveUsers();
    }

    @Override
    public Observable<Boolean> removeValue(User user) {
        return conversationListDatabase.removeConversation(user);
    }


    @Override
    public void deleteConversationBySenderId(String sender) {
        //conversationListDatabase.deleteConversation(sender);
    }

}
