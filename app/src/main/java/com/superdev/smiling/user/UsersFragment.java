package com.superdev.smiling.user;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sinch.android.rtc.SinchError;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.SinchBaseFragment;
import com.superdev.smiling.navigation.AndroidConversationsNavigator;
import com.superdev.smiling.navigation.AndroidNavigator;
import com.superdev.smiling.sinch.SinchService;
import com.superdev.smiling.user.presenter.UsersPresenter;

/**
 * Created by marco on 19/08/16.
 */

public class UsersFragment extends SinchBaseFragment implements SinchService.StartFailedListener {

    private UsersPresenter presenter;

    private AndroidConversationsNavigator navigator;

    public static UsersFragment newInstance() {
        Bundle args = new Bundle();
        UsersFragment fragment = new UsersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_users, container, false);

        navigator = new AndroidConversationsNavigator((AppCompatActivity) getActivity(), new AndroidNavigator(getActivity()));
        presenter = new UsersPresenter(
                rootView.findViewById(R.id.usersView),
                navigator,
                Dependencies.INSTANCE.getPhoneLoginService(),
                Dependencies.INSTANCE.getUserService()
        );
        return rootView;

    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.stopPresenting();
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter("SEARCH");
        getActivity().registerReceiver(searchReceiver, filter);

        if (getSinchServiceInterface() != null && !getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(Remember.getString(Constants.FIREBASE_USERS_PHONE, ""));
        } else {
            presenter.setSinchService(getSinchServiceInterface());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (searchReceiver.isOrderedBroadcast()) {
            getActivity().unregisterReceiver(searchReceiver);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (searchReceiver.isOrderedBroadcast()) {
            getActivity().unregisterReceiver(searchReceiver);
        }
    }

    private final BroadcastReceiver searchReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String text = intent.getStringExtra("search");
            presenter.filterUsers(text);
        }
    };

    @Override
    protected void onServiceConnected() {
        if (getSinchServiceInterface() != null && getSinchServiceInterface().isStarted()) {
            presenter.setSinchService(getSinchServiceInterface());
        } else {
            getSinchServiceInterface().startClient(Remember.getString(Constants.FIREBASE_USERS_PHONE, ""));
            getSinchServiceInterface().setStartListener(this);
        }
    }

    @Override
    protected void onServiceDisconnected() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onStartFailed(SinchError error) {
        //   Toast.makeText(getContext(), "" + error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStarted() {
        presenter.setSinchService(getSinchServiceInterface());
    }
}
