package com.superdev.smiling.user.service;

import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;

import java.util.Map;

import rx.Observable;

/**
 * Created by marco on 31/07/16.
 */

public interface UserService {

    Observable<Users> syncUsers();

    Observable<DatabaseResult<Users>> getRegisteredUsers();

    Observable<DatabaseResult<User>> getUser(String userId);

    Observable<DatabaseResult<User>> getContact(String phoneNumber);

    Observable<Users> userMapping(String phoneNumber);

    Observable<DatabaseResult<Users>> getUsers();

    void setName(User user, String name);

    void setProfileImage(User user, String image);

    void setStatus(User user, String status);

    void setFcm(String token);
}