package com.superdev.smiling.user.data_model;

import android.os.Parcel;
import android.os.ParcelFormatException;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

/**
 * Created by marco on 27/07/16.
 */

public class User implements Parcelable {

    private String uid;
    private String name;
    private String image;
    private String imageUri;
    private String phone;
    private long lastSeen;
    private String fcm;
    private int phoneType;
    private String register;
    private String status;

    @SuppressWarnings("unused") //Used by Firebase
    public User() {
    }

    public int getPhoneType() {
        return phoneType;
    }

    public User(String uid, String name, String image, String imageUri, String phone, long lastSeen, String fcm, int phoneType, String register, String status) {
        this.uid = uid;
        this.name = name;
        this.image = image;
        this.imageUri = imageUri;
        this.phone = phone;
        this.lastSeen = lastSeen;
        this.fcm = fcm;
        this.phoneType = phoneType;
        this.register = register;
        this.status = status;
    }

    public User(String id, String name, String image, String phone) {
        this.uid = id;
        this.name = name;
        this.image = image;
        this.phone = phone;
    }

    public User(String id, String name, String image, String phone, String fcm) {
        this.uid = id;
        this.name = name;
        this.image = image;
        this.phone = phone;
        this.fcm = fcm;
    }

    public User(String name, String phoneNumber, int phoneType, String imageUri) {
        this.name = name;
        this.phone = phoneNumber;
        this.phoneType = phoneType;
        this.imageUri = imageUri;
    }

    public User(Parcel in) {
        this.uid = in.readString();
        this.name = in.readString();
        this.image = in.readString();
        this.phone = in.readString();
        this.lastSeen = in.readLong();
        this.fcm = in.readString();
        this.phoneType = in.readInt();
        this.register = in.readString();
        this.status = in.readString();
        this.imageUri = in.readString();
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }

    public String getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    @Exclude
    public String getShortName() {
        return name.split(" ")[0];
    }

    public String getPhone() {
        return phone;
    }

    public void setFcm(String fcm) {
        this.fcm = fcm;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setLastSeen(long lastSeen) {
        this.lastSeen = lastSeen;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPhoneType(int phoneType) {
        this.phoneType = phoneType;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getImage() {
        return image;
    }

    public long getLastSeen() {
        return lastSeen;
    }

    public String getFcm() {
        return fcm;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        User user = (User) o;

        if (uid != null ? !uid.equals(user.uid) : user.uid != null) {
            return false;
        }
        if (name != null ? !name.equals(user.name) : user.name != null) {
            return false;
        }
        return image != null ? image.equals(user.image) : user.image == null;

    }

    @Override
    public int hashCode() {
        int result = uid != null ? uid.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (image != null ? image.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        try {
            parcel.writeString(this.uid);
            parcel.writeString(this.name);
            parcel.writeString(this.image);
            parcel.writeString(this.phone);
            parcel.writeLong(this.lastSeen);
            parcel.writeString(this.fcm);
            parcel.writeInt(this.phoneType);
            parcel.writeString(this.register);
            parcel.writeString(this.status);
            parcel.writeString(this.imageUri);
        } catch (ParcelFormatException e) {
            e.printStackTrace();
        }
    }

    public static final Creator CREATOR = new Creator() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Exclude
    public String getPhoneTypeString() {
        return "MOBILE";
    }


}
