package com.superdev.smiling.user.database;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.iid.FirebaseInstanceId;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Remember;
import com.superdev.smiling.rx.FirebaseObservableListeners;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;

import org.apache.poi.ss.formula.functions.T;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by marco on 27/07/16.
 */

public class FirebaseUserDatabase implements UserDatabase {

    private final DatabaseReference usersDB;
    private final DatabaseReference userContactDB;
    private final FirebaseObservableListeners firebaseObservableListeners;
    private FirebaseUser firebaseUser;
    private FirebaseAuth firebaseAuth;

    public FirebaseUserDatabase(FirebaseDatabase firebaseDatabase, FirebaseObservableListeners firebaseObservableListeners) {
        usersDB = firebaseDatabase.getReference(Constants.FIREBASE_USERS);
        userContactDB = firebaseDatabase.getReference(Constants.FIREBASE_CONTACTS);
        this.firebaseObservableListeners = firebaseObservableListeners;
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
    }

    @Override
    public Observable<Users> observeUsers() {
        return firebaseObservableListeners.listenToValueEvents(userContactDB.child(Remember.getString(Constants.FIREBASE_USERS_PHONE, "")), toUsers());
    }

    @Override
    public Observable<User> readUserFrom(String userId) {
        return firebaseObservableListeners.listenToSingleValueEvents(usersDB.child(userId), as(User.class));
    }

    @Override
    public Observable<User> fetchContactByPhone(String phoneNumber) {
        return firebaseObservableListeners.listenToSingleValueEvents(usersDB.orderByChild("phone").equalTo(phoneNumber), mapUser());
    }

    @Override
    public Observable<Users> singleObserveUsers() {
        return firebaseObservableListeners.listenToSingleValueEvents(usersDB, toUsers());
    }

    @Override
    public Observable<User> observeUser(String userId) {
        return firebaseObservableListeners.listenToValueEvents(usersDB.child(userId), as(User.class));
    }

    @Override
    public Observable<Users> observeUserMapping(String phoneNumber) {
        usersDB.orderByChild("phone").equalTo(phoneNumber);
        return firebaseObservableListeners.listenToSingleValueEvents(usersDB, mappingOfUser());
    }

    @Override
    public Observable<Boolean> initUserLastSeen() {
        DatabaseReference amOnline = usersDB.getParent().child(".info").child("connected");

        return firebaseObservableListeners.listenToValueEvents(amOnline, lastSeenHandler());
    }

    @Override
    public void setUserLastSeen(String userId) {
        DatabaseReference lastSeenRef = usersDB.child(userId).child(Constants.FIREBASE_USERS_LASTSEEN);
        lastSeenRef.setValue(0);
        lastSeenRef.onDisconnect().removeValue();
        lastSeenRef.onDisconnect().setValue(ServerValue.TIMESTAMP);
    }

    @Override
    public void setUserName(String userId, String name) {
        usersDB.child(userId).child(Constants.FIREBASE_USERS_NAME).setValue(name);
    }

    @Override
    public void setUserImage(String userId, String image) {
        usersDB.child(userId).child(Constants.FIREBASE_USERS_IMAGE).setValue(image);
    }

    @Override
    public void setUserStatus(String userId, String status) {
        usersDB.child(userId).child(Constants.FIREBASE_USER_STATUS).setValue(status);
    }

    @Override
    public Observable<Users> getRegisteredUsers() {
        if (firebaseUser == null) {
            if (firebaseAuth == null) {
                firebaseAuth = FirebaseAuth.getInstance();
            }
            firebaseUser = firebaseAuth.getCurrentUser();
        }
        DatabaseReference databaseReference = userContactDB.child(firebaseUser.getPhoneNumber());
        databaseReference.orderByChild("register").equalTo("1");
        return firebaseObservableListeners.listenToSingleValueEvents(databaseReference, mappingOfRegisterUser());
    }

    @Override
    public void setFcm(String token) {
        if (firebaseUser == null) {
            if (firebaseAuth == null) {
                firebaseAuth = FirebaseAuth.getInstance();
            }
            if (firebaseAuth != null)
                firebaseUser = firebaseAuth.getCurrentUser();
        }
        if (firebaseUser != null) {
            usersDB.child(firebaseUser.getUid() + "/" + Constants.FIREBASE_FCM).setValue(token);
            DatabaseReference tokenRef = FirebaseDatabase.getInstance().getReference(Constants.FIREBASE_FCM);
            tokenRef.child(firebaseUser.getUid() + "/" + Constants.FIREBASE_FCM_TOKEN).setValue(token);
        }
    }

    private Func1<DataSnapshot, Boolean> lastSeenHandler() {
        return new Func1<DataSnapshot, Boolean>() {
            @Override
            public Boolean call(DataSnapshot dataSnapshot) {
                return dataSnapshot.getValue(Boolean.class);
            }
        };
    }


    private Func1<DataSnapshot, Users> mappingOfUser() {
        return new Func1<DataSnapshot, Users>() {
            @Override
            public Users call(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                List<User> users = new ArrayList<>();
                for (DataSnapshot child : children) {
                    User message = child.getValue(User.class);
                    users.add(message);
                }
                return new Users(users);
            }
        };
    }

    private Func1<DataSnapshot, Users> mappingOfRegisterUser() {
        return new Func1<DataSnapshot, Users>() {
            @Override
            public Users call(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                List<User> users = new ArrayList<>();
                for (DataSnapshot child : children) {
                    User message = child.getValue(User.class);
                    if (message != null && firebaseUser != null && message.getUid() != null && !firebaseUser.getUid().equals(message.getUid()) && message.getRegister().equalsIgnoreCase("1")) {
                        users.add(message);
                    }
                }
                return new Users(users);
            }
        };
    }

    private Func1<DataSnapshot, User> mapUser() {
        return new Func1<DataSnapshot, User>() {
            @Override
            public User call(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                User user=null;
                for (DataSnapshot child : children) {
                    User message = child.getValue(User.class);
                    if (message != null && firebaseUser != null && message.getUid() != null && !firebaseUser.getUid().equals(message.getUid()) && message.getRegister().equalsIgnoreCase("1")) {
                        user = message;
                    }
                }
                return user;
            }
        };
    }

    private Func1<DataSnapshot, Users> toUsers() {
        return new Func1<DataSnapshot, Users>() {
            @Override
            public Users call(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                List<User> users = new ArrayList<>();
                for (DataSnapshot child : children) {
                    User message = child.getValue(User.class);
                    users.add(message);
                }
                return new Users(users);
            }
        };
    }

    private <T> Func1<DataSnapshot, T> as(final Class<T> tClass) {
        return new Func1<DataSnapshot, T>() {
            @Override
            public T call(DataSnapshot dataSnapshot) {
                return dataSnapshot.getValue(tClass);
            }
        };
    }
}