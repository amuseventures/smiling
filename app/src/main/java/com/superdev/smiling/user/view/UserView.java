package com.superdev.smiling.user.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.user.data_model.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by marco on 19/08/16.
 */

public class UserView extends FrameLayout {

    @BindView(R.id.iv_user_image)
    CircleImageView ivUserImage;

    @BindView(R.id.tv_user_name)
    EmojiconTextView tvUserName;

    @BindView(R.id.tv_number_type)
    TextView tvNumberType;

    @BindView(R.id.content_layout)
    LinearLayout contentLayout;

    @BindView(R.id.iv_call)
    ImageView ivCall;

    @BindView(R.id.ic_video_call)
    ImageView icVideoCall;

    private int layoutResId;

    public UserView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int[] attrsArray = {
                    android.R.attr.layout
            };
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            layoutResId = array.getResourceId(0, R.layout.merge_users_item_view);
            array.recycle();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), layoutResId, this);
        ButterKnife.bind(this);
    }

    public void display(User user) {
        Utils.loadImageElseBlack(user.getImage(), ivUserImage, getContext(), R.drawable.avatar_contact_large);
        tvUserName.setText(user.getName());
        if (user.getUid() != null) {
            ivCall.setVisibility(VISIBLE);
            icVideoCall.setVisibility(VISIBLE);
        } else {
            ivCall.setVisibility(GONE);
            icVideoCall.setVisibility(GONE);
        }
        tvNumberType.setText(user.getPhone());
    }

    public void displayContact(User user) {
        if (user.getImage() != null) {
            Utils.loadImageElseBlack(user.getImage(), ivUserImage, getContext(),R.drawable.avatar_contact_large);
        } else if (user.getImageUri() != null) {
            try {
                Bitmap bp = MediaStore.Images.Media
                        .getBitmap(getContext().getContentResolver(),
                                Uri.parse(user.getImageUri()));
                ivUserImage.setImageBitmap(bp);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.loadImageElseBlack(user.getImage(), ivUserImage, getContext(),R.drawable.avatar_contact_large);
            }
        } else {
            Utils.loadImageElseBlack(user.getImage(), ivUserImage, getContext(),R.drawable.avatar_contact_large);
        }
        tvUserName.setText(user.getName());
        ivCall.setVisibility(GONE);
        icVideoCall.setVisibility(GONE);
        tvNumberType.setText(user.getPhone());
    }
}

