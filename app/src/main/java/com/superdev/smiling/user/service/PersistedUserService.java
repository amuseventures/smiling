package com.superdev.smiling.user.service;

import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;
import com.superdev.smiling.user.database.UserDatabase;

import java.util.Map;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by marco on 31/07/16.
 */

public class PersistedUserService implements UserService {

    private final UserDatabase userDatabase;

    public PersistedUserService(UserDatabase userDatabase) {
        this.userDatabase = userDatabase;
    }

    @Override
    public Observable<Users> syncUsers() {
        return userDatabase.observeUsers();
    }

    @Override
    public Observable<DatabaseResult<Users>> getRegisteredUsers() {
        return userDatabase.getRegisteredUsers()
                .map(DatabaseResult::new)
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    @Override
    public Observable<DatabaseResult<User>> getUser(String userId) {
        return userDatabase.observeUser(userId)
                .map(DatabaseResult::new)
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    @Override
    public Observable<DatabaseResult<User>> getContact(String phoneNumber) {
        return userDatabase.fetchContactByPhone(phoneNumber)
                .map(DatabaseResult::new)
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    @Override
    public Observable<Users> userMapping(String phoneNumber) {
        return userDatabase.observeUserMapping(phoneNumber);
    }

    @Override
    public Observable<DatabaseResult<Users>> getUsers() {
        return userDatabase.singleObserveUsers()
                .map(DatabaseResult::new)
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    @Override
    public void setName(User user, String name) {
        userDatabase.setUserName(user.getUid(), name);
    }

    @Override
    public void setProfileImage(User user, String image) {
        userDatabase.setUserImage(user.getUid(), image);
    }

    @Override
    public void setStatus(User user, String status) {
        userDatabase.setUserStatus(user.getUid(), status);
    }

    @Override
    public void setFcm(String token) {
        userDatabase.setFcm(token);
    }

}
