package com.superdev.smiling.user.view;

import android.view.View;

import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;

/**
 * Created by marco on 01/09/16.
 */

public interface UsersDisplayer {

    void display(Users users);

    void attach(UserInteractionListener userInteractionListener);

    void detach(UserInteractionListener userInteractionListener);

    void showProgress();

    void hideProgress();

    interface UserInteractionListener {

        void onUserSelected(User user);

        void onCallSelected(User user);

        void onVideoSelected(User user);

        void onImageSelected(View view, User user);

        void onContactSelected(User user);
    }

    void filter(String text);

}
