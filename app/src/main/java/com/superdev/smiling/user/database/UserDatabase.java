package com.superdev.smiling.user.database;

import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;

import java.util.Map;

import rx.Observable;

/**
 * Created by marco on 27/07/16.
 */

public interface UserDatabase {

    Observable<Users> observeUsers();

    Observable<User> readUserFrom(String userId);

    Observable<User> fetchContactByPhone(String phoneNumber);

    Observable<Users> singleObserveUsers();

    Observable<User> observeUser(String userId);

    Observable<Users> observeUserMapping(String phoneNumber);

    Observable<Boolean> initUserLastSeen();

    void setUserLastSeen(String userId);

    void setUserName(String userId, String name);

    void setUserImage(String userId, String image);

    void setUserStatus(String userId, String status);

    Observable<Users> getRegisteredUsers();

    void setFcm(String token);
}

