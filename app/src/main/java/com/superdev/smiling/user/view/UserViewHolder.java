package com.superdev.smiling.user.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.superdev.smiling.user.data_model.User;

/**
 * Created by marco on 19/08/16.
 */

public class UserViewHolder extends RecyclerView.ViewHolder {

    private final UserView userView;

    public UserViewHolder(UserView itemView) {
        super(itemView);
        this.userView = itemView;
    }

    public void bind(final User user, final UserSelectionListener listener) {
        userView.display(user);

        userView.setOnClickListener(v -> listener.onUserSelected(user));

        userView.icVideoCall.setOnClickListener(v -> listener.onVideoSelected(user));

        userView.ivCall.setOnClickListener(v -> listener.onCallSelected(user));

        userView.ivUserImage.setOnClickListener(v -> listener.onImageSelected(v,user));
    }

    public UserView getUserView() {
        return userView;
    }

    public interface UserSelectionListener {

        void onUserSelected(User user);

        void onCallSelected(User user);

        void onVideoSelected(User user);

        void onImageSelected(View view,User user);
    }
}