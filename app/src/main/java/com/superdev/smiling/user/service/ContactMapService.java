package com.superdev.smiling.user.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.Remember;
import com.superdev.smiling.sync.SyncUtils;
import com.superdev.smiling.user.data_model.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ContactMapService extends IntentService {

    private static final String TAG = "ContactMapService";

    private UserService userService;

    public ContactMapService() {
        super("TAG");
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Dependencies.INSTANCE.init(this);
        userService = Dependencies.INSTANCE.getUserService();
        final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference reference = firebaseDatabase.getReference();
        SyncUtils.getContactList(this, "smile", "12345")
                 .subscribeOn(Schedulers.io())
                 .observeOn(Schedulers.io())
                 .subscribe(new Action1<List<User>>() {
                     @Override
                     public void call(List<User> users) {
                         if (users == null || users.isEmpty()) {
                             return;
                         }
                         /*for (final User user : users) {
                             if (user.getPhone().equalsIgnoreCase(Remember.getString(Constants.FIREBASE_USERS_PHONE, ""))) {
                                 continue;
                             }
                             reference.child("users").orderByChild("phone").equalTo(user.getPhone())
                                      .addValueEventListener(new ValueEventListener() {
                                          @Override
                                          public void onDataChange(DataSnapshot dataSnapshot) {
                                              User userEntity = null;
                                              HashMap<String, User> userHashMap = new HashMap<>();
                                              for (DataSnapshot userShot : dataSnapshot.getChildren()) {
                                                  userEntity = userShot.getValue(User.class);
                                              }
                                              if (userEntity != null) {
                                                  userEntity.setPhoneType(user.getPhoneType());
                                                  userHashMap.put(userEntity.getPhone(), userEntity);
                                              } else {
                                                  user.setRegister("0");
                                                  userHashMap.put(user.getPhone(), user);
                                              }
                                              for (Map.Entry<String, User> entry : userHashMap.entrySet()) {
                                                  reference.child(Constants.FIREBASE_CONTACTS).child(Remember.getString(Constants.FIREBASE_USERS_PHONE, ""))
                                                           .child(entry.getValue().getPhone()).setValue(entry.getValue());
                                              }
                                              userHashMap.clear();
                                          }

                                          @Override
                                          public void onCancelled(DatabaseError databaseError) {

                                          }
                                      });
                         }*/

                     }
                 });
    }

}