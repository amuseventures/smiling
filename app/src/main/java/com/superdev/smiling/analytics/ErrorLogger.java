package com.superdev.smiling.analytics;

public interface ErrorLogger {

    void reportError(Throwable throwable, Object... args);

}
