package com.superdev.smiling.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

public class SyncContact extends AsyncTask<String, Void, String> {
    private Context mContext;
    private String usertoken;
    private String username, id;
    private int type;

    public SyncContact(Context mContext, String mid, String name) {
        this.mContext = mContext;
        this.username = name;
        this.id = mid;

    }

    @Override
    public String doInBackground(String... params) {
        String jsonContact = SyncUtils.GetContact_forSync(mContext, id,
                username);
        //Http Operation
        //Post jsoncontact to server and fetch existing contact in server or intersection of contacts
        return "";

    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
    }

    @Override
    public void onPostExecute(String result) {
        if (result != null && result.length() != 0) {
            PreferenceManager.getDefaultSharedPreferences(mContext).edit()
                    .putString("CONTACTS", result).apply();

        }

    }

}
