package com.superdev.smiling.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.superdev.smiling.Constants;
import com.superdev.smiling.Remember;
import com.superdev.smiling.user.data_model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;
import rx.Observable;

@SuppressWarnings("ALL")
public class SyncUtils {

    public static String AUTHORITY = "com.android.contacts";
    public static String ACCOUNT_TYPE = "com.superdev.smiling";

    private static final String TAG = "SyncUtils";


    public static void CreateAccount(Context ctxt, String user, String pass) {
        Account account = new Account(user, ACCOUNT_TYPE);
        AccountManager am = AccountManager.get(ctxt);
        if (am.addAccountExplicitly(account, pass, null)) {
            Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);

            ContentResolver.setIsSyncable(account, "com.android.contacts", 1);
            ContentResolver.setSyncAutomatically(account, AUTHORITY, true);

            Log.i("SyncUtils", "Account Added Successfully");

        }
    }

    public static void sendContactsSync(final Context context, final String id,
                                        final String username) {
        if (context != null && id != null && username != null)
            new Handler(Looper.getMainLooper()).post(new Runnable() {

                @Override
                public void run() {
                    Log.d(TAG, "Perform Sync: ");
                    //new SyncContact(context, id, username).execute();
                    /*Intent intent = new Intent(context, ContactSyncService.class);
                    context.startService(intent);*/

                }
            });
    }

    public static String GetContact_forSync(Context context, String id,
                                            String username) {
        List<User> contactEntityList = new ArrayList<>();
        JSONObject parent = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonarray = new JSONArray();
        String json = null;
        Cursor emails = null;
        ContentResolver cr = context.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String ids = cur.getString(cur.getColumnIndex(BaseColumns._ID));
                if (Integer
                        .parseInt(cur.getString(cur
                                .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(Phone.CONTENT_URI, null, Phone.CONTACT_ID + " = ?", new String[]{ids}, null);
                    try {
                        if (pCur != null) {
                            while (pCur.moveToNext()) {
                                int phoneType = pCur
                                        .getInt(pCur
                                                .getColumnIndex(Phone.TYPE));
                                String phoneNumber = pCur
                                        .getString(
                                                pCur.getColumnIndex(Phone.NUMBER))
                                        .replaceAll("\\s", "");


                                String userName = pCur.getString(pCur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                                JSONObject jsonId = new JSONObject();
                                try {
                                    jsonId.put("id", id);
                                    jsonId.put("nos",
                                            phoneNumber.replaceAll("[-()+]", ""));
                                    jsonId.put("name", userName);
                                    jsonId.put("phoneType", phoneType);
                                    jsonarray.put(jsonId);
                                    contactEntityList.add(new User(userName, phoneNumber, phoneType, ""));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                            pCur.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            cur.close();
        }
        try {
            jsonObject.put("cid", id);
            jsonObject.put("user", username);
            jsonObject.put("contacts", jsonarray);
            jsonObject.put("country", "india");
            parent.put("query", jsonObject);
            Log.i("sync json", parent.toString());
            try {
                json = Base64.encodeToString(parent.toString()
                        .getBytes("utf-8"), Base64.DEFAULT);
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        try {
            Writer output = null;
            File file = new File(Environment.getExternalStorageDirectory(), "output.json");
            file.createNewFile();
            output = new BufferedWriter(new FileWriter(file));
            output.write(parent.toString());
            output.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public static Observable<List<User>> getContactList(Context context, String id,
                                                        String username) {
        HashSet<User> hashSet = new HashSet<>();
        String image_uri = "";
        ContentResolver cr = context.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                null, null, null);
        if (cur != null && cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String ids = cur.getString(cur.getColumnIndex(BaseColumns._ID));
                if (Integer
                        .parseInt(cur.getString(cur
                                .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(Phone.CONTENT_URI, null, Phone.CONTACT_ID + " = ? AND LENGTH(" + Phone.NUMBER + ">=10)", new String[]{ids}, null);

                    try {
                        if (pCur != null) {
                            while (pCur.moveToNext()) {
                                int phoneType = pCur
                                        .getInt(pCur
                                                .getColumnIndex(Phone.TYPE));
                                String phoneNumber = pCur
                                        .getString(
                                                pCur.getColumnIndex(Phone.NUMBER))
                                        .replaceAll("\\s", "")
                                        .replaceAll("-", "")
                                        .replaceAll("\\(\\(?<!^\\s-*\\)\\+|[^\\d+]+\\)", "");


                                String userName = pCur.getString(pCur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                                image_uri = cur
                                        .getString(cur
                                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

                                final String currentCode = Remember.getString(Constants.CURRENT_COUNTRY_CODE, "+91");

                                if (!TextUtils.isEmpty(currentCode)) {
                                    if (phoneNumber.length() >= 10) {
                                        if (phoneNumber.startsWith("0")) {
                                            String contact = phoneNumber.substring(1, phoneNumber.length());
                                            phoneNumber = currentCode + contact;
                                        } else if (!phoneNumber.contains("+") && !phoneNumber.startsWith("0")) {
                                            phoneNumber = currentCode + phoneNumber;
                                        }
                                        try {
                                            PhoneNumberUtil phoneUtil = PhoneNumberUtil.createInstance(context);
                                            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, currentCode);
                                            int countryCode = numberProto.getCountryCode();
                                            long nationalNumber = numberProto.getNationalNumber();
                                            hashSet.add(new User(userName, "+" + countryCode + "" + nationalNumber, phoneType, image_uri));
                                        } catch (NumberParseException e) {
                                            System.err.println("NumberParseException was thrown: " + phoneNumber);
                                        }
                                    }
                                }
                            }
                            pCur.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            cur.close();
        }
        List<User> userArrayList = new ArrayList<>(hashSet);
        Collections.sort(userArrayList, new Comparator<User>() {
            @Override
            public int compare(User user, User t1) {
                return user.getName().compareTo(t1.getName());
            }
        });
        return Observable.just(userArrayList);
    }
}
