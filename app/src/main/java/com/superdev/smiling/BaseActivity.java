package com.superdev.smiling;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.akexorcist.localizationactivity.LocalizationActivity;
import com.quickblox.sample.core.gcm.GooglePlayServicesHelper;
import com.quickblox.sample.core.utils.SharedPrefsHelper;
import com.superdev.smiling.quickutils.QBResRequestExecutor;
import com.superdev.smiling.utility.PrefManager;

/**
 * Created by marco on 28/07/16.
 */

public abstract class BaseActivity extends LocalizationActivity {

    protected GooglePlayServicesHelper googlePlayServicesHelper;

    protected QBResRequestExecutor requestExecutor;

    protected SharedPrefsHelper sharedPrefsHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Dependencies.INSTANCE.init(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        requestExecutor = App.getInstance().getQbResRequestExecutor();

        sharedPrefsHelper = SharedPrefsHelper.getInstance();

        googlePlayServicesHelper = new GooglePlayServicesHelper();

        PrefManager prefManager = new PrefManager(this);

        switch (prefManager.getLanguageSettings()) {
            case -1 :
                break;
            case 0 :
                setLanguage("en");
                break;
            case 1 :
                setLanguage("ps");
                break;
            case 2 :
                setLanguage("fa");
                break;
            case 3 :
                setLanguage("ar");
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
