package com.superdev.smiling.common.intents;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;

import com.superdev.smiling.BuildConfig;
import com.superdev.smiling.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

public class ProfilePhotoIntent extends AppCompatActivity {

    public static final int IMAGE_GALLERY_REQUEST = 201;
    public static final int IMAGE_CAMERA_REQUEST = 202;

    private File filePathImageCamera;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() != null)
            if (getIntent().getAction() != null) {
                String action = getIntent().getAction();
                if (action.equals(IntentConstants.PHOTO_FROM_GALLERY_ACTION)) {

                    FilePickerBuilder.getInstance().setMaxCount(1)
                                     .showFolderView(true)
                                     .enableImagePicker(true)
                                     .showGifs(false)
                                     .enableVideoPicker(false)
                                     .setActivityTheme(R.style.AppThemeActionBar)
                                     .pickPhoto(this);

                } else if (action.equals(IntentConstants.PHOTO_FROM_CAMERA_ACTION)) {
                    String nomeFoto = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
                    filePathImageCamera = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), nomeFoto + "camera.jpg");
                    Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    Uri photoURI = FileProvider.getUriForFile(ProfilePhotoIntent.this,
                            BuildConfig.APPLICATION_ID + ".provider",
                            filePathImageCamera);
                    it.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(it, IMAGE_CAMERA_REQUEST);
                } else if (action.equals(IntentConstants.PHOTO_NONE_ACTION)) {
                    setResult(RESULT_CANCELED, new Intent(IntentConstants.PHOTO_NONE_ACTION));
                    finish();
                }
            }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO) {
            if (data == null) {
                finish();
                return;
            }
            if (resultCode != Activity.RESULT_OK) {
                finish();
                return;
            }
            ArrayList<String> photoPaths = new ArrayList<>();
            photoPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));

            Intent intent = new Intent();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoPaths.get(0));
            setResult(RESULT_OK, intent);
            finish();
        } else if (requestCode == IMAGE_CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent();
                /*Uri photoURI = FileProvider.getUriForFile(ProfilePhotoIntent.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        filePathImageCamera);*/
                intent.putExtra(MediaStore.EXTRA_OUTPUT, filePathImageCamera.getAbsolutePath());
                setResult(RESULT_OK, intent);
                finish();
            } else {
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
