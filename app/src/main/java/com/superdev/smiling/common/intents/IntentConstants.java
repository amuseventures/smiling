package com.superdev.smiling.common.intents;

public class IntentConstants {
    //if you change here, change also in AndroidManifest.xml
    private static String p = "com.superdev.smiling.common";
    public final static String WALLPAPER_DEFAULT_ACTION = p + ".intent.action.WALLPAPER_DEFAULT";
    public final static String WALLPAPER_NONE_ACTION = p + ".intent.action.WALLPAPER_NONE";
    public final static String WALLPAPER_FROM_LIBRARY_ACTION = p + ".intent.action.WALLPAPER_FROM_LIBRARY";
    public final static String WALLPAPER_FROM_GALLERY_ACTION = p + ".intent.action.WALLPAPER_FROM_GALLERY";
    public final static String WALLPAPER_FROM_SOLID_COLOR_ACTION = p + ".intent.action.WALLPAPER_FROM_SOLID_COLOR";

    //Photo Profile Intent
    public static final String PHOTO_FROM_CAMERA_ACTION = p + ".intent.action.PHOTO_FROM_CAMERA";
    public static final String PHOTO_FROM_GALLERY_ACTION = p + ".intent.action.PHOTO_FROM_GALLERY";
    public static final String PHOTO_NONE_ACTION = p + ".intent.action.PHOTO_NONE";
    public static final String PHOTO_FROM_LIBRARY_ACTION = p + ".intent.PHOTO_FROM_LIBRARY";

    //Photo Default Packages
    public static final String PHOTO_GALLERY_PACKAGE_NAME = p + ".intents.ProfilePhotoIntentGallery";
    public static final String PHOTO_NONE_PACKAGE_NAME = p + ".intents.ProfilePhotoIntentNone";
    public static final String PHOTO_CAMERA_PACKAGE_NAME = p + ".intents.ProfilePhotoIntentCamera";


    public final static String WALLPAPER_DEFAULT_PACKAGE_NAME = p + ".WallpaperDefault";
    public final static String WALLPAPER_NONE_PACKAGE_NAME = p + ".WallpaperNone";
    public final static String WALLPAPER_FROM_LIBRARY_PACKAGE_NAME = p + ".Wallpaper";
    public final static String WALLPAPER_FROM_GALLERY_PACKAGE_NAME = p + ".WallpaperGallery";
    public final static String WALLPAPER_FROM_SOLID_COLOR_PACKAGE_NAME = p + ".WallpaperColor";
}