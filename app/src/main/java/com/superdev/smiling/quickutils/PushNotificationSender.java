package com.superdev.smiling.quickutils;

import android.os.AsyncTask;
import android.os.Bundle;

import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.messages.QBPushNotifications;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBEvent;
import com.quickblox.messages.model.QBNotificationType;
import com.quickblox.messages.model.QBPushType;
import com.quickblox.messages.model.QBSubscription;
import com.superdev.smiling.R;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.rx.FCM;


import java.util.ArrayList;

/**
 * Created by tereha on 13.05.16.
 */
public class PushNotificationSender {

    public static void sendPushMessage(ArrayList<Integer> recipients, String senderName) {
        String outMessage = String.format(String.valueOf(R.string.text_push_notification_message), senderName);

        // Send Push: create QuickBlox Push Notification Event
        QBEvent qbEvent = new QBEvent();
        qbEvent.setNotificationType(QBNotificationType.PUSH);
        qbEvent.setEnvironment(QBEnvironment.PRODUCTION);
        qbEvent.setPushType(QBPushType.GCM);
        // Generic push - will be delivered to all platforms (Android, iOS, WP, Blackberry..)
        qbEvent.setMessage(outMessage);
        StringifyArrayList<Integer> userIds = new StringifyArrayList<>(recipients);
        qbEvent.setUserIds(userIds);

        QBPushNotifications.createEvent(qbEvent).performAsync(new QBEntityCallbackImpl<QBEvent>(){
            @Override
            public void onSuccess(QBEvent result, Bundle params) {
                super.onSuccess(result, params);
            }

            @Override
            public void onError(QBResponseException responseException) {
                super.onError(responseException);
            }
        });
    }

    public static void sendCallNotification(Channel channel, String  outMessage){
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                FCM.sendCallNotification(channel.getUid(), outMessage);
                return null;
            }
        }.execute();
    }
}
