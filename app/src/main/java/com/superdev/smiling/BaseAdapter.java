package com.superdev.smiling;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

public abstract class BaseAdapter<Type extends RecyclerView.ViewHolder, T> extends RecyclerView.Adapter<Type> {

    public interface ItemClickListener<T> {

        void onItemClick(T object, View view);

        void onItemLongClick(T object, View view);
    }

    protected ItemClickListener<T> mItemClickListener;

    public void setItemClickListener(ItemClickListener<T> conversationClickListener) {
        mItemClickListener = conversationClickListener;
    }

    public abstract void setData(List<T> data);
}