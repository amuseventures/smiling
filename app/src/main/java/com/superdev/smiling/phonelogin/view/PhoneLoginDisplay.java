package com.superdev.smiling.phonelogin.view;

import com.google.firebase.auth.PhoneAuthCredential;

/**
 * Created by gautam on 9/2/17.
 */

public interface PhoneLoginDisplay {

    void attach(LoginActionListener actionListener);

    void detach(LoginActionListener actionListener);

    void showOtpVerification();

    void showPhoneVerificationDialog(LoginActionListener actionListener);

    void showAuthenticationError(String message);

    void showErrorFromResourcesString(int id);


    interface LoginActionListener {

        void loginWithPhoneNumber(String phoneNumber);

        void onOkButtonSelected(String phoneNumber);

        void onCancelButtonSelected();

        void onResendOtpSelected(String phoneNumber);

        void loginWithCredential(PhoneAuthCredential credential);

        void verifyPhoneWithCode();
    }
}
