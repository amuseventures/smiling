package com.superdev.smiling.phonelogin.database;

import com.google.firebase.auth.PhoneAuthCredential;
import com.superdev.smiling.login.data_model.Authentication;

import rx.Observable;

/**
 * Created by gautam on 9/2/17.
 */

public interface PhoneAuthDatabase {

    Observable<Authentication> readAuthentication();

    Observable<Authentication> loginWithPhoneNumber(PhoneAuthCredential credential);

}
