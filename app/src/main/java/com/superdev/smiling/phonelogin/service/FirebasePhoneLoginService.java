package com.superdev.smiling.phonelogin.service;

import com.google.firebase.auth.PhoneAuthCredential;
import com.jakewharton.rxrelay.BehaviorRelay;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.main.database.CloudMessagingDatabase;
import com.superdev.smiling.phonelogin.database.PhoneAuthDatabase;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func0;

/**
 * Created by gautam on 9/2/17.
 */

public class FirebasePhoneLoginService implements PhoneLoginService {

    private final PhoneAuthDatabase authDatabase;

    private final CloudMessagingDatabase cloudMessagingDatabase;

    private BehaviorRelay<Authentication> authRelay;

    public FirebasePhoneLoginService(PhoneAuthDatabase authDatabase, CloudMessagingDatabase cloudMessagingDatabase) {
        this.authDatabase = authDatabase;
        this.cloudMessagingDatabase = cloudMessagingDatabase;
        authRelay = BehaviorRelay.create();
    }

    @Override
    public Observable<Authentication> getAuthentication() {
        return authRelay
                .startWith(initRelay());
    }

    @Override
    public void loginWithCredential(PhoneAuthCredential phoneAuthCredential) {
        authDatabase.loginWithPhoneNumber(phoneAuthCredential)
                .subscribe(new Action1<Authentication>() {
                    @Override
                    public void call(Authentication authentication) {
                        if (authentication.isSuccess()) {
                            cloudMessagingDatabase.enableToken(authentication.getUser().getUid());
                        }
                        authRelay.call(authentication);
                    }
                });
    }

    private Observable<Authentication> initRelay() {
        return Observable.defer(() -> {
            if (authRelay.hasValue() && authRelay.getValue().isSuccess()) {
                return Observable.empty();
            } else {
                return fetchUser();
            }
        });
    }

    private Observable<Authentication> fetchUser() {
        return authDatabase.readAuthentication()
                .doOnNext(authRelay)
                .ignoreElements();
    }

}
