package com.superdev.smiling.phonelogin;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.superdev.smiling.BaseActivity;
import com.superdev.smiling.Constants;
import com.superdev.smiling.DelayUtils;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.PhoneFormat.PhoneFormat;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.Utils;
import com.superdev.smiling.gcm.QuickstartPreferences;
import com.superdev.smiling.gcm.RegistrationIntentService;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.main.MainActivity;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.services.OnClearFromRecentService;
import com.superdev.smiling.sync.SyncUtils;
import com.superdev.smiling.user.service.ContactMapService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscriber;
import rx.Subscription;

public class PhoneLoginActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.textView)
    TextView textView;

    @BindView(R.id.tvCountryName)
    Button tvCountryName;

    @BindView(R.id.tvCountryCode)
    EditText tvCountryCode;

    @BindView(R.id.tvMobileNumber)
    EditText tvMobileNumber;

    @BindView(R.id.btn_next)
    Button btnNext;

    @BindView(R.id.btn_verify)
    Button btnVerify;

    @BindView(R.id.view_one)
    RelativeLayout viewOne;

    @BindView(R.id.tvWaitingText)
    TextView tvWaitingText;

    @BindView(R.id.tvOtpNumber)
    EditText tvOtpNumber;

    @BindView(R.id.resendOtp)
    LinearLayout resendOtp;

    @BindView(R.id.view_two)
    RelativeLayout viewTwo;

    @BindView(R.id.view_switcher)
    ViewSwitcher viewSwitcher;

    @BindView(R.id.activity_login)
    LinearLayout activityLogin;

    private ArrayList<String> countriesArray = new ArrayList<>();

    private HashMap<String, String> countriesMap = new HashMap<>();

    private HashMap<String, String> codesMap = new HashMap<>();

    private HashMap<String, String> phoneFormatMap = new HashMap<>();

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private boolean isReceiverRegistered;

    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final String TAG = "LoginActivity";

    private static final int STATE_INITIALIZED = 1;

    private static final int STATE_CODE_SENT = 2;

    private static final int STATE_VERIFY_FAILED = 3;

    private static final int STATE_VERIFY_SUCCESS = 4;

    private static final int STATE_SIGNIN_FAILED = 5;

    private static final int STATE_SIGNIN_SUCCESS = 6;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private String phoneNumber;

    private FirebaseAuth mAuth;

    private boolean mVerificationInProgress = false;

    private String mVerificationId;

    private PhoneAuthProvider.ForceResendingToken mResendToken;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private Subscription subscription;

    private PhoneLoginService loginService;

    public static final int COUNTRY_PICK_UP_REQUEST = 101;

    private boolean ignoreOnTextChange = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_login);
        startService(new Intent(getBaseContext(), OnClearFromRecentService.class));
        ButterKnife.bind(this);
        //    activityLogin.setVisibility(View.GONE);
        //setSupportActionBar(toolbar);
        loginService = Dependencies.INSTANCE.getPhoneLoginService();
        createAccount();
        toolbarTitle.setText(R.string.login_title);
        tvCountryCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (ignoreOnTextChange) {
                    return;
                }
                ignoreOnTextChange = true;
                String text = PhoneFormat.stripExceptNumbers(tvCountryCode.getText().toString());
                tvCountryCode.setText(text);
                if (text.length() == 0) {
                    tvCountryName.setText(R.string.select_country_title);
                    tvMobileNumber.setHint(null);
                } else {
                    String country;
                    boolean ok = false;
                    String textToSet = null;
                    if (text.length() > 4) {
                        for (int a = 4; a >= 1; a--) {
                            String sub = text.substring(0, a);
                            country = codesMap.get(sub);
                            if (country != null) {
                                ok = true;
                                textToSet = text.substring(a, text.length()) + tvMobileNumber.getText().toString();
                                tvCountryCode.setText(text = sub);
                                break;
                            }
                        }
                        if (!ok) {
                            textToSet = text.substring(1, text.length()) + tvMobileNumber.getText().toString();
                            tvCountryCode.setText(text = text.substring(0, 1));
                        }
                    }
                    country = codesMap.get(text);
                    if (country != null) {
                        int index = countriesArray.indexOf(country);
                        if (index != -1) {
                            tvCountryName.setText(countriesArray.get(index));
                            String hint = phoneFormatMap.get(text);
                            tvMobileNumber.setHint(hint != null ? hint.replace('X', '–') : null);
                        } else {
                            tvCountryName.setText(R.string.wrong_country_code);
                            tvMobileNumber.setHint(null);
                        }
                    } else {
                        tvCountryName.setText(R.string.wrong_country_code);
                        tvMobileNumber.setHint(null);
                    }
                    if (!ok) {
                        tvCountryCode.setSelection(tvCountryCode.getText().length());
                    }
                    if (textToSet != null) {
                        tvMobileNumber.requestFocus();
                        tvMobileNumber.setText(textToSet);
                        tvMobileNumber.setSelection(tvMobileNumber.length());
                    }
                }
                ignoreOnTextChange = false;
            }
        });

        tvOtpNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 6) {
                    if (!TextUtils.isEmpty(mVerificationId)) {
                        verifyPhoneNumberWithCode(mVerificationId, tvOtpNumber.getText().toString());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        HashMap<String, String> languageMap = new HashMap<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getResources().getAssets().open("countries.txt")));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] args = line.split(";");
                countriesArray.add(0, args[2]);
                countriesMap.put(args[2], args[0]);
                codesMap.put(args[0], args[2]);
                if (args.length > 3) {
                    phoneFormatMap.put(args[0], args[3]);
                }
                languageMap.put(args[1], args[2]);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }

        Collections.sort(countriesArray, String::compareTo);

        String country = null;

        try {
            TelephonyManager telephonyManager = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager != null) {
                country = telephonyManager.getSimCountryIso().toUpperCase();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }

        if (country != null) {
            String countryName = languageMap.get(country);
            if (countryName != null) {
                int index = countriesArray.indexOf(countryName);
                if (index != -1) {
                    tvCountryCode.setText(countriesMap.get(countryName));
                }
            }
        }
        if (tvCountryCode.length() == 0) {
            tvCountryName.setText(R.string.select_country_title);
            tvMobileNumber.setHint(null);
        }

        if (tvCountryCode.length() != 0) {
            Utils.showKeyboard(tvMobileNumber);
            tvMobileNumber.requestFocus();
            tvMobileNumber.setSelection(tvMobileNumber.length());
        } else {
            Utils.showKeyboard(tvCountryCode);
            tvCountryCode.requestFocus();
        }

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                Log.d(TAG, "onVerificationCompleted:" + credential);
                mVerificationInProgress = false;
                updateUI(STATE_VERIFY_SUCCESS, credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);
                mVerificationInProgress = false;
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    tvMobileNumber.setText("Invalid Phone Number");
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                }
                updateUI(STATE_VERIFY_FAILED);
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                showLoading(false);
                Log.d(TAG, "onCodeSent:" + verificationId);
                mVerificationId = verificationId;
                mResendToken = token;
                updateUI(STATE_CODE_SENT);
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean sentToken = Remember.getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    Dependencies.INSTANCE.initFirebaseToken();
                    fetchUserData();
                    if (mVerificationInProgress && validatePhoneNumber()) {
                        startPhoneNumberVerification(String.format("%s%s", tvCountryCode.getText(), tvMobileNumber.getText()));
                    }
                } else {
                    Snackbar.make(activityLogin, "Some error occurred, please try again", Snackbar.LENGTH_INDEFINITE)
                            .setAction("RETRY", view -> {

                                startService(new Intent(PhoneLoginActivity.this, RegistrationIntentService.class));
                            }).show();
                }
            }
        };
        if (checkPlayServices()) {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_VERIFY_IN_PROGRESS, mVerificationInProgress);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mVerificationInProgress = savedInstanceState.getBoolean(KEY_VERIFY_IN_PROGRESS);
    }


    private void updateUI(int uiState) {
        updateUI(uiState, null);
    }

    private void updateUI(int uiState, PhoneAuthCredential cred) {
        switch (uiState) {
            case STATE_INITIALIZED:
                if (viewSwitcher.getCurrentView() != viewOne) {
                    viewSwitcher.showNext();
                }
                break;
            case STATE_CODE_SENT:
                // Code sent state, show the verification field, the
                if (viewSwitcher.getCurrentView() != viewTwo) {
                    viewSwitcher.showNext();
                }
                break;
            case STATE_VERIFY_FAILED:
                if (viewSwitcher.getCurrentView() != viewTwo) {
                    viewSwitcher.showNext();
                }
                break;
            case STATE_VERIFY_SUCCESS:
                if (cred != null) {
                    if (cred.getSmsCode() != null) {
                        tvOtpNumber.setText(cred.getSmsCode());
                    } else {
                        loginService.loginWithCredential(cred);
                    }
                }
                break;
            case STATE_SIGNIN_FAILED:
                break;
            case STATE_SIGNIN_SUCCESS:
                // Np-op, handled by sign-in check
                break;
        }
        fetchUserData();
        showLoading(false);
    }

    @OnClick(R.id.resendOtp)
    public void onResendOtp() {
        if (!TextUtils.isEmpty(phoneNumber)) {
            showLoading(true);
            resendVerificationCode(phoneNumber, mResendToken);
        }
    }

    private void showLoading(boolean isLoading) {
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        showLoading(true);
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        loginService.loginWithCredential(credential);
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        showLoading(true);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

        mVerificationInProgress = true;
    }

    private boolean validatePhoneNumber() {
        String countryCode = tvCountryCode.getText().toString();
        String phoneNumber = tvMobileNumber.getText().toString();

        if (TextUtils.isEmpty(countryCode)) {
            tvCountryCode.setError("Invalid country code.");
            return false;
        }

        if (TextUtils.isEmpty(phoneNumber)) {
            tvMobileNumber.setError("Invalid phone number.");
            return false;
        }
        return true;
    }


    @OnClick(R.id.btn_next)
    void onViewClicked() {
        if (TextUtils.isEmpty(tvMobileNumber.getText().toString())) {
            tvMobileNumber.setError(getString(R.string.enterValidNumber));
            return;
        }
        showAlertDialogToVerifyPhone();
    }

    private void showAlertDialogToVerifyPhone() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PhoneLoginActivity.this);
        View view = LayoutInflater.from(builder.getContext()).inflate(R.layout.layout_verify_phone, null, false);
        builder.setView(view);
        final AlertDialog dialog = builder.create();
        final TextView tvPhoneNumber = view.findViewById(R.id.tv_phone_number);
        Button btnEdit = view.findViewById(R.id.btn_edit);
        Button btnOk = view.findViewById(R.id.btn_ok);
        tvPhoneNumber.setText(String.format("+%s%s", tvCountryCode.getText(), tvMobileNumber.getText()));
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbarTitle.setText(String.format(getString(R.string.verify_number_title), tvPhoneNumber.getText()));
                tvWaitingText.setText(String.format(getString(R.string.waiting_otp_text), tvPhoneNumber.getText()));
                phoneNumber = tvPhoneNumber.getText().toString();
                Remember.putString(Constants.CURRENT_COUNTRY_CODE, "+" + tvCountryCode.getText().toString());
                startPhoneNumberVerification(phoneNumber);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void fetchUserData() {
        subscription = loginService.getAuthentication()
                .subscribe(new Subscriber<Authentication>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Authentication authentication) {
                        if (authentication.isSuccess()) {
                            Remember.putString(Constants.FIREBASE_USER_ID, authentication.getUser().getUid());
                            if (!TextUtils.isEmpty(Remember.getString(Constants.CURRENT_COUNTRY_CODE, ""))) {
                                Intent intent = new Intent(PhoneLoginActivity.this, ContactMapService.class);
                                intent.setAction("com.superdev.smiling.SYNC_CONTACT");
                                startService(intent);
                            }
                            startActivity(new Intent(PhoneLoginActivity.this, MainActivity.class));
                            finish();
                        } else {
                        }
                    }

                });
        //    DelayUtils.delay(3, () -> activityLogin.setVisibility(View.VISIBLE));
    }

    public void selectCountry(String name) {
        int index = countriesArray.indexOf(name);
        if (index != -1) {
            ignoreOnTextChange = true;
            String code = countriesMap.get(name);
            tvCountryCode.setText(code);
            tvCountryName.setText(name);
            String hint = phoneFormatMap.get(code);
            tvMobileNumber.setHint(hint != null ? hint.replace('X', '–') : null);
            ignoreOnTextChange = false;
        }
    }


    @OnClick(R.id.tvCountryName)
    public void onCountryNameSelected() {
        Intent intent = new Intent(PhoneLoginActivity.this, CountryListActivity.class);
        startActivityForResult(intent, COUNTRY_PICK_UP_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == COUNTRY_PICK_UP_REQUEST) {
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra(Constants.COUNTRY_DATA_EXTRA);
                selectCountry(name);
            }
        }
    }

    public void createAccount() {
        AccountManager am = AccountManager.get(this);
        Account[] accounts = am.getAccountsByType(SyncUtils.ACCOUNT_TYPE);
        for (Account acc : accounts) {
            if (acc.type.equals(SyncUtils.ACCOUNT_TYPE)) {
                Bundle b = new Bundle();
                b.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                ContentResolver.requestSync(acc, SyncUtils.AUTHORITY, b);
            }
        }
        try {
            SyncUtils.CreateAccount(this, "smile", "1234");
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(PhoneLoginActivity.this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }

    private void registerReceiver() {
        if (!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(PhoneLoginActivity.this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @OnClick(R.id.btn_verify)
    public void onVerify() {
        if (!TextUtils.isEmpty(mVerificationId)) {
            verifyPhoneNumberWithCode(mVerificationId, tvOtpNumber.getText().toString());
        }
    }
}
