package com.superdev.smiling.phonelogin.service;

import com.google.firebase.auth.PhoneAuthCredential;
import com.superdev.smiling.login.data_model.Authentication;

import rx.Observable;

/**
 * Created by gautam on 9/2/17.
 */

public interface PhoneLoginService {

    Observable<Authentication> getAuthentication();

    void loginWithCredential(PhoneAuthCredential phoneAuthCredential);

}
