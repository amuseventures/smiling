package com.superdev.smiling.phonelogin.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.superdev.smiling.BaseAdapter;
import com.superdev.smiling.R;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by gautam on 9/3/17.
 */

public class CountryAdapter extends BaseAdapter<CountryAdapter.CountryViewHolder, CountryAdapter.Country> implements FastScrollRecyclerView.SectionedAdapter {

    private List<Country> countryList = new ArrayList<>();

    private ArrayList<String> sortedCountries = new ArrayList<>();

    private static final String TAG = "CountryAdapter";

    public CountryAdapter(Context context) {
        try {
            InputStream stream = context.getResources().getAssets().open("countries.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] args = line.split(";");
                Country c = new Country();
                c.name = args[2];
                c.code = args[0];
                c.shortname = args[1];
                String n = c.name.substring(0, 1).toUpperCase();
                countryList.add(c);
                sortedCountries.add(n);
            }
            reader.close();
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }

        Collections.sort(sortedCountries, new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return lhs.compareTo(rhs);
            }
        });


        Collections.sort(countryList, new Comparator<Country>() {
            @Override
            public int compare(Country country, Country country2) {
                return country.name.compareTo(country2.name);
            }
        });
    }

    @Override
    public void setData(List<Country> data) {

    }

    public List<Country> getCountryList() {
        return countryList;
    }

    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.country_row_item, parent, false);
        return new CountryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CountryViewHolder holder, int position) {
        final Country country = countryList.get(position);
        holder.tvCountryName.setText(country.name);
        holder.tvCountryCode.setText(String.format("+%s", country.code));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListener.onItemClick(country, view);
            }
        });

    }

    @Override
    public int getItemCount() {
        return countryList != null && !countryList.isEmpty() ? countryList.size() : 0;
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        return sortedCountries.get(position);
    }

    static class CountryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_country_name)
        TextView tvCountryName;

        @BindView(R.id.tv_country_code)
        TextView tvCountryCode;

        public CountryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class Country {
        public String name;
        public String code;
        public String shortname;
    }
}
