package com.superdev.smiling.phonelogin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.superdev.smiling.BaseAdapter;
import com.superdev.smiling.Constants;
import com.superdev.smiling.R;
import com.superdev.smiling.phonelogin.view.CountryAdapter;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountryListActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.search_view)
    MaterialSearchView searchView;

    @BindView(R.id.recycler)
    FastScrollRecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_list);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getString(R.string.select_country_title));
        }
        initRecyclerView();
    }

    private void initRecyclerView() {
        CountryAdapter countryAdapter = new CountryAdapter(this);
        countryAdapter.setItemClickListener(new BaseAdapter.ItemClickListener<CountryAdapter.Country>() {
            @Override
            public void onItemClick(CountryAdapter.Country object, View view) {
                Intent intent = new Intent();
                intent.putExtra(Constants.COUNTRY_DATA_EXTRA, object.name);
                setResult(RESULT_OK, intent);
                onBackPressed();
            }

            @Override
            public void onItemLongClick(CountryAdapter.Country object, View view) {

            }
        });
        recycler.setAdapter(countryAdapter);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setHasFixedSize(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
