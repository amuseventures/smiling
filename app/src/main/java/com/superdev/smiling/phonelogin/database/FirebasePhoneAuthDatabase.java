package com.superdev.smiling.phonelogin.database;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.iid.FirebaseInstanceId;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Remember;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.user.data_model.User;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by gautam on 9/2/17.
 */

public class FirebasePhoneAuthDatabase implements PhoneAuthDatabase {

    private final FirebaseAuth firebaseAuth;

    public FirebasePhoneAuthDatabase(FirebaseAuth firebaseAuth) {
        this.firebaseAuth = firebaseAuth;
    }

    @Override
    public Observable<Authentication> readAuthentication() {
        return Observable.create(new Observable.OnSubscribe<Authentication>() {
            @Override
            public void call(Subscriber<? super Authentication> subscriber) {
                FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                if (currentUser != null) {
                    subscriber.onNext(authenticationFrom(currentUser));
                }
                subscriber.onCompleted();
            }
        });
    }

    @Override
    public Observable<Authentication> loginWithPhoneNumber(final PhoneAuthCredential credential) {
        return Observable.create(new Observable.OnSubscribe<Authentication>() {
            @Override
            public void call(final Subscriber<? super Authentication> subscriber) {
                firebaseAuth.signInWithCredential(credential)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    FirebaseUser firebaseUser = task.getResult().getUser();
                                    subscriber.onNext(authenticationFrom(firebaseUser));
                                } else {
                                    subscriber.onNext(new Authentication(task.getException()));
                                }
                                subscriber.onCompleted();
                            }
                        });
            }
        });
    }

    private Authentication authenticationFrom(FirebaseUser currentUser) {
        Uri photoUrl = currentUser.getPhotoUrl();
        Remember.putString(Constants.FIREBASE_USERS_PHONE, currentUser.getPhoneNumber());
        return new Authentication(new User(currentUser.getUid(), currentUser.getDisplayName(), photoUrl == null ? "" : photoUrl.toString(), currentUser.getPhoneNumber(), FirebaseInstanceId.getInstance().getToken()));
    }
}
