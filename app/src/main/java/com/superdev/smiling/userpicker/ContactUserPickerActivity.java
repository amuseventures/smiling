package com.superdev.smiling.userpicker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.sinch.android.rtc.SinchError;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.navigation.AndroidConversationsNavigator;
import com.superdev.smiling.navigation.AndroidNavigator;
import com.superdev.smiling.sinch.SinchBaseActivity;
import com.superdev.smiling.sinch.SinchService;
import com.superdev.smiling.userpicker.presenter.ContactUserPresenter;

public class ContactUserPickerActivity extends SinchBaseActivity implements SinchService.StartFailedListener {

    private ContactUserPresenter presenter;

    private AndroidConversationsNavigator navigator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_user_picker);
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        navigator = new AndroidConversationsNavigator(this, new AndroidNavigator(this));
        presenter = new ContactUserPresenter(
                findViewById(R.id.usersView),
                navigator,
                Dependencies.INSTANCE.getPhoneLoginService(),
                Dependencies.INSTANCE.getUserService()
        );
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter("SEARCH_RECENT_CALLS");
        registerReceiver(searchReceiver, filter);

        if (getSinchServiceInterface() != null && !getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(Remember.getString(Constants.FIREBASE_USERS_PHONE, ""));
        } else {
            presenter.setSinchService(getSinchServiceInterface());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.stopPresenting();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(searchReceiver);
    }

    @Override
    protected void onServiceConnected() {
        if (getSinchServiceInterface() != null && getSinchServiceInterface().isStarted()) {
            presenter.setSinchService(getSinchServiceInterface());
        } else {
            getSinchServiceInterface().startClient(Remember.getString(Constants.FIREBASE_USERS_PHONE, ""));
            getSinchServiceInterface().setStartListener(this);
        }
    }

    @Override
    protected void onServiceDisconnected() {

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private final BroadcastReceiver searchReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String text = intent.getStringExtra("search");
            presenter.filterUsers(text);
        }
    };

    @Override
    public void onStartFailed(SinchError error) {

    }

    @Override
    public void onStarted() {
        presenter.setSinchService(getSinchServiceInterface());
    }
}
