package com.superdev.smiling.userpicker.presenter;

import android.os.Bundle;
import android.view.View;

import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.navigation.AndroidConversationsNavigator;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.sinch.SinchService;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;
import com.superdev.smiling.user.service.UserService;
import com.superdev.smiling.user.view.UsersDisplayer;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by gautam on 9/28/17.
 */

public class UserPickerPresenter {

    private static final String SENDER = "sender";
    private static final String DESTINATION = "destination";

    private UsersDisplayer usersDisplayer;
    private AndroidConversationsNavigator navigator;
    private PhoneLoginService loginService;
    private UserService userService;

    private Subscription loginSubscription;
    private Subscription userSubscription;

    private SinchService.SinchServiceInterface mSinchService;

    private User self;

    public UserPickerPresenter(
            UsersDisplayer conversationListDisplayer,
            AndroidConversationsNavigator navigator,
            PhoneLoginService loginService,
            UserService userService) {
        this.usersDisplayer = conversationListDisplayer;
        this.navigator = navigator;
        this.loginService = loginService;
        this.userService = userService;
    }

    public void startPresenting() {
        usersDisplayer.attach(conversationInteractionListener);
        usersDisplayer.showProgress();
        final Subscriber usersSubscriber = new Subscriber<Users>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(Users users) {
                usersDisplayer.hideProgress();
                usersDisplayer.display(users);
            }
        };
        Subscriber userSubscriber = new Subscriber<Authentication>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(Authentication authentication) {
                userSubscription = userService.getRegisteredUsers()
                        .subscribe(usersSubscriber);
            }
        };

        loginSubscription = loginService.getAuthentication()
                .filter(successfullyAuthenticated())
                .doOnNext(new Action1<Authentication>() {
                    @Override
                    public void call(Authentication authentication) {
                        self = authentication.getUser();
                    }
                })
                .subscribe(userSubscriber);
    }

    public void setSinchService(SinchService.SinchServiceInterface mSinchService) {
        this.mSinchService = mSinchService;
    }

    public void stopPresenting() {
        usersDisplayer.detach(conversationInteractionListener);
        loginSubscription.unsubscribe();
        if (userSubscription != null)
            userSubscription.unsubscribe();
    }

    private Func1<Authentication, Boolean> successfullyAuthenticated() {
        return new Func1<Authentication, Boolean>() {
            @Override
            public Boolean call(Authentication authentication) {
                return authentication.isSuccess();
            }
        };
    }

    private final UsersDisplayer.UserInteractionListener conversationInteractionListener = new UsersDisplayer.UserInteractionListener() {

        @Override
        public void onUserSelected(User user) {
        }

        @Override
        public void onCallSelected(User user) {
        }

        @Override
        public void onVideoSelected(User user) {
        }

        @Override
        public void onImageSelected(View view, User user) {
        }

        @Override
        public void onContactSelected(User user) {
            Bundle bundle = new Bundle();
            bundle.putString(SENDER, self.getUid());
            bundle.putString(DESTINATION, user.getUid());
            navigator.toSelectedConversation(bundle);
        }
    };

    public void filterUsers(String text) {
        usersDisplayer.filter(text);
    }
}
