package com.superdev.smiling.userpicker.adapters;

import android.support.v7.widget.RecyclerView;

import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.userpicker.view.ContactUserPickerView;

/**
 * Created by gautam on 9/29/17.
 */

public class ContactUserPickerViewHolder extends RecyclerView.ViewHolder {

    ContactUserPickerView itemView;

    public ContactUserPickerViewHolder(ContactUserPickerView itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    public void bind(User user, ContactUserPickerViewHolder.UserSelectionListener listener) {
        itemView.displayContact(user);
        itemView.getContactPickerCallButton().setOnClickListener(view -> listener.onCallClicked(user));
        itemView.getContactPickerVideoCallButton().setOnClickListener(view -> listener.onVideoCallClicked(user));
    }

    interface UserSelectionListener {

        void onCallClicked(User user);

        void onVideoCallClicked(User user);
    }

}
