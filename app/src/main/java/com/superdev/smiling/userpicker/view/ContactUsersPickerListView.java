package com.superdev.smiling.userpicker.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.superdev.smiling.R;
import com.superdev.smiling.user.data_model.Users;
import com.superdev.smiling.userpicker.adapters.ContactUserPickerAdapter;
import com.superdev.smiling.userpicker.displayer.ContactUserDisplayer;
import com.superdev.smiling.views.EmptyRecyclerView;
import com.superdev.smiling.views.TextViewWithImages;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by gautam on 9/29/17.
 */

public class ContactUsersPickerListView extends LinearLayout implements ContactUserDisplayer {

    private final ContactUserPickerAdapter contactUserPickerAdapter;

    private ContactUserPickerAdapter contactUserPickerFilteredAdapter;

    private ContactUserDisplayer.UserInteractionListener usersInteractionListener;

    @BindView(R.id.tvTitle)
    EmojiconTextView tvTitle;

    @BindView(R.id.tvSubTitle)
    EmojiconTextView tvSubTitle;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.conversationsRecyclerView)
    EmptyRecyclerView conversations;

    @BindView(R.id.welcome_chats_message)
    TextViewWithImages view;

    @BindView(R.id.btn_float)
    FloatingActionButton btnFloat;

    public ContactUsersPickerListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
        contactUserPickerAdapter = new ContactUserPickerAdapter(LayoutInflater.from(context));
        contactUserPickerFilteredAdapter = new ContactUserPickerAdapter(LayoutInflater.from(context));
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_user_picker_list_view, this);
        ButterKnife.bind(this);
        tvTitle.setText(R.string.select_contacts);
        btnFloat.setVisibility(GONE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        conversations.setLayoutManager(layoutManager);
        conversations.setAdapter(contactUserPickerAdapter);
    }

    @Override
    public void display(Users users) {
        contactUserPickerAdapter.update(users);
        if (users.getUsers() != null && !users.getUsers().isEmpty())
            tvSubTitle.setText(String.format(Locale.getDefault(), getContext().getString(R.string.total_contacts), users.getUsers().size()));
        else
            tvSubTitle.setText(R.string.no_contacts_found);
    }

    @Override
    public void attach(ContactUserDisplayer.UserInteractionListener userInteractionListener) {
        this.usersInteractionListener = userInteractionListener;
        contactUserPickerAdapter.attach(userInteractionListener);
        contactUserPickerFilteredAdapter.attach(usersInteractionListener);
    }

    @Override
    public void detach(ContactUserDisplayer.UserInteractionListener userInteractionListener) {
        this.usersInteractionListener = null;
        contactUserPickerAdapter.detach(userInteractionListener);
        contactUserPickerFilteredAdapter.detach(userInteractionListener);
    }

    @Override
    public void filter(String text) {
        if (text.equals(""))
            conversations.setAdapter(contactUserPickerAdapter);
        else {
            contactUserPickerFilteredAdapter.update(contactUserPickerAdapter.getUsers());
            contactUserPickerFilteredAdapter.filter(text);
            conversations.setAdapter(contactUserPickerFilteredAdapter);
        }
    }
}
