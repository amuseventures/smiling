package com.superdev.smiling.userpicker.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.user.data_model.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by gautam on 9/29/17.
 */

public class ContactUserPickerView extends FrameLayout {

    @BindView(R.id.contactpicker_row_photo)
    CircleImageView contactPickerRowPhoto;

    @BindView(R.id.contact_selector)
    FrameLayout contactSelector;

    @BindView(R.id.invite)
    TextView invite;

    @BindView(R.id.contactpicker_call_button)
    ImageButton contactPickerCallButton;

    @BindView(R.id.contactpicker_videocall_button)
    ImageButton contactPickerVideoCallButton;

    @BindView(R.id.buttons)
    LinearLayout buttons;

    @BindView(R.id.contactpicker_row_name)
    EmojiconTextView contactPickerRowName;

    @BindView(R.id.contactpicker_row_phone_type)
    TextView contactPickerRowPhoneType;

    @BindView(R.id.contactpicker_row_status)
    EmojiconTextView contactPickerRowStatus;

    @BindView(R.id.callsfragment_contactpicker_row_phone_type)
    EmojiconTextView callsFragmentContactPickerRowPhoneType;

    private int layoutResId;

    public ContactUserPickerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int[] attrsArray = {
                    android.R.attr.layout
            };
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            layoutResId = array.getResourceId(0, R.layout.merge_contact_user_picker_view);
            array.recycle();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), layoutResId, this);
        ButterKnife.bind(this);
    }

    public void displayContact(User user) {
        if (user.getImage() != null) {
            Utils.loadImageElseBlack(user.getImage(), contactPickerRowPhoto, getContext(),R.drawable.avatar_contact_large);
        } else {
            Utils.loadImageElseBlack(user.getImage(), contactPickerRowPhoto, getContext(),R.drawable.avatar_contact_large);
        }
        contactPickerCallButton.setVisibility(VISIBLE);

        contactPickerVideoCallButton.setVisibility(VISIBLE);

        contactPickerRowName.setText(user.getName());

        contactPickerRowStatus.setVisibility(GONE);

        contactPickerRowPhoneType.setVisibility(GONE);

        callsFragmentContactPickerRowPhoneType.setText(user.getPhoneTypeString());
    }

    public ImageButton getContactPickerCallButton() {
        return contactPickerCallButton;
    }

    public ImageButton getContactPickerVideoCallButton() {
        return contactPickerVideoCallButton;
    }
}
