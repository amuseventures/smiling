package com.superdev.smiling.userpicker.adapters;

import android.support.v7.widget.RecyclerView;

import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.userpicker.view.UserPickerView;

/**
 * Created by gautam on 9/28/17.
 */

public class UserPickerViewHolder extends RecyclerView.ViewHolder {

    private UserPickerView itemView;

    public UserPickerViewHolder(UserPickerView itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    public void bind(User user, UserPickerViewHolder.UserSelectionListener listener) {
        itemView.displayContact(user);
        itemView.setOnClickListener(view -> listener.onUserPick(user));
    }

    interface UserSelectionListener {

        void onUserPick(User user);
    }
}
