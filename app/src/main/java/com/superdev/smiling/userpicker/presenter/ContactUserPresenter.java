package com.superdev.smiling.userpicker.presenter;

import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.navigation.AndroidConversationsNavigator;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.sinch.SinchService;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;
import com.superdev.smiling.user.service.UserService;
import com.superdev.smiling.userpicker.displayer.ContactUserDisplayer;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by gautam on 9/29/17.
 */

public class ContactUserPresenter {

    private static final String SENDER = "sender";
    private static final String DESTINATION = "destination";

    private ContactUserDisplayer contactUserDisplayer;
    private AndroidConversationsNavigator navigator;
    private PhoneLoginService loginService;
    private UserService userService;

    private Subscription loginSubscription;
    private Subscription userSubscription;

    private SinchService.SinchServiceInterface mSinchService;

    private User self;

    public ContactUserPresenter(
            ContactUserDisplayer contactUserDisplayer,
            AndroidConversationsNavigator navigator,
            PhoneLoginService loginService,
            UserService userService) {
        this.contactUserDisplayer = contactUserDisplayer;
        this.navigator = navigator;
        this.loginService = loginService;
        this.userService = userService;
    }

    public void startPresenting() {
        contactUserDisplayer.attach(conversationInteractionListener);

        final Subscriber usersSubscriber = new Subscriber<DatabaseResult<Users>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onNext(DatabaseResult<Users> usersDatabaseResult) {
                if (usersDatabaseResult.isSuccess()) {
                    Users users = usersDatabaseResult.getData();
                    contactUserDisplayer.display(users);
                }
            }
        };
        Subscriber userSubscriber = new Subscriber<Authentication>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(Authentication authentication) {
                userSubscription = userService.getRegisteredUsers()
                        .subscribe(usersSubscriber);
            }
        };

        loginSubscription = loginService.getAuthentication()
                .filter(successfullyAuthenticated())
                .doOnNext(authentication -> self = authentication.getUser())
                .subscribe(userSubscriber);
    }

    public void setSinchService(SinchService.SinchServiceInterface mSinchService) {
        this.mSinchService = mSinchService;
    }

    public void stopPresenting() {
        contactUserDisplayer.detach(conversationInteractionListener);
        loginSubscription.unsubscribe();
        if (userSubscription != null)
            userSubscription.unsubscribe();
    }

    private Func1<Authentication, Boolean> successfullyAuthenticated() {
        return new Func1<Authentication, Boolean>() {
            @Override
            public Boolean call(Authentication authentication) {
                return authentication.isSuccess();
            }
        };
    }

    private final ContactUserDisplayer.UserInteractionListener conversationInteractionListener = new ContactUserDisplayer.UserInteractionListener() {
        @Override
        public void onCallSelected(User user) {
            navigator.placeCall(mSinchService, user);
        }

        @Override
        public void onVideoSelected(User user) {
            navigator.placeVideoCall(mSinchService, user);
        }

    };

    public void filterUsers(String text) {
        contactUserDisplayer.filter(text);
    }
}
