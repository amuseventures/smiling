package com.superdev.smiling.userpicker.displayer;

import android.view.View;

import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;
import com.superdev.smiling.user.view.UsersDisplayer;

/**
 * Created by gautam on 9/29/17.
 */

public interface ContactUserDisplayer {

    void display(Users users);

    void attach(ContactUserDisplayer.UserInteractionListener userInteractionListener);

    void detach(ContactUserDisplayer.UserInteractionListener userInteractionListener);

    interface UserInteractionListener {

        void onCallSelected(User user);

        void onVideoSelected(User user);

    }

    void filter(String text);
}
