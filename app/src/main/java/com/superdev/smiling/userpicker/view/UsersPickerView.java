package com.superdev.smiling.userpicker.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.superdev.smiling.R;
import com.superdev.smiling.user.data_model.Users;
import com.superdev.smiling.user.view.UsersDisplayer;
import com.superdev.smiling.userpicker.adapters.UserPickerAdapter;
import com.superdev.smiling.views.EmptyRecyclerView;
import com.superdev.smiling.views.TextViewWithImages;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by gautam on 9/28/17.
 */

public class UsersPickerView extends LinearLayout implements UsersDisplayer {

    private final UserPickerAdapter userPickerAdapter;

    private UserPickerAdapter userPickerFilteredAdapter;

    private UserInteractionListener usersInteractionListener;

    @BindView(R.id.tvTitle)
    EmojiconTextView tvTitle;

    @BindView(R.id.tvSubTitle)
    EmojiconTextView tvSubTitle;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.conversationsRecyclerView)
    EmptyRecyclerView conversations;

    @BindView(R.id.welcome_chats_message)
    TextViewWithImages view;

    @BindView(R.id.btn_float)
    FloatingActionButton btnFloat;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    public UsersPickerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
        userPickerAdapter = new UserPickerAdapter(LayoutInflater.from(context));
        userPickerFilteredAdapter = new UserPickerAdapter(LayoutInflater.from(context));
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_user_picker_list_view, this);
        ButterKnife.bind(this);
        tvTitle.setText(R.string.select_contacts);
        btnFloat.setVisibility(GONE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        conversations.setLayoutManager(layoutManager);
        conversations.setAdapter(userPickerAdapter);
    }

    @Override
    public void display(Users users) {
        userPickerAdapter.update(users);
        if (users.getUsers() != null && !users.getUsers().isEmpty())
            tvSubTitle.setText(String.format(Locale.getDefault(), "%s contacts", users.getUsers().size()));
    }

    @Override
    public void attach(UserInteractionListener userInteractionListener) {
        this.usersInteractionListener = userInteractionListener;
        userPickerAdapter.attach(userInteractionListener);
        userPickerFilteredAdapter.attach(usersInteractionListener);
    }

    @Override
    public void detach(UserInteractionListener userInteractionListener) {
        this.usersInteractionListener = null;
        userPickerAdapter.detach(userInteractionListener);
        userPickerFilteredAdapter.detach(userInteractionListener);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(GONE);
    }

    @Override
    public void filter(String text) {
        if (text.equals(""))
            conversations.setAdapter(userPickerAdapter);
        else {
            userPickerFilteredAdapter.update(userPickerAdapter.getUsers());
            userPickerFilteredAdapter.filter(text);
            conversations.setAdapter(userPickerFilteredAdapter);
        }
    }
}
