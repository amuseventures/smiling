package com.superdev.smiling.global.presenter;

import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.global.data_model.Chat;
import com.superdev.smiling.global.data_model.Message;
import com.superdev.smiling.global.service.GlobalService;
import com.superdev.smiling.global.view.GlobalDisplayer;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.navigation.Navigator;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.data_model.Users;
import com.superdev.smiling.user.service.UserService;

import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func2;

/**
 * Created by marco on 08/08/16.
 */

public class GlobalPresenter {

    private final PhoneLoginService loginService;
    private final GlobalService globalService;
    private final GlobalDisplayer globalDisplayer;
    private final UserService userService;
    private final Navigator navigator;

    private User user;

    private Subscription subscription;

    public GlobalPresenter(
            PhoneLoginService loginService,
            GlobalService globalService,
            GlobalDisplayer globalDisplayer,
            UserService userService,
            Navigator navigator
    ) {
        this.loginService = loginService;
        this.globalService = globalService;
        this.globalDisplayer = globalDisplayer;
        this.userService = userService;
        this.navigator = navigator;
    }

    public void startPresenting() {
        globalDisplayer.attach(actionListener);
        globalDisplayer.disableInteraction();

        final Subscriber messagesSubscriber = new Subscriber<Message>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(final Message message) {
                if (message == null)
                    return;
                userService.getUser(message.getUid())
                        .subscribe(userDatabaseResult -> {
                            if (userDatabaseResult.isSuccess()) {
                                globalDisplayer.addToDisplay(message, userDatabaseResult.getData(), user);
                            }
                        });
            }
        };

        final Subscriber chatSubscriber = new Subscriber<DatabaseResult<Chat>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(final DatabaseResult<Chat> chatDatabaseResult) {
                final Chat chat = chatDatabaseResult.getData();
                userService.getUsers()
                        .subscribe(new Observer<DatabaseResult<Users>>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {

                            }

                            @Override
                            public void onNext(DatabaseResult<Users> usersDatabaseResult) {
                                if (usersDatabaseResult.isSuccess()) {
                                    Users users = usersDatabaseResult.getData();
                                    globalDisplayer.display(chat, users, user);
                                    globalService.syncMessages()
                                            .subscribe(messagesSubscriber);
                                }
                            }
                        });
            }
        };

        loginService.getAuthentication()
                .subscribe(new Subscriber<Authentication>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Authentication authentication) {
                        if (authentication.isSuccess()) {
                            user = authentication.getUser();
                            subscription = globalService.getChat()
                                    .subscribe(chatSubscriber);
                        }
                    }
                });

    }

    public void stopPresenting() {
        globalDisplayer.detach(actionListener);
        subscription.unsubscribe();
    }

    private final GlobalDisplayer.GlobalActionListener actionListener = new GlobalDisplayer.GlobalActionListener() {

        @Override
        public void onUpPressed() {
            navigator.toParent();
        }

        @Override
        public void onMessageLengthChanged(int messageLength) {
            if (messageLength > 0) {
                globalDisplayer.enableInteraction();
            } else {
                globalDisplayer.disableInteraction();
            }
        }

        @Override
        public void onSubmitMessage(String message) {
            if (user != null)
                globalService.sendMessage(new Message(user.getUid(), message));
        }

    };

    static class Pair {

        public final DatabaseResult<Chat> conversationDatabaseResult;
        public final Authentication auth;

        private Pair(DatabaseResult<Chat> conversationDatabaseResult, Authentication auth) {
            this.conversationDatabaseResult = conversationDatabaseResult;
            this.auth = auth;
        }

        static Func2<DatabaseResult<Chat>, Authentication, Pair> asPair() {
            return new Func2<DatabaseResult<Chat>, Authentication, Pair>() {
                @Override
                public Pair call(DatabaseResult<Chat> chatDatabaseResult, Authentication authentication) {
                    return new Pair(chatDatabaseResult, authentication);
                }
            };
        }

    }

}
