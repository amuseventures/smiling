package com.superdev.smiling.global.service;

import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.global.data_model.Chat;
import com.superdev.smiling.global.data_model.Message;
import com.superdev.smiling.global.database.GlobalDatabase;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by marco on 08/08/16.
 */

public class PersistedGlobalService implements GlobalService {

    private final GlobalDatabase globalDatabase;

    public PersistedGlobalService(GlobalDatabase globalDatabase) {
        this.globalDatabase = globalDatabase;
    }

    @Override
    public Observable<Message> syncMessages() {
        return globalDatabase.observeAddMessage();
    }

    @Override
    public Observable<DatabaseResult<Chat>> getChat() {
        return globalDatabase.observeChat()
                .map(asDatabaseResult())
                .onErrorReturn(DatabaseResult.errorAsDatabaseResult());
    }

    private static Func1<Chat, DatabaseResult<Chat>> asDatabaseResult() {
        return DatabaseResult::new;
    }

    @Override
    public void sendMessage(Message message) {
        globalDatabase.sendMessage(message);
    }

}
