package com.superdev.smiling.global.database;

import com.superdev.smiling.global.data_model.Chat;
import com.superdev.smiling.global.data_model.Message;

import rx.Observable;

/**
 * Created by marco on 08/08/16.
 */

public interface GlobalDatabase {

    Observable<Message> observeAddMessage();

    Observable<Chat> observeChat();

    void sendMessage(Message message);

}

