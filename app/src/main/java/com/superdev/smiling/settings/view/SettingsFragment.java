package com.superdev.smiling.settings.view;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.RingtonePreference;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.colorpicker.ColorPickerDialog;
import com.superdev.smiling.colorpicker.ColorPickerSwatch;

/**
 * Created by gautam on 29/10/17.
 */

public class SettingsFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener {

    public static final String PREF_NOTIFICATION_KEY = "pref_key_notifications";
    public static final String PREF_LED_KEY = "pref_key_led";
    public static final String PREF_THEME_LED_KEY = "pref_key_theme_led";
    public static final String PREF_WAKE_KEY = "pref_key_wake";
    public static final String PREF_TICKER_KEY = "pref_key_ticker";
    public static final String PREF_VIBRATION_KEY = "pref_key_vibration";

    private int[] mLedColors;


    private ColorPickerDialog mLedColorPickerDialog;

    private CheckBoxPreference notificationPref, notificationLed, notificationWake, notificationTicker, notificationVibration;
    private Preference notificationThemeLed;

    public static SettingsFragment newInstance() {
        Bundle args = new Bundle();
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_notifications);

        notificationPref = (CheckBoxPreference) findPreference(PREF_NOTIFICATION_KEY);
        notificationLed = (CheckBoxPreference) findPreference(PREF_LED_KEY);
        notificationThemeLed = findPreference(PREF_THEME_LED_KEY);
        notificationWake = (CheckBoxPreference) findPreference(PREF_WAKE_KEY);
        notificationTicker = (CheckBoxPreference) findPreference(PREF_TICKER_KEY);
        notificationVibration = (CheckBoxPreference) findPreference(PREF_VIBRATION_KEY);

        notificationPref.setChecked(Remember.getBoolean(PREF_NOTIFICATION_KEY, true));
        notificationLed.setChecked(Remember.getBoolean(PREF_LED_KEY, true));
        notificationWake.setChecked(Remember.getBoolean(PREF_WAKE_KEY, false));
        notificationTicker.setChecked(Remember.getBoolean(PREF_TICKER_KEY, true));
        notificationVibration.setChecked(Remember.getBoolean(PREF_VIBRATION_KEY, true));

        mLedColors = new int[]{
                ContextCompat.getColor(getActivity(), R.color.blue_light), ContextCompat.getColor(getActivity(), R.color.purple_light),
                ContextCompat.getColor(getActivity(), R.color.green_light), ContextCompat.getColor(getActivity(), R.color.yellow_light),
                ContextCompat.getColor(getActivity(), R.color.red_light), ContextCompat.getColor(getActivity(), R.color.white_pure)
        };

        mLedColorPickerDialog = new ColorPickerDialog();
        mLedColorPickerDialog.initialize(R.string.pref_theme_led, mLedColors, Integer.parseInt(Remember.getString(PREF_THEME_LED_KEY, "-48060")), 3, 2);
        mLedColorPickerDialog.setOnColorSelectedListener(color -> Remember.putString(PREF_THEME_LED_KEY, "" + color));
        notificationThemeLed.setOnPreferenceClickListener(this);

        notificationPref.setOnPreferenceChangeListener(this);
        notificationLed.setOnPreferenceChangeListener(this);
        notificationWake.setOnPreferenceChangeListener(this);
        notificationTicker.setOnPreferenceChangeListener(this);
        notificationVibration.setOnPreferenceChangeListener(this);

    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String key = preference.getKey();
        switch (key) {
            case PREF_NOTIFICATION_KEY:
                notificationPref.setChecked((Boolean) newValue);
                Remember.putBoolean(PREF_NOTIFICATION_KEY, (Boolean) newValue);
                break;
            case PREF_LED_KEY:
                notificationLed.setChecked((Boolean) newValue);
                Remember.putBoolean(PREF_LED_KEY, (Boolean) newValue);
                break;
            case PREF_WAKE_KEY:
                notificationWake.setChecked((Boolean) newValue);
                Remember.putBoolean(PREF_WAKE_KEY, (Boolean) newValue);
                break;
            case PREF_TICKER_KEY:
                notificationTicker.setChecked((Boolean) newValue);
                Remember.putBoolean(PREF_TICKER_KEY, (Boolean) newValue);
                break;
            case PREF_VIBRATION_KEY:
                notificationVibration.setChecked((Boolean) newValue);
                Remember.putBoolean(PREF_VIBRATION_KEY, (Boolean) newValue);
                break;
        }
        return false;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        String key = preference.getKey() != null ? preference.getKey() : "";
        switch (key) {
            case PREF_THEME_LED_KEY:
                mLedColorPickerDialog.show(getActivity().getFragmentManager(), "colorpicker");
                break;
        }
        return false;
    }
}
