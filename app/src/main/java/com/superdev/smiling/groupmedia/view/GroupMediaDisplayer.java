package com.superdev.smiling.groupmedia.view;

import com.superdev.smiling.groupconversation.data_model.GroupChat;

/**
 * Created by gautam on 28/10/17.
 */

public interface GroupMediaDisplayer {

    void display(GroupChat chat);

    void showProgress();

    void hideProgress();
}
