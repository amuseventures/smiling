package com.superdev.smiling.groupmedia.view;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.groupconversation.data_model.GroupChat;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.groupmedia.presenter.GroupDocumentPresenter;
import com.superdev.smiling.views.EmptyRecyclerView;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by gautam on 28/10/17.
 */

public class GroupDocumentFragment extends Fragment implements GroupMediaDisplayer, AllGroupDocumentAdapter.OnItemClickListener {

    @BindView(R.id.media_recycler)
    EmptyRecyclerView mediaRecycler;

    @BindView(R.id.progressbar)
    CircularProgressView progressbar;

    private GroupDocumentPresenter presenter;

    private AllGroupDocumentAdapter adapter;

    private Channel channel;

    Unbinder unbinder;

    public static GroupDocumentFragment newInstance(Channel channel) {
        Bundle args = new Bundle();
        args.putParcelable(Constants.FIREBASE_CHANNELS, channel);
        GroupDocumentFragment fragment = new GroupDocumentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        channel = getArguments().getParcelable(Constants.FIREBASE_CHANNELS);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mdeia, container, false);
        unbinder = ButterKnife.bind(this, view);
        initRecyclerView();
        presenter = new GroupDocumentPresenter(
                Dependencies.INSTANCE.getGroupConversationService(),
                channel,
                this
        );
        return view;
    }

    private void initRecyclerView() {
        adapter = new AllGroupDocumentAdapter();
        adapter.setOnItemClickListener(this);
        mediaRecycler.setAdapter(adapter);
        mediaRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mediaRecycler.setHasFixedSize(true);
    }


    @Override
    public void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.stopPresenting();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void display(GroupChat chat) {
        adapter.setData(chat.getGroupMessages());
    }

    @Override
    public void showProgress() {
        progressbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressbar.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(GroupMessage message) {
        File file;
        if (!Remember.getString(Constants.FIREBASE_USER_ID, "").equalsIgnoreCase(message.getAuthor().getUid())) {
            file = new File(Utils.getDocumentPath() + "/" + message.getDocModel().getDocName());
        } else {
            file = new File(Utils.getSentDocumentPath() + "/" + message.getDocModel().getDocName());
        }
        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, message.getDocModel().getMimeType());
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getContext(), "No Application Available to View", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
