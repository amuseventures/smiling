package com.superdev.smiling.groupmedia.presenter;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.channel.service.ChannelService;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.groupconversation.data_model.GroupChat;
import com.superdev.smiling.groupconversation.service.GroupConversationService;
import com.superdev.smiling.groupmedia.view.GroupMediaDisplayer;

import rx.Observer;
import rx.Subscription;

/**
 * Created by gautam on 28/10/17.
 */

public class GroupMediaPresenter {

    private final GroupConversationService groupConversationService;
    private final Channel channel;
    private final GroupMediaDisplayer displayer;

    private Subscription mediaSubscription;

    public GroupMediaPresenter(GroupConversationService groupConversationService, Channel channel, GroupMediaDisplayer displayer) {
        this.groupConversationService = groupConversationService;
        this.channel = channel;
        this.displayer = displayer;
    }

    public void startPresenting() {
        displayer.showProgress();
        mediaSubscription = groupConversationService.getAllGroupImageMedia(channel)
                .subscribe(new Observer<DatabaseResult<GroupChat>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        displayer.hideProgress();
                    }

                    @Override
                    public void onNext(DatabaseResult<GroupChat> groupChatDatabaseResult) {
                        displayer.hideProgress();
                        if (groupChatDatabaseResult.isSuccess()) {
                            displayer.display(groupChatDatabaseResult.getData());
                        }
                    }
                });
    }

    public void stopPresenting() {
        if (mediaSubscription != null) {
            mediaSubscription.unsubscribe();
        }
    }
}
