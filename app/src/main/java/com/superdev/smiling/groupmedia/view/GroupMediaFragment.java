package com.superdev.smiling.groupmedia.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.FullScreenImageActivity;
import com.superdev.smiling.groupconversation.data_model.GroupChat;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.groupmedia.presenter.GroupMediaPresenter;
import com.superdev.smiling.views.EmptyRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import droidninja.filepicker.utils.AndroidLifecycleUtils;
import droidninja.filepicker.utils.GridSpacingItemDecoration;

/**
 * Created by gautam on 28/10/17.
 */

public class GroupMediaFragment extends Fragment implements GroupMediaDisplayer, AllGroupMediaAdapter.OnItemClickListener {

    @BindView(R.id.media_recycler)
    EmptyRecyclerView mediaRecycler;

    @BindView(R.id.progressbar)
    CircularProgressView progressbar;

    private AllGroupMediaAdapter adapter;

    private static final int SCROLL_THRESHOLD = 30;

    private RequestManager mGlideRequestManager;

    private Channel channel;

    private GroupMediaPresenter presenter;

    Unbinder unbinder;

    public static GroupMediaFragment newInstance(Channel channel) {
        Bundle args = new Bundle();
        args.putParcelable(Constants.FIREBASE_CHANNELS, channel);
        GroupMediaFragment fragment = new GroupMediaFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGlideRequestManager = Glide.with(getContext());
        channel = getArguments().getParcelable(Constants.FIREBASE_CHANNELS);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mdeia, container, false);
        unbinder = ButterKnife.bind(this, view);
        initRecyclerView();
        presenter = new GroupMediaPresenter(Dependencies.INSTANCE.getGroupConversationService(), channel, this);
        return view;
    }

    private void initRecyclerView() {
        adapter = new AllGroupMediaAdapter(getContext(), mGlideRequestManager);
        adapter.setOnItemClickListener(this);
        mediaRecycler.setAdapter(adapter);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        int spanCount = 2; // 2 columns
        int spacing = 5; // 5px
        boolean includeEdge = false;
        mediaRecycler.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        mediaRecycler.setLayoutManager(layoutManager);
        mediaRecycler.setItemAnimator(new DefaultItemAnimator());

        mediaRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // Log.d(">>> Picker >>>", "dy = " + dy);
                if (Math.abs(dy) > SCROLL_THRESHOLD) {
                    mGlideRequestManager.pauseRequests();
                } else {
                    resumeRequestsIfNotDestroyed();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    resumeRequestsIfNotDestroyed();
                }
            }
        });
    }

    private void resumeRequestsIfNotDestroyed() {
        if (!AndroidLifecycleUtils.canLoadImage(this)) {
            return;
        }
        mGlideRequestManager.resumeRequests();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.stopPresenting();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void display(GroupChat chat) {
        adapter.setData(chat.getGroupMessages());
    }

    @Override
    public void showProgress() {
        progressbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressbar.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(View view, GroupMessage message) {
        Intent intent = new Intent(getContext(), FullScreenImageActivity.class);
        intent.putExtra(Constants.GROUP_MESSAGE_EXTRA, message);
        intent.putExtra(Constants.SENDER_ID, message.getAuthor().getUid());
        View sharedView = view.findViewById(R.id.iv_photo);
        String transitionName = getContext().getString(R.string.transition_photo);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), sharedView, transitionName);
        startActivity(intent, options.toBundle());
    }
}
