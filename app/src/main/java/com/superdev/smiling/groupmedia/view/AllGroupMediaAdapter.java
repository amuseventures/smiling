package com.superdev.smiling.groupmedia.view;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.RequestManager;
import com.google.firebase.storage.StorageReference;
import com.superdev.smiling.BaseAdapter;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.Utils;
import com.superdev.smiling.common.SquaredImageView;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.profile.view.GroupMediaAdapter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by gautam on 28/10/17.
 */

public class AllGroupMediaAdapter extends BaseAdapter<AllGroupMediaAdapter.ImageViewHolder, GroupMessage> {

    private List<GroupMessage> messageList;

    private final RequestManager glide;

    private final Context context;

    private int imageSize;

    private OnItemClickListener onItemClickListener;

    public AllGroupMediaAdapter(Context context, RequestManager requestManager) {
        this.glide = requestManager;
        this.context = context;
        messageList = new ArrayList<>();
        setColumnNumber(context, 3);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageViewHolder(LayoutInflater.from(context).inflate(R.layout.item_image_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int i) {
        GroupMessage message = messageList.get(i);
        setUpImage(message, holder);
        holder.ivPhoto.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(view, message);
            }
        });
    }

    private void setColumnNumber(Context context, int columnNum) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        int widthPixels = metrics.widthPixels;
        imageSize = widthPixels / columnNum;
    }

    private void setUpImage(GroupMessage message, ImageViewHolder holder) {
        File file;
        if (!Remember.getString(Constants.FIREBASE_USER_ID, "").equalsIgnoreCase(message.getAuthor().getUid())) {
            file = new File(Utils.getImagePath() + "/" + message.getFileModel().getFileName());
        } else {
            file = new File(Utils.getImageSentPath() + "/" + message.getFileModel().getFileName());
        }
        if (file.exists()) {
            setImage(file, holder);
        } else if (message.getFileModel().getFileUri() != null) {
            File file1 = new File(Utils.getRealPathFromURI(holder.ivPhoto.getContext(), Uri.parse(message.getFileModel().getFileUri())));
            setImage(file1, holder);
        } else {
            StorageReference storageRef = Dependencies.INSTANCE.getStorageService().getChatImageReference();
            StorageReference imageRef = storageRef.child(message.getFileModel().getFileName());
            File downloadFile;
            if (!Remember.getString(Constants.FIREBASE_USER_ID, "").equalsIgnoreCase(message.getAuthor().getUid())) {
                downloadFile = new File(Utils.getImagePath() + "/" + message.getFileModel().getFileName());
            } else {
                downloadFile = new File(Utils.getImageSentPath() + "/" + message.getFileModel().getFileName());
            }
            if (!downloadFile.exists()) {
                try {
                    downloadFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (!downloadFile.exists()) {
                return;
            }
            imageRef.getFile(downloadFile)
                    .addOnFailureListener(e -> {

                    }).addOnSuccessListener(taskSnapshot -> {
                setUpImage(message, holder);
            }).addOnProgressListener(taskSnapshot -> {
            });

        }
    }

    private void setImage(File file, ImageViewHolder holder) {
        glide.load(Uri.fromFile(file))
                .centerCrop()
                .override(imageSize, imageSize)
                .placeholder(droidninja.filepicker.R.drawable.image_placeholder)
                .thumbnail(0.5f)
                .into(holder.ivPhoto);

/*        Glide.with(holder.ivPhoto.getContext())
                .load(Uri.fromFile(file))
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        holder.ivPhoto.setImageBitmap(resource);
                    }
                });*/
    }

    @Override
    public void setData(List<GroupMessage> messageList) {
        this.messageList = messageList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return messageList != null && !messageList.isEmpty() ? messageList.size() : 0;
    }

    static class ImageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_photo)
        SquaredImageView ivPhoto;

        @BindView(R.id.transparent_bg)
        View transparentBg;

        @BindView(R.id.video_icon)
        ImageView videoIcon;

        ImageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, GroupMessage message);
    }

}

