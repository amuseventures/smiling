package com.superdev.smiling;

/**
 * Created by marco on 09/09/16.
 */

public class Constants {

    public static final String FIREBASE_USERS = "users";
    public static final String FIREBASE_MESSAGES = "messages";
    public static final String FIREBASE_GLOBALMESSAGES = "globalmessages";
    public static final String FIREBASE_FCM = "fcm";
    public static final String FIREBASE_CHAT = "chat";
    public static final String FIREBASE_CONTACTS = "contacts";
    public static final String FIREBASE_NOTIFICATION = "notifications";
    public static final String FIREBASE_GROUP_NOTIFICATION = "groupnotifications";
    public static final String FIREBASE_RECENT_CALLS = "calls";
    public static final String FIREBASE_OWNERS = "owners";

    public static final String FIREBASE_USERS_NAME = "name";
    public static final String FIREBASE_USERS_UID = "uid";
    public static final String FIREBASE_USERS_IMAGE = "image";
    public static final String FIREBASE_USERS_PHONE = "phone";
    public static final String FIREBASE_USERS_LASTSEEN = "lastSeen";
    public static final String FIREBASE_USERS_FCM = "fcm";
    public static final String FIREBASE_USER_REGISTER = "register";
    public static final String FIREBASE_USER_STATUS = "status";
    public static final String FIREBASE_CHANNELS = "firebase_channels";
    public static final String FIREBASE_SELECTED_MEMBERS = "firebase_selected_members";

    public static final String FIREBASE_FCM_TOKEN = "token";
    public static final String FIREBASE_FCM_ENABLED = "enabled";

    public static final String FIREBASE_CHAT_MESSAGES = "messages";
    public static final String FIREBASE_CHAT_TYPING = "typing";
    public static final String FIREBASE_CHAT_IP_ADDRESS = "ip_address";

    public static final String FIREBASE_MESSAGE_STATUS = "messageStatus";

    public static final String CURRENT_COUNTRY_CODE = "current_country_code";
    public static final String COUNTRY_DATA_EXTRA = "country_data_extra";
    public static final String MESSAGE_EXTRA = "message_extra";
    public static final String GROUP_MESSAGE_EXTRA = "group_message_extra";

    public static final String MAIN_DIRECTORY = "Smile";
    public static final String SMILE_MEDIA_DIRECTORY = "Media";
    public static final String SMILE_IMAGES = "Smile Images";
    public static final String SMILE_WALLPAPER = "Wallpaper";
    public static final String SMILE_ANIMATED_GIFS = "Smile Animated Gifs";
    public static final String SMILE_AUDIO = "Smile Audio";
    public static final String SMILE_VOICE = "Smile Voice";
    public static final String SMILE_DOCUMENTS = "Smile Documents";
    public static final String SMILE_PROFILE_PHOTOS = "Smile Profile Photos";
    public static final String SMILE_VIDEOS = "Smile Videos";
    public static final String SMILE_SENT = "Sent";

    public static final int TYPE_OUT = 0;
    public static final int TYPE_IN = 1;
    public static final int TYPE_MISSED = 2;
    public static final int AUDIO_TYPE = 1;
    public static final int VIDEO_TYPE = 2;

    public static final String SENDER_ID = "sender";
    public static final String DESTINATION_ID = "destination";
    public static final String CONVERSATION_EXTRA = "CONVERSATION_EXTRA";
    public static final String BITMAP = "bmp";
    public static final String FIREBASE_USERS_STATUS = "status";
    public static final String FIREBASE_USER_ID = "userid";
    public static final String INTENT_ACTION_REFRESH_LIST = "intent.action.REFRESH_LIST";

    public static final long MAX_VIDEO_SIZE = 15728640L;
    public static final String CONTACT_PICK_EXTRA = "contact_pick_extra";


}
