package com.superdev.smiling.media.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.conversation.FullScreenImageActivity;
import com.superdev.smiling.conversation.data_model.Chat;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.media.presenter.MediaPresenter;
import com.superdev.smiling.views.EmptyRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import droidninja.filepicker.utils.AndroidLifecycleUtils;
import droidninja.filepicker.utils.GridSpacingItemDecoration;

/**
 * Created by gautam on 28/10/17.
 */

public class MediaFragment extends Fragment implements MediaDisplayer, AllMediaAdapter.OnItemClickListener {

    @BindView(R.id.media_recycler)
    EmptyRecyclerView mediaRecycler;

    @BindView(R.id.progressbar)
    CircularProgressView progressView;

    private static final int SCROLL_THRESHOLD = 30;

    Unbinder unbinder;

    private AllMediaAdapter adapter;

    private RequestManager mGlideRequestManager;

    private MediaPresenter presenter;

    private String sender, destination;

    public static MediaFragment newInstance(String self, String destination) {
        Bundle args = new Bundle();
        args.putString(Constants.SENDER_ID, self);
        args.putString(Constants.DESTINATION_ID, destination);
        MediaFragment fragment = new MediaFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGlideRequestManager = Glide.with(getContext());
        sender = getArguments().getString(Constants.SENDER_ID);
        destination = getArguments().getString(Constants.DESTINATION_ID);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mdeia, container, false);
        unbinder = ButterKnife.bind(this, view);
        initRecyclerView();
        presenter = new MediaPresenter(
                Dependencies.INSTANCE.getConversationService(),
                Dependencies.INSTANCE.getUserService(),
                this,
                sender,
                destination
        );
        return view;
    }

    private void initRecyclerView() {
        adapter = new AllMediaAdapter(getContext(), mGlideRequestManager);
        adapter.setOnItemClickListener(this);
        mediaRecycler.setAdapter(adapter);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        int spanCount = 2; // 2 columns
        int spacing = 5; // 5px
        boolean includeEdge = false;
        mediaRecycler.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        mediaRecycler.setLayoutManager(layoutManager);
        mediaRecycler.setItemAnimator(new DefaultItemAnimator());

        mediaRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // Log.d(">>> Picker >>>", "dy = " + dy);
                if (Math.abs(dy) > SCROLL_THRESHOLD) {
                    mGlideRequestManager.pauseRequests();
                } else {
                    resumeRequestsIfNotDestroyed();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    resumeRequestsIfNotDestroyed();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.stopPresenting();
    }

    @Override
    public void displayMedia(Chat chat) {
        adapter.setData(chat.getMessages());
    }

    @Override
    public void showProgress() {
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void resumeRequestsIfNotDestroyed() {
        if (!AndroidLifecycleUtils.canLoadImage(this)) {
            return;
        }
        mGlideRequestManager.resumeRequests();
    }

    @Override
    public void onItemClick(View view, Message message) {
        Intent intent = new Intent(getContext(), FullScreenImageActivity.class);
        intent.putExtra(Constants.MESSAGE_EXTRA, message);
        View sharedView = view.findViewById(R.id.iv_photo);
        String transitionName = getContext().getString(R.string.transition_photo);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), sharedView, transitionName);
        startActivity(intent, options.toBundle());
    }
}
