package com.superdev.smiling.media.presenter;

import com.superdev.smiling.conversation.data_model.Chat;
import com.superdev.smiling.conversation.service.ConversationService;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.media.view.MediaDisplayer;
import com.superdev.smiling.user.service.UserService;

import rx.Observer;
import rx.Subscription;

/**
 * Created by gautam on 28/10/17.
 */

public class DocumentPresenter {

    private final ConversationService conversationService;

    private final UserService userService;

    private final MediaDisplayer mediaDisplayer;

    private final String self;

    private final String destination;

    private Subscription userSubscription;

    private Subscription mediaSubscription;


    public DocumentPresenter(ConversationService conversationService, UserService userService, MediaDisplayer mediaDisplayer, String self, String destination) {
        this.conversationService = conversationService;
        this.userService = userService;
        this.mediaDisplayer = mediaDisplayer;
        this.self = self;
        this.destination = destination;
    }

    public void startPresenting() {
        mediaDisplayer.showProgress();
        mediaSubscription = conversationService.syncAllDocuments(self, destination)
                .subscribe(new Observer<DatabaseResult<Chat>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        mediaDisplayer.hideProgress();
                    }

                    @Override
                    public void onNext(DatabaseResult<Chat> chatDatabaseResult) {
                        mediaDisplayer.hideProgress();
                        if (chatDatabaseResult.isSuccess()) {
                            mediaDisplayer.displayMedia(chatDatabaseResult.getData());
                        }
                    }
                });
    }

    public void stopPresenting() {
        if (mediaSubscription != null) {
            mediaSubscription.unsubscribe();
        }
    }
}
