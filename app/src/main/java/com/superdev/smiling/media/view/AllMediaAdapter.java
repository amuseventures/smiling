package com.superdev.smiling.media.view;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.RequestManager;
import com.google.firebase.storage.StorageReference;
import com.superdev.smiling.BaseAdapter;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.common.SquaredImageView;
import com.superdev.smiling.conversation.data_model.Message;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by gautam on 28/10/17.
 */

public class AllMediaAdapter extends BaseAdapter<AllMediaAdapter.ImageViewHolder, Message> {

    private List<Message> messageList;

    private final RequestManager glide;

    private final Context context;

    private int imageSize;

    private OnItemClickListener onItemClickListener;

    AllMediaAdapter(Context context, RequestManager requestManager) {
        this.glide = requestManager;
        this.context = context;
        messageList = new ArrayList<>();
        setColumnNumber(context, 3);
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageViewHolder(LayoutInflater.from(context).inflate(R.layout.item_image_layout, parent, false));
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int i) {
        Message message = messageList.get(i);
        setUpImage(message, holder);
        holder.itemView.setOnClickListener(view -> {
            if (onItemClickListener!=null){
                onItemClickListener.onItemClick(view, message);
            }
        });
    }

    private void setColumnNumber(Context context, int columnNum) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        int widthPixels = metrics.widthPixels;
        imageSize = widthPixels / columnNum;
    }

    private void setUpImage(Message message, ImageViewHolder holder) {
        File file;
        if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
            file = new File(Utils.getImagePath() + "/" + message.getFileModel().getFileName());
        } else {
            file = new File(Utils.getImageSentPath() + "/" + message.getFileModel().getFileName());
        }
        if (file.exists()) {
            setImage(file, holder);
        } else if (message.getFileModel().getFileUri() != null) {
            File file1 = new File(Utils.getRealPathFromURI(context, Uri.parse(message.getFileModel().getFileUri())));
            setImage(file1, holder);
        } else {
            StorageReference storageRef = Dependencies.INSTANCE.getStorageService().getChatImageReference();
            StorageReference imageRef = storageRef.child(message.getFileModel().getFileName());
            File downloadFile;
            if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
                downloadFile = new File(Utils.getImagePath() + "/" + message.getFileModel().getFileName());
            } else {
                downloadFile = new File(Utils.getImageSentPath() + "/" + message.getFileModel().getFileName());
            }
            if (!downloadFile.exists()) {
                try {
                    downloadFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (!downloadFile.exists()) {
                return;
            }
            imageRef.getFile(downloadFile)
                    .addOnFailureListener(e -> {

                    }).addOnSuccessListener(taskSnapshot -> {
                setUpImage(message, holder);
            }).addOnProgressListener(taskSnapshot -> {
            });
        }
    }

    private void setImage(File file, ImageViewHolder holder) {
        glide.load(Uri.fromFile(file))
                .centerCrop()
                .override(imageSize, imageSize)
                .placeholder(droidninja.filepicker.R.drawable.image_placeholder)
                .thumbnail(0.5f)
                .into(holder.ivPhoto);
    }

    @Override
    public void setData(List<Message> messageList) {
        this.messageList = messageList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return messageList != null && !messageList.isEmpty() ? messageList.size() : 0;
    }

    static class ImageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_photo)
        SquaredImageView ivPhoto;

        @BindView(R.id.transparent_bg)
        View transparentBg;

        @BindView(R.id.video_icon)
        ImageView videoIcon;

        ImageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, Message message);
    }

}
