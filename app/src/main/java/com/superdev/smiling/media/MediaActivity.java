package com.superdev.smiling.media;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.media.view.DocumentFragment;
import com.superdev.smiling.media.view.MediaFragment;
import com.superdev.smiling.user.data_model.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observer;
import rx.functions.Action1;

public class MediaActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tabs)
    TabLayout tabs;

    @BindView(R.id.appbar)
    AppBarLayout appbar;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        Dependencies.INSTANCE.getUserService()
                .getUser(getIntent().getStringExtra(Constants.DESTINATION_ID))
                .subscribe(userDatabaseResult -> {
                    if (userDatabaseResult.isSuccess()) {
                        if (actionBar != null) {
                            actionBar.setTitle(userDatabaseResult.getData().getName());
                        }
                    }
                });
        setupViewPager();
    }

    private void setupViewPager() {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(MediaFragment.newInstance(getIntent().getStringExtra(Constants.SENDER_ID), getIntent().getStringExtra(Constants.DESTINATION_ID)), "Media");
        adapter.addFragment(DocumentFragment.newInstance(getIntent().getStringExtra(Constants.SENDER_ID), getIntent().getStringExtra(Constants.DESTINATION_ID)), "Documents");
        viewPager.setAdapter(adapter);
        tabs.setupWithViewPager(viewPager);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
