package com.superdev.smiling.media.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.superdev.smiling.BaseAdapter;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.conversation.data_model.Message;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by gautam on 28/10/17.
 */

class AllDocumentAdapter extends BaseAdapter<AllDocumentAdapter.DocumentViewHolder, Message> {

    private List<Message> messageList;

    AllDocumentAdapter() {
        messageList = new ArrayList<>();
    }

    private OnItemClickListener onItemClickListener;

    @Override
    public void setData(List<Message> messageList) {
        this.messageList = messageList;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public DocumentViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new DocumentViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_document_layout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(DocumentViewHolder holder, int i) {
        Message message = messageList.get(i);
        setUpDocPreview(message, holder);
        holder.itemView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(message);
            }
        });
    }

    private void setUpDocPreview(Message message, DocumentViewHolder holder) {
        switch (message.getDocModel().getDocType()) {
            case "PDF":
                holder.fileIv.setImageResource(R.drawable.ic_pdf);
                break;
            case "WORD":
                holder.fileIv.setImageResource(R.drawable.icon_file_doc);
                break;
            case "EXCEL":
                holder.fileIv.setImageResource(R.drawable.icon_file_xls);
                break;
            case "PPT":
                holder.fileIv.setImageResource(R.drawable.icon_file_ppt);
                break;
            case "TXT":
                holder.fileIv.setImageResource(R.drawable.icon_file_doc);
                break;
            case "UNKNOWN":
                String fileExtension = droidninja.filepicker.utils.Utils.getFileExtension(message.getDocModel().getOriginalFileName());
                holder.fileIv.setImageBitmap(Utils.writeTextOnBitmap(holder.fileIv.getContext(), R.drawable.icon_file_unknown, fileExtension.toUpperCase()));
                break;
        }
        holder.fileNameTv.setText(message.getDocModel().getDocName());
        holder.fileSizeTv.setText(Utils.readableFileSize(message.getDocModel().getDocSize()));
    }

    @Override
    public int getItemCount() {
        return messageList != null && !messageList.isEmpty() ? messageList.size() : 0;
    }

    static class DocumentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.file_iv)
        ImageView fileIv;

        @BindView(R.id.file_name_tv)
        TextView fileNameTv;

        @BindView(R.id.file_size_tv)
        TextView fileSizeTv;

        DocumentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {

        void onItemClick(Message message);
    }
}
