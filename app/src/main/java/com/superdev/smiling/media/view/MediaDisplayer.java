package com.superdev.smiling.media.view;


import com.superdev.smiling.conversation.data_model.Chat;

/**
 * Created by gautam on 28/10/17.
 */

public interface MediaDisplayer {

    void displayMedia(Chat chat);

    void showProgress();

    void hideProgress();
}
