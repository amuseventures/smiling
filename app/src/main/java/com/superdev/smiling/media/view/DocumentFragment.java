package com.superdev.smiling.media.view;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.superdev.smiling.Constants;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.conversation.data_model.Chat;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.media.presenter.DocumentPresenter;
import com.superdev.smiling.media.presenter.MediaPresenter;
import com.superdev.smiling.views.EmptyRecyclerView;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by gautam on 28/10/17.
 */

public class DocumentFragment extends Fragment implements MediaDisplayer, AllDocumentAdapter.OnItemClickListener {

    @BindView(R.id.media_recycler)
    EmptyRecyclerView mediaRecycler;

    @BindView(R.id.progressbar)
    CircularProgressView progressView;

    private DocumentPresenter presenter;

    private AllDocumentAdapter adapter;

    Unbinder unbinder;

    private String sender, destination;

    public static DocumentFragment newInstance(String sender, String destination) {
        Bundle args = new Bundle();
        args.putString(Constants.SENDER_ID, sender);
        args.putString(Constants.DESTINATION_ID, destination);
        DocumentFragment fragment = new DocumentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sender = getArguments().getString(Constants.SENDER_ID);
        destination = getArguments().getString(Constants.DESTINATION_ID);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mdeia, container, false);
        unbinder = ButterKnife.bind(this, view);
        initRecyclerView();

        presenter = new DocumentPresenter(
                Dependencies.INSTANCE.getConversationService(),
                Dependencies.INSTANCE.getUserService(),
                this,
                sender,
                destination);

        return view;
    }

    private void initRecyclerView() {
        adapter = new AllDocumentAdapter();
        adapter.setOnItemClickListener(this);
        mediaRecycler.setAdapter(adapter);
        mediaRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mediaRecycler.setHasFixedSize(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.stopPresenting();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void displayMedia(Chat chat) {
        adapter.setData(chat.getMessages());
    }

    @Override
    public void showProgress() {
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(Message message) {
        File file;
        if (message.getSelf().equalsIgnoreCase(message.getDestination())) {
            file = new File(Utils.getDocumentPath() + "/" + message.getDocModel().getDocName());
        } else {
            file = new File(Utils.getSentDocumentPath() + "/" + message.getDocModel().getDocName());
        }
        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, message.getDocModel().getMimeType());
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getContext(), "No Application Available to View PDF", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
