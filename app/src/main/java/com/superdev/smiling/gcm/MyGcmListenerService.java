package com.superdev.smiling.gcm;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.google.android.gms.gcm.GcmPubSub;
import com.quickblox.sample.core.utils.SharedPrefsHelper;
import com.quickblox.sample.core.utils.constant.GcmConsts;
import com.quickblox.sample.core.utils.constant.MimeType;
import com.quickblox.users.model.QBUser;
import com.sinch.android.rtc.NotificationResult;
import com.sinch.android.rtc.SinchHelpers;
import com.sinch.gson.GsonBuilder;
import com.superdev.smiling.Constants;
import com.superdev.smiling.MimeTypeEnum;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;
import com.superdev.smiling.Utils;
import com.superdev.smiling.conversation.ConversationActivity;
import com.superdev.smiling.conversation.data_model.Message;
import com.superdev.smiling.groupconversation.GroupConversationActivity;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.main.MainActivity;
import com.superdev.smiling.navigation.Navigator;
import com.superdev.smiling.quick.activities.OpponentsActivity;
import com.superdev.smiling.quick.service.CallService;
import com.superdev.smiling.quickutils.Consts;
import com.superdev.smiling.settings.view.SettingsFragment;
import com.superdev.smiling.sinch.SinchService;

import java.io.IOException;

import static com.superdev.smiling.Constants.FIREBASE_USER_ID;


/**
 * Created by gautam on 7/29/17.
 */

public class MyGcmListenerService extends GcmListenerService implements ServiceConnection {

    public static final int CHAT_NOTIFICATION_ID = 2;

    public static final int BROADCAST_NOTIFICATION_ID = 102;

    private static final long[] VIBRATION = {0, 200, 200, 200};

    private static final long[] VIBRATION_SILENT = {0, 0};

    private static final String GROUP = "GroupMessage";

    private static int notificationId = 2222;

    private static int groupNotificationId = 2223;

    private static final String TAG = "MyGcmListenerService";

    private Intent mIntent;

    @Override
    public void handleIntent(Intent intent) {
        if (SinchHelpers.isSinchPushIntent(intent)) {
            mIntent = intent;
            connectToService();
        } else {
            super.handleIntent(intent);
            Message chatModel = null;
            GroupMessage groupMessage = null;
            if (intent.getExtras() != null) {
                if (intent.hasExtra("message")) {
                    String callMessage = intent.getExtras().getString(GcmConsts.EXTRA_GCM_MESSAGE);
                    String id = intent.getExtras().getString(FIREBASE_USER_ID);
                    Log.d(TAG, "handleIntent: " + id);
                    SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
                    if (sharedPrefsHelper.hasQbUser() && !Remember.getString(Constants.FIREBASE_USER_ID, "").equalsIgnoreCase(id)) {
                        Log.d(TAG, "App have logined user");
                        QBUser qbUser = sharedPrefsHelper.getQbUser();
                        startLoginService(qbUser);
                    }
                }
                if (intent.hasExtra("chat")) {
                    chatModel = new GsonBuilder().create().fromJson(intent.getStringExtra("chat"), Message.class);
                }
                if (intent.hasExtra("group")) {
                    groupMessage = new GsonBuilder().create().fromJson(intent.getStringExtra("group"), GroupMessage.class);
                }
                String title = intent.getStringExtra("title");
                String message = intent.getStringExtra("body");
                if (!ConversationActivity.isInForeground && chatModel != null) {
                    sendNotification(title, message, chatModel);
                }
                if (groupMessage != null) {
                    if (groupMessage.getMsgType().equalsIgnoreCase(MimeTypeEnum.MEMBER.getValue())) {
                        subscribeToTopic(groupMessage.getChannel().getUid());
                    } else if (groupMessage.getMsgType().equalsIgnoreCase(MimeTypeEnum.REMOVE.getValue())) {
                        ubSubscribeToTopic(groupMessage.getChannel().getUid());
                    }
                }

                if (!GroupConversationActivity.isInForeground && groupMessage != null) {
                    if (!groupMessage.getAuthor().getUid().equalsIgnoreCase(Remember.getString(FIREBASE_USER_ID, "")))
                        sendNotification(title, message, groupMessage);
                }
            }
        }
    }

    private void ubSubscribeToTopic(String uid) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                GcmPubSub pubSub = GcmPubSub.getInstance(MyGcmListenerService.this);
                try {
                    pubSub.unsubscribe(Remember.getString(Constants.FIREBASE_USERS_FCM, ""), "/topics/" + uid);
                    Log.d(TAG, "subscribe: ");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    private void subscribeToTopic(String topic) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                GcmPubSub pubSub = GcmPubSub.getInstance(MyGcmListenerService.this);
                try {
                    pubSub.subscribe(Remember.getString(Constants.FIREBASE_USERS_FCM, ""), "/topics/" + topic, null);
                    Log.d(TAG, "subscribe: ");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    @Override
    public void onMessageReceived(String s, Bundle bundle) {
        super.onMessageReceived(s, bundle);
        /*Log.d(TAG, "onMessageReceived: " + s);
        Log.d(TAG, "onMessageReceived: " + bundle.toString());
        GroupMessage chatModel = new GsonBuilder().create().fromJson(bundle.getString("chat"), GroupMessage.class);
        if (!ConversationActivity.isInForeground) {
            sendNotification(bundle.getString("title"), bundle.getString("body"), chatModel);
        }*/
    }

    private void sendNotification(String title, String messageBody, Message chatModel) {

        String body = Utils.getCustomBody(chatModel.getMsgType(), messageBody, MyGcmListenerService.this);

        Intent intent = new Intent(getApplicationContext(), ConversationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.SENDER_ID, chatModel.getDestination());
        intent.putExtra(Constants.DESTINATION_ID, chatModel.getSender());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);

        notificationBuilder.setSmallIcon(R.drawable.ic_notification_small);

        notificationBuilder.setContentTitle(title);

        notificationBuilder.setContentText(body);

        notificationBuilder.setAutoCancel(true);

        notificationBuilder.setVibrate(VIBRATION_SILENT);

        notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);

        if (Remember.getBoolean(SettingsFragment.PREF_TICKER_KEY, true)) {
            notificationBuilder.setTicker(body);
        }
        if (Remember.getBoolean(SettingsFragment.PREF_VIBRATION_KEY, true)) {
            notificationBuilder.setVibrate(VIBRATION);
        }

        if (Remember.getBoolean(SettingsFragment.PREF_LED_KEY, true)) {
            notificationBuilder.setLights(getLedColor(), 1000, 1000);
        }

        notificationBuilder.setSound(defaultSoundUri);

        notificationBuilder.setContentIntent(pendingIntent);

        notificationBuilder.setGroup(GROUP);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, notificationBuilder.build());

        if (Remember.getBoolean(SettingsFragment.PREF_WAKE_KEY, false)) {
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            boolean isScreenOn = false;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT_WATCH) {
                isScreenOn = pm.isInteractive();
            } else {
                isScreenOn = pm.isScreenOn();
            }
            if (!isScreenOn) {
                PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "MyLock");
                wl.acquire(10000);
                wl.release();
            }
        }
    }

    private void sendNotification(String title, String messageBody, GroupMessage chatModel) {

        String body = Utils.getCustomBody(chatModel.getMsgType(), messageBody, MyGcmListenerService.this);

        Intent intent = new Intent(getApplicationContext(), GroupConversationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.FIREBASE_CHANNELS, chatModel.getChannel());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);

        notificationBuilder.setSmallIcon(R.drawable.ic_notification_small);

        notificationBuilder.setVibrate(VIBRATION_SILENT);

        notificationBuilder.setContentTitle(title);

        notificationBuilder.setContentText(body);

        notificationBuilder.setAutoCancel(true);

        notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);

        if (Remember.getBoolean(SettingsFragment.PREF_TICKER_KEY, true)) {
            notificationBuilder.setTicker(body);
        }
        if (Remember.getBoolean(SettingsFragment.PREF_VIBRATION_KEY, true)) {
            notificationBuilder.setVibrate(VIBRATION);
        }

        if (Remember.getBoolean(SettingsFragment.PREF_LED_KEY, true)) {
            notificationBuilder.setLights(getLedColor(), 1000, 1000);
        }

        notificationBuilder.setSound(defaultSoundUri);

        notificationBuilder.setContentIntent(pendingIntent);

        notificationBuilder.setGroup(GROUP);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(groupNotificationId, notificationBuilder.build());

        if (Remember.getBoolean(SettingsFragment.PREF_WAKE_KEY, false)) {
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "FlashActivity");
            wl.acquire();
            wl.release();
        }
    }

    private void connectToService() {
        getApplicationContext().bindService(new Intent(this, SinchService.class), this, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (SinchHelpers.isSinchPushIntent(mIntent)) {
            SinchService.SinchServiceInterface sinchService = (SinchService.SinchServiceInterface) iBinder;
            if (sinchService != null) {
                NotificationResult result = sinchService.relayRemotePushNotificationPayload(mIntent);
                // handle result, e.g. show a notification or similar
            }
        }
        mIntent = null;
    }

    private void sendNotification(String message) {
        Intent intent = new Intent(this, OpponentsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(Consts.EXTRA_IS_STARTED_FOR_CALL, true);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification_small)
                .setContentText(message)
                .setAutoCancel(false)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setOngoing(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(10, notificationBuilder.build());
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
    }

    private int getLedColor() {
        int color = Integer.parseInt(Remember.getString(SettingsFragment.PREF_THEME_LED_KEY, "" + ContextCompat.getColor(MyGcmListenerService.this, R.color.red_light)));

        if (color == ContextCompat.getColor(MyGcmListenerService.this, R.color.blue_light) || color == ContextCompat.getColor(MyGcmListenerService.this, R.color.blue_dark))
            return ContextCompat.getColor(MyGcmListenerService.this, R.color.blue_dark);
        if (color == ContextCompat.getColor(MyGcmListenerService.this, R.color.purple_light) || color == ContextCompat.getColor(MyGcmListenerService.this, R.color.purple_dark))
            return ContextCompat.getColor(MyGcmListenerService.this, R.color.purple_dark);
        if (color == ContextCompat.getColor(MyGcmListenerService.this, R.color.green_light) || color == ContextCompat.getColor(MyGcmListenerService.this, R.color.green_dark))
            return ContextCompat.getColor(MyGcmListenerService.this, R.color.green_dark);
        if (color == ContextCompat.getColor(MyGcmListenerService.this, R.color.yellow_light) || color == ContextCompat.getColor(MyGcmListenerService.this, R.color.yellow_dark))
            return ContextCompat.getColor(MyGcmListenerService.this, R.color.yellow_dark);
        if (color == ContextCompat.getColor(MyGcmListenerService.this, R.color.red_light) || color == ContextCompat.getColor(MyGcmListenerService.this, R.color.red_dark))
            return ContextCompat.getColor(MyGcmListenerService.this, R.color.red_dark);

        return ContextCompat.getColor(MyGcmListenerService.this, R.color.white_pure);
    }

    private void startLoginService(QBUser qbUser) {
        CallService.start(this, qbUser);
    }
}
