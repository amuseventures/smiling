package com.superdev.smiling.channel_list.presenter;

import com.superdev.smiling.Utils;
import com.superdev.smiling.analytics.Analytics;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.channel.model.Channels;
import com.superdev.smiling.channel.presenter.Config;
import com.superdev.smiling.channel.service.ChannelService;
import com.superdev.smiling.channel_list.data_model.ChannelConversation;
import com.superdev.smiling.channel_list.view.ChannelListDisplayer;
import com.superdev.smiling.database.DatabaseResult;
import com.superdev.smiling.groupconversation.data_model.GroupMessage;
import com.superdev.smiling.groupconversation.service.GroupConversationService;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.navigation.ChannelNavigator;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.user.data_model.User;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Func1;

/**
 * Created by gautam on 17/10/17.
 */

public class ChannelListPresenter {

    private final ChannelListDisplayer channelListDisplayer;
    private final ChannelService channelService;
    private final GroupConversationService groupConversationService;
    private final PhoneLoginService loginService;
    private final Config config;
    private final ChannelNavigator navigator;
    private final Analytics analytics;

    private Subscription subscription;
    private User user;

    public ChannelListPresenter(
            ChannelListDisplayer channelListDisplayer,
            GroupConversationService groupConversationService,
            ChannelService channelService,
            PhoneLoginService loginService,
            Config config,
            ChannelNavigator navigator,
            Analytics analytics
    ) {
        this.channelListDisplayer = channelListDisplayer;
        this.groupConversationService = groupConversationService;
        this.channelService = channelService;
        this.loginService = loginService;
        this.config = config;
        this.navigator = navigator;
        this.analytics = analytics;
    }

    public void startPresenting() {
        channelListDisplayer.attach(channelsInteractionListener);
        subscription = loginService.getAuthentication()
                .filter(successfullyAuthenticated())
                .doOnNext(authentication -> user = authentication.getUser())
                .flatMap(channelsForUser())
                .map(sortIfConfigured())
                .subscribe(new Observer<Channels>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Channels channels) {
                        if (channels == null || channels.getChannels().isEmpty())
                            return;
                        for (Channel channel : channels.getChannels()) {
                            channelService.subscribeToChannel(channel, user);
                            groupConversationService.getLastMessageFor(channel)
                                    .subscribe(new Observer<DatabaseResult<GroupMessage>>() {
                                        @Override
                                        public void onCompleted() {

                                        }

                                        @Override
                                        public void onError(Throwable throwable) {
                                            throwable.printStackTrace();
                                        }

                                        @Override
                                        public void onNext(DatabaseResult<GroupMessage> groupMessageDatabaseResult) {
                                            if (groupMessageDatabaseResult.isSuccess()) {
                                                GroupMessage groupMessage = groupMessageDatabaseResult.getData();
                                                channelListDisplayer.addToDisplay(new ChannelConversation(groupMessage.getMessage(), groupMessage.getTimestamp(), groupMessage.getMsgType(),
                                                        groupMessage.getMessageKey(), channel, groupMessage.getFileModel(), groupMessage.getMapModel(), groupMessage.getDocModel(), groupMessage.getAudioModel(), groupMessage.getVoiceModel(),
                                                        groupMessage.getVideoModel(), groupMessage.getContactModel(), groupMessage.getAuthor()));
                                            } else {
                                                channelListDisplayer.addToDisplay(new ChannelConversation(null, Utils.getCurrentTimestamp(), null,
                                                        null, channel, null, null, null, null, null,
                                                        null, null, null));
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private Func1<Authentication, Observable<Channels>> channelsForUser() {
        return authentication -> channelService.getChannelsFor(authentication.getUser())
                .map(channelsDatabaseResult -> {
                    if (channelsDatabaseResult.isSuccess()) {
                        return channelsDatabaseResult.getData();
                    } else {
                        return null;
                    }
                });
    }

    private Func1<Channels, Channels> sortIfConfigured() {
        return channels -> {
            if (config.orderChannelsByName()) {
                return channels.sortedByName();
            } else {
                return channels;
            }
        };
    }

    private Func1<Authentication, Boolean> successfullyAuthenticated() {
        return Authentication::isSuccess;
    }

    public void stopPresenting() {
        subscription.unsubscribe();
        channelListDisplayer.detach(channelsInteractionListener);
    }

    private final ChannelListDisplayer.ChannelsInteractionListener channelsInteractionListener = new ChannelListDisplayer.ChannelsInteractionListener() {
        @Override
        public void onChannelSelected(Channel channel) {
            analytics.trackSelectChannel(channel.getName());
            navigator.toChannel(channel);
        }

        @Override
        public void onUserImageSelected(Channel channel) {

        }

        @Override
        public void onFloatButtonClick() {

        }

    };
}

