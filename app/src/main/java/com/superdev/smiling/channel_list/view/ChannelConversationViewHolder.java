package com.superdev.smiling.channel_list.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.superdev.smiling.channel_list.data_model.ChannelConversation;

/**
 * Created by gautam on 17/10/17.
 */

public class ChannelConversationViewHolder extends RecyclerView.ViewHolder {

    private final ChannelConversationView conversationView;

    public ChannelConversationViewHolder(ChannelConversationView itemView) {
        super(itemView);
        this.conversationView = itemView;
    }

    public void bind(final ChannelConversation conversation, final ChannelConversationViewHolder.ConversationSelectionListener listener) {
        conversationView.display(conversation);
        conversationView.setOnClickListener(v -> listener.onConversationSelected(conversation));
        conversationView.setOnLongClickListener(view -> {
            listener.onConversationLongPressed(conversation);
            return true;
        });
        conversationView.profileImageView.setOnClickListener(v -> listener.onUserImageClick(v, conversation));
    }

    public interface ConversationSelectionListener {
        void onConversationSelected(ChannelConversation conversation);

        void onUserImageClick(View view, ChannelConversation conversation);

        void onConversationLongPressed(ChannelConversation conversation);
    }

}
