package com.superdev.smiling.channel_list.data_model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by gautam on 17/10/17.
 */

public class ChannelConversations {

    private final List<ChannelConversation> conversations;

    public ChannelConversations(List<ChannelConversation> conversations) {
        this.conversations = conversations;
    }

    public ChannelConversation getConversationAt(int position) {
        return conversations.get(position);
    }

    public int size() {
        return conversations.size();
    }

    public void add(ChannelConversation conversation) {
        if (!conversations.contains(conversation))
            conversations.add(conversation);
        else
            conversations.set(conversations.indexOf(conversation), conversation);
    }

    public void remove(ChannelConversation con) {
        if (conversations.contains(con)) {
            conversations.remove(con);
        }
    }

    public int indexOf(ChannelConversation con) {
        if (conversations.contains(con)) {
            return this.conversations.indexOf(con);
        } else {
            return -1;
        }
    }

    public ChannelConversations sortedByDate() {
        List<ChannelConversation> sortedList = new ArrayList<>(conversations);
        Collections.sort(sortedList, byDate());
        return new ChannelConversations(sortedList);
    }

    private static Comparator<? super ChannelConversation> byDate() {
        return (Comparator<ChannelConversation>) (o1, o2) -> o2.getTimestamp().compareTo(o1.getTimestamp());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChannelConversations conversations1 = (ChannelConversations) o;

        return conversations != null ? conversations.equals(conversations1.conversations) : conversations1.conversations == null;
    }

    @Override
    public int hashCode() {
        return conversations != null ? conversations.hashCode() : 0;
    }
}
