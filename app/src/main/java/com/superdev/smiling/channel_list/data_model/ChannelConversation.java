package com.superdev.smiling.channel_list.data_model;

import android.os.Parcel;
import android.os.ParcelFormatException;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.conversation.data_model.AudioModel;
import com.superdev.smiling.conversation.data_model.ContactModel;
import com.superdev.smiling.conversation.data_model.DocModel;
import com.superdev.smiling.conversation.data_model.FileModel;
import com.superdev.smiling.conversation.data_model.MapModel;
import com.superdev.smiling.conversation.data_model.VideoModel;
import com.superdev.smiling.conversation.data_model.VoiceModel;
import com.superdev.smiling.user.data_model.User;

/**
 * Created by gautam on 17/10/17.
 */

public class ChannelConversation implements Parcelable {

    private String message;

    private String timestamp;

    private String msgType;

    private String messageKey;

    private Channel channel;

    private FileModel fileModel;

    private MapModel mapModel;

    private DocModel docModel;

    private AudioModel audioModel;

    private VoiceModel voiceModel;

    private VideoModel videoModel;

    private ContactModel contactModel;

    private User author;

    public ChannelConversation() {
    }

    public ChannelConversation(String message, String timestamp, String msgType, String messageKey, Channel channel, FileModel fileModel, MapModel mapModel, DocModel docModel, AudioModel audioModel, VoiceModel voiceModel, VideoModel videoModel, ContactModel contactModel, User author) {
        this.message = message;
        this.timestamp = timestamp;
        this.msgType = msgType;
        this.messageKey = messageKey;
        this.channel = channel;
        this.fileModel = fileModel;
        this.mapModel = mapModel;
        this.docModel = docModel;
        this.audioModel = audioModel;
        this.voiceModel = voiceModel;
        this.videoModel = videoModel;
        this.contactModel = contactModel;
        this.author = author;
    }

    public ChannelConversation(Parcel in) {
        message = in.readString();
        timestamp = in.readString();
        msgType = in.readString();
        messageKey = in.readString();
        channel = in.readParcelable(Channel.class.getClassLoader());
        fileModel = in.readParcelable(FileModel.class.getClassLoader());
        mapModel = in.readParcelable(MapModel.class.getClassLoader());
        docModel = in.readParcelable(DocModel.class.getClassLoader());
        audioModel = in.readParcelable(AudioModel.class.getClassLoader());
        voiceModel = in.readParcelable(VoiceModel.class.getClassLoader());
        videoModel = in.readParcelable(VideoModel.class.getClassLoader());
        contactModel = in.readParcelable(ContactModel.class.getClassLoader());
        author = in.readParcelable(User.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
        dest.writeString(timestamp);
        dest.writeString(msgType);
        dest.writeString(messageKey);
        dest.writeParcelable(channel, flags);
        dest.writeParcelable(fileModel, flags);
        dest.writeParcelable(mapModel, flags);
        dest.writeParcelable(docModel, flags);
        dest.writeParcelable(audioModel, flags);
        dest.writeParcelable(voiceModel, flags);
        dest.writeParcelable(videoModel, flags);
        dest.writeParcelable(contactModel, flags);
        dest.writeParcelable(author, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChannelConversation> CREATOR = new Creator<ChannelConversation>() {
        @Override
        public ChannelConversation createFromParcel(Parcel in) {
            return new ChannelConversation(in);
        }

        @Override
        public ChannelConversation[] newArray(int size) {
            return new ChannelConversation[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChannelConversation conversation = (ChannelConversation) o;

        return channel.getUid() != null && channel.getUid().equals(conversation.channel.getUid());
    }

    @Override
    public int hashCode() {
        int result = channel.getUid() != null ? channel.getUid().hashCode() : 0;
        result = 31 * result;
        return result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public FileModel getFileModel() {
        return fileModel;
    }

    public void setFileModel(FileModel fileModel) {
        this.fileModel = fileModel;
    }

    public MapModel getMapModel() {
        return mapModel;
    }

    public void setMapModel(MapModel mapModel) {
        this.mapModel = mapModel;
    }

    public DocModel getDocModel() {
        return docModel;
    }

    public void setDocModel(DocModel docModel) {
        this.docModel = docModel;
    }

    public AudioModel getAudioModel() {
        return audioModel;
    }

    public void setAudioModel(AudioModel audioModel) {
        this.audioModel = audioModel;
    }

    public VoiceModel getVoiceModel() {
        return voiceModel;
    }

    public void setVoiceModel(VoiceModel voiceModel) {
        this.voiceModel = voiceModel;
    }

    public VideoModel getVideoModel() {
        return videoModel;
    }

    public void setVideoModel(VideoModel videoModel) {
        this.videoModel = videoModel;
    }

    public ContactModel getContactModel() {
        return contactModel;
    }

    public void setContactModel(ContactModel contactModel) {
        this.contactModel = contactModel;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
}
