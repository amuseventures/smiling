package com.superdev.smiling.channel_list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.channel_list.presenter.ChannelListPresenter;
import com.superdev.smiling.navigation.AndroidChannelNavigator;
import com.superdev.smiling.navigation.AndroidNavigator;

/**
 * Created by gautam on 17/10/17.
 */

public class ChannelListFragment extends Fragment {

    private ChannelListPresenter presenter;

    public static ChannelListFragment newInstance() {
        Bundle args = new Bundle();
        ChannelListFragment fragment = new ChannelListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_channel_list, container, false);
        AndroidChannelNavigator channelNavigator = new AndroidChannelNavigator((AppCompatActivity) getActivity(), new AndroidNavigator(getActivity()));
        presenter = new ChannelListPresenter(
                view.findViewById(R.id.channelListView),
                Dependencies.INSTANCE.getGroupConversationService(),
                Dependencies.INSTANCE.getChannelService(),
                Dependencies.INSTANCE.getPhoneLoginService(),
                Dependencies.INSTANCE.getConfig(),
                channelNavigator,
                Dependencies.INSTANCE.getAnalytics()
        );
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.startPresenting();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.stopPresenting();
    }
}
