package com.superdev.smiling.channel_list.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.superdev.smiling.R;
import com.superdev.smiling.channel_list.data_model.ChannelConversation;
import com.superdev.smiling.channel_list.data_model.ChannelConversations;

import java.util.ArrayList;

/**
 * Created by gautam on 17/10/17.
 */

public class ChannelConversationListAdapter extends RecyclerView.Adapter<ChannelConversationViewHolder> {

    private ChannelConversations conversations = new ChannelConversations(new ArrayList<ChannelConversation>());
    private ChannelListDisplayer.ChannelsInteractionListener conversationInteractionListener;
    private final LayoutInflater inflater;

    ChannelConversationListAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void update(ChannelConversations conversations) {
        this.conversations = conversations.sortedByDate();
        notifyDataSetChanged();
    }

    public void add(ChannelConversation conversation) {
        this.conversations.add(conversation);
        this.conversations = this.conversations.sortedByDate();
        notifyDataSetChanged();
    }

    public void removeData(ChannelConversation conversation) {
        int pos = this.conversations.indexOf(conversation);
        this.conversations.remove(conversation);
        if (pos > -1) {
            notifyItemRemoved(pos);
        } else {
            notifyDataSetChanged();
        }
    }

    @Override
    public ChannelConversationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChannelConversationViewHolder((ChannelConversationView) inflater.inflate(R.layout.channel_conversation_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(ChannelConversationViewHolder holder, int position) {
        holder.bind(conversations.getConversationAt(position), clickListener);
    }

    @Override
    public int getItemCount() {
        return conversations.size();
    }

    @Override
    public long getItemId(int position) {
        return conversations.getConversationAt(position).hashCode();
    }

    public void attach(ChannelListDisplayer.ChannelsInteractionListener conversationInteractionListener) {
        this.conversationInteractionListener = conversationInteractionListener;
    }

    public void detach(ChannelListDisplayer.ChannelsInteractionListener conversationInteractionListener) {
        this.conversationInteractionListener = null;
    }

    private final ChannelConversationViewHolder.ConversationSelectionListener clickListener = new ChannelConversationViewHolder.ConversationSelectionListener() {
        @Override
        public void onConversationSelected(ChannelConversation conversation) {
            ChannelConversationListAdapter.this.conversationInteractionListener.onChannelSelected(conversation.getChannel());
        }

        @Override
        public void onUserImageClick(View view, ChannelConversation conversation) {
            ChannelConversationListAdapter.this.conversationInteractionListener.onUserImageSelected(conversation.getChannel());
        }

        @Override
        public void onConversationLongPressed(ChannelConversation conversation) {

        }
    };
}
