package com.superdev.smiling.channel_list.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superdev.smiling.MimeTypeEnum;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.channel_list.data_model.ChannelConversation;
import com.superdev.smiling.conversation_list.view.VerticalImageSpan;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by gautam on 17/10/17.
 */

public class ChannelConversationView extends FrameLayout {
    @BindView(R.id.profileImageView)
    CircleImageView profileImageView;

    @BindView(R.id.nameTextView)
    EmojiconTextView nameTextView;

    @BindView(R.id.timeTextView)
    TextView timeTextView;

    @BindView(R.id.messageTextView)
    EmojiconTextView messageTextView;

    @BindView(R.id.content_layout)
    LinearLayout contentLayout;

    private VerticalImageSpan audioSpan, videoSpan, imageSpan, contactSpan, documentSpan, locationSpan;

    private int layoutResId;

    public ChannelConversationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            int[] attrsArray = {
                    android.R.attr.layout
            };
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            layoutResId = array.getResourceId(0, R.layout.merge_conversation_list_item_view);
            array.recycle();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), layoutResId, this);
        ButterKnife.bind(this);
        Drawable audio = ContextCompat.getDrawable(getContext(), R.drawable.msg_status_audio).mutate();
        audio.setBounds(0, 0, audio.getIntrinsicWidth(), audio.getIntrinsicHeight());
        Drawable video = ContextCompat.getDrawable(getContext(), R.drawable.msg_status_video).mutate();
        video.setBounds(0, 0, video.getIntrinsicWidth(), video.getIntrinsicHeight());
        Drawable image = ContextCompat.getDrawable(getContext(), R.drawable.msg_status_cam).mutate();
        image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
        Drawable document = ContextCompat.getDrawable(getContext(), R.drawable.msg_status_doc).mutate();
        document.setBounds(0, 0, document.getIntrinsicWidth(), document.getIntrinsicHeight());
        Drawable contact = ContextCompat.getDrawable(getContext(), R.drawable.msg_status_contact).mutate();
        contact.setBounds(0, 0, contact.getIntrinsicWidth(), contact.getIntrinsicHeight());
        Drawable location = ContextCompat.getDrawable(getContext(), R.drawable.msg_status_location).mutate();
        location.setBounds(0, 0, contact.getIntrinsicWidth(), contact.getIntrinsicHeight());

        audioSpan = new VerticalImageSpan(audio);
        videoSpan = new VerticalImageSpan(video);
        imageSpan = new VerticalImageSpan(image);
        contactSpan = new VerticalImageSpan(contact);
        documentSpan = new VerticalImageSpan(document);
        locationSpan = new VerticalImageSpan(location);

    }

    public void display(ChannelConversation conversation) {
        if (conversation.getChannel() == null)
            return;
        if (conversation.getChannel() != null) {
            Utils.loadImageElseBlack((conversation.getChannel().getImage() != null ? conversation.getChannel().getImage() : null), profileImageView, getContext(), R.drawable.avatar_group);
        }
        nameTextView.setText(conversation.getChannel().getName());
        SpannableString messageString;
        String ldir = "";
        if (conversation.getMsgType() != null) {
            if (conversation.getMsgType().equalsIgnoreCase(MimeTypeEnum.IMAGE.getValue())) {
                messageString = new SpannableString(ldir + "  Photo");
                messageString.setSpan(imageSpan, ldir.length(), ldir.length() + 1, 0);
            } else if (conversation.getMsgType().equalsIgnoreCase(MimeTypeEnum.VIDEO.getValue())) {
                messageString = new SpannableString(ldir + "  Video");
                messageString.setSpan(videoSpan, ldir.length(), ldir.length() + 1, 0);
            } else if (conversation.getMsgType().equalsIgnoreCase(MimeTypeEnum.AUDIO.getValue())) {
                messageString = new SpannableString(ldir + "  Audio");
                messageString.setSpan(audioSpan, ldir.length(), ldir.length() + 1, 0);
            } else if (conversation.getMsgType().equalsIgnoreCase(MimeTypeEnum.VOICE.getValue())) {
                messageString = new SpannableString(ldir + "  Voice");
                messageString.setSpan(audioSpan, ldir.length(), ldir.length() + 1, 0);
            } else if (conversation.getMsgType().equalsIgnoreCase(MimeTypeEnum.CONTACT.getValue())) {
                messageString = new SpannableString(ldir + "  Contact");
                messageString.setSpan(contactSpan, ldir.length(), ldir.length() + 1, 0);
            } else if (conversation.getMsgType().equalsIgnoreCase(MimeTypeEnum.LOCATION.getValue())) {
                messageString = new SpannableString(ldir + "  Location");
                messageString.setSpan(locationSpan, ldir.length(), ldir.length() + 1, 0);
            } else if (conversation.getMsgType().equalsIgnoreCase(MimeTypeEnum.DOCUMENT.getValue())) {
                messageString = new SpannableString(ldir + "  Document");
                messageString.setSpan(documentSpan, ldir.length(), ldir.length() + 1, 0);
            } else {
                messageString = new SpannableString(conversation.getMessage());
            }

            messageTextView.setText(String.format("%1s: %2s", conversation.getAuthor().getShortName(), messageString));
            String timeStamp = Utils.getTime(conversation.getTimestamp());
            String date = Utils.getDate(conversation.getTimestamp());
            String today = Utils.getCurrentTimestamp();
            String[] time1 = date.split("/");
            String[] time2 = today.split("/");
            if ((time1[2] + time1[1] + time1[0]).equals(time2[0] + time2[1] + time2[2])) {
                timeTextView.setText(timeStamp);
            } else {
                timeTextView.setText(date);
            }
        }
    }

}
