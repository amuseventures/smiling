package com.superdev.smiling.channel_list.database;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.user.data_model.User;

import java.util.List;

import rx.Observable;

/**
 * Created by gautam on 17/10/17.
 */

public interface ChannelConversationListDatabase {

    Observable<List<String>> observeConversationsFor(Channel channel);

    Observable<Boolean> removeConversation(Channel channel);
}
