package com.superdev.smiling.channel_list.view;

import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.channel_list.data_model.ChannelConversation;
import com.superdev.smiling.channel_list.data_model.ChannelConversations;

/**
 * Created by gautam on 17/10/17.
 */

public interface ChannelListDisplayer {

    void display(ChannelConversations conversations);

    void addToDisplay(ChannelConversation conversation);

    void removeData(ChannelConversation conversation);

    void attach(ChannelListDisplayer.ChannelsInteractionListener channelsInteractionListener);

    void detach(ChannelListDisplayer.ChannelsInteractionListener channelsInteractionListener);

    interface ChannelsInteractionListener {

        void onChannelSelected(Channel channel);

        void onUserImageSelected(Channel channel);

        void onFloatButtonClick();

    }
}
