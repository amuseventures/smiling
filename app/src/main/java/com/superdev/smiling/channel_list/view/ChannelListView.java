package com.superdev.smiling.channel_list.view;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.superdev.smiling.R;
import com.superdev.smiling.channel_list.data_model.ChannelConversation;
import com.superdev.smiling.channel_list.data_model.ChannelConversations;
import com.superdev.smiling.views.EmptyRecyclerView;
import com.superdev.smiling.views.TextViewWithImages;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by gautam on 17/10/17.
 */

public class ChannelListView extends LinearLayout implements ChannelListDisplayer {

    private final ChannelConversationListAdapter conversationListAdapter;

    @BindView(R.id.conversationsRecyclerView)
    EmptyRecyclerView conversations;

    @BindView(R.id.welcome_chats_message)
    TextViewWithImages view;

    @BindView(R.id.btn_float)
    FloatingActionButton btnFloat;

    private Toolbar toolbar;

    private ChannelsInteractionListener channelsInteractionListener;

    public ChannelListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
        conversationListAdapter = new ChannelConversationListAdapter(LayoutInflater.from(context));
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.merge_conversation_list_view, this);
        ButterKnife.bind(this);

        view.setText(R.string.welcome_chats_message);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        conversations.setLayoutManager(layoutManager);
        conversations.setAdapter(conversationListAdapter);
        conversations.setEmptyView(view);
        btnFloat.setImageResource(R.drawable.ic_new_message_tip);
    }

    @Override
    public void display(ChannelConversations conversations) {
        conversationListAdapter.update(conversations);
    }

    @Override
    public void addToDisplay(ChannelConversation conversation) {
        conversationListAdapter.add(conversation);
    }

    @Override
    public void removeData(ChannelConversation conversation) {
        conversationListAdapter.removeData(conversation);
    }

    @Override
    public void attach(ChannelsInteractionListener channelsInteractionListener) {
        this.channelsInteractionListener = channelsInteractionListener;
        conversationListAdapter.attach(channelsInteractionListener);
    }

    @Override
    public void detach(ChannelsInteractionListener channelsInteractionListener) {
        this.channelsInteractionListener = null;
    }


    @OnClick(R.id.btn_float)
    public void onViewClicked() {
        if (channelsInteractionListener != null)
            channelsInteractionListener.onFloatButtonClick();
    }
}
