package com.superdev.smiling;

import android.annotation.SuppressLint;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.common.internal.Supplier;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.quickblox.sample.core.CoreApp;
import com.superdev.smiling.quickutils.QBResRequestExecutor;

import java.io.File;

import io.fabric.sdk.android.Fabric;

/**
 * Created by marco on 26/08/16.
 */

public class App extends CoreApp {

    @SuppressLint("StaticFieldLeak")
    private static App instance;

    private QBResRequestExecutor qbResRequestExecutor;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initApplication();
        Fabric.with(this, new Crashlytics());
        configFresco();
        Remember.init(getApplicationContext(), getString(R.string.app_name));
    }

    private void initApplication() {
        instance = this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public void configFresco() {
        Supplier<File> diskSupplier = () -> getApplicationContext().getCacheDir();

        DiskCacheConfig diskCacheConfig = DiskCacheConfig.newBuilder(getApplicationContext())
                .setBaseDirectoryName("images")
                .setBaseDirectoryPathSupplier(diskSupplier)
                .build();

        ImagePipelineConfig frescoConfig = ImagePipelineConfig.newBuilder(this)
                .setMainDiskCacheConfig(diskCacheConfig)
                .build();

        Fresco.initialize(this, frescoConfig);
    }

    public synchronized QBResRequestExecutor getQbResRequestExecutor() {
        return qbResRequestExecutor == null
                ? qbResRequestExecutor = new QBResRequestExecutor()
                : qbResRequestExecutor;
    }

}
