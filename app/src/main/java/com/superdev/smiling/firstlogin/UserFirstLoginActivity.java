package com.superdev.smiling.firstlogin;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.superdev.smiling.Constants;
import com.superdev.smiling.R;
import com.superdev.smiling.Remember;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

/**
 * Created by marco on 13/07/16.
 */

public class UserFirstLoginActivity extends AppCompatActivity
        implements View.OnClickListener {

    private static final int SELECT_PHOTO = 1;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.textView)
    TextView textView;

    @BindView(R.id.profileImageView)
    CircleImageView profileImageView;

    @BindView(R.id.image_upload_progress_bar)
    ProgressBar imageUploadProgressBar;

    @BindView(R.id.image_frame)
    FrameLayout imageFrame;

    @BindView(R.id.tv_user_name)
    EmojiconEditText tvUserName;

    @BindView(R.id.tv_counter)
    TextView tvCounter;

    @BindView(R.id.user_layout)
    RelativeLayout userLayout;

    @BindView(R.id.iv_smiley)
    ImageView ivSmiley;

    @BindView(R.id.btn_next)
    Button btnNext;

    @BindView(R.id.view_one)
    RelativeLayout viewOne;

    @BindView(R.id.currentRoot)
    LinearLayout currentRoot;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseStorage firebaseStorage;
    private StorageReference storageReference;
    private DatabaseReference databaseReference;

    private boolean hasImageChanged = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firstlogin);
        ButterKnife.bind(this);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        // TODO to implement
        String image = getIntent().getStringExtra(Constants.FIREBASE_USERS_IMAGE);

        EmojIconActions emojIcon = new EmojIconActions(this, currentRoot, tvUserName, ivSmiley);
        emojIcon.ShowEmojIcon();

        tvUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (s.length() > 0) {
                    tvCounter.setVisibility(View.VISIBLE);
                    tvCounter.setText(String.format(Locale.getDefault(), "%d", 24 - s.toString().length()));
                } else {
                    tvCounter.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        if (firebaseUser.getDisplayName() != null && firebaseUser.getDisplayName().length() > 0)
            tvUserName.setText(firebaseUser.getDisplayName());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    try {
                        final Uri imageUri = data.getData();
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        profileImageView.setImageBitmap(selectedImage);
                        hasImageChanged = true;
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }
        }
    }

    @OnClick({R.id.btn_next, R.id.profileImageView})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_next:
                if (tvUserName.getText() == null) {
                    Toast.makeText(this, "Name cannot be empty", Toast.LENGTH_SHORT).show();
                    return;
                } else if (tvUserName.getText().toString().trim().length() == 0) {
                    Toast.makeText(this, "Name cannot be empty", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (firebaseUser != null) {
                    final HashMap<String, String> values = new HashMap<>();
                    values.put(Constants.FIREBASE_USERS_PHONE, firebaseUser.getPhoneNumber());
                    values.put(Constants.FIREBASE_USERS_NAME, tvUserName.getText().toString());
                    values.put(Constants.FIREBASE_USERS_UID, firebaseUser.getUid());
                    values.put(Constants.FIREBASE_USERS_FCM, FirebaseInstanceId.getInstance().getToken());
                    values.put(Constants.FIREBASE_USER_REGISTER, "1");
                    values.put(Constants.FIREBASE_USERS_STATUS, getString(R.string.default_status));

                    if (hasImageChanged) {
                        profileImageView.setDrawingCacheEnabled(true);
                        profileImageView.buildDrawingCache();
                        Bitmap bitmap = profileImageView.getDrawingCache();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] data = baos.toByteArray();

                        final String imageRef = profileImageView.getDrawingCache().hashCode() + System.currentTimeMillis() + ".jpg";
                        StorageReference mountainsRef = storageReference.child(imageRef);
                        UploadTask uploadTask = mountainsRef.putBytes(data);
                        uploadTask.addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {

                            }
                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                values.put(Constants.FIREBASE_USERS_IMAGE, imageRef);
                                databaseReference.child(Constants.FIREBASE_USERS)
                                                 .child(firebaseUser.getUid()).setValue(values);
                                setToken();

                                finish();
                            }
                        });
                    } else {
                        databaseReference.child(Constants.FIREBASE_USERS)
                                         .child(firebaseUser.getUid()).setValue(values);
                        setToken();
                        finish();
                    }
                }
                break;
            case R.id.profileImageView:
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
                break;
        }
    }

    private void setToken() {
        DatabaseReference tokenRef = FirebaseDatabase.getInstance().getReference(Constants.FIREBASE_FCM);
        tokenRef.child(firebaseUser.getUid() + "/" + Constants.FIREBASE_FCM_TOKEN).setValue(FirebaseInstanceId.getInstance().getToken());
        tokenRef.child(firebaseUser.getUid() + "/" + Constants.FIREBASE_FCM_ENABLED).setValue(Boolean.TRUE.toString());
    }

}
