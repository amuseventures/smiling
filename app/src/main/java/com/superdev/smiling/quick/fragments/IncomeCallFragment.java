package com.superdev.smiling.quick.fragments;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quickblox.chat.QBChatService;
import com.quickblox.sample.core.utils.UiUtils;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.QBRTCTypes;
import com.superdev.smiling.App;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.database.QbUsersDbManager;
import com.superdev.smiling.quick.activities.OpponentsActivity;
import com.superdev.smiling.quickutils.CollectionsUtils;
import com.superdev.smiling.quickutils.Consts;
import com.superdev.smiling.quickutils.RingtonePlayer;
import com.superdev.smiling.quickutils.UsersUtils;
import com.superdev.smiling.quickutils.WebRtcSessionManager;
import com.superdev.smiling.sinch.AudioPlayer;
import com.superdev.smiling.sinch.CallScreenActivity;
import com.superdev.smiling.sinch.SinchService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * QuickBlox team
 */
public class IncomeCallFragment extends Fragment implements Serializable, View.OnClickListener {

    private static final String TAG = IncomeCallFragment.class.getSimpleName();
    private static final long CLICK_DELAY = TimeUnit.SECONDS.toMillis(2);

    @BindView(R.id.text_caller_name)
    TextView textCallerName;

    @BindView(R.id.call_type)
    TextView callType;

    @BindView(R.id.text_also_on_call)
    TextView textAlsoOnCall;

    @BindView(R.id.text_other_inc_users)
    TextView textOtherIncUsers;

    @BindView(R.id.layout_info_about_call)
    LinearLayout layoutInfoAboutCall;

    @BindView(R.id.decline_incoming_call_container)
    LinearLayout declineIncomingCallContainer;

    @BindView(R.id.accept_incoming_call_container)
    LinearLayout acceptIncomingCallContainer;

    @BindView(R.id.reply_incoming_call_container)
    LinearLayout replyIncomingCallContainer;

    Unbinder unbinder;

    @BindView(R.id.decline_incoming_call_view)
    ImageView declineIncomingCallView;

    @BindView(R.id.accept_incoming_call_view)
    ImageView acceptIncomingCallView;

    @BindView(R.id.iv_caller_image)
    CircleImageView ivCallerImage;

    @BindView(R.id.reply_incoming_call_view)
    ImageView replyIncomingCallView;

    private List<Integer> opponentsIds;
    private Vibrator vibrator;
    private QBRTCTypes.QBConferenceType conferenceType;
    private long lastClickTime = 0l;
    private RingtonePlayer ringtonePlayer;
    private IncomeCallFragmentCallbackListener incomeCallFragmentCallbackListener;
    private QBRTCSession currentSession;
    private QbUsersDbManager qbUserDbManager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            incomeCallFragmentCallbackListener = (IncomeCallFragmentCallbackListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCallEventsController");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setRetainInstance(true);

        Log.d(TAG, "onCreate() from IncomeCallFragment");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_income_call, container, false);
        unbinder = ButterKnife.bind(this, view);
        initFields();
        if (currentSession != null) {
            initUI();
            setDisplayedTypeCall(conferenceType);
            initButtonsListener();
        }

        ringtonePlayer = new RingtonePlayer(getActivity());
        return view;
    }

    private void initFields() {
        currentSession = WebRtcSessionManager.getInstance(getActivity()).getCurrentSession();
        qbUserDbManager = QbUsersDbManager.getInstance(getActivity().getApplicationContext());

        if (currentSession != null) {
            opponentsIds = currentSession.getOpponents();
            conferenceType = currentSession.getConferenceType();
            Log.d(TAG, conferenceType.toString() + "From onCreateView()");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        startCallNotification();
    }

    private void initButtonsListener() {
        declineIncomingCallContainer.setOnClickListener(this);
        acceptIncomingCallContainer.setOnClickListener(this);
    }

    private void initUI() {
        QBUser callerUser = qbUserDbManager.getUserById(currentSession.getCallerID());
        textCallerName.setText(UsersUtils.getUserNameOrId(callerUser, currentSession.getCallerID()));
        String image = callerUser != null && callerUser.getCustomData() != null ? callerUser.getCustomData() : null;
        Utils.loadImageElseBlack(image, ivCallerImage, getContext(), R.drawable.avatar_group);
        if (!TextUtils.isEmpty(getOtherIncUsersNames())) {
            textOtherIncUsers.setText(getOtherIncUsersNames());
            textOtherIncUsers.setVisibility(View.VISIBLE);
        } else {
            textOtherIncUsers.setVisibility(View.GONE);
        }
        setVisibilityAlsoOnCallTextView();
    }

    private void setVisibilityAlsoOnCallTextView() {
        if (opponentsIds.size() < 2) {
            textAlsoOnCall.setVisibility(View.GONE);
        } else {
            textAlsoOnCall.setVisibility(View.VISIBLE);
        }
    }

    private Drawable getBackgroundForCallerAvatar(int callerId) {
        return UiUtils.getColorCircleDrawable(callerId);
    }

    public void startCallNotification() {
        Log.d(TAG, "startCallNotification()");

        ringtonePlayer.play(false);

        vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

        long[] vibrationCycle = {0, 1000, 1000};
        if (vibrator.hasVibrator()) {
            vibrator.vibrate(vibrationCycle, 1);
        }
    }

    private void stopCallNotification() {
        Log.d(TAG, "stopCallNotification()");

        if (ringtonePlayer != null) {
            ringtonePlayer.stop();
        }

        if (vibrator != null) {
            vibrator.cancel();
        }
    }

    private String getOtherIncUsersNames() {
        ArrayList<QBUser> usersFromDb = qbUserDbManager.getUsersByIds(opponentsIds);
        ArrayList<QBUser> opponents = new ArrayList<>();
        opponents.addAll(UsersUtils.getListAllUsersFromIds(usersFromDb, opponentsIds));

        opponents.remove(QBChatService.getInstance().getUser());
        Log.d(TAG, "opponentsIds = " + opponentsIds);
        return CollectionsUtils.makeStringFromUsersFullNames(opponents);
    }

    private void setDisplayedTypeCall(QBRTCTypes.QBConferenceType conferenceType) {
        boolean isVideoCall = conferenceType == QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO;

        callType.setText(isVideoCall ? R.string.text_incoming_video_call : R.string.text_incoming_audio_call);
        acceptIncomingCallView.setImageResource(isVideoCall ? R.drawable.ic_call_accept_video : R.drawable.ic_call_accept_voice);
    }

    @Override
    public void onStop() {
        stopCallNotification();
        super.onStop();
        Log.d(TAG, "onStop() from IncomeCallFragment");
    }

    @Override
    public void onClick(View v) {

        if ((SystemClock.uptimeMillis() - lastClickTime) < CLICK_DELAY) {
            return;
        }
        lastClickTime = SystemClock.uptimeMillis();

        switch (v.getId()) {
            case R.id.decline_incoming_call_container:
                reject();
                break;

            case R.id.accept_incoming_call_container:
                accept();
                break;

            default:
                break;
        }
    }

    private void accept() {
        enableButtons(false);
        stopCallNotification();

        incomeCallFragmentCallbackListener.onAcceptCurrentSession();
        Log.d(TAG, "Call is started");
    }

    private void reject() {
        enableButtons(false);
        stopCallNotification();
        removeNotification();
        incomeCallFragmentCallbackListener.onRejectCurrentSession();
        Log.d(TAG, "Call is rejected");
    }

    private void enableButtons(boolean enable) {
        acceptIncomingCallContainer.setEnabled(enable);
        declineIncomingCallContainer.setEnabled(enable);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void removeNotification() {
        NotificationManager notificationManager = (NotificationManager) App.getInstance().getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(10);
    }
}
