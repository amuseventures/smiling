package com.superdev.smiling.quick.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quickblox.sample.core.ui.adapter.BaseSelectableListAdapter;
import com.quickblox.users.model.QBUser;
import com.superdev.smiling.R;
import com.superdev.smiling.Utils;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.views.SelectionCheckView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class OpponentsAdapter extends BaseSelectableListAdapter<QBUser> {

    private SelectedItemsCountsChangedListener selectedItemsCountChangedListener;

    public OpponentsAdapter(Context context, List<QBUser> users) {
        super(context, users);
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_opponents_list, null);

            holder = new ViewHolder();

            holder.opponentIcon = convertView.findViewById(R.id.contact_photo);

            holder.checkView = convertView.findViewById(R.id.selection_check);

            holder.opponentName = convertView.findViewById(R.id.opponentsName);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final QBUser user = getItem(position);

        if (user != null) {
            holder.opponentName.setText(user.getFullName());
            Utils.loadImageElseBlack((user.getCustomData() != null ? user.getCustomData() : null), holder.opponentIcon, holder.opponentIcon.getContext(), R.drawable.avatar_contact);

            if (selectedItems.contains(user)) {
                holder.checkView.setChecked(true);
            } else {
                holder.checkView.setChecked(false);
            }
        }

        convertView.setOnClickListener(v -> {
            toggleSelection(position);
            selectedItemsCountChangedListener.onCountSelectedItemsChanged(selectedItems.size());
        });

        return convertView;
    }

    public static class ViewHolder {

        CircleImageView opponentIcon;

        TextView opponentName;

        SelectionCheckView checkView;
    }

    public void setSelectedItemsCountsChangedListener(SelectedItemsCountsChangedListener selectedItemsCountsChanged) {
        if (selectedItemsCountsChanged != null) {
            this.selectedItemsCountChangedListener = selectedItemsCountsChanged;
        }
    }

    public interface SelectedItemsCountsChangedListener {
        void onCountSelectedItemsChanged(int count);
    }
}
