package com.superdev.smiling.quick.service;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.sample.core.utils.SharedPrefsHelper;
import com.quickblox.sample.core.utils.Toaster;
import com.quickblox.users.model.QBUser;
import com.superdev.smiling.App;
import com.superdev.smiling.Dependencies;
import com.superdev.smiling.R;
import com.superdev.smiling.channel.model.Channel;
import com.superdev.smiling.channel.model.Channels;
import com.superdev.smiling.channel.service.ChannelService;
import com.superdev.smiling.login.data_model.Authentication;
import com.superdev.smiling.phonelogin.service.PhoneLoginService;
import com.superdev.smiling.quickutils.Consts;
import com.superdev.smiling.quickutils.QBEntityCallbackImpl;
import com.superdev.smiling.quickutils.QBResRequestExecutor;
import com.superdev.smiling.quickutils.UsersUtils;
import com.superdev.smiling.user.data_model.User;
import com.superdev.smiling.user.service.UserService;

import rx.Observable;
import rx.Observer;
import rx.functions.Func1;

/**
 * Created by gautam on 08/11/17.
 */

public class QbUserSettings {
    private User self;
    private final Context context;
    private QBResRequestExecutor requestExecutor;
    private StringifyArrayList<String> tagsList;
    private PhoneLoginService loginService;
    private UserService userService;
    private ChannelService channelService;


    public QbUserSettings(Context context) {
        this.context = context;
        requestExecutor = App.getInstance().getQbResRequestExecutor();
        loginService = Dependencies.INSTANCE.getPhoneLoginService();
        userService = Dependencies.INSTANCE.getUserService();
        channelService = Dependencies.INSTANCE.getChannelService();

        loginService.getAuthentication()
                .filter(successfullyAuthenticated())
                .flatMap(getUserInfo())
                .flatMap(channelsForUser())
                .subscribe(new Observer<Channels>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Channels channels) {
                        if (channels == null || channels.getChannels().isEmpty())
                            return;
                        tagsList = new StringifyArrayList<>();
                        for (Channel channel : channels.getChannels()) {
                            tagsList.add("cha" + channel.getUid().replace("-", ""));
                        }
                        if (self == null)
                            return;
                        createQBUserWithCurrentData(self);
                    }
                });
    }

    private Func1<Authentication, Boolean> successfullyAuthenticated() {
        return Authentication::isSuccess;
    }

    private Func1<User, Observable<Channels>> channelsForUser() {
        return user -> channelService.getChannelsFor(user)
                .map(channelsDatabaseResult -> {
                    if (channelsDatabaseResult.isSuccess()) {
                        return channelsDatabaseResult.getData();
                    } else {
                        return null;
                    }
                });
    }

    private Func1<Authentication, Observable<User>> getUserInfo() {
        return authentication -> userService.getUser(authentication.getUser().getUid())
                .map(userDatabaseResult -> {
                    if (userDatabaseResult.isSuccess()) {
                        self = userDatabaseResult.getData();
                        return self;
                    } else {
                        return null;
                    }
                });
    }


    private void startSignUpNewUser(final QBUser newUser, boolean update) {
        requestExecutor.signUpNewUser(newUser, new QBEntityCallback<QBUser>() {
                    @Override
                    public void onSuccess(QBUser result, Bundle params) {
                        loginToChat(result);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        if (e.getHttpStatusCode() == Consts.ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
                            signInCreatedUser(newUser, update);
                        } else {
                            //Toaster.longToast(R.string.sign_up_error);
                        }
                    }
                }
        );
    }

    private void saveUserData(QBUser qbUser) {
        SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
        sharedPrefsHelper.save(Consts.PREF_CURREN_ROOM_NAME, qbUser.getTags().getItemsAsString());
        sharedPrefsHelper.saveQbUser(qbUser);
    }

    private void removeAllUserData(final QBUser user) {
        requestExecutor.deleteCurrentUser(user.getId(), new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                UsersUtils.removeUserData(App.getInstance().getApplicationContext());
                createUserWithEnteredData();
            }

            @Override
            public void onError(QBResponseException e) {
                //Toaster.longToast(R.string.sign_up_error);
            }
        });
    }

    private void signInCreatedUser(final QBUser user, final boolean deleteCurrentUser) {
        requestExecutor.signInUser(user, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser result, Bundle params) {
                if (deleteCurrentUser) {
                    removeAllUserData(result);
                }
            }

            @Override
            public void onError(QBResponseException responseException) {
               // Toaster.longToast(R.string.sign_up_error);
            }
        });
    }

    private void createUserWithEnteredData() {
        createQBUserWithCurrentData(self);
    }

    private void createQBUserWithCurrentData(User user) {
        QBUser qbUser = new QBUser();
        qbUser.setFullName(user.getName());
        qbUser.setLogin(user.getUid());
        qbUser.setPassword(Consts.DEFAULT_USER_PASSWORD);
        qbUser.setCustomData(user.getImage());
        qbUser.setPhone(user.getPhone());
        qbUser.setTags(tagsList);
        startSignUpNewUser(qbUser, true);
    }

    private void loginToChat(final QBUser qbUser) {
        qbUser.setPassword(Consts.DEFAULT_USER_PASSWORD);
        startLoginService(qbUser);
    }

    private void startLoginService(QBUser qbUser) {
        saveUserData(qbUser);
        Intent tempIntent = new Intent(context, CallService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, Consts.EXTRA_LOGIN_RESULT_CODE, tempIntent, 0);
        CallService.start(context, qbUser, pendingIntent);
    }
}
