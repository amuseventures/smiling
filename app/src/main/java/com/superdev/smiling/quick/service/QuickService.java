package com.superdev.smiling.quick.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by gautam on 08/11/17.
 */

public class QuickService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        new QbUserSettings(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
